# Argus: Real-Time Mindless Reading Detection for Corpus Reading Experiments
A distributed system for administering psycholinguistic experiments involving real-time identification of bouts of _mindless reading_ (i.e., mind-wandering during reading) based on eye-movements.  Once identified, the software is capable of making a gaze-contingent stimulus change in order to test specific hypothesis or simply to alert the reader of their attention lapse.


## Abstract
_Mind-wandering_ is a phenomenon of having thoughts unrelated to the immediate task context.  High incidence of mind-wandering has been found to correlate with low performance on many task (Smallwood, Fishman, & Schooler, 2007).  One reason why mind-wandering impacts performance so negatively is that its occurrences may go totally unnoticed by the person who lapses into it.  Mind-wandering has been documented in a wide range of activities (Killingsworth & Gilbert, 2010).  One such activity is reading.  When mind-wandering occurs during reading it is often referred to as mindless reading because while the reader's eyes continue moving across printed text in a reading-like manner, little or nothing is actually understood.  Indeed, frequent lapses into mindless reading are not conducive to good text comprehension (Schooler, Reichle, & Halpern, 2004).

Mindless reading is still a nebulous phenomenon.  The two primary difficulties in studying it are its ephemeral nature and its identification.  Specifically, while a number of factors influencing the propensity to read mindlessly has been identified (see, e.g., Loboda, 2014), the behavior is not amenable to experimental manipulation in that there are no stimuli that can consistently bring it out in a reader.  The reason is because mind-wandering is largly driven by internal rather than external stimuli.  That is, internally-relevant thoughts replace the current contents of one's consciousness thereby hijacking the attention and redirecting it away from the immediate task context.  As a result, while conditions conducive to lapsing into mindless reading can be created, the intermittent nature of this mode of "reading" calls for a novel identification method.  This softwares offers such a method.

To achieve its goal, the current software presents readers with ecologically valid text (e.g., entire books) and monitors the reader's eye-movements in real time.  By analyzing the reading patterns exhibited by the reader, it continuously estimates the probability of the reader being on task.  If that probability falls below a defined threshold, the software enters a gaze-contingent stimulus change (Rayner, 1975) mode.  In that mode, it alters the text being read taking into account the location of the current fixation.  Gaze-contingent changes are typically designed to test specific hypotheses.


## Contents
- [Features](#features)
- [Details](#details)
   * [Mindless Reading](#mindless-reading)
   * [Sampling Mindless Reading](#sampling-mindless-reading)
   * [Gaze-Contingent Stimulus Change](#gaze-contingent-stimulus-change)
      - [Character-Level Substitution](#character-level-substitution)
      - [Word-Level Substitution](#word-level-substitution)
      - [Mask Size](#mask-size)
      - [Punctuation and Word Boundary](#punctuation-and-word-boundary)
   * [System's Components](#systems-components)
      - [Experiment UI](#experiment-ui)
      - [Eye-movement Simulator](#eye-movement-simulator)
      - [Mindless Reading Detector](#mindless-reading-detector)
   * [Eye-Tracer Interface](#eye-tracer-interface)
   * [Logs](#logs)
   * [How Real-Time?](#how-real-time)
   * [The Name](#the-name)
- [Videos](#videos)
- [Basic Research Enabled](#basic-research-enabled)
- [Applied Research Enabled](#applied-research-enabled)
- [Project Status](#project-status)
- [Dependencies](#dependencies)
- [Acknowledgments](#acknowledgments)
- [References](#references)
- [Citing](#citing)


## Features
- Presentation of large text corpora (e.g., entire books)
- Simulated subjects emit eye-movements to help with programming and debugging experiments without eye tracker connectivity
- Mindless reading detector identifies bouts of mind-wandering in real time based on eye-movements of readers [this statement required more empirical validation]
- Gaze-contingent paradigm allows to test specific hypothesis
- Modular design allows distribution of the system's components across separate networked machines to optimize computational resources
- Requires SR Research EyeLink eye-tracker


## Details
### Mindless Reading
Mindless reading is a phenomenon that occurs when a reader's eyes continue moving across printed text in a reading-like manner but with little or nothing being understood.  Its incidence is higher than introspection might reveal.  For example, when readers are asked about being on- or off-task at unpredictable random intervals, they can be found to be reading mindlessly even a quarter of the time (Reichle, Reineberg, & Schooler, 2010; Loboda, 2014).  Certain factors make mindless reading more or less likely.  For example, being engaged with an interesting topic makes it less likely (Grodsky & Giambra, 1989) while craving cigarettes makes it harder to stay focused on what is being read (Sayette, Schooler, & Reichle, 2010).

### Sampling Mindless Reading
The state-of-the-art method of discovering the episodes of mindless reading (or mind-wandering in general) is _thought sampling_ (Smallwood & Schooler, 2006) which has been used multiple times.  In thought sampling, the subject is periodically probed at unpredictable times and the goal of the probes is to discover if the subject's attention is on-task (i.e., if they are reading mindfully) or off-task (i.e., if they are reading mindlessly).  This design is typically supplemented with the instruction to self-report to allow for sampling mindless reading that would otherwise be missed by probes alone.  The combination of probes and self-reports allows for the discovery of the so called probe-caught mindless reading and self-caught mindless reading.

This method has several shortcomings:

1. By relying on random sampling, it is bound to miss a portion of mindless reading episodes.
2. In an attempt to fix that problem it introduces self-report but by doing so it contaminates the reading (or any other) task with the instruction to self-monitor.  This threatens the generality of the findings.
3. It is unclear how well an average person can inspect the contents of their own consciences which only aggravates the situation.  That is, because self-reports rely on introspection, validating accuracy of those self-reports may be problematic.
4. Probes are obtrusive in that they interrupt the task that is being studied.  This further undermines the issue of ecological validity.

The method implemented in the present software attempts to avoid all of the above problems.

### Gaze-Contingent Stimulus Change
Argus can be configured to perform a gaze-contingent change to the stimulus upon detecting an attention lapse in the reader.  This enables a series of novel experiments which I describe later, in section [Basic Research Enabled](#basic-research-enabled).  A gaze-contingent change happens when the text being displayed is substituted by another text in an area around the location of current fixation.  Gaze-contingency paradigm has been known and used for a while.  For example, using a foveal mask, Rayner & Bertera (1979) established the size of perceptual span.

Argus lets the user control the way gaze-contingent changes are made through different substitution levels, substitution modes, mask size, and punctuation and word boundary.

#### Character-Level Substitution
In the character-level substitution, the text being displayed is replaced by another text with each character treated as a separate entity.  For example, substituting a five-letter word can be conceptualized as five one-letter substitutions.  The substitution character is selected according to one of the following three modes:

1. **Single-character mode.**  A designated character (`*` by default) is used to substitute every letter in the original text (Figure 2).
2. **Random-character mode.**  A random character is selected for every substitution (Figure 3).
3. **Matching-character mode.**  Substitution candidates are selected randomly, but only from within the following mutually exclusive sets: `aceimnorsuvwxz`, `bdfhklt`, and `gjpqy`  (Figure 4).  This strategy is just an example of how one could hope to test if ortographic coding drives eye-movements of mindless readers.  Phonological coding, lexical activation, and semantic activation require a substitution strategy beyond single character; such a strategy could be implemented in the present software to enable specific hypothesis tests.

<kbd>![Original text](assets/md-media/original-text.png)</kbd>
**Figure 1.** The original text (the first page of the _Sense and Sensibility_ novel by Jane Austen).<br /><br />

<kbd>![Char sub mode 1](assets/md-media/sub-char-mode-1.png)</kbd>
**Figure 2.** Single-character substitution mode.  Original text shown in Figure 1.<br /><br />

<kbd>![Char sub mode 2](assets/md-media/sub-char-mode-2.png)</kbd>
**Figure 3.** Random-character substitution mode.  Original text shown in Figure 1.<br /><br />

<kbd>![Char sub mode 3](assets/md-media/sub-char-mode-3.png)</kbd>
**Figure 4.** Matching-character substitution mode.  Original text shown in Figure 1.<br /><br />

Figures 2-4 illustrate the three available character-level substitutions.  One interesting observation is how much more "natural" Figure 4 looks like when compared to Figure 3.  [Videos 1 and 2](#videos) available later in this document further illustrate modes 1 and 3.

#### Word-Level Substitution
In the word-level substitution, the text being displayed is replaced by another text with each word being treated as a unit.  In this mode, each word is replaced with a possibly different word.  I say possibly, because depending on the modes defined below, no suitable substitutes may be available.  The substitution word is selected according to one of the following eight modes (all word replacements are matched on length):

1. **Random word.**  A random word is selected.
2. **Random word matched on frequency.**  A random word is selected from those that match the original word's frequency.
3. **Random l-frq word.**  A random low-frequency word is selected.
4. **Random h-frq word.**  A random high-frequency word is selected.
5. **Random h2l-frq word.**  A random low-frequency word is selected but only if it substitutes a high-frequency word (i.e., no low-frequency words will be replaced).
6. **Random l2h-frq word.**  A random high-frequency word is selected but only if it substitutes a low-frequency word (i.e., no high-frequency words will be replaced).
7. **Random word matched on part of speech.**  A random word is selected from those that match the original word's part-of-speech.
8. **Random word matched on frequency and part of speech.**  A random word is selected from those that match the original word's frequency and part-of-speech.

The frequency and part-of-speech matching is based on the [SUBTLEX.US](https://www.ugent.be/pp/experimentele-psychologie/en/research/documents/subtlexus/overview.htm) data.  The substitution process has been optimized for speed through the use a relational database with proper decomposition and indexing.  Specifically, a single word substitution takes less than 0.0001 s.  The search for a single non-word is longer and takes a little over 0.01 s, but because non-words are very rare in reading materials, this is a corner case not worth further consideration.

<kbd>![Word sub mode](assets/md-media/sub-word-1.png)</kbd>
**Figure 5.** Random word substitution mode.  Original text shown in Figure 1.<br /><br />

<kbd>![Word sub mode](assets/md-media/sub-word-2.png)</kbd>
**Figure 6.** Random word matched on frequency substitution mode.  Original text shown in Figure 1.<br /><br />

<kbd>![Word sub mode](assets/md-media/sub-word-7.png)</kbd>
**Figure 7.** Random word matched on part of speech substitution mode.  Original text shown in Figure 1.<br /><br />

For brevity, I only illustrate how modes 1, 2 and 7 of the word-level substitution look like (Figures 5-7).  Figures 6 and 7 reveal an important fact: Matching words on length and either frequency or part-of-speech results in many word repetitions.  This is most likely the artifact of the data set used (SUBTLEX.US).  Using another, larger, data set would likely result in a more random pattern of substitutions.  [Video 3](#videos) available later in this document further illustrates mode 1.

#### Mask Size
Mask size determines how much of the original text around the current fixation is substituted.  In the horizontal direction, the mask extends to the left and right counting from the currently fixated character in either character spaces (Figure 8) or words (Figures 9 and 10).  The reason why sizing the mask with words instead of character spaces may be useful is that it ensures that all the words in the currenly fixated line are shown in their entirety, i.e., it prevents chopping off parts of words (compare Figure 8 with Figures 9 and 10).  In the vertical direction, the mask extends up and down in lines.  This four-directions control over the mask size allows it to be asymmetric and accommodate findings like the perceptual span size which extends farther in the direction of reading (McConkie & Rayner, 1976).

<kbd>![Mask size 01](assets/md-media/mask-size-01.png)</kbd>
**Figure 8.** Gaze-contingent mask using the single-character substitution mode.  Mask size of (8,15,0,1) characters. Mask border and current fixation shown for clarity only.<br /><br />

<kbd>![Mask size 02](assets/md-media/mask-size-02.png)</kbd>
**Figure 9.** Gaze-contingent mask using the single-character substitution mode.  Mask size of (2,2,1,1) words.  Mask border and current fixation shown for clarity only.<br /><br />

<kbd>![Mask size 03](assets/md-media/mask-size-03.png)</kbd>
**Figure 10.** Gaze-contingent mask using the single-character substitution mode.  Mask size of (1,3,0,0) words. Mask border and current fixation shown for clarity only.<br /><br />

The user may opt to invert the mask.  Ordinarily, text substitution occurs inside of the mask, i.e., only the text around current fixation is changed.  When the mask gets inverted, only the text around current fixation is NOT changed.  One example of why inverting the mask in this way may be useful is that it enables checking how much of the parafoveal information is processed during mindless reading.

#### Punctuation and Word Boundary
Additionally to the above ways of substitution and mask control, the user can decide whether punctuation and word boundary characters should be substituted.  One example of how these options can be useful is determining the degree to which word boundary drives mindless reading.  For example, word boundary information is known to facilitate reading; reading slows down when word boundary information is restricted in the parafoveal preview.  It is unclear whether similar finding would be observed during mindless reading.

### System's Components
Argus is a distributed system which consists of the three components I describe below.

#### Experiment UI
Experiment UI is the only front-facing part of the system.  It handles all subject interactions (e.g., displaying stimuli and handling user input) as well as receiving and processing eye-movements.  It also passes eye-movements to the mindless reading detector.

#### Eye-movement Simulator
Eye-movement simulator (not emulator, there is a difference!) is an optional component which is useful in the development stage.  It is not needed in a live experiment.  It is a proxy for the eye-tracker and as such it makes developing faster and more pleasant.  To quantify that statement, establishing a connection with the EyeLink eye tracker (up to the point of the system being ready) take 10-20 seconds.  That is a consideratble amount of time if one is implementing a feature and need to run the experiment UI hundreds if not thousands of time.  Those savings add up quickly.  Simulated eye movements conform to the format provided by the actual EyeLink eye tracker so everything works with the live setup without hiccups.

The eye-movement simulator is an HTTP server and as such can be run on a separate machine.  This modular design allows for loosening the connection between the experiment UI and the simulator.  For example, a different implementation of the simulator can be used in the future without disturbing the implementation of the experiment UI.  Or, and even more interestingly, a reader interacting with a networked eye tracker can be used as well.

The experiment UI connects to the eye-movement simulator if launched in the subject-simulation mode.  From then, the UI instantiates one of the simulated subjects (see below) and controls it.  A simulated subject can be asked to gaze at the stimulus in a natural uninterrupted manner which can be stopped and resumed again at any point.  Moreover, when stopped, a simulated subject can be asked to generate a single cycle of eye-movements.  A single cycle consists of the following events executed in a strictly sequential manner (event names are consistent with the EyeLink programming documentation):

- `StartFixationEvent`
- `EndFixationEvent`
- `StartSaccadeEvent`
- `StartBlinkEvent`
- `EndBlinkEvent`
- `EndSaccadeEvent`

Blink events are emitted randomly with the probability 0.01 (constant `PROB_BLINK` in module `sub/sub.py`).  Implementation-wise, each event is first programmed and then executed.  This detail has little bearing on how the software works apart from the fact that when the simulated subject is in the step-through mode (i.e., it is asked to generate a single eye-movement cycle), it pauses after executing the `EndFixationEvent` event but before programming the `StartSaccadeEvent`.

Currently, the following five types of simulated subjects are implemented:

1. **Random Subject**.  Generates saccades to and fixations at a randomly selected screen coordinates (i.e., coordinates are independent).
2. **Random Walking Subject**.  Generates saccades to and fixations at randomly selected screen coordinates which is close to the location of the previous fixation (i.e., the new coordinates are dependent on the previous ones).
3. **Page Reading Subject**.  Generates saccades to and fixations at a randomly selected text bounding box coordinates (independent coordinates).
4. **Line Reading Subject**.  Generates reading-like eye-movements consistent with left-to-right orthographies.  This subject fixates a line of text from left to right and moves to the next line after the first saccade that would overshoot the end of the current line.  This subject will not respond to word characteristics such as length, frequncy, or predictability.  Coordinates are dependent.
5. **Word Reading Subject**.  Generates reading-like eye-movements consistent with left-to-right orthographies.  This subject programs saccades in a way consistent with how they are programed by the brain of a human reader, i.e., taking into account the size of the perceptual span, word boundary information, optimal viewing position, as well as word characteristics such as its freuency.  Coordinates are dependent. [implementaion of this subject is pending completion]

#### Mindless Reading Detector
Mindless reading detector predicts the probability of mindless reading given the subject's reading pattern.  In fact, there are more than one detector.  Fast detectors monitor up to 5 seconds of eye movement data and therefore can react to changes in data patterns fast.  Slow detectors look at 20-30 seconds of that data and while they take a while to come to a conclusion, it's likely that these decisions are more accurate.  There are also intermediate speed detectors.  The software imposes no limit on the type or number of detectors.  The idea is to let the researcher combine the adjudications made by different models as they see fit.

Most of these statistical models are deep neural networks.  Unfortunately, I did't have enough data to accommodate more sophisticated neural architectures.  I had hoped for three to four more large corpus reading experiments that would yield the data I needed; alas, that did not materialize.  Figures 11 and 12 show an example of evaluation of some of these neural nets.

![Neural nets eval 01](assets/md-media/nn-eval-top_n.png)
**Figure 11.** Evaluation of deep neural net models.<br /><br />

![Neural nets eval 01](assets/md-media/nn-eval-top_n-ext.png)
**Figure 12.** Evaluation of deep neural net models (external).<br /><br />

Much like the eye-movement simulator, mindless reading detector is an HTTP server and thus can be run on a separate machine.  This is an important part of the design of Argus because, apart from promoting modularization, it helps to avoid overloading the experiment computer (which might be quite old).  Nothing stops the detector from running on the same machine, however; such a setup would incur negligible network delays and negligible performance overhead.

The experiment UI connects to the mindless reading detector and "forwards" the relevant eye-movements upon receiving them from the eye tracker (or the eye-movement simulator).  This design keeps the coupling between the two components very loose.  The experiment UI does not wait for a response; if the detector wishes to communicate something back (e.g., "I Tawt I Taw a Puddy Tat"), it can do so through a separate and also uni-directional multiprocessing pipe.


### Eye-Tracer Interface
In its current incarnation, Argus only works with the EyeLink family of eye trackers.  In the live mode (e.g., during an actual experiment with live subjects), the computer running the experiment needs to be physically connected to the EyeLink host machine with the network cable (i.e., using the EyeLink manual terminology, that computer is the stimulus machine).  Of course, as I have described [earlier](#eye-movement-simulator), the experiment UI can also be started in the simulated subject mode where no eye-tracker connectivity is needed which is invaluable for programming and debugging the experiment.

### Logs
The present software does not affect the EyeLink eye tracker [EDF](https://en.wikipedia.org/wiki/European_Data_Format) files in any way.  In fact, it even downloads them from the host machine after the experimental session is over.  Those files contain a complete record of all eye-movements and events and can be analyzed as desired.  However, Argus creates several logs of its own and I describe them briefly next.

#### Experiment
Information on the subject's experimental session:

```
rec_ts         rec_ts_exp  rec_dt                      sub-id  date        time             dotw      t_dur
1502350111617   45982.391  2017-08-10 07:28:31.616601  test    2017.08.10  07:27:45.634210  Thu   45981.934
```

#### Events
A trace of all events that occured during the experimental session (i.e., experiment, trial, stimulus, etc.):

```
rec_ts         rec_ts_exp  rec_dt                      evt          inf
1502350065635       0.481  2017-08-10 07:27:45.634691  exp-begin    t0-date:2017-08-10 t0-time:07:27:45.634210 t0-ts:1502350065634 t0-ts-et:1502350065634 sub-id:test
1502350066536     901.695  2017-08-10 07:27:46.535905  trial-begin  trial-idx:0 trial-name:Questions t-exp:901.637
1502350066536     901.757  2017-08-10 07:27:46.535967  trial-end    trial-idx:0 trial-name:Questions t-exp:901.744 dur:0.0
1502350069294    3660.222  2017-08-10 07:27:49.294432  trial-begin  trial-idx:1 trial-name:Probes t-exp:3660.196
1502350069497    3862.957  2017-08-10 07:27:49.497167  stim-show    trial-idx:1 trial-name:Probes stim-name:c01z stim-type:t ch-num:1 pg-num:0 t-exp:3862.912 t-trial:202.716
1502350070481    4846.527  2017-08-10 07:27:50.480737  stim-next    trial-idx:1 trial-name:Probes stim-name:c01z stim-type:t ch-num:1 pg-num:0 t-exp:4846.489 t-trial:1186.293 dur:1139.045
1502350071353    5718.537  2017-08-10 07:27:51.352747  sacc         xy0:(10,10) xy1:(42,65) dur:40 ts:1502350071325 ts-exp-et:5691 ts-trial-et:2031 ts-stim-et:801
1502350071439    5804.41   2017-08-10 07:27:51.438620  fix          xy:(42,65) dur:446 ts:1502350071336 ts-exp-et:5702 ts-trial-et:2042 ts-stim-et:812

...

1502350108867   43232.755  2017-08-10 07:28:28.866965  sacc         xy0:(312,306) xy1:(635,306) dur:70 ts:1502350108786 ts-exp-et:43152 ts-trial-et:39491 ts-stim-et:38262
1502350109445   43811.05   2017-08-10 07:28:29.445260  fix          xy:(635,306) dur:563 ts:1502350108871 ts-exp-et:43237 ts-trial-et:39577 ts-stim-et:38347
1502350111575   45941.111  2017-08-10 07:28:31.575321  trial-end    trial-idx:1 trial-name:Probes t-exp:45941.076 dur:42280.88
1502350111616   45981.984  2017-08-10 07:28:31.616194  exp-end      sub-id:test dur:45981.934
```

#### Trials
Information on the experimental trials the subject has gone through:

```
rec_ts         rec_ts_exp  rec_dt                      sub_id  trial_idx  trial_name    t0_exp     t_dur
1502350066536     901.778  2017-08-10 07:27:46.535988  test            0  Questions    901.744      0.0
1502350111575   45941.151  2017-08-10 07:28:31.575361  test            1  Probes      3660.196  42280.88
```

#### Stimuli
Information on the experimental stimuli presented to the subject:

```
rec_ts         rec_ts_exp  rec_dt                      sub_id  trial_idx  trial_name  stim  type  ch_num  pg_num    t0_exp  t0_trial     t_dur
1502350070481    4846.577  2017-08-10 07:27:50.480787  test            1  Probes      c01z     t       1       0  3707.444    47.248  1139.045
```

#### Questions: End-of-Chapter
Information on end-of-chapter questions which measure text comprehension.  Those questions are typically employed in reading studies to make sure that subject exert effort to understand the text.

#### Questions: Within-Chapter
Information on within-chapter questions.

#### Reading Events
Information on custom reading events (e.g., mindless reading probes, self-reports, etc.).

#### Mindless Reading Detection
Information on the computational units making detection:

```
begin
    dt: 2017-08-10 07:27:46.191278
    ts: 1502350066191
    n-comp-units: 4
    t-unit: ms

comp-units
    unit-0
        name: 2000.1000
        ts-name: 2000.1000
        ts-t-store: 2000
        ts-t-emit: 1000
    unit-1
        name: 2000.1500
        ts-name: 2000.1500
        ts-t-store: 2000
        ts-t-emit: 1500
    unit-2
        name: 2000.2000
        ts-name: 2000.2000
        ts-t-store: 2000
        ts-t-emit: 2000
    unit-3
        name: 3000.1000
        ts-name: 3000.1000
        ts-t-store: 3000
        ts-t-emit: 1000

end
    dt: 2017-08-10 07:28:31.981853
    ts: 1502350111981
    t: 45790
    comp-n: 49
```

Each of the computational units has its own separate log containing the trace of its computations along with the complete input data:

```
begin
    dt: 2017-08-10 07:27:46.196247
    ts: 1502350066196

compute
    i: 1
    ts: 1502350073500
    fix-dur-tot: 2196.816406
    fix-n: 6
    fix-ts-0: 1502350071782
    fix-ts-n: 1502350073805
    p: 0.166667
    data: ...

compute
    i: 2
    ts: 1502350073851
    fix-dur-tot: 2102.144043
    fix-n: 5
    fix-ts-0: 1502350073499
    fix-ts-n: 1502350073935
    p: 0.4
    data: ...

...

end
    dt: 2017-08-10 07:28:31.577978
    ts: 1502350111577
    t: 45381
    comp-n: 15
```

### How Real-Time?
Baring all delays imposed by the measurement instrument used here, i.e., the eye-tracker with all its software (e.g., the fixations identification algorithm), and the transmission medium (i.e., the network connection to the eye-tracker), the current software analyzes eye-movements as soon as they become available.  If the mindless reading detector server runs on another machine, network latency starts adding in to the total delay.  However, delays of under 1 ms should be expected from a reasonable local area network (LAN).  Because the focus is on reading where the average fixation duration is between 200 and 300 ms, single-digit ms should not be a cause of concern.

### The Name
Argus Panoptes was a Greek mythological giant with many eyes.  Some sources describe him as having as many as a hundred eyes.  He was the guardian of the nymph Io.  By a bad analogy, the present software watches the subject closely, so closely in fact that it can tell when they lapse into inattentive state that they may not be aware of themselves (one of the markers of mind-wandering is a possible inability to recognize it in oneself).  For brevity, I shorten the name to Argus (which means "giant").


## Videos
Below are three videos that show the line-reading simulated subject "reading" the first page of the _Sense and Sensibility_ by Jane Austen while different substitution and mask settings are enforced.  In all videos, the mask border and current fixation are shown for clarity only.

[![Video 1](http://img.youtube.com/vi/dKjUuQB74Dc/0.jpg)](http://www.youtube.com/watch?v=dKjUuQB74Dc)<br />
**Video 1.** Substitution level: character; substitution mode: single-character (i.e., mode 1); mask size: (6,15,1,1) characters.<br /><br />

[![Video 2](http://img.youtube.com/vi/RgOZY-yCLlY/0.jpg)](http://www.youtube.com/watch?v=RgOZY-yCLlY)<br />
**Video 2.** Substitution level: character; substitution mode: matching-character (i.e., mode 3); mask size: (6,15,1,1) characters.<br /><br />

[![Video 3](http://img.youtube.com/vi/6KsC5auvfA4/0.jpg)](http://www.youtube.com/watch?v=6KsC5auvfA4)<br />
**Video 3.** Substitution level: word; substitution mode: rando-word (i.e., mode 3); mask size: (2,2,1,1) words.


## Basic Research Enabled
In this section, I describe experiments that the present software enables as well as the experiments which should be run before this software is involved in order to lay foundations for its proper evaluation.

### Experiment 1: Establish the Probe Hit Ratio (PHR) Baseline
This experiment will run 10-or-so subjects with random mindless reading probes shown every 2-4 minutes (for consistency with prior research), but with no instruction to self-report.  I would expect to see a higher PHR than that observed in prior experiments because (a) probes are not going to be delayed by self-reports and (b) subjects are not going to be as self-aware.  Mindless reading detection experiments coming later on will rely on probes alone as well (although those probes will no longer be random) and therefore establishing the baseline PHR is critical in discovering if mindless reading detection provides an improvement over random sampling.

### Experiment 2: More Frequent Probes
With no requirement to self-report, waiting for 2-4 minutes between probes may be too long in terms of sampling mindless reading.  Subject are likely to lapse into reading mindlessly easier than when they are asked to self-report and the increased probe frequency would attempt to discover those possibly more frequent bouts of mind-wandering.  This experiment, therefore, would also help to establish a baseline PHR.  Additionally, it would help to understand if bouts of mindless reading should be expected to happen more frequently than with the incidence established by Experiment 1.

There are distributional reasons for which this experiment should be run (see Loboda, 2014).

### Experiment 3: Attention-Contingent (AC) Paradigm
The goal of this AC reading experiment will be to evaluate the first version of mindless reading detector.  The main measure of performance will be PHR.  That ratio will be compared against the baseline established in Experiments 1 and 2.  A good detector will perform on an above-chance level.  There will be no need to address any theoretical questions at this stage.

This experiment may be broken down into several sub-experiments, each of which would evaluate a different model at the heart of the mindless reading detector.  Testing of candidate models might proceed simultaneously as the subject reads.  That is, the software could automatically switch to the next model once it has obtained a sufficient number of samples to calculate PHR for the current model.

It is not necessary to employ gaze-contingent (GC) display changes until making sure the detector is sensitive enough to sample the actual mindless reading (i.e., wait until the next experiment).  At the same time, it would be prudent to have a simple GC text change ready for the very end of the experiment.  If unnoticed by the subjects, such manipulation would provide another compelling proof for on-line mindless reading detection.

### Experiment 4: Attention-and-Gaze-Contingent (AGC) Paradigm
This experiment will perform GC text substitutions, but only when subjects are reading mindlessly.  Experiment 3 will need to be a success in order for us to be confident about deploying the AC paradigm and combining it with the GC paradigm.

At this stage, we will be certain that mindless reading detector is sensitive enough so probes will no longer be needed (other than as described in the next paragraph).  Therefore, upon the subject lapsing into mindless reading, instead of launching probes the software will administer a GC change to test one or more theoretical questions.

This and all subsequent experiments will be a further evaluation of the mindless reading detection model(s) in that PHRs can still be compared against the baseline (i.e., Experiments 1 and 2) and the ratios established later (i.e., Experiment 3).  In fact, each experiment could begin with an AC-only paradigm to measure the PHR and later transition into adding the GC component.  To automate this process, the software could remain in the AC-only mode for as long as a certain threshold of PHR has not been reached; it would only transition to the AGC mode once it has established that mindless reading detection works as expected.  This would also be a good security valve for cases of subjects with hard-to-pinpoint attention focus (i.e., on or off task).  To take this even further, the experimental software could go through a sequence of alternative detection models in an attempt to discover the one that works best with each subject before it decides which one to use in the AC+GC part of the experiment.

### Experiment 5: Reading with a Fovea but Without Attention
A remake of a classic experiment (Rayner & Bertera, 1979).  An attention-and-gaze contingent text change will mask text being read to establish effects of mindless reading on foveal and parafoveal processing.  It is possible that reading without attention will have similar effect to reading without a fovea, i.e., nothing will be comprehended as if the text was masked as done in the 1979 experiment.

### Experiment 6: Parafovea-on-Fovea Effects in Ecologically Valid Reading
An attention-and-gaze contingent text change will be made after the subject lapses into mindless reading.  This change will modify word n+1 by showing another word, obliterating it into total gibrish, or something in-between.  We will be able to choose between matching the word on length, frequency, part-of-speech, or any other variables we will have access to at that point.  Inferences about mindful reading will be waranted after it has been disentangled from mindless reading.


## Applied Research Enabled
Because mindless reading has been linked to deteriorated text comprehension, possibly as its cause (e.g., Smallwood, McSpadden, & Schooler, 2008), identifying and eliminating it may ameliorate those deleterious effect.  For example, studying education material often involves a great deal of reading, but currently there is no way to ensure that this effort is not, to some degree at least, wasted.  Reading could become more efficient if an assistive technology existed that would track people's eyes while they read.  Such technology could alert readers about their on-going attention lapse right away (ideally, by harnessing [pre-attentive processing](https://en.wikipedia.org/wiki/Pre-attentive_processing)) or suggest re-reading of parts which were previously read mindlessly.  While the current software is just a research prototype aimed mainly at gaining a better understanding of reading and mindless reading, it is a step in that direction.  This is especially true with the advent of technologies such as virtual reality headsets that integrate high accuracy eye tracking.


## Project Status
The project is currently stalled because I have been waiting for a computer that I need to continue this work.


## Dependencies
- [Python 3.6](https://www.python.org)
- [pygame](https://www.pygame.org)
- pylink (included)
- [pillow](https://pillow.readthedocs.io/en/latest/)
- [dotmap](https://github.com/drgrib/dotmap)
- [numpy](https://www.numpy.org)
- [scipy](https://www.scipy.org)
- [sklearn](https://www.sklearn.org)
- [pandas](https://pandas.pydata.org)
- [matplotlib](https://matplotlib.org)
- [h5py](https://www.h5py.org)
- [python-pushover](https://github.com/Thibauth/python-pushover)
- [tensorflow](https://www.tensorflow.org)
- [keras](https://keras.io)
- [SR Research](https://www.sr-research.com) EyeLink eye-tracker


## Acknowledgments
I worked as a post-doctoral research associate at the University of Pittsburgh when I developed this software.


## References
Grodsky, A. & Giambra, L. (1989).  Task unrelated images and thoughts whilst reading.  In _Imagery: Current perspectives_.  New York: Plenum Press.

Killingsworth, M.A. & Gilbert, D.T. (2010).  A wandering mind is an unhappy mind.  _Science, 330(6006)_.

Loboda, T.D. (2014).  Study and detection of mindless reading.  _PhD Dissertation_.

McConkie, G.W. & Rayner, K. (1976).  Asymmetry of the perceptual span in reading.  _Bulletin of the Psychonomic Society, 8_, 365-68.

Rayner, K. (1975).  The perceptual span and peripheral cues in reading.  _Cognitive Psychology, 7_, 65–81.

Rayner, K. & Bertera, J.H. (1979)  Reading without a fovea. _Sience, 206_, 468-469.

Reichle, E.D., Reineberg, A.E., & Schooler, J.W. (2010).  Eye movements during mindless reading.  _Psychological Science, 21(9)_, 1300-10.

Sayette, M.A., Schooler, J.W., & Reichle, E.D. (2010).  Out for a smoke: The impact of cigarette craving on zoning out during reading.  _Psychological Science, 21(1)_, 26–30.

Schooler, J.W., Reichle, E.D., & Halpern, D.V. (2004).  Zoning-out during reading: Evidence for dissociations between experience and meta-consciousness.  _Thinking and seeing: Visual metacognition in adults and children_ (pp. 203-226).  Cambridge, MA: MIT Press.

Smallwood, J., Fishman, D.J., & Schooler, J.W. (2007).  Counting the cost of an absent mind: Mind wandering as an under-recognized influence on educational performance.  _Psychonomic Bulletin & Review_, 14, 230-236.

Smallwood, J., McSpadden, M., & Schooler, J. W. (2008).  When attention matters: The curious incident of the wandering mind.  _Memory & Cognition, 36(6)_, 1144–1150.

Smallwood, J. & Schooler, J.W. (2006).  The restless mind.  _Psychological Bulletin, 132_, 946–958.


## Citing
Loboda, T.D. (2018).  Argus: Real-Time Mindless Reading Detection for Corpus Reading Experiments [computer software].  Available at https://gitlab.com/ubiq-x/argus
