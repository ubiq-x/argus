import random

from timer import Timer


# ======================================================================================================================
class ProbeTimer(Timer):
	def __init__(self, t_min, t_max, exec_fn):
		super(ProbeTimer, self).__init__(1, exec_fn)

		self.t_min = t_min
		self.t_max = t_max

	def start(self):
		if self.is_started: return

		self.t_timeout = random.uniform(self.t_min, self.t_max)
		super(ProbeTimer, self).start()
