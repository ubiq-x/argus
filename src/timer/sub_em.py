import pylink
import random
import time

from timer import Timer


# ======================================================================================================================
class SubjectEyeMovementTimer(Timer):
	def __init__(self, eye, x1, x2, y1, y2, exec_n, exec_fn):
		super(SubjectEyeMovementTimer, self).__init__(exec_n, exec_fn)

		self.eye = eye
		self.x1 = x1
		self.x2 = x2
		self.y1 = y1
		self.y2 = y2

		self.exec_fn = None      # called by the superclass with no arguments, so...
		self.exec_fn_ = exec_fn  # ...must store it here and do some basic lambda gymnastics

	def start(self):
		if self.is_started: return

		r = random.randint

		dur = r(200, 300)  # [ms]

		t_now = time.time()
		t = (t_now - dur, t_now)

		gaze = (r(self.x1, self.x2), r(self.y1, self.y2))
		href = (r(0, 99), r(0, 99))

		fix = pylink.EndFixationEvent(time.time(), "ENDFIX", self.eye, None, t[0], gaze[0], gaze[0], href[0], href[1], 0.0, 0.0, 0.0, 0.0, t[1], gaze[1], gaze[1], gaze[0], gaze[1], href[0], href[1], href[0], href[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

		self.t_timeout = (dur / 1000.0)
		self.exec_fn = lambda: self.exec_fn_(fix)
		super(SubjectEyeMovementTimer, self).start()
