import threading
import time


# ======================================================================================================================
class Timer(object):
	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	EXEC_INDEF = -1  # value for "keep executing indefinitely"

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, exec_n, exec_fn):
		self.t_timeout = -1
		self.t_start   = -1
		self.t_pause   = -1

		self.exec_i  = 0         # number of times the event has fired
		self.exec_n  = exec_n    # number of times the event should fire before stopping
		self.exec_fn = exec_fn  # function to call upon timeout

		self.timer = None

		self.is_started = False
		self.is_paused  = False  # needs to be started before it can be paused

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if self.is_started:
			self.cancel()

	# ------------------------------------------------------------------------------------------------------------------
	def cancel(self):
		if not self.is_started: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  cancel")

		self.timer.cancel()
		self.timer = None  # TODO: This used to cause some issues
		self.is_paused = False
		self.is_started = False

	# ------------------------------------------------------------------------------------------------------------------
	def delay(self, t):
		if not self.is_started: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  delay " + str(t))

		self.t_timeout += t
		self.timer.cancel()
		self.timer = threading.Timer(self.t_timeout - (time.time() - self.t_start), self.execute)
		self.timer.start()

	# ------------------------------------------------------------------------------------------------------------------
	def execute(self):
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  execute")

		self.t_timeout = -1
		self.t_start   = -1
		self.timer = None  # TODO: This used to cause some issues

		self.is_started = False

		if self.exec_fn is not None:
			self.exec_fn()
			self.exec_i += 1
			if self.exec_n == self.EXEC_INDEF or self.exec_i < self.exec_n: self.start()

	# ------------------------------------------------------------------------------------------------------------------
	def pause(self):
		if not self.is_started or self.is_paused: return

		self.timer.cancel()
		self.t_pause = time.time()
		self.is_paused = True

		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  pause " + str(self.t_pause - self.t_start))

	# ------------------------------------------------------------------------------------------------------------------
	def restart(self):
		self.cancel()
		self.start()

	# ------------------------------------------------------------------------------------------------------------------
	def resume(self):
		if not self.is_started or not self.is_paused: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  resume " + str(self.t_timeout - (self.t_pause - self.t_start)))

		self.timer = threading.Timer(self.t_timeout - (self.t_pause - self.t_start), self.execute)
		self.t_pause = -1
		self.timer.start()
		self.is_paused = False

	# ------------------------------------------------------------------------------------------------------------------
	def start(self):
		''' Classes overloading this method need to set 'self.t_timeout' before calling it. '''

		if self.is_started or self.t_timeout <= 0: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  start " + str(self.t_timeout))

		self.timer = threading.Timer(self.t_timeout, self.execute)
		self.t_start = time.time()
		self.timer.start()
		self.is_started = True

	# ------------------------------------------------------------------------------------------------------------------
	def start_timeout(self, timeout, do_force):
		'''
		Start the timer after setting the specified timeout. If already running, the timers won't start unless
		'do_force' is True.
		'''

		if not do_force and self.is_started: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  start-timeout " + str(timeout) + ("  (forcing)" if do_force else ""))

		if self.timer is not None:
			self.timer.cancel()
			self.timer = None

		self.t_timeout = timeout
		self.timer = threading.Timer(self.t_timeout, self.execute)
		self.t_start = time.time()
		self.timer.start()
		self.is_started = True

	# ------------------------------------------------------------------------------------------------------------------
	def start_timeout_fn(self, timeout, fn_exec, do_force):
		'''
		Start the timer after setting the specified timeout and the callback function. If already running, the timers
		won't start unless 'do_force' is True.
		'''

		if not do_force and self.is_started: return
		if self.DEBUG_LVL >= 1: print(self.__class__.__name__ + "  start-timeout-fn " + str(timeout) + ("  (forcing)" if do_force else ""))

		if self.timer is not None:
			self.timer.cancel()
			self.timer = None

		self.t_timeout = timeout
		self.exec_fn = fn_exec
		self.timer = threading.Timer(self.t_timeout, self.execute)
		self.t_start = time.time()
		self.timer.start()
		self.is_started = True

	# ------------------------------------------------------------------------------------------------------------------
	def step(self):
		''' Generate one event. Only works if paused. '''

		# TODO: ...


# ======================================================================================================================
if __name__ == "__main__":
	import sys

	# Force-starting with pausing:
	ta = Timer(1, lambda: sys.stdout.write("a\n"))
	ta.DEBUG_LVL = 1

	ta.start_timeout(5, False)
	ta.pause()
	ta.start_timeout(3, False)  # this will not work because the timer is already running
	ta.start_timeout(1, True)   # this however will force-start (actually, restart) the timer

	time.sleep(3)

	# Force-starting without pausing:
	tb = Timer(1, lambda: sys.stdout.write("b\n"))
	tb.DEBUG_LVL = 1

	tb.start_timeout(5, False)
	tb.start_timeout(3, False)  # this will not work because the timer is already running
	tb.start_timeout(1, True)   # this however will force-restart the timer
