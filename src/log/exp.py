# TODO
#     On-line processing
#         Save eye movements, words they occured on, and events in real time
#             Note: This would likely only be possible for first-pass reading, but
#                   with GC text change we wouldn't scrutinize second-pass reading
#             Note: Make this efficient and resource unintensive
#     Off-line processing
#         Convert saved data into a cohesive entity
#         Convert that entity into a Pegasus database import script
#             Note: This would bypass the regular import routin
#             Note: There would be no need for creating stimuli in Pegasus which
#                   would be ideal because currently it isn't able to handle GC
#                   text change
#             Note: Save info on words, n-2, n-1, n, n+1, and n+2 (and possible more)
#                   to grant Pegasus it's querying flexibility
#         Auto-run off-line processing at the end of experiment
#
# ----------------------------------------------------------------------------------------------------------------------


# ======================================================================================================================
class ExperimentLog(object):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__():
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		pass
