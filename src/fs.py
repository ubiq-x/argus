import os


# ----------------------------------------------------------------------------------------------------------------------
def get_root():
	dir_root = os.path.dirname(__file__)
	os.chdir(dir_root)

	if not os.path.isfile("is-root"):
		dir_root = os.path.dirname(dir_root)
		os.chdir(dir_root)

	if not os.path.isfile("is-root"):
		dir_root = os.path.dirname(dir_root)
		os.chdir(dir_root)

	if not os.path.isfile("is-root"):
		dir_root = os.path.dirname(dir_root)
		os.chdir(dir_root)

	return dir_root


# ======================================================================================================================
class FileSystem(object):
	'''
	Handle all filesystem related operations (e.g., creating directory structure).

	This class is used by all parts of the distributed application (i.e., the experiment app, the mindless reading
	detection app, and the subject server app).
	'''

	DIR_ROOT            = get_root()

	DIR_ASSETS          = os.path.join(DIR_ROOT, "assets")                    # assets (text, images, etc.); changed in the constructor
	DIR_STIM            = os.path.join(DIR_ROOT, "assets", "stim")            # stimuli; extended in exp_init()

	DIR_RT              = os.path.join(DIR_ROOT, "tmp-rt")                    # runtime temporary
	DIR_RT_EXP          = os.path.join(DIR_ROOT, "tmp-rt", "exp")             # runtime temporary: experiment
	DIR_RT_MRD          = os.path.join(DIR_ROOT, "tmp-rt", "mrd")             # runtime temporary: mindless reading detection

	DIR_RES             = os.path.join(DIR_ROOT, "res")                       # results
	DIR_RES_EXP         = os.path.join(DIR_ROOT, "res", "exp")                # results: experiment
	DIR_RES_EXP_EDF     = os.path.join(DIR_ROOT, "res", "exp", "edf")         # results: EDF files transported from the EyeLink host
	DIR_RES_EXP_SUB     = os.path.join(DIR_ROOT, "res", "exp", "sub")         # results: subjects
	DIR_RES_EXP_SUB_SUB = os.path.join(DIR_ROOT, "res", "exp", "sub", "...")  # results: the current subject's experiment results; changed in init_exp()

	DIR_RES_MRD         = os.path.join(DIR_ROOT, "res", "mrd")                # results: mindless reading detection
	DIR_RES_MRD_SUB     = os.path.join(DIR_ROOT, "res", "mrd", "sub")         # results: subjects
	DIR_RES_MRD_SUB_SUB = os.path.join(DIR_ROOT, "res", "mrd", "sub", "...")  # results: the current subject's mindless reading detection results; changed in init_mrdet()

	# ------------------------------------------------------------------------------------------------------------------
	def exp_begin(self, dir_sub):
		''' Called by Experiment.begin() '''

		if not os.path.exists(self.DIR_RT)          : os.makedirs(self.DIR_RT)
		if not os.path.exists(self.DIR_RT_EXP)      : os.makedirs(self.DIR_RT_EXP)

		if not os.path.exists(self.DIR_RES)         : os.makedirs(self.DIR_RES)
		if not os.path.exists(self.DIR_RES_EXP)     : os.makedirs(self.DIR_RES_EXP)
		if not os.path.exists(self.DIR_RES_EXP_EDF) : os.makedirs(self.DIR_RES_EXP_EDF)
		if not os.path.exists(self.DIR_RES_EXP_SUB) : os.makedirs(self.DIR_RES_EXP_SUB)

		self.DIR_RES_EXP_SUB_SUB = os.path.join(self.DIR_RES_EXP_SUB, dir_sub)
		if not os.path.exists(self.DIR_RES_EXP_SUB_SUB): os.makedirs(self.DIR_RES_EXP_SUB_SUB)

	# ------------------------------------------------------------------------------------------------------------------
	def exp_init(self, ctx_str):
		''' Called by Experiment.__init__() '''

		self.DIR_STIM = os.path.join(self.DIR_STIM, ctx_str)

	# ------------------------------------------------------------------------------------------------------------------
	def mrdet_begin(self, dir_sub, n_lvl_up=0):
		''' Called by MRDetMan.begin() '''

		if not os.path.exists(self.DIR_RT)      : os.makedirs(self.DIR_RT)
		if not os.path.exists(self.DIR_RT_EXP)  : os.makedirs(self.DIR_RT_EXP)

		if not os.path.exists(self.DIR_RES)     : os.makedirs(self.DIR_RES)
		if not os.path.exists(self.DIR_RES_MRD) : os.makedirs(self.DIR_RES_MRD)
		if not os.path.exists(self.DIR_RES_MRD_SUB) : os.makedirs(self.DIR_RES_MRD_SUB)

		self.DIR_RES_MRD_SUB_SUB = os.path.join(self.DIR_RES_MRD_SUB, dir_sub)
		if not os.path.exists(self.DIR_RES_MRD_SUB_SUB): os.makedirs(self.DIR_RES_MRD_SUB_SUB)
