#
# Problem statement
#     Find the most space-efficient way to store vectors of integers and floats. Time is not a factor. Ultimately,
#     store the vectors in a database. Each vector is relatively short (on the order of hundreds to thousands of
#     elements), but there are many of them.
#
# Python 3.6.2
#
# ----------------------------------------------------------------------------------------------------------------------

import bz2
import gzip
import numpy as np
import random

from array import array
from sklearn.metrics import accuracy_score
from sys import getsizeof as size


# ----------------------------------------------------------------------------------------------------------------------
# (1) Generate and use:
n = 1000
y = np.asarray([random.randint(0, 1) for b in range(n)])
z = np.asarray([random.uniform(0, 1) for _ in range(n)])

print("y:{}  z:{}\n".format(y.dtype, z.dtype))

acc_01 = accuracy_score(y, np.where(z > 0.5, 1, 0))


# ----------------------------------------------------------------------------------------------------------------------
# (2) Compress:
# (2.1) Array (ar):
y_ar    = array('i', y)
y_ar_gz = gzip.compress(y_ar)
y_ar_bz = bz2 .compress(y_ar)

z_ar    = array('d', z)
z_ar_gz = gzip.compress(z_ar)
z_ar_bz = bz2 .compress(y_ar)

# (2.2) Tostring (ts):
y_ts    = y.tostring()
y_ts_gz = gzip.compress(y_ts)
y_ts_bz = bz2 .compress(y_ts)

z_ts = z.tostring()
z_ts_gz = gzip.compress(z_ts)
z_ts_bz = bz2 .compress(z_ts)

# (2.3) Pickle (pi):
y_pi    = y.dumps()
y_pi_gz = gzip.compress(y_pi)
y_pi_bz = bz2 .compress(y_pi)

z_pi    = z.dumps()
z_pi_gz = gzip.compress(z_pi)
z_pi_bz = bz2 .compress(z_pi)

# (2.4) Summary:
print('==[ Y ]======================================')
print('   {:>9}  {:>9}  {:>9}  {:>9}'.format('ndarr', 'ar/ts/pi', 'gz', 'bz2'))
print('---------------------------------------------')
print('ar {:>9}  {:>9}  {:>9}  {:>9}'.format(size(y), size(y_ar), size(y_ar_gz), size(y_ar_bz)))
print('ts {:>9}  {:>9}  {:>9}  {:>9}'.format(size(y), size(y_ts), size(y_ts_gz), size(y_ts_bz)))
print('pi {:>9}  {:>9}  {:>9}  {:>9}'.format(size(y), size(y_pi), size(y_pi_gz), size(y_pi_bz)))
print('=============================================\n')

print('==[ Z ]======================================')
print('   {:>9}  {:>9}  {:>9}  {:>9}'.format('ndarr', 'ar/ts/pi', 'gz', 'bz2'))
print('---------------------------------------------')
print('ar {:>9}  {:>9}  {:>9}  {:>9}'.format(size(z), size(z_ar), size(z_ar_gz), size(z_ar_gz)))
print('ts {:>9}  {:>9}  {:>9}  {:>9}'.format(size(z), size(z_ts), size(z_ts_gz), size(z_ts_bz)))
print('pi {:>9}  {:>9}  {:>9}  {:>9}'.format(size(z), size(z_pi), size(z_pi_gz), size(z_pi_bz)))
print('=============================================\n')


# ----------------------------------------------------------------------------------------------------------------------
# (3) Decompress and use:
# (3.1) Array:
y_ar_ = array('i')
y_ar_.frombytes(gzip.decompress(y_ar_gz))
y_ = np.asarray(y_ar_)

assert(y_ar_ == y_ar)
assert(np.array_equal(y_, y))

z_ar_ = array('d')
z_ar_.frombytes(gzip.decompress(z_ar_gz))
z_ = np.asarray(z_ar_)

assert(z_ar_ == z_ar)
assert(np.array_equal(z_, z))

acc_02 = accuracy_score(y_, np.where(z_ > 0.5, 1, 0))
print('acc-02: {}  {}'.format(acc_01, acc_02))
assert(acc_01 == acc_02)

print("y:{}  z:{}\n".format(y_.dtype, z_.dtype))

# (3.2) Tostring:
y_ = np.fromstring(gzip.decompress(y_ts_gz), dtype=int)
z_ = np.fromstring(gzip.decompress(z_ts_gz), dtype=float)

assert(np.array_equal(y_, y))
assert(np.array_equal(z_, z))

acc_03 = accuracy_score(y_, np.where(z_ > 0.5, 1, 0))
print('acc-03: {}  {}'.format(acc_01, acc_03))
assert(acc_01 == acc_03)

print("y:{}  z:{}\n".format(y_.dtype, z_.dtype))

# (3.2) Pickle:
# Pickle sucks so skipping this part...


# ----------------------------------------------------------------------------------------------------------------------
# (3) Conclusions:
#     If n < 10000  : array > tostring > pickle; BZ2 > GZ
#     If n > 100000 : ditto, but differences are small
#     If lazy AF    : pickle (the easiest) + BZ2
#
#     Random integers compress much better than random floats (duh!)
#     Random floats barely compress at all (duh!)
#
#     Pickle + GZ for arrays of floats blows up the size so don't use (use BZ2 if you choose to pickle)
#     Pickle + compress performance depends on n (again, better to avoid this combo to begin with)


# ----------------------------------------------------------------------------------------------------------------------
# (4) Database test (array):
import sqlite3

conn = sqlite3.connect(':memory:')

with conn as c:
	c.execute('CREATE TABLE t (y BLOB, z BLOB)')
	c.execute('INSERT INTO t (y, z) VALUES (?, ?)', [bz2.compress(array('i', y)), bz2.compress(array('d', z))])

y_ar_bz_db, z_ar_bz_db = c.execute('SELECT y, z FROM t LIMIT 1').fetchone()

y_ar_db = array('i')
y_ar_db.frombytes(bz2.decompress(y_ar_bz_db))
y_db = np.asarray(y_ar_db)

z_ar_db = array('d')
z_ar_db.frombytes(bz2.decompress(z_ar_bz_db))
z_db = np.asarray(z_ar_db)

assert(np.array_equal(y_db, y))
assert(np.array_equal(z_db, z))

acc_04 = accuracy_score(y_db, np.where(z_db > 0.5, 1, 0))
print('acc-04: {}  {}'.format(acc_01, acc_04))
assert(acc_01 == acc_04)

conn.close()

print("y {}:{}    z {}:{}\n".format(size(y_ar), size(y_ar_db), size(z_ar), size(z_ar_db)))
	# size is different; that's a bit odd, but arrays are still identical


# ----------------------------------------------------------------------------------------------------------------------
# (5) Database test (tostring):
import sqlite3

conn = sqlite3.connect(':memory:')

with conn as c:
	c.execute('CREATE TABLE t (y BLOB, z BLOB)')
	c.execute('INSERT INTO t (y, z) VALUES (?, ?)', [bz2.compress(y.tostring()), bz2.compress(z.tostring())])

y_ts_bz_db, z_ts_bz_db = c.execute('SELECT y, z FROM t LIMIT 1').fetchone()

y_db = np.fromstring(bz2.decompress(y_ts_bz_db), dtype='int64')
z_db = np.fromstring(bz2.decompress(z_ts_bz_db), dtype='float64')

assert(np.array_equal(y_db, y))
assert(np.array_equal(z_db, z))

acc_05 = accuracy_score(y_db, np.where(z_db > 0.5, 1, 0))
print('acc-05: {}  {}'.format(acc_01, acc_05))
assert(acc_01 == acc_05)

conn.close()

print("y {}:{}    z {}:{}".format(size(y), size(y_db), size(z), size(z_db)))


# ----------------------------------------------------------------------------------------------------------------------
# (6) Partial output trace:
#
# y:int64  z:float64
#
# n=100
# 		==[ Y ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar       896        472         93         95
# 		ts       896        833         96         99
# 		pi       896       1041        277        292
# 		=============================================
#
# 		==[ Z ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar       896        880        856        856
# 		ts       896        833        856       1087
# 		pi       896       1447       1202       1288
# 		=============================================
#
# n=1000  (*)
# 		==[ Y ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar     80096      40404       2208       1646
# 		ts     80096      80033       2497       1656
# 		pi     80096      80242       2748       1900
# 		=============================================
#
# 		==[ Z ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar     80096      80744      75490      75490
# 		ts     80096      80033      75490      76823
# 		pi     80096     120291      90852      78386
# 		=============================================
#
# n=10000
# 		==[ Y ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar     80096      40404       2213       1646
# 		ts     80096      80033       2496       1649
# 		pi     80096      80242       2747       1906
# 		=============================================
#
# 		==[ Z ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar     80096      80744      75524      75524
# 		ts     80096      80033      75524      76874
# 		pi     80096     119872      90772      78331
# 		=============================================
#
# n=100000
# 		==[ Y ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar    800096     408344      20470      15539
# 		ts    800096     800033      23673      15601
# 		pi    800096     800244      23986      15856
# 		=============================================
#
# 		==[ Z ]======================================
# 		       ndarr   ar/ts/pi         gz        bz2
# 		---------------------------------------------
# 		ar    800096     816624     754495     754495
# 		ts    800096     800033     754495     763784
# 		pi    800096    1199067     903151     777405
# 		=============================================
#
#
# (*)
# 		acc-02: 0.5  0.5
# 		y:int32  z:float64
#
# 		acc-03: 0.5  0.5
# 		y:int64  z:float64
#
# 		acc-04: 0.5  0.5
# 		y 40404:42576    z 80744:85088
#
# 		acc-05: 0.5  0.5
# 		y 80096:80096    z 80096:80096


# ----------------------------------------------------------------------------------------------------------------------
# (7) Real data test:
#
# - n: length of vector
# - i: ndarray of ints
# - d: ndarray of doubles
# - BZ2
#
#          array     tostr
#    n   i     d   i     d
#   80  57   465  67   439
#   80  57   464  67   458
#   80  57   457  67   461
#   85  57   507  68   468
#   85  56   484  67   461
#   85  57   490  68   466
#   85  56   481  67   481
#   85  57   471  68   464
#   85  56   480  67   469
#   87  58   471  65   441
#   87  58   476  65   444
#   87  58   461  65   443
#   96  59   544  70   509
#   96  59   541  70   506
#   96  59   543  70   510
#   98  62   548  71   520
#   98  62   552  71   503
#   98  62   534  71   514
#  102  64   531  69   499
#  102  64   532  69   494
#  102  64   533  69   498
#  106  60   556  65   532
#  106  60   549  65   528
#  106  60   552  65   524
#  122  67   649  75   604
#  122  67   629  75   599
#  122  67   631  75   601
#  129  65   703  73   632
#  129  65   700  73   634
#  129  65   711  73   648
#  207  70   995  78  1029
#  207  70  1052  78  1006
#  207  70  1040  78   974
#  225  74  1086  82  1068
#  225  74  1100  82  1088
#  225  74  1091  82  1083
#  250  77  1239  84  1219
#  250  77  1250  84  1194
#  250  77  1232  84  1192
#  265  78  1338  90  1231
#  265  78  1320  90  1222
#  265  78  1297  90  1249
#  279  75  1358  86  1341
#  279  75  1340  86  1299
#  279  75  1337  86  1323
#  281  78  1355  87  1198
#  281  78  1341  87  1244
#  281  78  1361  87  1252
#  291  80  1314  86  1217
#  291  80  1325  86  1208
#  291  80  1335  86  1257
#  302  78  1435  90  1310
#  302  78  1447  90  1312
#  302  78  1482  90  1348
#  304  82  1447  94  1452
#  304  82  1459  94  1448
#  304  82  1474  94  1464
#  320  77  1549  89  1441
#  320  77  1501  89  1359
#  320  77  1521  89  1402
#
# Conclusions
#     If int    : array > tostr
#     If dobule : tostr > array

9  145  14  758  2017-09-21  FC  FC-16-relu.FC-16-relu  2  30  64  0  210  2011 gg  roi.30-_.ps1.adj.gg.20.2011--n10-5  10  5  8  21  1  2049  3622  445  440  726  86  98  979  7  0.5344  0.5349  0.6122  40  62  496
9  145  14  759  2017-09-21  FC  FC-16-relu.FC-16-relu  2  30  64  0  210  2011 gg  roi.30-_.ps1.adj.gg.20.2011--n10-5  10  5  8  21  2  2525  3578  483  446  736  94  80  1457  21  0.5978  0.5532  0.625  40  57  428

10  86  14  760  2017-09-21  FC  FC-16-relu.FC-16-relu  2  30  64  1  218  2011 gg  roi.30-_.ps1.adj.gg.20.2011--n10-5  10  5  8  21  1  2559  3622  445  440  726  86  98  1548  22  0.6171  0.6744  0.4592  40  62  511
10  86  14  761  2017-09-21  FC  FC-16-relu.FC-16-relu  2  30  64  1  218  2011 gg  roi.30-_.ps1.adj.gg.20.2011--n10-5  10  5  8  21  2  1954  3578  483  446  736  94  80  899  6  0.5734  0.617  0.6125  40  57  444
