import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pygame

from abc import abstractmethod

from util import Time


# ======================================================================================================================
class Stimulus(object):
	'''
	This is an abstract class.
	'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, et, name, em_vis=None, gc=None):
		self.et = et

		self.ts_0_et = self.et.ts()
		self.dt_0    = Time.dt()

		self.surf = surf
		self.name = name
		self.em_vis = em_vis
		self.gc = gc

		if self.bg_img is None:  # fallback (a subclass should typically set this before we get here)
			self.bg_img = pygame.Surface(surf.get_size()).convert()
			self.bg_img.fill((255, 255, 255))

		self.has_been_disp = False  # has the stimulus been displayed at least once?

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def do_evt_be(self, blink):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def do_evt_bs(self, blink):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def do_evt_fe(self, fix):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def do_evt_fs(self, fix):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_se(self, sacc):
		if self.em_vis is not None:
			self.em_vis.do_se(sacc)

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_ss(self, sacc):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def display(self, do_flip=True):
		if not self.has_been_disp:  # update 'dt_0' right prior to first display
			self.ts_0_et = self.et.ts()
			self.dt_0    = Time.dt()

			self.has_been_disp = True

		if self.em_vis is not None:
				self.em_vis.draw()
				self.em_vis.bg_set(self.bg_img)
				self.em_vis.reset()
				# TODO:
				# If the current stimulus is displayed again, all the possibly visualized eye movements will be reset.
				# This problem is of low priority, but it should be addressed at some point.

		if do_flip:
			pygame.display.flip()  # flip() updates the entire screen while update() can update only a part of it

	# ------------------------------------------------------------------------------------------------------------------
	def gc_set(self, gc):
		self.gc = gc

		# if (self.gc is not None) and (self.em_vis is not None):
		# 	self.em_vis.bg_set(self.gc.mask_img)
		# problem is gc.mask_img is not defined here yet because it's set in gc.stim_set() which is called later
