# TODO
#     Based on GC, save the image the subject actually saw (along with text and ROIs) to their data directory
#         Need to go with first-pass ready only. Otherwise there may be competing version of what the subject saw.
#         Use low-quality JPGs to conserve space or skip them completely (Pegasus only uses images as background,
#         but we have ROIs files already).
#     Recode to respond to the FixationStart event (currently it's the FixationEnd)
#     Make mask size change interactive
#         Right now only the xy is stored. The actual center of mask needs to be stored as well. Then, the size change
#         should be made relative to that center point (in out case, a fixation).
#
#     Add an option to hide lenght of lines? When masked, all lines would be displayed to have the same length.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile
__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import math
import os
import pygame
import random
import re
import string

import fs


# ======================================================================================================================
class TextStimulusGC(object):
	# Mask size:
	MASK_MODE_NONE =  0
	MASK_MODE_CHAR = 10  # character-level (i.e., size changes by character)
	MASK_MODE_WORD = 20  # word-level (i.e., size changes by word; resulting mask will be of varying size from fixation to fixation)

	# Substitution modes (character-level):
	SUB_MODE_CHAR_ONE   = 10  # single character ('self.sub_char')
	SUB_MODE_CHAR_RAND  = 11  # random
	SUB_MODE_CHAR_MATCH = 12  # matching letter (e.g., t --> l, b --> d, y --> g; but not t --> y)

	# Substitution modes (word-level):
	SUB_MODE_WORD_RAND    = 21  # random word
	SUB_MODE_WORD_FRQ     = 22  # random word matched on frequency
	SUB_MODE_WORD_FRQ_L   = 23  # random l-frq word
	SUB_MODE_WORD_FRQ_H   = 24  # random h-frq word
	SUB_MODE_WORD_FRQ_H2L = 25  # random l-frq word (only if it substitutes a h-frq word; i.e., no l-frq words will be substituted)
	SUB_MODE_WORD_FRQ_L2H = 26  # random h-frq word (only if it substitutes a l-frq word; i.e., no h-frq words will be substituted)
	SUB_MODE_WORD_FRQ_POS = 27  # random word matched on frequency and part of speech
	SUB_MODE_WORD_POS     = 28  # random word matched on part of speech
		# all words are matched on length (naturally as they are shown on top of an already formatted text)

	SUB_MODE_2_FNAME = {  # filenames (actually suffixes) to be used when a mask is generated for future loading (i.e., to save time on generating the mask again)
		SUB_MODE_CHAR_ONE     : "gc-char-one.txt",
		SUB_MODE_CHAR_RAND    : "gc-char-rand.txt",
		SUB_MODE_CHAR_MATCH   : "gc-char-match.txt",

		SUB_MODE_WORD_RAND    : "gc-word-rand.txt",
		SUB_MODE_WORD_FRQ     : "gc-word-frq.txt",
		SUB_MODE_WORD_FRQ_L   : "gc-word-frq-l.txt",
		SUB_MODE_WORD_FRQ_H   : "gc-word-frq-h.txt",
		SUB_MODE_WORD_FRQ_H2L : "gc-word-frq-h2l.txt",
		SUB_MODE_WORD_FRQ_L2H : "gc-word-frq-l2h.txt",
		SUB_MODE_WORD_FRQ_POS : "gc-word-frq-pos.txt",
		SUB_MODE_WORD_POS     : "gc-word-pos.txt"
	}

	SUB_CHAR_ONE = "*"

	SUB_CHAR_MATCH_a = "aceimnorsuvwxz"  # any of these letters is a substitution candidate for any other (in the SUB_MODE_CHAR_MATCH mode)
	SUB_CHAR_MATCH_b = "bdfhklt"         # ^
	SUB_CHAR_MATCH_g = "gjpqy"           # ^

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, plm, mask_size, do_invert=True, do_hide_punct=True, do_hide_space=False, mask_mode=MASK_MODE_NONE, sub_mode=SUB_MODE_CHAR_ONE, sub_char=SUB_CHAR_ONE, sub_word_min_len=3):
		'''
		mask_size = (chars-or-words-to-the-left, chars-or-words-to-the-right, lines-above, lines-below)
		'''

		self.surf = surf
		self.plm = plm  # PsycholingMan

		self.do_hide_punct = do_hide_punct  # hide punctuation in masked text?
		self.do_hide_space = do_hide_space  # hide word boundary in masked text?
		self.do_invert = do_invert

		self.mask_size = mask_size

		self.sub_mode = sub_mode
		self.sub_char = sub_char

		self.sub_word_min_len = sub_word_min_len

		self.mask_mode = None
		self.mask_mode_set(mask_mode)

		self.stim = None

		# self.stim_set() sets more object variables

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		# Undo mask inversion changes done to the stimulus:
		if (self.do_invert) and (self.stim is not None):
			self.stim.bg_img = self.inv_0_bg_img
			# TODO:
			# The BG image should be displayed here if this method is called before we are done with the stimulus.
			# That currently isn't the case so at this point this doesn't need attention, but soemthing to keep in mind.

		self.stim = None

	# ------------------------------------------------------------------------------------------------------------------
	def __debug_mask_disp__(self, do_show):
		''' Shows and hides the entire mask. '''

		if do_show:
			self.surf.blit(self.mask_img, (0, 0))
			pygame.display.update()
		else:
			self.surf.blit(self.bg_img, (0, 0))
			pygame.display.update()

		self.mask.dirty = 1
		self.mask_grp.draw(self.surf)

	# ------------------------------------------------------------------------------------------------------------------
	# When the current fixation is drawn, the wrong BG image is used when it is removed.  Consequently, instead of the
	# mask BG image being restored, the original image is restored instead.  Below is an attempt (still unsuccessuf) to
	# fix that.

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_se(self, sacc, roi_i):
		'''
		rot_i: index of the ROI being fixated.
		'''

		if (self.mask_mode == self.MASK_MODE_NONE) or (self.mask is None): return

		# (1) Character mask mode:
		if (self.mask_mode == self.MASK_MODE_CHAR):
			g = sacc.getEndGaze()

			# Find the top-left coordinates of the character at the 'xy' coordinates:
			x = math.floor((g[0] - self.txt_xy[0]) / self.char_w) * self.char_w + self.txt_xy[0]
			y = math.floor((g[1] - self.txt_xy[1]) / self.ln_h)   * self.ln_h   + self.txt_xy[1]

			# Compute the mask size:
			self.mask_xy = (x - self.mask_size[0] * self.char_w - 1, y - self.mask_size[2] * self.ln_h)  # - 1 makes it look better
			self.draw()

		# (2) Word mask mode:
		elif (self.mask_mode == self.MASK_MODE_WORD):
			(self.mask_xy, self.mask_wh) = self.stim.roi_get_rect_at(roi_i, self.mask_size)
			self.draw(True)

	# ------------------------------------------------------------------------------------------------------------------
	def do_hide_punct_set(self, do):
		self.do_hide_punct = do
		self.mask_img = self.mask_img_gen()
		self.mask.mask_img_set(self.mask_img)

		self.mask_upd()

	# ------------------------------------------------------------------------------------------------------------------
	def do_hide_space_set(self, do):
		self.do_hide_space = do
		self.mask_img = self.mask_img_gen()
		self.mask.mask_img_set(self.mask_img)

		self.mask_upd()

	# ------------------------------------------------------------------------------------------------------------------
	def do_invert_set(self, do_invert):
		self.do_invert = do_invert
		self.mask_upd()

	# ------------------------------------------------------------------------------------------------------------------
	def draw(self, has_mask_size_changed=False):
		if self.stim is None: return

		self.mask.update(self.mask_xy, (self.mask_wh) if (has_mask_size_changed) else (None))
		self.mask_grp.draw(self.surf)

	# ------------------------------------------------------------------------------------------------------------------
	def mask_img_gen(self):
		img = pygame.Surface(self.surf.get_size())
		img.fill(self.bg_color)
		font = pygame.font.Font(os.path.join(fs.FileSystem().DIR_ASSETS, "font", self.font["family"], self.font["file"]), self.font["size"])

		txt = self.mask_img_gen__get_text()

		ln_n = 0
		for line in txt.split("\n"):
			img_txt = font.render(line, 1, self.font["color"])
			# img_txt_xy = (self.txt_xy[0] + self.char_w - 6, self.txt_xy[1] + ln_n * self.ln_h + 6)  # TODO: Why is -6 and +6 needed?
			img_txt_xy = (self.txt_xy[0] + self.char_w, self.txt_xy[1] + ln_n * self.ln_h + 6)  # TODO: Why is +6 needed?
			img.blit(img_txt, img_txt_xy)
			ln_n += 1

		return img.convert()

	# ----^-------------------------------------------------------------------------------------------------------------
	def mask_img_gen__gen_txt__sub_char_one(self):
		txt = re.sub(r"[\w]", self.sub_char, self.txt)
		if self.do_hide_punct:
			txt = re.sub(r"[" + string.punctuation + "]", self.sub_char, txt)
		if self.do_hide_space:
			txt = re.sub(r"[ \t]", self.sub_char, txt)

		return txt

	# ----^-------------------------------------------------------------------------------------------------------------
	def mask_img_gen__gen_txt__sub_char_rand(self):
		txt = list(self.txt)
		for (i, c) in enumerate(txt):
			if (c == "\n") or (c in string.digits) or (c in [" ", "\t"] and not self.do_hide_space) or (c in string.punctuation and not self.do_hide_punct):
				txt[i] = c
			else:
				if c in string.punctuation:
					txt[i] = random.choice(self.SUB_CHAR_MATCH_a)
				elif c.islower():
					txt[i] = random.choice(string.ascii_lowercase)
				else:
					txt[i] = random.choice(string.ascii_uppercase)
		txt = "".join(txt)

		return txt

	# ----^-------------------------------------------------------------------------------------------------------------
	def mask_img_gen__gen_txt__sub_char_match(self):
		txt = list(self.txt)
		for (i, c) in enumerate(txt):
			if (c == "\n") or (c in string.digits) or (c in [" ", "\t"] and not self.do_hide_space) or (c in string.punctuation and not self.do_hide_punct):
				txt[i] = c
			else:
				if c.isupper():
					txt[i] = random.choice(string.ascii_uppercase)
				else:
					if   c in self.SUB_CHAR_MATCH_b: txt[i] = random.choice(self.SUB_CHAR_MATCH_b)
					elif c in self.SUB_CHAR_MATCH_g: txt[i] = random.choice(self.SUB_CHAR_MATCH_g)
					else:                            txt[i] = random.choice(self.SUB_CHAR_MATCH_a)
		txt = "".join(txt)

		return txt

	# ----^-------------------------------------------------------------------------------------------------------------
	#
	# Profiling
	#     Baseline (before optimization; MBP15)
	#         SUB_MODE_WORD_RAND
	#             2.311821  (n=100)
	#         SUB_MODE_WORD_FRQ
	#             0.064095  (n=100)
	#         SUB_MODE_WORD_POS
	#             0.434131  (n=100)
	#         SUB_MODE_WORD_FRQ_POS
	#             0.586348  (n=100)
	#     Loading the DB into memory (two- to three-fold slow-downs; MBP15)
	#         SUB_MODE_WORD_RAND
	#             ?  (n=100)
	#         SUB_MODE_WORD_FRQ
	#             ?  (n=100)
	#         SUB_MODE_WORD_POS
	#             0.997726  (n=100)
	#         SUB_MODE_WORD_FRQ_POS
	#             1.438220  (n=100)
	#     Conclusion: Do not load the DB into memory
	#
	# ----
	def mask_img_gen__gen_txt__sub_word(self, fn_match):
		'''
		The 'fn_match' controls how a match for words is made. It will typically be a word matching method from the
		PsycholingMan class. Only one argument will be passed to that function: A word to be substituted.
		'''

		# import time
		# n = 100
		# t0 = time.time()
		# for i in range(n):
		#     ...
		# t = (time.time() - t0) / n
		# print("time: %f" % t)

		res = []

		for l in self.txt.split("\n"):
			for w in l.split(" "):
				if (len(w) >= self.sub_word_min_len):
					w_sub = fn_match(w, True)
					if w_sub["is-word-sub"]:
						if w[0].isupper():
							w = w_sub["word"][0].upper() + w_sub["word"][1:]
						else:
							w = w_sub["word"]
				res.extend([w, " "])
			res.append("\n")

		return "".join(res)

	# ----^-------------------------------------------------------------------------------------------------------------
	def mask_img_gen__get_text(self):
		'''
		Loads text for the current substitution mode from a file. If the file does not exist, it generates the text
		and saves it in a file for later loading.
		'''

		txt = None
		path = os.path.join(self.stim.dir_self, "%s-%s" % (self.stim.name, self.SUB_MODE_2_FNAME[self.sub_mode]))

		if os.path.isfile(path):
			with open(path, "r") as f:
				txt = f.read()
		else:
			txt = {
				self.SUB_MODE_CHAR_ONE:     self.mask_img_gen__gen_txt__sub_char_one,
				self.SUB_MODE_CHAR_RAND:    self.mask_img_gen__gen_txt__sub_char_rand,
				self.SUB_MODE_CHAR_MATCH:   self.mask_img_gen__gen_txt__sub_char_match,

				self.SUB_MODE_WORD_RAND:    lambda: self.mask_img_gen__gen_txt__sub_word(self.plm.word_match_by_len),
				self.SUB_MODE_WORD_FRQ:     lambda: self.mask_img_gen__gen_txt__sub_word(self.plm.word_match_by_len_frq),
				self.SUB_MODE_WORD_POS:     lambda: self.mask_img_gen__gen_txt__sub_word(self.plm.word_match_by_len_pos),
				self.SUB_MODE_WORD_FRQ_POS: lambda: self.mask_img_gen__gen_txt__sub_word(self.plm.word_match_by_len_frq_pos)
			}.get(self.sub_mode)()

			with open(path, "w") as f:
				f.write(txt)

		return txt

	# ------------------------------------------------------------------------------------------------------------------
	def mask_size_set(self, size):
		self.mask_size = (max(size[0], 0), max(size[1], 0), max(size[2], 0), max(size[3], 0))
		self.mask_wh = ((self.mask_size[0] + self.mask_size[1] + 1) * self.char_w, self.ln_h + (self.mask_size[2] + self.mask_size[3]) * self.ln_h)

		if self.mask_xy is not None:
			self.draw(True)
			pygame.display.update()

	# ------------------------------------------------------------------------------------------------------------------
	def mask_upd(self):
		'''
		This method is called after changing of the following:

		- Stimulus
		- Punctuation masking
		- Space masking
		- Mask inversion

		The new mask should be generated before this method is called.
		'''

		if self.do_invert:
			self.mask.mask_img_set(self.bg_img)            # update the sprite
			self.mask_grp.clear(self.surf, self.mask_img)  # update the sprite group
			self.surf.blit(self.mask_img, (0, 0))          # update the screen
		else:
			self.mask.mask_img_set(self.mask_img)          # update the sprite
			self.mask_grp.clear(self.surf, self.bg_img)    # update the sprite group
			self.surf.blit(self.bg_img, (0, 0))            # update the screen

		if self.mask_xy is not None:
			self.draw()

		pygame.display.update()

	# ------------------------------------------------------------------------------------------------------------------
	def mask_mode_set(self, mode):
		if self.mask_mode == mode: return

		self.mask_mode = mode

	# ------------------------------------------------------------------------------------------------------------------
	def stim_set(self, stim, bg_img, bg_img_actual, txt, txt_xy, char_w, ln_h, bg_color, font):
		''' Called every time a new stimulus is loaded. Typically that new stimulus will call this method. '''

		self.stim = stim
		self.bg_img = bg_img
		self.bg_img_actual = bg_img_actual
		self.txt = txt
		self.txt_xy = txt_xy  # top-left text coords
		self.char_w = char_w
		self.ln_h = ln_h
		self.bg_color = bg_color
		self.font = font

		self.mask_img = self.mask_img_gen()
		self.mask_xy = None
		self.mask_wh = ((1 + self.mask_size[0] + self.mask_size[1]) * self.char_w, (1 + self.mask_size[2] + self.mask_size[3]) * self.ln_h)
		self.mask = TextStimulusGC_Mask(self.surf, self.mask_img, self.mask_wh)  # sprite
		self.mask_grp = pygame.sprite.LayeredDirty()                             # sprite group
		self.mask_grp.add(self.mask)
		self.mask_grp.clear(self.surf, self.bg_img)

		self.mask_upd()


# ======================================================================================================================
class TextStimulusGC_Mask(pygame.sprite.DirtySprite):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, mask_img, mask_wh):
		super(TextStimulusGC_Mask, self).__init__()

		self.blendmode = 0
		self.dirty     = 0
		self.visible   = 1

		self.surf = surf
		self.mask_img = mask_img
		self.mask_wh = mask_wh

		self.image = pygame.Surface(mask_wh)  # this is where the current mask will be copied from 'self.mask_img' and subsequently displayed
		self.rect = self.image.get_rect()
		self.rect.topleft = (-1000, -1000)  # move off screen

	# ------------------------------------------------------------------------------------------------------------------
	def draw(self):
		''' Not used if the sprite is drawn by the group. '''

		self.surf.blit(self.image, self.rect)

	# ------------------------------------------------------------------------------------------------------------------
	def mask_img_set(self, img):
		self.mask_img = img

	# ------------------------------------------------------------------------------------------------------------------
	def update(self, xy, wh=None):
		'''
		Updates which portion of the mask image should be displayed.

		If 'wh' is None, it is assumed that the size of the mask has not been changed. In other words, 'wh' should only
		be supplied if the mask size changes.
		'''

		if (wh is not None) and (self.mask_wh != wh):
			self.image = pygame.Surface(wh)
			self.mask_wh = wh

		self.image.blit(self.mask_img, (0, 0), (xy, self.mask_wh))
		pygame.draw.rect(self.image, (255, 0, 0), pygame.Rect((0, 0), self.mask_wh), 1)  # DEBUG: Visualizes the mask border
		self.image = self.image.convert()

		self.rect = self.image.get_rect()  # TODO: Is the following better?  self.rect = (xy, self.mask_wh)
		self.rect.topleft = xy

		self.dirty = 1
