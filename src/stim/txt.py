# TODO
#     Draw text on first display
#         Delete all images once they aren't needed any more not to hog resources
#     Load word frequency for each word
#     Optimization
#         Organize ROIs into lines, dispatch search to the line, then search ROIs contained within
#         Start searching with the previously fixated line (or the one above to account for regressions and tracking or motor errors)
#     Keep lines ordered
#         Use dictionary with line Y-coords as key [currently implemented solution]
#         from collections import OrderedDict
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import math
import pygame
import pygame.draw
import os

import img


# ======================================================================================================================
class TextStimulus(img.ImageStimulus):
	DEBUG_LVL = 1  # 0=none, 1=normal, 2=full

	ROI_FNAME = "roi.txt"

	ROI_IDX_WORD     = 0
	ROI_IDX_RECT_XY  = 1  # rectangle: ((x1, y1), (x2, y2))
	ROI_IDX_RECT_WH  = 2  # rectangle: (x1, y1, w, h)
	ROI_IDX_W_LEN    = 3
	ROI_IDX_W_FRQ    = 4
	ROI_IDX_W_PRED   = 5
	ROI_IDX_W_POS    = 6
	ROI_IDX_LN_ID    = 7  # link to 'self.lines'            (generated after ROIs are loaded and lines infered)
	ROI_IDX_LN_IDX   = 8  # index in 'ln[self.LN_IDX_ROIS]' (generated after ROIs are loaded and lines infered)

	LN_IDX_RECT_XY = 0
	LN_IDX_RECT_WH = 1
	LN_IDX_Y_MID   = 2  # a mid-y coordinates (may be useful for emulating fixations)
	LN_IDX_ROIS    = 3  # a list of ROIs belonging to the line

	DISP_MODE_NORMAL   = 0  # these modes are useful for debugging while programming experiments to make sure ROIs and lines are properly defined with their coordinates
	DISP_MODE_LN       = 1  # ^
	DISP_MODE_ROI      = 2  # ^
	DISP_MODE_ROI_FILE = 3  # ^

	BG_COLOR = (255, 255, 255)
	FONT = { "family": "Anonymous_Pro", "file": "AnonymousPro-Regular.ttf", "size": 28, "color": (0, 0, 0) }

	VIS_ROI_COLOR = (0, 184, 46)
	VIS_ROI_W = 1

	VIS_LN_COLOR = (255, 162, 0)
	VIS_LN_W = 1

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_stim, et, name, img_ext, surf, em_vis=None, gc=None):
		super(TextStimulus, self).__init__(dir_stim, et, name, img_ext, surf, em_vis, gc)

		self.txt = self.txt_load()
		self.rois = self.rois_load()

		self.char_w = (math.floor(self.rois[0][self.ROI_IDX_RECT_WH][2] / len(self.rois[0][self.ROI_IDX_WORD]))) if (self.rois is not None) else (None)
		self.ln_h = (self.rois[0][self.ROI_IDX_RECT_WH][3]) if (self.rois is not None) else (None)  # all ROIs are assumed to be the same height

		self.lines = self.lines_infer()

		self.disp_mode = self.DISP_MODE_NORMAL

		self.gc_has_fired = False
		self.gc_bg_img = self.bg_img.copy()  # what's been actually presented to the subject (only relevant if GC is turned on)  # TODO: Check if copy() creates a deep copy of the image rather than just a reference.
		self.gc_txt = self.txt               # ^
		self.gc_rois = self.rois             # ^

		self.gc_set(gc)

	# ------------------------------------------------------------------------------------------------------------------
	def __debug_display__(self, do_flip=True):
		if self.gc is not None:
			pass  # the GC object handles display (otherwise the GC-drawn display will be overwritten)
		else:
			super(TextStimulus, self).display(do_flip)

	# ------------------------------------------------------------------------------------------------------------------
	def __debug_display_lines__(self):
		img = pygame.image.load(self.path_img).convert()
		self.surf.blit(img, (0, 0))

		for k in self.lines:
			r = self.lines[k][self.LN_IDX_RECT_WH]
			pygame.draw.rect(self.surf, self.VIS_LN_COLOR, pygame.Rect((r[0], r[1]), (r[2], r[3])), self.VIS_LN_W)

		pygame.display.flip()

	# ------------------------------------------------------------------------------------------------------------------
	def __debug_display_rois__(self):
		img = pygame.image.load(self.path_img).convert()
		self.surf.blit(img, (0, 0))

		for r in self.rois:
			rr = r[self.ROI_IDX_RECT_WH]
			pygame.draw.rect(self.surf, self.VIS_ROI_COLOR, pygame.Rect((rr[0], rr[1]), (rr[2], rr[3])), self.VIS_ROI_W)

		pygame.display.flip()

	# ------------------------------------------------------------------------------------------------------------------
	def __debug_display_rois_file__(self):
		path_img_rois = os.path.join(self.dir_stim, self.name, self.name + "-rois." + self.img_ext)
		if not os.path.exists(path_img_rois):
			self.vis_mode = self.DISP_MODE_NORMAL
			super(TextStimulus, self).display()
			return

		img = pygame.image.load(path_img_rois).convert()
		self.surf.blit(img, (0, 0))

		pygame.display.flip()

	# ------------------------------------------------------------------------------------------------------------------
	# DEBUG: Shows the original image alone, with lines superimposed, with ROIs superimposed, etc.
	def display(self):
		{
			self.DISP_MODE_NORMAL:   self.__debug_display__,
			self.DISP_MODE_LN:       self.__debug_display_lines__,
			self.DISP_MODE_ROI:      self.__debug_display_rois__,
			self.DISP_MODE_ROI_FILE: self.__debug_display_rois_file__
		}.get(self.disp_mode)()

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_fe(self, fix):
		if self.rois is None: return

		# (1) Find the word being fixated:
		g = fix.getAverageGaze()

		(roi_i, roi) = self.roi_get_at(g)

		# (2) Process the word being fixated:
		if roi is not None:
			# (2.1) Gaze-contingent word-substitution:
			# if self.gc is not None:
			# 	self.gc_has_fired = True
			#
			# 	self.gc.do_evt_fix(fix, roi_i)
			#
			# 	# rr = roi[self.ROI_IDX_RECT_WH]
			# 	# self.gc.do_evt_fix(fix, (rr[0], rr[1]), (rr[2], rr[3]), roi[self.ROI_IDX_WORD])
			#
			# 	if self.em_vis is not None:
			# 		self.em_vis.bg_set(self.gc_bg_img)  # TODO: Test if this works.

			# (2.2) Mindless reading detection:
			# TODO: HOW THE FUCK TO MOVE THIS TO THE TOP!? FUCKING PYTHON PIECE OF SHIT!!!
			import mrdet.mrdet
			self.et.mrdet_client.cmd_ts_add_item(mrdet.mrdet.MRDet.get_data_item(fix, roi))

			# (2.3) Debug:
			if self.DEBUG_LVL == 1:
				dur = int(fix.getEndTime() - fix.getStartTime())
				print("%-24s: %2d %g %g  (%4d ms)" % (roi[self.ROI_IDX_WORD].strip(), roi[self.ROI_IDX_W_LEN], roi[self.ROI_IDX_W_FRQ], roi[self.ROI_IDX_W_PRED], dur))

			if self.DEBUG_LVL == 2:
				dur = int(fix.getEndTime() - fix.getStartTime())
				rr = roi[self.ROI_IDX_RECT_XY]
				print("%s: %d %g %g     [ x: %d %d-%d,  y: %d %d-%d, dur: %d ms ]" % (
					roi[self.ROI_IDX_WORD].strip(), roi[self.ROI_IDX_W_LEN], roi[self.ROI_IDX_W_FRQ], roi[self.ROI_IDX_W_PRED],
					g[0], rr[0][0], rr[1][0], g[1], rr[0][1], rr[1][1], dur)
				)

		super(TextStimulus, self).do_evt_fe(fix)

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_se(self, sacc):
		if self.rois is None: return

		# (1) Find the word being fixated:
		g = sacc.getEndGaze()

		(roi_i, roi) = self.roi_get_at(g)

		# (2) Process the word being fixated:
		if roi is not None:
			# (2.1) Gaze-contingent word-substitution:
			if self.gc is not None:
				self.gc_has_fired = True

				self.gc.do_evt_se(sacc, roi_i)

				# rr = roi[self.ROI_IDX_RECT_WH]
				# self.gc.do_evt_fix(fix, (rr[0], rr[1]), (rr[2], rr[3]), roi[self.ROI_IDX_WORD])

				# if self.em_vis is not None:
				# 	self.em_vis.bg_set(self.gc_bg_img)  # TODO: Test if this works.
				# ^ commented out because it was changing the BG back to the original stimulus while the mask BG should be used

		super(TextStimulus, self).do_evt_se(sacc)

	# ------------------------------------------------------------------------------------------------------------------
	def gc_set(self, gc):
		''' Typically called only once by the constructor. '''

		super(TextStimulus, self).gc_set(gc)

		if (self.gc is not None) and (self.txt is not None) and (self.rois is not None):
			self.gc.stim_set(self, self.bg_img, self.gc_bg_img, self.txt, self.rois[0][self.ROI_IDX_RECT_XY][0], self.char_w, self.ln_h, self.BG_COLOR, self.FONT)

			if self.em_vis is not None:
				self.em_vis.draw()
				self.em_vis.bg_set(gc.mask_img)
				self.em_vis.reset()

				# pygame.image.save(gc.bg_img,   '/Users/Tomek/01-bg.png')
				# pygame.image.save(gc.mask_img, '/Users/Tomek/02-mask.png')
		else:
			self.gc = None  # no point in keeping an object that will not be used due to lack of text on page

		self.display()

	# ------------------------------------------------------------------------------------------------------------------
	def lines_get_cnt(self):
		return len(self.lines)

	# ------------------------------------------------------------------------------------------------------------------
	def lines_get_lst(self, do_keep_rois=True):
		'''
		Returns a sorted list of lines. The ROIs list can be stripped from each line if necessary. The
		reason why one would not want to keep the ROIs is that that makes the list longer and therefore less
		amenable to pickling and transport.
		'''

		lines_sorted = map(lambda k: self.lines[k], sorted(self.lines.keys()))

		if do_keep_rois:
			return lines_sorted

		res = []
		for l in lines_sorted:
			res.append([l[self.LN_IDX_RECT_XY], l[self.LN_IDX_RECT_WH], l[self.LN_IDX_Y_MID]])
		return res

	# ------------------------------------------------------------------------------------------------------------------
	def lines_infer(self):
		''' Infers lines based on ROIs. '''

		if self.rois is None: return {}

		lines = {}
		roi_ln_idx = 0

		for r in self.rois:
			rr = r[self.ROI_IDX_RECT_XY]
			ln_key = r[self.ROI_IDX_LN_ID]

			if ln_key not in lines:  # new line
				ln = []
				# ln.append(map(list, rr))  # LN_IDX_RECT_XY; convert a touple-of-touples to a list-of-lists (which is mutable)
				ln.append(list(map(list, rr)))  # LN_IDX_RECT_XY; convert a touple-of-touples to a list-of-lists (which is mutable)
				ln.append([rr[0][0], rr[0][1], rr[1][0] - rr[0][0], self.ln_h])  # LN_IDX_RECT_WH
				ln.append(rr[0][1] + math.floor(self.ln_h / 2))  # LN_IDX_Y_MID
				ln.append([r])  # LN_IDX_ROIS

				lines[ln_key] = ln

				roi_ln_idx = 0
				r[self.ROI_IDX_LN_IDX] = roi_ln_idx
			else:  # existing line
				ln_r_xy = lines[ln_key][self.LN_IDX_RECT_XY]
				ln_r_xy[0][0] = min(ln_r_xy[0][0], rr[0][0])  # x1
				ln_r_xy[1][0] = max(ln_r_xy[1][0], rr[1][0])  # x2

				ln_r_wh = lines[ln_key][self.LN_IDX_RECT_WH]
				ln_r_wh[0] = min(ln_r_wh[0], rr[0][0])       # x
				ln_r_wh[2] = ln_r_xy[1][0] - ln_r_xy[0][0]   # w

				lines[ln_key][self.LN_IDX_ROIS].append(r)

				roi_ln_idx += 1
				r[self.ROI_IDX_LN_IDX] = roi_ln_idx

		# TODO: DEBUG:
		# for k in sorted(lines.keys()):
		# 	ln_r_xy = lines[k][self.LN_IDX_RECT_XY]
		# 	ln_r_wh = lines[k][self.LN_IDX_RECT_WH]
		# 	ln_y    = lines[k][self.LN_IDX_Y_MID]
		# 	if self.name == "c01p01": print("(%4d, %4d)  y: %4d-%4d  x: %4d-%4d  w: %4d  h: %4d  y-mid: %d" % (k[0], k[1], ln_r_xy[0][1], ln_r_xy[1][1], ln_r_xy[0][0], ln_r_xy[1][0], ln_r_wh[2], ln_r_wh[3], ln_y))

		return lines

	# ------------------------------------------------------------------------------------------------------------------
	def is_point_in_rect(self, p, r):
		'''
		p = (x, y)
		r = ((x1, y1), (x2, y2))
		'''

		return p[0] >= r[0][0] and p[0] <= r[1][0] and p[1] >= r[0][1] and p[1] <= r[1][1]

	# ------------------------------------------------------------------------------------------------------------------
	def roi_get_at(self, xy):
		''' Returns the ROI at the 'xy' coordinates. '''

		# TODO: Optimize by searching for the line first and then within the line.

		for (i, roi) in enumerate(self.rois):
			if self.is_point_in_rect(xy, roi[self.ROI_IDX_RECT_XY]):
				return (i, roi)

		return (-1, None)

	# ------------------------------------------------------------------------------------------------------------------
	def roi_get_line(self, roi):
		''' Returns the line the ROI belongs to. '''

		return self.lines[roi[self.ROI_IDX_LN_ID]]

	# ------------------------------------------------------------------------------------------------------------------
	def roi_get_rect_at(self, roi_i, extend_by):
		'''
		Returns the rectangle encompasing the ROI at the 'roi_i' index extended to the left, right, top and
		bottom as specified by:

			extend_by = (words-to-the-left, words-to-the-right, lines-above, lines-below)

		which corresponds to the 'mask_size' tuple from the GC object.

		Out: ((x, y), (w, h))
		'''

		roi = self.rois[roi_i]
		ln = self.roi_get_line(roi)
		ln_rois = ln[self.LN_IDX_ROIS]

		roi_ln_idx_min = max(roi[self.ROI_IDX_LN_IDX] - extend_by[0], 0)                 # the left-most  ROI still in the mask
		roi_ln_idx_max = min(roi[self.ROI_IDX_LN_IDX] + extend_by[1], len(ln_rois) - 1)  # the right-most ROI still in the mask

		roi_min_xy = ln_rois[roi_ln_idx_min][self.ROI_IDX_RECT_XY]
		roi_max_xy = ln_rois[roi_ln_idx_max][self.ROI_IDX_RECT_XY]

		x = roi_min_xy[0][0]
		y = roi_min_xy[0][1] - (extend_by[2] * self.ln_h)
		w = roi_max_xy[1][0] - roi_min_xy[0][0]
		h = (1 + extend_by[2] + extend_by[3]) * self.ln_h  # 1 because the rect is always at least one line high

		return ((x, y), (w, h))

	# ------------------------------------------------------------------------------------------------------------------
	def rois_load(self):
		''' 'self.ln_h' is not set yet because ROIs need to be read, so we cannot use it here. '''

		path = os.path.join(self.dir_stim, self.name, self.ROI_FNAME)

		if not os.path.exists(path): return None

		with open(path, "r") as f:
			txt = f.read()
			f.close()

		a = [i.split("\t") for i in txt.split("\n")]
		b = [[i[0], i[1].split(",")] for i in a]
		c = [[
			i[0],  # word
			((int(i[1][0]), int(i[1][1])), (int(i[1][2]), int(i[1][3]))),  # rectangle: ((x1,y1), (x2,y2))
			(int(i[1][0]), int(i[1][1]), int(i[1][2]) - int(i[1][0]) + 1, int(i[1][3]) - int(i[1][1]) + 1),  # rectangle: (x1, y1, w, h); +1 to recover the proper dimensions
			len(i[0].strip()),  # word length (each word begins with a space, so we strip it)
			float(0.33),  # word frequency
			float(0.66),  # word predictability
			"unc",  # part of speech
			int(i[1][1]),  # key for 'self.lines'
			-1  # index in the line this roi belongs to (set in 'self.infer_lines()')
		] for i in b]

		return c

	# ------------------------------------------------------------------------------------------------------------------
	def txt_load(self):
		path = os.path.join(self.dir_stim, self.name, self.name + ".txt")

		if not os.path.exists(path): return None

		with open(path, "r") as f:
			txt = f.read()
			f.close()

		return txt
