# Monospace font
#     My ranking
#         1 Anonymous_Pro
#         2 Source_Code_Pro
#         3 Droid_Sans_Mono
#         4 Inconsolata
#         5 Oxygen_Mono
#     Source
#         https://fonts.google.com/?category=Monospace&sort=alpha&selection.family=Anonymous+Pro|Cousine|Droid+Sans+Mono|Fira+Mono|Inconsolata|Overpass+Mono|Oxygen+Mono|PT+Mono|Roboto+Mono|Source+Code+Pro
#
# References
#     http://pillow.readthedocs.io/en/3.1.x/reference
#
# TODO
#     ROIs overlap X-wise -- shorten one of the ROIs
#     Make a script that uses this class
#     Generate images from arbitrary text
#         Book title page
#             EN
#                 Sense and Sensibility\n\nBy Jane Austen
#             SP
#                 Sentido y sensibilidad\n\nPor Jane Austen
#         Chapter title pages
#             EN
#             SP
#         Instructions
#         End-of-chapter questions
#     Every word in the ROI file has a space in front of it. Should that be changed for line-opening words?
#
# asu:  w=1600 h=1200 c=102 l=26 x=400
# pitt: w=1680 h=1050 c=105 l=23 x=600


import os
import re
import shutil
import sys
import textwrap

from PIL import Image, ImageDraw, ImageFont


TXT_CHAPTER_SEP  = "\s*=== C\d\d\s*"
TXT_PG_LN_LEN    = 105
TXT_PG_LN_CNT    = 23

IMG_DIM          = (1680, 1050)
IMG_EXT          = "png"
IMG_BG_COLOR     = (255, 255, 255)
IMG_ROI_COLOR    = (0, 150, 255)
IMG_FONT         = { "family": "Anonymous_Pro", "file": "AnonymousPro-Regular.ttf", "size": 28, "color": (0, 0, 0) }
IMG_LN_SPACE     = 13
IMG_TXT_OFFSET_X = 50
IMG_TXT_OFFSET_Y = 50


os.chdir(os.path.dirname(sys.argv[0]))

PATH_TXT  = os.path.join("..", "assets", "txt", "sense-and-sensibility-en.txt")
PATH_FONT = os.path.join("..", "assets", "font", IMG_FONT.get("family"), IMG_FONT.get("file"))
DIR_OUT   = os.path.join("..", "work", "stim")


# ======================================================================================================================
class TextStimulusGenerator(object):
	ROI_FNAME = "roi.txt"
	ROI_IMG_CNT = 3  # how many images to draw ROIs on to test the validity of the ROI coordinates

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_out, txt_chapter_sep, txt_pg_ln_len, txt_pg_ln_cnt, img_dim, img_ext, img_bg_color, img_roi_color, img_font_path, img_font_size, img_font_color, img_ln_space, img_txt_offset_x, img_txt_offset_y):
		self.dir_out = dir_out

		self.txt_chapter_sep = txt_chapter_sep
		self.txt_pg_ln_len = txt_pg_ln_len
		self.txt_pg_ln_cnt = txt_pg_ln_cnt

		self.txt_chapters = []

		self.img_font = ImageFont.truetype(img_font_path, img_font_size)
		self.img_ch_w = self.img_font.getsize("_")[0]
		self.img_ln_h = self.img_font.getsize("ly")[1]

		self.img_dim = img_dim
		self.img_ext = img_ext
		self.img_bg_color = img_bg_color
		self.img_roi_color = img_roi_color
		self.img_font_color = img_font_color
		self.img_font_size = img_font_size
		self.img_ln_space = img_ln_space
		self.img_txt_offset_y = img_txt_offset_y
		self.img_txt_offset_x = img_txt_offset_x - self.img_ch_w
			# Subtract the width of one character becuase the space should be part of the following word and not the
			# preceding one and as a result we will shift the text one character width left.  If this explanation
			# doesn't make sense, just trust me, it works as intended.

		self.proc_page_cnt = 0

	# ------------------------------------------------------------------------------------------------------------------
	def do_text(self, path_txt):
		if not os.path.exists(self.dir_out): os.makedirs(self.dir_out)
		self.load_text(path_txt)

		for i in range(len(self.txt_chapters)):
			self.do_chapter(i)

		# self.do_chapter(0)

	# ------------------------------------------------------------------------------------------------------------------
	def do_chapter(self, chapter_i):
		c = self.txt_chapters[chapter_i]

		# (1) Split text into lines of the correct length:
		tw = textwrap.TextWrapper(width=self.txt_pg_ln_len, drop_whitespace=True, replace_whitespace=False, break_long_words=False, break_on_hyphens=False)

		paragraphs = map(tw.wrap, c.splitlines())
		lines = []
		for i in paragraphs:
			if len(i) > 0:
				for j in i:
					lines.append(j)
			else:
				lines.append("")

		# (2) Split lines into pages:
		pages = [lines[i:i + self.txt_pg_ln_cnt] for i in range(0, len(lines), self.txt_pg_ln_cnt)]  # list of lists of lines

		# (3) Process pages:
		for page_i, page in enumerate(pages):
			# Remove empty leading lines on a page:
			if len(page[0]) == 0:
				page.pop(0)

			page_name = "c%02dp%02d" % (chapter_i + 1, page_i + 1)
			self.do_page(page_name, page)

	# ------------------------------------------------------------------------------------------------------------------
	def do_page(self, name, lines):
		# (1) Prep the FS:
		dir_page = os.path.join(self.dir_out, name)

		if os.path.exists(dir_page): shutil.rmtree(dir_page)
		os.makedirs(dir_page)

		# (2) Save the text:
		f = open(os.path.join(dir_page, name + ".txt"), "w")
		f.write("\n".join(lines))
		f.close()

		# (3) Generate the image and the ROI file:
		img, rois = self.do_page_lines(lines, None, True, False, True)
		img.save(os.path.join(dir_page, name + "." + self.img_ext))

		f = open(os.path.join(dir_page, self.ROI_FNAME), "w")
		f.write("\n".join(rois))
		f.close()

		if self.proc_page_cnt < self.ROI_IMG_CNT:
			self.do_page_lines(lines, img, False, True, False)
			img.save(os.path.join(dir_page, name + "-rois." + self.img_ext))

		self.proc_page_cnt += 1

	# ------------------------------------------------------------------------------------------------------------------
	def do_page_lines(self, lines, img, do_draw_txt, do_draw_roi, do_gen_roi):
		if img is None:
			img = Image.new("RGBA", self.img_dim, color=self.img_bg_color)

		draw = ImageDraw.Draw(img)
		dt = draw.text
		dr = draw.rectangle
		rois = []

		y = self.img_txt_offset_y
		for ln in lines:
			if len(ln.strip()) == 0:  # skip empty lines
				y += self.img_ln_h + self.img_ln_space
				continue

			x = self.img_txt_offset_x
			for word in ln.split(" "):
				word_w = self.img_ch_w * (len(word) + 1) - 1  # subtract 1 to avoid overlap of adjacent ROIs
				roi = [x, y - (self.img_ln_space // 2), x + word_w, y + self.img_ln_h + (self.img_ln_space // 2)]

				if do_draw_txt:
					dt((x + self.img_ch_w, y), word, font=self.img_font, fill=self.img_font_color)

				if do_draw_roi and len(word) > 0:
					dr(roi, fill=None, outline=self.img_roi_color)

				if do_gen_roi:
					rois.append(" %s\t%d,%d,%d,%d" % (word, roi[0], roi[1], roi[2], roi[3]))

				x += word_w + 1  # add 1 to avoid overlap of adjacent ROIs
			y += self.img_ln_h + self.img_ln_space

		return img, rois

	# ------------------------------------------------------------------------------------------------------------------
	def load_text(self, path):
		with open(path, "r") as f:
			txt = f.read()
			f.close()

		self.txt_chapters = re.split(self.txt_chapter_sep, txt)

		if len(self.txt_chapters[0]) == 0:
			self.txt_chapters = self.txt_chapters[1:]


# ======================================================================================================================
if __name__ == "__main__":
	tp = TextStimulusGenerator(DIR_OUT, TXT_CHAPTER_SEP, TXT_PG_LN_LEN, TXT_PG_LN_CNT, IMG_DIM, IMG_EXT, IMG_BG_COLOR, IMG_ROI_COLOR, PATH_FONT, IMG_FONT.get("size"), IMG_FONT.get("color"), IMG_LN_SPACE, IMG_TXT_OFFSET_X, IMG_TXT_OFFSET_Y)
	tp.do_text(PATH_TXT)
