# Image size: sips -g pixelHeight -g pixelWidth

# import os
# import sys
# from inspect import getsourcefile
#
# __file__ = os.path.abspath(getsourcefile(lambda: None))
# sys.path.append(os.path.dirname(__file__))
# sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import os
import pygame

import stim


# ======================================================================================================================
class ImageStimulus(stim.Stimulus):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_stim, et, name, img_ext, surf, em_vis=None, gc=None):
		self.dir_stim = dir_stim
		self.img_ext = img_ext
		self.dir_self = os.path.join(dir_stim, name)
		self.path_img = os.path.join(self.dir_self, name + "." + img_ext)

		self.bg_img = pygame.image.load(self.path_img).convert()
		# self.bg_img_xy = ((self.surf.get_rect().w - self.img.get_rect().w) // 2, (self.surf.get_rect().h - self.img.get_rect().h) // 2)

		super(ImageStimulus, self).__init__(surf, et, name, em_vis, gc)

	# ------------------------------------------------------------------------------------------------------------------
	def display(self, do_flip=True):
		self.surf.blit(self.bg_img, (0, 0))

		super(ImageStimulus, self).display(do_flip)
