import os
import sys
from inspect import getsourcefile
__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import gc
import os
import pygame

from util import Time


# ======================================================================================================================
class Trial(object):
	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	IMG_FORMAT = "png"

	STIM_SEQ_IDX_NAME     = 0
	STIM_SEQ_IDX_TYPE     = 1
	STIM_SEQ_IDX_CH_NUM   = 2
	STIM_SEQ_IDX_PG_NUM   = 3
	STIM_SEQ_IDX_Q_NUM_CH = 4

	STIM_TYPE_T  = "t"   # text
	STIM_TYPE_QE = "qe"  # end-of-chapter question

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, exp, et, surf, ctx_str, t_dur, allow_nav_back, fn_end, name):
		self.dt_0    = None  # set after the trial has begun and after the setup has completed (therefore equal to 'self.t_setup_end')
		self.ts_0_et = None  # ^

		self.exp = exp  # experiment
		self.et = et    # eye tracker
		self.surf = surf
		self.ctx_str = ctx_str
		self.name = name

		self.allow_nav_back = allow_nav_back
		self.fn_end = fn_end

		self.stim_seq       = []  # stimuli sequence
		self.stim_seq_i     = 0
		self.stim_seq_i_min = 0  # no stimuli before that will be visited with the go-prev navigation

		self.stim = None  # the current stimulus

		self.setup         = []  # setup sequence
		self.setup_i       = 0
		self.is_setup_done = False

		self.t_dur = t_dur     # trial duration [s] (or None)

		self.t_begin     = None  # time when trials begins
		self.t_setup_end = None  # time when setup is done
		self.t_end       = None  # time when trials ends

		self.is_running = False

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if self.is_running:
			self.end()

	# ------------------------------------------------------------------------------------------------------------------
	def begin(self, stim_id, ch_num):
		if self.is_running: return

		self.is_running = True

		# Degenerated trial with duration of 0:
		if self.t_dur == 0:
			self.t_begin = Time.dt()
			self.exp.log_man.put("evt", ["trial-begin", "trial-idx:%d trial-name:%s t-exp:%s" % (self.exp.trial_i, self.name, Time.diffs(self.exp.dt_0, self.t_begin))])
			self.end()
			return

		# Find the first stimulus by id:
		if stim_id is not None:
			self.stim_seq_go_id(stim_id)
			self.stim_seq_go_next(False)  # do not display yet; setup first
			self.stim_seq_i_min = self.stim_seq_i  # TODO: The method called above changes 'stim_seq_i'. Is it possible to use return value here or change 'stim_seq_i' inside?

		# Find the first stimulus by chapter number:
		if ch_num is not None:
			self.stim_seq_go_ch_num(ch_num)
			self.stim_seq_i_min = self.stim_seq_i  # TODO: The method called above changes 'stim_seq_i'. Is it possible to use return value here or change 'stim_seq_i' inside?

		# Begin trial:
		gc.collect()
		gc.disable()

		self.t_begin = Time.dt()
		self.setup_step()

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt(self, e):
		if not self.is_running: return

		key = e.key if e.type == pygame.KEYDOWN else None

		# (1) Processing instructions:
		if not self.is_setup_done:
			if key == pygame.K_RETURN:
				self.setup_step()

		# (2) Processing stimuli:
		else:
			# (2.1) Previous stimulus:
			if (key == pygame.K_LEFT) and (self.allow_nav_back) and (self.stim_seq_is_txt()):
				self.stim_seq_go_prev()

			# (2.2) Next stimulus:
			elif (key == pygame.K_RIGHT) and (self.stim_seq_is_txt()):
				self.stim_seq_go_next()

			# (2.3) Answer to an end-of-chapter question (which always has four options):
			elif (key in [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4]) and (self.stim_seq_is_qe()):
				dt_1 = Time.dt()
				s = self.stim_get()
				ss = self.stim_seq_get()

				ans = key
				ans_correct = int(ss[self.STIM_SEQ_IDX_A_CORR_NUM])
				str_is_ans_corr = ("1") if (ans == ans_correct) else ("0")

				self.exp.log_man.put("evt", ["quest-eoc-ans", "trial-idx:%d trial-name:%s stim-name:%s ch-num:%s ans-correct:%d ans:%d is-ans-correct:%s t-exp:%s t-trial:%s rt:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ans_correct, ans, str_is_ans_corr, Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1))])
				self.exp.log_man.put("quest-eoc", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], str(ans_correct), str(ans), str_is_ans_corr, Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1)])

				self.stim_seq_go_next()

		# if self.DEBUG : print(self.DEBUG_CLS_NAME + "  time-left " + str(self.t_dur - time.time() + self.t_setup_end))
		if self.DEBUG_LVL >= 1 and self.t_dur is not None: print("%s  time-left  %f sec" % (self.__class__.__name__, (self.t_dur - (Time.dt() + self.t_setup_end))))
			# TODO: Does this actually work?

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt_be(self, blink): self.stim_get().do_evt_be(blink)  # forward all eye-movement events to the current stimulus
	def do_evt_bs(self, blink): self.stim_get().do_evt_bs(blink)  # ^
	def do_evt_fe(self, fix):   self.stim_get().do_evt_fe(fix)    # ^
	def do_evt_fs(self, fix):   self.stim_get().do_evt_fs(fix)    # ^
	def do_evt_se(self, sacc):  self.stim_get().do_evt_se(sacc)   # ^
	def do_evt_ss(self, sacc):  self.stim_get().do_evt_ss(sacc)   # ^

	# ------------------------------------------------------------------------------------------------------------------
	def end(self):
		if not self.is_running: return

		if self.t_end is None:
			self.t_end = Time.dt()

			if self.t_setup_end is None:
				self.t_setup_end = self.t_end

			# Log the end-of-trial event and the current trial info:
			self.exp.log_man.put("evt", ["trial-end", "trial-idx:%d trial-name:%s t-exp:%s dur:%s" % (self.exp.trial_i, self.name, Time.diffs(self.exp.dt_0, self.t_end), Time.diffs(self.t_setup_end, self.t_end))])
			self.exp.log_man.put("trial", [self.exp.sub_id, str(self.exp.trial_i), self.name, Time.diffs(self.exp.dt_0, self.t_setup_end), Time.diffs(self.t_setup_end, self.t_end)])

		self.et.trial_end()
		gc.enable()
		self.is_running = False

		if self.fn_end is not None:
			self.fn_end()

	# ------------------------------------------------------------------------------------------------------------------
	def scr_img_disp_file(self, fname):
		if not self.is_running: return

		img = pygame.image.load(os.path.join(self.exp.fs.DIR_ASSETS, "img", self.ctx_str + "-" + fname + "." + self.IMG_FORMAT))
		self.scr_img_disp_img(img)

	# ------------------------------------------------------------------------------------------------------------------
	def scr_img_disp_img(self, img):
		if not self.is_running: return

		self.surf.blit(img, ((self.surf.get_rect().w - img.get_rect().w) // 2, (self.surf.get_rect().h - img.get_rect().h) // 2))
		pygame.display.flip()

	# ------------------------------------------------------------------------------------------------------------------
	def setup_step(self):
		if not self.is_running: return

		self.is_setup_done = (self.setup_i >= len(self.SETUP))
		if not self.is_setup_done:
			self.scr_img_disp_file(self.SETUP[self.setup_i])
			self.setup_i += 1
		else:
			# (1) Log the beginning-of-trial event:
			self.ts_0_et     = self.et.ts()
			self.dt_0        = Time.dt()
			self.t_setup_end = self.dt_0

			self.exp.log_man.put("evt", ["trial-begin", "trial-idx:%d trial-name:%s t-exp:%s" % (self.exp.trial_i, self.name, Time.diffs(self.exp.dt_0, self.t_setup_end))])

			# (2) Display the stimulus and begin the trial:
			self.stim_seq_disp()
			self.et.trial_begin(self, self.name, 100, 100)  # TODO: Get good values for drift_corr coords

	# ------------------------------------------------------------------------------------------------------------------
	def stim_get(self):
		'''
		Return the current stimulus (not stimulus sequence entry, but which stimulus is loaded is based on the current
		stimulus sequence entry. Also stores that stimulus in 'self.stim' for later use (e.g., checking if the
		stimulus has changed upon this method being called again).
		'''

		ss = self.stim_seq_get()
		if self.stim is None or self.stim.name != ss[self.STIM_SEQ_IDX_NAME]:
			self.stim = self.exp.stim_get(self.stim_seq_get())

		return self.stim

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_disp(self):
		if not self.is_running: return

		s = self.stim_get()

		# (1) Log the first display event:
		if s.has_been_disp:
			dt_1 = Time.dt()
			ss = self.stim_seq_get()
			self.exp.log_man.put("evt", ["stim-show", "trial-idx:%d trial-name:%s stim-name:%s stim-type:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_TYPE], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1))])

		# (2) Display:
		s.display()

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_get_id(self):
		if len(self.stim_seq) == 0 or self.stim_seq_i > len(self.stim_seq) - 1:
			return None
		else:
			return self.stim_seq_get()[self.STIM_SEQ_IDX_NAME]

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_get(self):
		return (self.stim_seq[self.stim_seq_i]) if (len(self.stim_seq) > 0) else (None)

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_go_ch_num(self, num):
		for i in range(len(self.stim_seq)):
			if int(self.stim_seq[i][self.STIM_SEQ_IDX_CH_NUM]) == num:
				self.stim_seq_i = i
				if self.DEBUG_LVL >= 1: print("%s  stim-go-ch-num  %d  %s  %s" % (self.__class__.__name__, self.stim_seq_i, self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE], self.stim_seq_get()[self.STIM_SEQ_IDX_NAME]))
				return

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_go_id(self, id):
		for i in range(len(self.stim_seq)):
			if self.stim_seq[i][self.STIM_SEQ_IDX_NAME] == id:
				self.stim_seq_i = i
				if self.DEBUG_LVL >= 1: print("%s  stim-go-id  %d  %s  %s" % (self.__class__.__name__, self.stim_seq_i, self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE], self.stim_seq_get()[self.STIM_SEQ_IDX_NAME]))

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_go_prev(self):
		if not self.is_running or self.stim_seq_i == self.stim_seq_i_min: return

		# (1) Log:
		dt_1 = Time.dt()
		s = self.stim_get()
		ss = self.stim_seq_get()

		# (1.1) Log the next-stimulus-navigation event:
		self.exp.log_man.put("evt", ["stim-prev", "trial-idx:%d trial-name:%s stim-name:%s stim-type:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s dur:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_TYPE], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1))])

		# (1.2) Log the current stimulus info:
		self.exp.log_man.put("stim", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_TYPE], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, s.dt_0), Time.diffs(self.dt_0, s.dt_0), Time.diffs(s.dt_0, dt_1)])

		# (2) Navigate to the previous stimulus:
		self.stim_seq_i -= 1
		self.stim_seq_disp()

		if self.DEBUG_LVL >= 1: print("%s  stim-go-prev  %d  %s  %s" % (self.__class__.__name__, self.stim_seq_i, self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE], self.stim_seq_get()[self.STIM_SEQ_IDX_NAME]))

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_go_next(self, do_disp=True):
		if not self.is_running: return

		# (1) Compute trial-end conditions:
		stimuli_are_exhausted = (self.stim_seq_i >= len(self.stim_seq) - 1)
		# time_is_over = ((self.t_dur - time.time() + self.t_setup_end) <= 0) if self.t_dur is not None else False
		time_is_over = ((Time.diff(self.t_setup_end, Time.dt())) <= self.t_dur) if self.t_dur is not None else False

		ch_curr = self.stim_seq[self.stim_seq_i][self.STIM_SEQ_IDX_CH_NUM]
		ch_next = (self.stim_seq[self.stim_seq_i + 1][self.STIM_SEQ_IDX_CH_NUM]) if (not stimuli_are_exhausted) else (-1)

		chapter_is_finished = (ch_next != ch_curr)

		# (2) End trial:
		if stimuli_are_exhausted or (time_is_over and chapter_is_finished):
			self.end()
			if self.fn_end is not None:
				self.fn_end()
				return
			else:
				return

		# (3) Continue iterating through stimuli:
		# (3.1) Log:
		if do_disp:
			dt_1 = Time.dt()
			s = self.stim_get()
			ss = self.stim_seq_get()

			# Log the stimulus-next-navigation event:
			self.exp.log_man.put("evt", ["stim-next", "trial-idx:%d trial-name:%s stim-name:%s stim-type:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s dur:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_TYPE], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1))])

			# Log the current stimulus info:
			self.exp.log_man.put("stim", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_TYPE], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, s.dt_0), Time.diffs(self.dt_0, s.dt_0), Time.diffs(s.dt_0, dt_1)])

		# (3.2) Navigate to the next stimulus:
		self.stim_seq_i += 1
		gc.collect()

		if do_disp:
			self.stim_seq_disp()
			if (self.et is not None) and (self.is_setup_done):
				self.et.on_after_stim_disp()

		if self.DEBUG_LVL >= 1: print("%s  stim-go-next  %d  %s  %s" % (self.__class__.__name__, self.stim_seq_i, self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE], self.stim_seq_get()[self.STIM_SEQ_IDX_NAME]))

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_is_qe(self):
		return self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE] == self.STIM_TYPE_QE

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_is_txt(self):
		return self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE] == self.STIM_TYPE_T
