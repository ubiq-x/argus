import os
import sys
from inspect import getsourcefile
__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import csv
import os
import pygame

import trial

from util import Time


# ======================================================================================================================
class Trial_1(trial.Trial):
	STIM_SEQ_IDX_Q_NUM_PG   = 5
	STIM_SEQ_IDX_Q_ID       = 6
	STIM_SEQ_IDX_A_CNT      = 7
	STIM_SEQ_IDX_A_ORD      = 8
	STIM_SEQ_IDX_A_CORR_NUM = 9
	STIM_SEQ_IDX_Q_WORD_A   = 10
	STIM_SEQ_IDX_Q_WORD_B   = 11

	STIM_TYPE_QI = "q"  # in-line question

	SETUP = ["instr-t1-a", "instr-t1-b"]

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, exp, et, scr, lang, t_dur, allow_nav_back, fn_end, name):
		super(Trial_1, self).__init__(exp, et, scr, lang, t_dur, allow_nav_back, fn_end, name)

		self.stim_seq = list(csv.reader(open(os.path.join(self.exp.fs.DIR_STIM, "stim-seq-t1.txt"), "r"), delimiter="\t"))
			# TODO: Parse to recover integers etc. (must update logging afterwards because right now strings are expected)

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt(self, e):
		super(Trial_1, self).do_evt(e)

		key = e.key if e.type == pygame.KEYDOWN else None
		if key in [pygame.K_a, pygame.K_b, pygame.K_c] and self.stim_seq_is_qi():
			# ... chr(key)
			self.stim_seq_go_next()
		elif key in [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4] and self.stim_seq_is_qi():
			ans = key - 48
			if ans <= int(self.stim_seq_get()[self.STIM_SEQ_IDX_A_CNT]):
				dt_1 = Time.dt()
				s = self.stim_get()
				ss = self.stim_seq_get()

				ans = key - 48
				ans_correct = int(ss[self.STIM_SEQ_IDX_A_CORR_NUM])
				str_is_ans_corr = ("1") if (ans == ans_correct) else ("0")

				self.exp.log_man.put("evt", ["quest-wc-ans", "trial-idx:%d trial-name:%s stim-name:%s ch-num:%s ans-correct:%d ans:%d is-ans-correct:%d t-exp:%s t-trial:%s rt:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], str(ans_correct), str(ans), str_is_ans_corr, Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1))])
				self.exp.log_man.put("quest-wc", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], str(ans_correct), str(ans), str_is_ans_corr, Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1)])

				self.stim_seq_go_next()

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_is_qi(self):
		return self.stim_seq_get()[self.STIM_SEQ_IDX_TYPE] == self.STIM_TYPE_QI
