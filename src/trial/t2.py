# TODO
#     The resonse to the probe is slow (i.e., probe stays on screen for too long). What is causing that?
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile
__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import csv
import os
import time

import pygame

# import sub.client
import trial

from timer.probe import ProbeTimer
from util        import Time


# ======================================================================================================================
class Trial_2(trial.Trial):
	STIM_SEQ_IDX_A_CORR_NUM = 5

	# PROBE_T_MIN =  2  # [s]
	# PROBE_T_MAX =  2  # [s]

	# PROBE_T_MIN =  5  # [s]
	# PROBE_T_MAX = 10  # [s]

	PROBE_T_MIN = 10000000000000  # [s]
	PROBE_T_MAX = 30000000000000  # [s]

	PROBE_SELF_REPORT_RESTART_T = PROBE_T_MIN  # time that the probe timer will be restarted with if a self-report happens [s]

	SETUP = ["instr-t2"]

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, exp, et, scr, lang, t_dur, allow_nav_back, fn_end, name):
		super(Trial_2, self).__init__(exp, et, scr, lang, t_dur, allow_nav_back, fn_end, name)

		self.stim_seq = list(csv.reader(open(os.path.join(self.exp.fs.DIR_STIM, "stim-seq-t2.txt"), "r"), delimiter="\t"))
			# TODO: Parse to recover integers etc. (must update logging afterwards because right now strings are expected)

		self.probe_img = pygame.image.load(os.path.join(self.exp.fs.DIR_ASSETS, "img", self.ctx_str + "-zo-probe.png"))
		self.probe_timer = ProbeTimer(self.PROBE_T_MIN, self.PROBE_T_MAX, self.probe_disp)
		self.probe_t_disp = None  # display time
		self.probe_t_resp = None  # response time
		self.probe_do_resume_sub = False  # should the subject be resumed after the probe? (this should happen only if the subject wasn't paused at the time of the probe presentation)
		self.probe_stim_img_buffer = None  # a buffer to store the stimulus while it's replaced by a probe (the stimulus is recalled from the buffer after the probe has been responded to)

		self.is_probe = False

		# X1:
		# if self.et.emu_sub_id is not None:
		# 	self.emu_sub_client = sub_client.SubjectClient(self.et.EMU_SUB_SERVER_HOST, self.et.EMU_SUB_SERVER_PORT)
		# 	# self.emu_sub_client.connect_load(self.et.emu_sub_id)
		# else:
		# 	self.emu_sub_client = None

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.end()
		super(Trial_2, self).__del__()

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt(self, e):
		if (e.type != pygame.KEYDOWN): return

		key = e.key

		# Waiting for response to probe:
		if self.is_probe:
			if key in [pygame.K_n, pygame.K_y]:
				self.probe_handle_resp(key)
			return  # do not handle the event further until the probe is responded to

		# Normal execution:
		super(Trial_2, self).do_evt(e)

		if key == pygame.K_z:
			self.do_self_report()

	# ------------------------------------------------------------------------------------------------------------------
	def do_self_report(self):
		# (1) Log event:
		dt_1 = Time.dt()
		s = self.stim_get()
		ss = self.stim_seq_get()

		self.exp.log_man.put("evt", ["read-self-mindless", "trial_idx:%d trial-name:%s stim-name:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s t-stim:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1))])
		self.exp.log_man.put("read", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], "self-mindless", Time.diffs(self.exp.dt_0, dt_1), Time.diffs(self.t_setup_end, dt_1), Time.diffs(s.dt_0, dt_1), "0"])

		# (2) Delay the upcoming probe:
		self.probe_timer.start_timeout(self.PROBE_SELF_REPORT_RESTART_T, True)

		# (3) Print:
		if self.DEBUG_LVL >= 1: print("Self-report")

	# ------------------------------------------------------------------------------------------------------------------
	def end(self):
		super(Trial_2, self).end()
		self.probe_timer.cancel()

		# X1:
		# if self.emu_sub_client is not None:
		# 	self.emu_sub_client.disconnect()

	# ------------------------------------------------------------------------------------------------------------------
	def probe_disp(self):
		# (1) Display the probe:
		print(1)
		self.et.do_proc_em_set(False)
		time.sleep(0.1)
		print(2)

		if self.et.emu_sub_id is not None:
			print(3)
			self.probe_do_resume_sub = not self.et.emu_sub_client.is_paused()
			print(4)
			self.et.emu_sub_client.pause()
			print(5)

		# X1:
		# if self.emu_sub_client is not None:
		# 	self.et.do_proc_em_set(False)
		# 	self.probe_do_resume_sub = not self.emu_sub_client.is_paused()
		# 	print(self.probe_do_resume_sub)
		# 	self.emu_sub_client.pause()
		# 	if self.probe_do_resume_sub:
		# 		self.emu_sub_client.pause()

		self.probe_stim_img_buffer = pygame.image.tostring(self.surf, "RGB")  # save the current surface till after the probe
		print(6)
		self.scr_img_disp_img(self.probe_img)  # show the probe
		print(7)
		self.is_probe = True
		self.probe_t_disp = Time.dt()
		print(8)

		# (2) Log event:
		s = self.stim_get()
		ss = self.stim_seq_get()
		print(9)
		self.exp.log_man.put("evt", ["read-probe-disp", "trial_idx:%d trial-name:%s stim-name:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s t-stim:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, self.probe_t_disp), Time.diffs(self.t_setup_end, self.probe_t_disp), Time.diffs(s.dt_0, self.probe_t_disp))])
		print(10)

	# ------------------------------------------------------------------------------------------------------------------
	def probe_handle_resp(self, key):
		self.probe_t_resp = Time.dt()

		probe_resp = chr(key).lower()

		s = self.stim_get()
		ss = self.stim_seq_get()

		if probe_resp == "n":
			self.exp.log_man.put("evt", ["read-probe-resp-normal", "trial_idx:%d trial-name:%s stim-name:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s t-stim:%s rt:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, self.probe_t_resp), Time.diffs(self.t_setup_end, self.probe_t_resp), Time.diffs(s.dt_0, self.probe_t_resp), Time.diffs(self.probe_t_disp, self.probe_t_resp))])
			self.exp.log_man.put("read", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], "probe-normal", Time.diffs(self.exp.dt_0, self.probe_t_disp), Time.diffs(self.t_setup_end, self.probe_t_disp), Time.diffs(s.dt_0, self.probe_t_disp), Time.diffs(self.probe_t_disp, self.probe_t_resp)])
		else:
			self.exp.log_man.put("evt", ["read-probe-resp-mindless", "trial_idx:%d trial-name:%s stim-name:%s ch-num:%s pg-num:%s t-exp:%s t-trial:%s t-stim:%s rt:%s" % (self.exp.trial_i, self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], Time.diffs(self.exp.dt_0, self.probe_t_resp), Time.diffs(self.t_setup_end, self.probe_t_resp), Time.diffs(s.dt_0, self.probe_t_resp), Time.diffs(self.probe_t_disp, self.probe_t_resp))])
			self.exp.log_man.put("read", [self.exp.sub_id, str(self.exp.trial_i), self.name, ss[self.STIM_SEQ_IDX_NAME], ss[self.STIM_SEQ_IDX_CH_NUM], ss[self.STIM_SEQ_IDX_PG_NUM], "probe-mindless", Time.diffs(self.exp.dt_0, self.probe_t_disp), Time.diffs(self.t_setup_end, self.probe_t_disp), Time.diffs(s.dt_0, self.probe_t_disp), Time.diffs(self.probe_t_disp, self.probe_t_resp)])

		if self.DEBUG_LVL >= 1: print("Probe response: %s (%s ms)" % (chr(key), Time.diffs(self.probe_t_disp, self.probe_t_resp)))

		# time.sleep(0.25)
			# TODO: The program doesn't seem to cancel the probe timer properly (or starts another timer) if the probe
			#       is responded to too quickly. This is an attempted quick fix until I figure this one out.
			#
			#       [2017.06.29] The error doesn't seem exist any more so I've commented sleep for now.

		self.is_probe = False

		# self.stim_seq_disp()
		# self.surf.frombuffer(self.probe_stim_img_buffer, self.surf.get_size(), "RGB")  # restore the surface from before the probe
		self.surf.blit(pygame.image.frombuffer(self.probe_stim_img_buffer, self.surf.get_size(), "RGB"), (0, 0))  # restore the surface from before the probe
		pygame.display.update()

		# Text stimulus -- make sure the probe timers is running (start it if it isn't):
		if self.stim_seq_is_txt():
			if not self.probe_timer.is_started:
				self.probe_timer.start()
		# Non-text stimulus -- cancel the potential probe timer:
		else:
			self.probe_timer.cancel()

		# Resume the subject if it has been paused by 'self.probe_disp()'
		self.et.do_proc_em_set(True)
		if self.probe_do_resume_sub:
			self.et.emu_sub_client.resume()
		# X1:
		# if self.probe_do_resume_sub:
		# 	self.emu_sub_client.resume()

	# ------------------------------------------------------------------------------------------------------------------
	def stim_seq_disp(self):
		super(Trial_2, self).stim_seq_disp()

		# Text stimulus -- make sure the probe timers is running (start it if it isn't):
		if self.stim_seq_is_txt():
			if not self.probe_timer.is_started:
				self.probe_timer.start()
		# Non-text stimulus -- cancel the potential probe timer:
		else:
			self.probe_timer.cancel()
