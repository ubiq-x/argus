# ----------------------------------------------------------------------------------------------------------------------
#
# Copyright (c) 2017-2018 Tomek D. Loboda (tomek.loboda@gmail.com)
#
# All right reserved. The use of all the code associated with this project is prohibited unless an explicit permission
# has been granted by the author.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     NEXT
#         Move to str.format() -- https://pyformat.info
#         Move to Python 3
#             docs.python.org/2/library/2to3.html
#         Add enumarations
#             from enum import Enum, auto
#             >>> class Color(Enum):
#             ...     red = auto()
#             ...     blue = auto()
#             ...     green = auto()
#             ...
#             >>> list(Color)
#             [<Color.red: 1>, <Color.blue: 2>, <Color.green: 3>]
#         Machine learning
#             www.deeplearning.net/tutorial/contents.html
#         Discover POS from text (using www.nltk.org)
#         py.path
#         github.com/habnabit/passacre
#         Tell stimulus when to start and stop discarding eye-movements when probe is displayed: Trial_2.probe_disp()
#         Make sure the probe is correctly stopping gaze-contingent display redraw
#         Use pylink.currentTime()
#             Add a utility method to the EyeTracker class to get the time. Return pylink.currentTime() if connected, and Util.ts() if not
#         Mask isn't applied if fixation is made outside of text - fix dat
#         Generate and cache images (TextStimulus class)
#         Live experiment
#             [+] Make everything work in mouse simulation mode
#             Test with actual eye-movements
#                 Calibration
#                 Drift correction
#                     Display fixation cross (for drift correction) in the top-left corner (start of text)
#             Check EDF file to make sure it's correctly created
#             Embed messages in the EDF file
#             Get good values for drift_corr coords: Trial.setup_step()
#         ExperimentLog class
#
#     LATER
#         Show info that we are transfering EDF files because it may take a while: EyeTracker.stop()
#         Add the end-of-experiment screen after all trials are done and wait for F10 to end the expriment
#         Change setup into a list of commands (then calibration can be added there easily)
#
#     DEV
#         MacOS and Windows Python differences
#         Private members and methods in Python
#         Check naming convensions for: function, variable, class, method, method arguments, members
#         Is it possible to set random.seed() inside the Subject object only for localized reproducability?
#         Add license information
#
#     www.sr-support.com/forum/frequently-asked-questions/eyelink-host-pc-and-hardware-faq/5173-answers-to-eyelink-host-pc-and-hardware-faq
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Milestones (environment)
#     Development started (Python 2.7; MacOS)                                             [2017.04.10]
#     Ported to Python 3                                                                  [2017.08.11]
#     Ported to FreeBSD (MRDet app)                                                       []
#     Ported to Windows (Experiment app)                                                  []
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Milestones (code)
#     Setup the development environment for Python                                        [2017.04.10]
#     Reproduce ASU-Pitt 2014 grant Experiment 1 in pygame                                [2017.04.10 - 2017.04.18]
#
#     TextStimulusGenerator class
#         Split text into pages                                                           [2017.05.04 - 2017.05.05]
#         Generate images and ROIs from text                                              [2017.05.04 - 2017.05.05]
#         Generate images from arbitrary text                                             [2017.05.05 - ...]
#
#     App class                                                                           [2017.06.14]
#
#     Experiment class
#         Automatic upload of all collected files (e.g., EDF, EED, etc.) at finish        []
#         Experimental condition selection at start                                       []
#
#     FileSystem class                                                                    [2017.08.28]
#
#     EyeTracker class
#         Simulation mode (no simulated eye movements; MODE_SIM)                          [2017.04.21]
#         Live mode (connectivity only, i.e., ignore eye movements; MODE_LIVE)            [2017.04.21 - 2017.04.26]
#         Live mode (process eye movements; MODE_LIVE)                                    [2017.04.25 - ...]
#         Live mode (mouse-simulated eye movements; MODE_LIVE_MOUSE)                      [2017.04.25 - 2017.04.27]
#         Live mode (EDF file playback; MODE_LIVE_EDF)                                    [2017.04.25 - 2017.05.02; impossible]
#         Live mode (record eye movements to EED file; {MODE_LIVE, MODE_EDF})             []
#         Elumation mode (mouse-simulated eye movements; MODE_EMU_MOUSE)                  []
#         Elumation mode (randomly generated eye movements; MODE_EMU_RAND)                []
#         Elumation mode (randomly generated reading-like eye movements; MODE_EMU_READ)   []
#         Elumation mode (eye movements read from EDF file; MODE_EMU_EDF)                 []
#         Elumation mode (eye movements read from ASC file; MODE_EMU_ASC)                 []
#         Elumation mode (eye movements read from EED file; MODE_EMU_FILE)                []
#         Locally perform EDF-2-EED / ASC-2-EED file conversion                           []
#         Visualize eye movements
#             All fixations and saccades (VIS_MODE_ALL)                                   [2017.05.03 - ...]
#             Only the current fixation (VIS_MODE_ONE)                                    [2017.05.03 - ...]
#
#     TCP server and TCP client classes (and subclasses)
#         Base server and client classes                                                  [2017.04.28 - 2017.07.22]
#         Subject
#             SubjectServer class                                                         [2017.04.28 - 2017.06.15]
#                 Session management
#                     Store subject in the session                                        [2017.06.15]
#             SubjectClient class                                                         [2017.04.28 - 2017.06.15]
#                 Measure connection speed                                                []
#         Experiment
#             ExperimentServer class                                                      []
#                 Initialize new subject directory                                        []
#                 Recieve all experiment files (e.g., logs, reports, EDFs)                []
#             ExperimentClient class                                                      []
#                 Measure connection speed                                                []
#         Remote servers manager
#             Mindless reading detection server manager                                   []
#             Experiment                                                                  []
#
#     Multiprocessing server and client classes (and subclasses)
#         Mindless reading detector
#             MRDetServer class                                                           [2017.07.14 - ...]
#                 ...                                                                     []
#             MRDetClient class                                                           [2017.07.14 - ...]
#                 Measure connection speed                                                []
#
#     Subject class (and subclasses)
#         Base class                                                                      [2017.04.28 - 2017.05.17]
#         File-based
#             EDF file                                                                    []
#             EED file                                                                    []
#                 Check eye movement assignment to ROIs with known EM coords              []
#         Uniform
#             Random (SUB_TYPE_RAND)                                                      [2017.04.28 - 2017.05.04]
#             Random walk (SUB_TYPE_RAND_WALK)                                            [2017.04.28 - 2017.05.04]
#         Gamma
#             Page reader (SUB_TYPE_READ_PAGE)                                            [2017.05.03 - 2017.05.04]
#             Line reader (SUB_TYPE_READ_LINE)                                            [2017.05.03 - 2017.06.14]
#             Word reader (SUB_TYPE_READ_WORD)                                            []
#             ...                                                                         []
#
#     Stimulus class (and subclasses)
#         Base class                                                                      [2017.05.05 - 2017.05.19]
#         ImageStimulus class                                                             [2017.05.05 - 2017.05.19]
#         TextStimulus class                                                              [2017.05.05 - ...]
#             Generate and cache images                                                   []
#             Gaze-contingent paradigm
#                 Identify the word being fixated and access it's variables               [2017.05.05 - 2017.05.08]
#                 Page reading summary
#                     Compute reading variables as reading progresses                     []
#                     Persist on page turn                                                []
#                         Handle previous-page traversal                                  []
#
#     GC class
#         Masking
#             Foveal mask                                                                 [2017.05.24 - 2017.06.14]
#             Parafoveal mask (mask inversion)                                            [2017.05.24 - 2017.06.14]
#             Mask size
#                 Character-based                                                         [2017.05.30 - 2017.06.14]
#                 Word-based                                                              [2017.06.04 - 2017.06.14]
#         Substitution
#             Character-level
#                 Single character                                                        [2017.05.24 - 2017.06.04]
#                 Random character                                                        [2017.05.24 - 2017.06.04]
#                 Matching charcater                                                      [2017.05.24 - 2017.06.04]
#             Word-level
#                 Meaningful word                                                         [2017.05.24 - 2017.06.14]
#         Saving text (and image) actually presented                                      []
#
#     Utility classes
#         FS                                                                              [2017.08.14 - 2017.10.20]
#         Size                                                                            [2017.08.14]
#         Str                                                                             [2017.06.21]
#         Time                                                                            [2017.06.20 - 2017.06.21]
#
#     LogMan and Log classes                                                              [2017.06.15 - 2017.06.21]
#
#     PsycholingMan class
#         Word frequency
#             Generate the SUBTLEX-US database (based on the text file)                   [2017.06.04 - 2017.06.10]
#             Create queries to retrieve words based on conditions (e.g., frq matching)   [2017.06.06 - 2017.06.12]
#             Optimize to make real-time usage possible                                   [2017.06.13 - 2017.06.14]
#         Word predictability
#             ...
#
#     ExperimentLog class
#         On-line processing
#             Save eye movements, words they occured on, and events in real time          []
#         Off-line processing
#             Convert saved data into a cohesive entity                                   []
#             Convert that entity into a Pegasus database import script                   []
#             Auto-run off-line processing at the end of experiment                       []
#
#     MRDet class
#         Implement decision process                                                      [2017.07.20 - ...]
#         Connect with MRDetModel class                                                   []
#         Test off-line on made-up data sets                                              []
#         Test on-line on made-up data sets                                               []
#         Use simulated eye-movements (recorded and mouse) in on-line mode                []
#         Use live eye-movements in on-line mode                                          []
#         Attention-and-gaze-contingent (gaze-tenntion) paradigm                          []
#             ...
#
#     NN package
#         Filesystem class                                                                [2017.09.19 - ...]
#         Dataset class                                                                   [2017.08.10 - ...]
#         DB class                                                                        [2017.08.20 - ...]
#         DBAnalysis class                                                                [2017.09.26 - ...]
#         HyperParameterOptimizerGrid class                                               [2017.08.27 - ...]
#         ExternalEvaluator class                                                         [2017.10.14 - ...]
#         Model classes
#             Model                                                                       [2017.08.28 - ...]
#             ModelLogReg                                                                 [2017.09.04 - ...]
#             ModelFC                                                                     [2017.08.28 - ...]
#             Model...                                                                    []
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Experiments: Human subject
#
#     1. Probe hit ratio (PHR) baseline
#        Run 10 subjects with probes shown every 2-4 minutes, but with no instruction to self-report.  I'd expect to
#        discover a higher PHR than that observed in prior experiments because (a) probes aren't going to be delayed by
#        self-reports and (b) subjects aren't going to be as self-aware.  Mindless reading detection experiments
#        coming later on will only rely on probes (i.e., no longer on self-reports) and therefore establishing the
#        baseline PHR is critical in discovering if mindless reading detection provides an improvement over random
#        sampling.
#
#     2. More frequent probes
#        With no requirement to self-report, waiting for 2-4 minutes between probes may be too long in terms of
#        sampling mindless reading.  Subject are likely to lapse into reading mindlessly easier than when they're
#        asked to self-report and the increased probe frequency would attempt to disover those possibly more frequent
#        bouts of mind-wandering.  This experiment, therefore, would also help to establish a baseling PHR.
#        Additionally, it would help to understand if bounts of mindless reading should be expected to happen more
#        frequently than with the incidence established by Experiment 1.
#
#        There are distributional reasons for which this experiment should be run (see Loboda, 2014, PhD Dissertation).
#
#     3. Attention-contingent paradigm
#        The goal of this first attention-contingent (AC) reading experiment will be to evaluate the first version
#        of a mindless reading detector.  The main measure of performance will be probe hit ratio (PHR).  That ratio
#        will be compared with the baseline established in Experiments 1 and 2.  A good detector will perform on an
#        above-chance level.  There will be no need to address any theoretical questions related to reading and
#        mindless reading at this stage.
#
#        This experiment may be broken down into several sub-experiments, each of which would evaluate a different
#        model at the heart of the mindless reading detector.  The testing of several candidate mindless reading
#        detectors might proceed seemlessly as the subject reads.  That is, the experimental software will
#        automatically move to the next model once it has obtained a sufficient number of samples to calculate PHR for
#        the current model.
#
#        It is imperative to hold-off with gaze-contingent (GC) display changes until making sure the detector is
#        sensitive enough to sample the actual mindless reading.  However, it would be prudent to have a simple GC
#        text change ready for the very end of the experiment.  If unnoticed by the subjects, such change would
#        provide a compeling proof for on-line mindless reading detection.
#
#     4. Attention-and-gaze-contingent (AGC) paradigm
#        This experiment will perform gaze-contingent (GC) text substitutions, but only when subjects are reading
#        mindlessly.  Experiment 3 will need to be a success in order for us to be confident about deploying the
#        attention-contingent (AC) paradigm and combining it with the GC paradigm.
#
#        At this stage we will be certain that the mindless reading detector is sensitive enough so probes will no
#        longer be needed (other than as descibed in the following paragraph).  Therefore, upon the subject lapsing
#        into mindless reading, instead of launching probes we will administer a GC change to test one or more
#        theoretical questions.  We will iron out the details later, but just as an example, while mind-wandering
#        subject will "read" random words (matched only on length).
#
#        This and all subsequent experiemnts will be a further evaluation of the mindless reading detection model(s) in
#        that probe hit ratios (PHRs) can still be compared against the baseline (Experiments 1 and 2) and the ratios
#        established later (Experiment 3).  In fact, each experiment could begin with an AC-only paradigm to
#        measure the PHR and later transition into adding the GC component.  To automate this process, the experimental
#        software could remain within the AC-only part of the experiment for as long as a certain threshold of PHR has
#        not been reached; it would only transition to the AGC part once it has established that the mindless reading
#        detection works as expected.  As a bonus, this may be a good security valve for cases of subjects with
#        hard-to-pinpoint attention focus (i.e., on or off task).  To take this even further, the experimental
#        software could go through a sequence of alternative detection models in an attempt to discover the one that
#        works best with each subject before it decided which one to use in the AC+GC part of the experiment.
#
#     5. Reading with a fovea but without attention
#        A remake of a classic experiment (Rayner & Bertera, 1979).  An AGC text change will mask text being read to
#        establish effects of mindless reading on foveal and parafoveal processing.  It is possible that reading
#        without attention will have similar effect to reading without a fovea, i.e., nothing will be comprehended
#        as if the text was masked as done in the 1979 experiment.
#
#        This experiment will be done only by Erik and myself and must naturally go to Science :-)
#
#     6. Successor effect in ecologically valid reading
#        An AGC text change will be made after the subject lapses into mindless reading.  This change will modify word
#        n+1 by showing another word (or by obliterating it entirely).  We will be able to choose between matching the
#        word on length, frequency, part-of-speech, or any other variables we will have access to at that point.
#        The specifics will be determined later.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Experiments: Computational
#
#     1. Deep reader
#        This experiment will build a computational model of reading based on deep neural networks.  The model will be
#        trained end-to-end on eye-movement reading record from human subject experiments.  In order to achieve that,
#        the model will contain a feature extraction convolutional neaural network (CNN) that will interface the
#        textual stimulus and a recurrent neural network (RNN) that will learn the sequence of saccade-fixation pairs.
#        Eye movements to be accounted for later on include blinks and pupil diameter.
#
#        Another potentially fruitful extensions will use the analog signal (e.g., gaze position samples) instead of
#        saccades and fixations stream to train the model.  Despite having samples as the input, the output could still
#        be expressed in terms of saccades and fixations -- the model could output gaze coordinates along with the
#        time it would keep the gaze unmoved.
#
#        Details
#            Feature extraction
#                Hand-crafted
#                    At first, there will be no feature learning.  All features of the textual stimulus will be
#                    provided to the model along with eye movements.  This will test the RNN.
#                CNN-A
#                    This network will be pre-trained on images of pages of text.  The first attempt will use images
#                    with identical dimentions and resolution and identical font metrics.  One obvious extension would
#                    be to allow the network to learn textual stimulus features independently of those variables.
#            Reading
#                RNN-A
#                    ...
#
#        Relavant work
#            Saccade gaze prediction using a recurrent neural network
#                vision.ece.ucsb.edu/sites/vision.ece.ucsb.edu/files/publications/2017_icip_thuyen.pdf
#            Automatic scanpath generation with deep recurrent neural networks
#                www.researchgate.net/publication/304533420_Automatic_scanpath_generation_with_deep_recurrent_neural_networks
#                dl.acm.org/citation.cfm?id=2948726
#            Benchmarking n-grams, Topic Models and Recurrent Neural Networks by Cloze Completions, EEGs and Eye Movements
#                www.researchgate.net/publication/314190083_Benchmarking_n-grams_Topic_Models_and_Recurrent_Neural_Networks_by_Cloze_Completions_EEGs_and_Eye_Movements
#            End-to-End Eye Movement Detection Using Convolutional Neural Networks
#                arxiv.org/abs/1609.02452
#                www.researchgate.net/publication/307930552_End-to-End_Eye_Movement_Detection_Using_Convolutional_Neural_Networks
#            Using machine learning to detect events in eye-tracking data
#                www.researchgate.net/publication/313903771_Using_machine_learning_to_detect_events_in_eye-tracking_data
#
#     2. Deep eye tracking reading filter
#        The precision of eye trackers is bound by a non-zero spacial error.  That makes it problematic for eye
#        tracking studies that employ high resolution stimuli such as text.  In text, the proximity of both adjacent
#        words and adjacent lines make it likely for the eye tracking system to misclassify gaze location as
#        "belonging" to the wrong word or line.
#
#        This proposed experiment will attempt to ameliorate those issues by selecting the most likely line of text
#        given the recorded eye movements.  A recurrent neural network (RNN) will be at the heart of this software.
#        The network will be trained on sequences of eye movements made on every line in the text and the output will
#        be one of all the lines of text on the current page.  In that way, this is a multiclass classification
#        problem -- select the most likely sentence given the sequence of eye movements.
#
#        One extension to this approach would be to train the network not with lines of text but with sequences of
#        several adjacent words.  If such set up would yield an acceptable performance it would be better in addressing
#        cases of lines read only partially.  Perhaps convolution layers would serve as a natural way of looking at
#        parts of lines.
#
#        Although probably computationally prohibitive, the ultimate solution might be to simply cast this problem as
#        trying to match eye movements to all the words on a page.  This way sentence structures could be utilized to
#        a higher degree because sentences would no longer be broken apart by line breaks.  It might also be helpful to
#        instantiate and use a language model somehow.
#
#        Relavant work
#            End-to-End Eye Movement Detection Using Convolutional Neural Networks
#                arxiv.org/abs/1609.02452
#                www.researchgate.net/publication/307930552_End-to-End_Eye_Movement_Detection_Using_Convolutional_Neural_Networks
#            Idendifying Eye Movements using Neural Networks for Human Computer Interaction
#                research.ijcaonline.org/volume105/number8/pxc3899658.pdf
#            Predicting eye movements from deep neural network activity decoded from fMRI responses to natural scenes
#                www.biorxiv.org/content/early/2017/07/21/166421
#            Neural Networks of Eye-Movements During Reading (poster; bullshit)
#                scholarsarchive.byu.edu/cgi/viewcontent.cgi?article=1316&context=fhssconference_studentpub
#            Predicting Eye Fixations using Convolutional Neural Networks
#                www.cv-foundation.org/openaccess/content_cvpr_2015/papers/Liu_Predicting_Eye_Fixations_2015_CVPR_paper.pdf
#            Classification of eye movements using electrooculography and neural networks
#                www.slideshare.net/waqastariq16/classification-of-eye-movements-using-electrooculography-and-neural-networks
#
#     3. Eye commander
#        Monitor and interpret eye movements as commands.  The big thing here is not to use gaze location based eye
#        movements (e.g., fixations) but rather on physical movements of the eye as recorded not by an eye tracker but
#        by a regular and ubiquitous Web cam.  That would lend itself to being a more
#
#        Similar work
#            Use your eyes and Deep Learning to command your computer — A.I. Odyssey
#                chatbotslife.com/finding-the-genre-of-a-song-with-deep-learning-da8f59a61194
#                hackernoon.com/talk-to-you-computer-with-you-eyes-and-deep-learning-a-i-odyssey-part-2-7d3405ab8be1
#
#     4. Make a neural network play video games
#        ...
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Questions investigated
#     Stimulus image format: PNG or JPG?
#         All images @1650x1080
#             PNG: 195MB
#             JPG: 120MB (60% quality is optimal)
#             Conversion: mkdir -p img; for i in *.png; do sips -s format jpeg -s formatOptions 60 $i --out img/$i.jpg; done
#         Conclusion: Image type shouldn't make a big difference
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources: Development
#     VirtualEnv
#         www.digitalocean.com/community/tutorials/common-python-tools-using-virtualenv-installing-with-pip-and-managing-packages
#         python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/#virtualenvironments-ref
#
#     Python code examples
#         www.programcreek.com/python
#
#     Execution timing and profiling
#         Python 2
#             stackoverflow.com/questions/1557571/how-to-get-time-of-a-python-program-execution
#         Python 3
#             stackoverflow.com/questions/1557571/how-to-get-time-of-a-python-program-execution/12344609#12344609
#             pastebin.com/rSqrkLPE
#         Profile hooks
#             pypi.python.org/pypi/profilehooks
#
#     Extending with C
#         ctypes, Cython, Swig, ...
#
#     Multithreading and concurrency
#         chriskiehl.com/article/parallelism-in-one-line
#         docs.python.org/3/library/threading.html
#         effbot.org/zone/thread-synchronization.htm
#
#     Message passing
#         Actor model
#             chimera.labs.oreilly.com/books/1230000000393/ch12.html#_solution_206
#             Pykka
#                 www.pykka.org/en/latest
#                 [no remoting]
#             Thespian
#                 thespianpy.com/doc
#             Spinoff
#                 pypi.python.org/pypi/spinoff/0.5.3
#             Cell
#                 cell.readthedocs.io/en/latest/introduction.html
#         Message passing
#             RabbitMQ
#                 www.rabbitmq.com/getstarted.html
#                 mo.github.io/2016/03/16/about-message-queues-and-rabbitmq.html
#         Use select
#             synack.me/blog/using-python-tcp-sockets  [bottom of page]
#         gevent
#             www.gevent.org
#         Polling Multiple Thread Queues
#             chimera.labs.oreilly.com/books/1230000000393/ch12.html#_solution_209
#         Queue for inter-threat communication
#             chimera.labs.oreilly.com/books/1230000000393/ch12.html#_problem_199
#         Security
#             SSL
#                 carlo-hamalainen.net/2013/01/24/python-ssl-socket-echo-test-with-self-signed-certificate
#
#     Data serialization
#         en.wikipedia.org/wiki/Comparison_of_data_serialization_formats
#         Protocol buffers (protobuf)
#             en.wikipedia.org/wiki/Protocol_Buffers
#             developers.google.com/protocol-buffers
#             github.com/google/protobuf
#
#     Distributed and parallel computation
#         General
#                 chimera.labs.oreilly.com/books/1230000000393/ch12.html
#         Multiprocessing
#             Secrets of the Multiprocessing Module
#                 www.usenix.org/system/files/login/articles/login1210_beazley.pdf
#             An introduction to Python concurrency
#                 www.slideshare.net/dabeaz/an-introduction-to-python-concurrency
#             Multiprocessing with Python
#                 www.slideshare.net/pvergain/multiprocessing-with-python-presentation
#             Distributed computing in Python with multiprocessing
#                 eli.thegreenplace.net/2012/01/24/distributed-computing-in-python-with-multiprocessing
#             Socket server example
#                 gist.github.com/micktwomey/606178
#             Communication Between Processes
#                 pymotw.com/2/multiprocessing/communication.html
#         Pebble
#             pythonhosted.org/Pebble
#         Twisted
#             i: Useful mostly when talking using a lot of different protocols
#             twistedmatrix.com/trac/
#             stackoverflow.com/questions/5458631/whats-so-cool-about-twisted
#             erlerobotics.gitbooks.io/erle-robotics-python-gitbook-free/server_architecture/python_facts_to_take_into_account.html
#         MPI
#             MPI Tutorial Introduction
#                 mpitutorial.com/tutorials/mpi-introduction
#             mpi4py
#                 pythonhosted.org/mpi4py
#         Pyro
#             pythonhosted.org/Pyro4
#         RPC
#             gRPC
#                 gRPC Examples
#                     grpc.io/docs/tutorials/basic/python.html
#                     gitlab.com/thoughtvector/grpc-ping-pong
#                     scotch.io/tutorials/implementing-remote-procedure-calls-with-grpc-and-protocol-buffers
#                     Bidirectional streaming
#                         grpc.io/docs/tutorials/basic/python.html
#                         github.com/ridha/grpc-streaming-demo
#                 Bringing Learnings from Googley Microservices with gRPC
#                     issuu.com/datawireio/docs/googley-grpc
#                 REST v. gRPC
#                     husobee.github.io/golang/rest/grpc/2016/05/28/golang-rest-v-grpc.html
#         Asynchronous sockets
#         Multithreaded sockets
#             Code sample - socket client thread in Python
#                 eli.thegreenplace.net/2011/05/18/code-sample-socket-client-thread-in-python
#
#     Statistics
#         General
#             7 Steps to Mastering Machine Learning With Python
#                 www.kdnuggets.com/2015/11/seven-steps-machine-learning-python.html
#             Python for Data Analysis
#                 hamelg.blogspot.com/2015/12/python-for-data-analysis-index.html
#         Spark
#             Apache Spark in Python: Beginner's Guide
#                 www.datacamp.com/community/tutorials/apache-spark-python#gs.jdkvJ7Y
#         Chapel
#             Chapel docs
#                 chapel.cray.com/docs/latest
#             Chapel Tutorials
#                 chapel.cray.com/tutorials.html
#             Chapel for Python Programmers
#                 chapel-for-python-programmers.readthedocs.io
#         Microsoft Cognitive Toolkit
#             Model gallery
#                 www.microsoft.com/en-us/cognitive-toolkit/features/model-gallery
#             Recurrent neural nets (sequence classification)
#                 github.com/Microsoft/CNTK/tree/master/Examples/SequenceClassification/SimpleExample/Python
#         Torch
#         Pytorch
#             pytorch.org
#         MxNet
#             [Apache so might be good]
#         Caffe2
#             [Looks like mostly for images]
#         Keras
#             [A wrapper around several DL libraries so might be the way to go; supposed to have an easy API that supports rapid development]
#             Web site
#                 keras.io
#             Blog
#                 blog.keras.io/index.html
#             Groups
#                 groups.google.com/forum/#!forum/keras-users
#                 stackoverflow.com/questions/tagged/keras
#             Keras Cheat Sheet: Neural Networks in Python
#                 www.datacamp.com/community/blog/keras-cheat-sheet
#                 s3.amazonaws.com/assets.datacamp.com/blog_assets/Keras_Cheat_Sheet_Python.pdf
#         TensorFlow
#             Site
#                 www.tensorflow.org
#                 github.com/tensorflow
#             Hidden Markov models
#                 github.com/MarvinBertin/HiddenMarkovModel_TensorFlow
#             Neural nets
#                 www.tensorflow.org/tutorials/wide
#                 adventuresinmachinelearning.com/python-tensorflow-tutorial
#             Convolutional neural nets
#             Recurrent neural nets (sequence classification)
#                 Book: Machine Learning with TensorFlow
#                     tensorflowbook.com
#                 Introduction to Recurrent Networks in TensorFlow
#                     danijar.com/introduction-to-recurrent-networks-in-tensorflow
#                 Tensor Flow tutorials
#                     www.tensorflow.org/tutorials/recurrent
#                     www.tensorflow.org/tutorials/seq2seq
#                 Sequence Classification
#                     danijar.com/introduction-to-recurrent-networks-in-tensorflow
#                     gist.github.com/danijar/c7ec9a30052127c7a1ad169eeb83f159
#                 Sequence Labelling
#                     danijar.com/introduction-to-recurrent-networks-in-tensorflow
#                     gist.github.com/danijar/61f9226f7ea498abce36187ddaf51ed5
#                 Variable-Length Sequence Classification
#                     danijar.com/variable-sequence-lengths-in-tensorflow
#                     gist.github.com/danijar/3f3b547ff68effb03e20c470af22c696
#                 Misc
#                     danijar.com/what-is-a-tensorflow-session/
#         TF Learn
#             tflearn.org
#         Tensor Fire
#             tenso.rs
#         VS: Spark, Chapel, and Tensor Flow
#             Next Generation HPC?
#                 ljdursi.github.io/Spark-Chapel-TF-UMich-2016
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources: Statistics
#     Bayesian nets
#         Software
#             BayesiaLab
#                 www.bayesia.com/bibliography-psychology
#             Stan
#                 mc-stan.org
#
#     Neural nets
#         Software comparison
#             www.teglor.com/b/deep-learning-libraries-language-cm569
#             deeplearning4j.org/compare-dl4j-torch7-pylearn
#             deeplearning.net/software_links
#             developer.nvidia.com/deep-learning-frameworks
#             arxiv.org/pdf/1608.07249v7.pdf
#                 "Our experimental results show that all tested tools can make good use of GPUs to achieve significant speedup over their CPU counterparts. However, there is no single software tool that can consistently outperform others, which implies that there exist some opportunities to further optimize the performance."
#         General
#             Weekly BigData & ML Roundups
#                 blog.pocketcluster.io
#             Awesome Deep Learning
#                 github.com/ChristosChristofidis/awesome-deep-learning
#             deeplearning.net
#                 www.deeplearning.net/tutorial/contents.html
#             Neural Networks and Deep Learning
#                 neuralnetworksanddeeplearning.com
#                 ufldl.stanford.edu/tutorial
#             Hacker's guide to Neural Networks
#                 karpathy.github.io/neuralnets
#             What's wrong with deep learning
#                 drive.google.com/file/d/0BxKBnD5y2M8NVHRiVXBnOVpiYUk/view
#             Machine learning and deep learning
#                 www.programcreek.com/machine-learning-and-deep-learning
#             Implementing a Neural Network from Scratch in Python – An Introduction
#                 www.wildml.com/2015/09/implementing-a-neural-network-from-scratch
#         Linear model neural nets
#             Tensor Flow turorials
#                 www.tensorflow.org/tutorials/wide
#         Convolutional neural nets
#             en.wikipedia.org/wiki/Convolutional_neural_network
#             deeplearning.net/tutorial/lenet.html
#             CS231n: Convolutional Neural Networks for Visual Recognition
#                 cs231n.stanford.edu/syllabus.html
#         Recurrent neural nets
#             Understanding LSTM Networks
#                 colah.github.io/posts/2015-08-Understanding-LSTMs
#             Long Short-Term Memory: Tutorial on LSTM Recurrent Networks
#                 people.idsia.ch/~juergen/lstm
#             The Unreasonable Effectiveness of Recurrent Neural Networks
#                 karpathy.github.io/2015/05/21/rnn-effectiveness
#             Book: Machine Learning with TensorFlow
#                 tensorflowbook.com
#             Tensor Flow tutorials
#                 www.tensorflow.org/tutorials/recurrent
#                 www.tensorflow.org/tutorials/seq2seq
#         Time series modeling
#             Intro
#                 deeplearning4j.org/lstm.html
#                 machinelearningmastery.com/gentle-introduction-long-short-term-memory-networks-experts   [no code, just words]
#             Tutorials
#                 machinelearningmastery.com/memory-in-a-long-short-term-memory-network   [RNNs and LSTMs]
#                 machinelearningmastery.com/understanding-stateful-lstm-recurrent-neural-networks-python-keras   [learning the alphabet]
#                 machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras   [embeddings]
#                 www.mlowl.com/post/character-language-model-lstm-tensorflow
#                 iamtrask.github.io/2015/11/15/anyone-can-code-lstm
#                 axon.cs.byu.edu/~martinez/classes/778/Papers/lstm.pdf   [CS778 – Winter 2016]
#             Questions
#                 github.com/fchollet/keras/issues/4523   [LSTM autoencoder for audio - input shape?] [Sequence to sequence]
#         Anomaly detection in time series
#             Anomaly Detection in Time Series using Auto Encoders
#                 philipperemy.github.io/anomaly-detection
#         Behavioral cloning
#             Examples
#                 medium.com/@fromtheast/implement-fit-generator-in-keras-61aa2786ce98
#                 github.com/dyelax/CarND-Behavioral-Cloning
#
#     Deep reinforcement learning
#         CS 294: Deep Reinforcement Learning, Fall 2017
#             rll.berkeley.edu/deeprlcourse
#         Playing Atari with Deep Reinforcement Learning (2013)
#             www.cs.toronto.edu/~vmnih/docs/dqn.pdf
#             www.youtube.com/watch?v=V1eYniJ0Rnk
#             www.youtube.com/watch?v=79pmNdyxEGo
#         Guest Post (Part I): Demystifying Deep Reinforcement Learning
#             www.intelnervana.com/demystifying-deep-reinforcement-learning
#         Misc
#             www.youtube.com/watch?v=QDzM8r3WgBw
#
#     Computer vision
#         Awesome Computer Vision
#             github.com/jbhuang0604/awesome-computer-vision
#         Awesome Deep Vision
#             github.com/kjw0612/awesome-deep-vision
#
#     Ensembles of classifiers
#         Boosting algorithms are considered stronger than bagging on noise free data. However, there are strong
#         empirical indications that bagging is much more robust than boosting in noisy settings. For this reason,
#         Kotsiantis and Pintelas (2004) built an ensemble using a voting methodology of bagging and boosting ensembles
#         that give better classification accuracy.
#
#     Hyper-parameter optimization
#         Random Search for Hyper-Parameter Optimization (Bergstra & Bengio, 2012)
#             URL: jmlr.csail.mit.edu/papers/volume13/bergstra12a/bergstra12a.pdf
#             Conclusions
#                 Random search > grid search
#                 Random search should be a baseling to compare adaptive (sequential) hyper-parameter optimization algorithms against
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Courses
#     www.udacity.com/course/deep-learning--ud730
#     web.stanford.edu/class/cs20si
#     github.com/oxford-cs-deepnlp-2017/lectures
#     www.edx.org/course/artificial-intelligence-ai-columbiax-csmm-101x-0
#     Berkley - CS 294: Deep Reinforcement Learning, Fall 2017
#         rll.berkeley.edu/deeprlcourse
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Solved or being solved
#     Importing modules from parent and sibling directories
#         stackoverflow.com/questions/1054271/how-to-import-a-python-class-that-is-in-a-directory-above
#         www.python.org/dev/peps/pep-0328
#         docs.python.org/2.7/tutorial/modules.html#packages
#         stackoverflow.com/questions/4348452/python-packaging-for-relative-imports
#         docs.python.org/2.5/whatsnew/pep-328.html
#         stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python?lq=1
#         stackoverflow.com/questions/11536764/how-to-fix-attempted-relative-import-in-non-package-even-with-init-py/27876800#27876800
#     Multiprocessing: Pool of custom processes
#         stackoverflow.com/questions/740844/python-multiprocessing-pool-of-custom-processes
#         stackoverflow.com/questions/27318290/why-can-i-pass-an-instance-method-to-multiprocessing-process-but-not-a-multipro
#         bytes.com/topic/python/answers/552476-why-cant-you-pickle-instancemethods  [Steven Bethard's response]
#     Multiprocessing: Sharing objects between processes
#         stackoverflow.com/questions/3671666/sharing-a-complex-object-between-python-processes
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources: Other
#     Code complexity
#         cloc /Volumes/D/prj/eye-link/src
#     List and kill a process
#         Any python process
#             ps aux | grep ".[p]y$" | awk '{print $2"  "$8"  "$9"  "$10"  "$12}'
#             kill -9 $(ps aux | grep ".[p]y$" | awk '{print $2}')
#         The experiment app
#             ps aux | grep "app.[p]y$" | awk '{print $2"  "$8"  "$9"  "$10"  "$12}'
#             kill -9 $(ps aux | grep "app.[p]y$" | awk '{print $2}')
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Deployment
#     Mac OS
#         brew update
#         brew install python3
#         brew install mercurial
#         brew install git
#         brew install sdl sdl_image sdl_mixer sdl_ttf smpeg portmidi
#
#         python -m venv /Volumes/D/prj/eye-link
#         cd /Volumes/D/prj/eye-link
#         source ./bin/activate
#
#         python -m pip install hg+http://bitbucket.org/pygame/pygame
#             python3
#             import pygame
#             pygame.init()
#
#         pip install --upgrade pip
#         easy_install -U pip
#
#         python -m pip install pillow dotmap numpy scipy sklearn pandas matplotlib h5py python-pushover tensorflow keras
#
#     Mac OS (unchecked)
#         www.pyimagesearch.com/2017/09/29/macos-for-deep-learning-with-python-tensorflow-and-keras
# ----
#
#     Ubuntu Server: OS  (16.04.3)
#         sudo apt-get install openssh-server emacs25-nox tmux
#         sudo service ssh restart
#
#         sudo apt-get install build-essential python3-dev libffi-dev libssl-dev zlib1g-dev libbz2-dev libsqlite3-dev
#
#     Ubuntu Server: VirtualBox
#         # Insert the Guest Additions CD
#         sudo mount /dev/cdrom /media/cdrom
#         cd /media/cdrom
#         sudo apt-get install -y dkms build-essential linux-headers-generic linux-headers-$(uname -r)
#         sudo su
#         ./VBoxLinuxAdditions.run
#         reboot
#
#     Ubuntu Server: Install python 3.6
#         wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
#         tar -xvf Python-3.6.3.tar.xz
#         cd Python-3.6.3
#         OR
#             ./configure
#             ./configure --enable-optimizations
#         OR
#             sudo make
#             sudo make -j2   # VirtualBox
#             sudo make -j25  # LRDC
#         OR
#             sudo make install
#             sudo make altinstall  # preserve the current python (recommended)
#
#     Ubuntu Server: Init venv
#         mkdir -p /home/tomek/prj
#         python -m venv /home/tomek/prj/eye-link
#         sudo apt-get install python3-venv
#         OR
#             /usr/local/bin/python3.5 -m venv /home/tomek/prj/eye-link
#             /usr/local/bin/python3.6 -m venv /home/tomek/prj/eye-link
#         cd /home/tomek/prj/eye-link
#         source ./bin/activate
#
#     Ubuntu Server: Init venv python
#         pip install --upgrade pip
#         easy_install -U pip
#
#         python -m pip install dotmap numpy scipy sklearn pandas matplotlib h5py python-pushover
#
#         CPU
#             python -m pip install --upgrade tensorflow
#
#         GPU
#             python -m pip install --upgrade tensorflow-gpu
#             python -m pip install cuDNN
#
#         python -m pip install keras
#
#         # pip3 install --upgrade tensorflow  # pip3 install --upgrade tensorflow-gpu
#
# ----
#
#     FreeBSD  (common)
#         bash
#         export BATCH=yes
#
#         sudo portsnap fetch
#         sudo portsnap extract
#         sudo portsnap update
#         sudo pkg audit -F
#
#         sudo echo "DEFAULT_VERSIONS+=python=3.6 python3=3.6" >> /etc/make.conf
#         sudo echo "devel_llvm38_UNSET=LLDB"                  >> /etc/make.conf
#         sudo cat /etc/make.conf
#
#         sudo pkg install python36
#         sudo /usr/local/bin/python3.6 -m ensurepip --upgrade
#
#         sudo ln -s /usr/local/bin/python3.6 /usr/local/bin/python
#         sudo ln -s /usr/local/bin/python3.6 /usr/local/bin/python3
#
#         python -m venv /home/tomek/prj/eye-link
#         cd /home/tomek/prj/eye-link
#         source ./bin/activate
#
#     FreeBSD  (10.3R)
#         python -m pip install pillow
#         python -m pip install dotmap
#
#         # python -m pip install numpy
#             # forums.freebsd.org/threads/60447
#             # github.com/numpy/numpy/issues/7779
#
#         cd /usr/ports/math/py-numpy && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/numpy* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#
#         python -m pip install scipy
#         python -m pip install sklearn
#         python -m pip install pandas
#         python -m pip install matplotlib    # forums.freebsd.org/threads/56160
#
#         cd /usr/ports/science/py-tensorflow/ && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/tensorflow* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#         --- FAIL ---
#
#         --- FIX ATTEMPT ---
#         cd /usr/ports/devel/protobuf && sudo make install clean
#         cd /usr/ports/devel/bazel-clang38 && sudo make install clean
#         --- FAIL ---
#
#         --- NOT PERFORMED ---
#         cd /usr/ports/science/py-h5py && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/h5py* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#
#         python -m pip install keras
#
#     FreeBSD  (11.1R)
#         python -m pip install pillow
#         python -m pip install dotmap
#
#         cd /usr/ports/math/py-numpy && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/numpy* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#
#         python -m pip install scipy
#         python -m pip install sklearn
#         python -m pip install pandas
#         python -m pip install matplotlib    # forums.freebsd.org/threads/56160
#
#         cd /usr/ports/science/py-tensorflow/ && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/tensorflow* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#         --- FAIL ---
#
#         --- FIX ATTEMPT ---
#         cd /usr/ports/textproc/py-sphinx && sudo deinstall clean
#         sudo pkg remove py-sphinx
#         cd /usr/ports/textproc/py3-sphinx && sudo make install clean
#         --- FAIL ---
#
#         --- NOT PERFORMED ---
#         cd /usr/ports/science/py-h5py && sudo make install clean
#         cp -R /usr/local/lib/python3.6/site-packages/h5py* /home/tomek/prj/eye-link/lib/python3.6/site-packages/
#
#         python -m pip install keras
#
#     Shell script
#         #!/usr/bin/env python3.6
#
# ----
#
#     Windows
#         ...
#
# ----
#
#     Python
#         pip install -r requirements.txt
#         github.com/python/cpython/tree/3.6/Lib
#
# ----------------------------------------------------------------------------------------------------------------------
#
# https://www.nature.com/news/the-future-of-the-postdoc-1.17253
# https://www.nature.com/news/stop-blocking-postdocs-paths-to-success-1.22515
#
# ----------------------------------------------------------------------------------------------------------------------

import sys

import et.em_vis
import et.et
import exp
import stim.txt_gc
import sub.sub


RAND_SEED       = 17

SCR_DIM         = (1680, 1050)
LANG            = 'en'
IMG_EXT         = 'png'
ALLOW_BACK_NAV  = False
CH_NUM_START    = None  # opening chapter number (for starting in the middle of the book)
T1_DUR          = 0  # trial 1 duration [s] (0=skip, None=not time-constrained)  # TODO: Fix Trial 1.
SUB_ID          = 'test'  # 8 or fewer characters; only 0-9, A-Z, and '_' allowed
DO_LOG_EM_EVT   = True

ET_MODE         = et.et.EyeTracker.MODE_EMU_SUB  # MODE_EMU_SUB | MODE_LIVE
ET_EMU_SUB_TYPE = sub.sub.Subject.SUB_TYPE_READ_LINE
ET_IS_VERBOSE   = False
EM_VIS_MODE     = et.em_vis.EyeMovementVis.MODE_LAST

# GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_CHAR_ONE
# GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_CHAR_RAND
# GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_CHAR_MATCH

GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_WORD_RAND
# GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_WORD_FRQ
# GC_SUB_MODE = stim.txt_gc.TextStimulusGC.SUB_MODE_WORD_POS

# GC_MASK_MODE, GC_MASK_SIZE = stim.txt_gc.TextStimulusGC.MASK_MODE_NONE, (8, 15, 1, 1)
# GC_MASK_MODE, GC_MASK_SIZE = stim.txt_gc.TextStimulusGC.MASK_MODE_CHAR, (6, 15, 1, 1)
GC_MASK_MODE, GC_MASK_SIZE = stim.txt_gc.TextStimulusGC.MASK_MODE_WORD, (2, 2, 1, 1)


# ======================================================================================================================
class App(object):
	def __init__(self):
		self.exp = None

	def start(self):
		# random.seed(RAND_SEED)

		self.exp = exp.Experiment(SCR_DIM, LANG, IMG_EXT, ALLOW_BACK_NAV, CH_NUM_START, T1_DUR, SUB_ID, ET_MODE, ET_EMU_SUB_TYPE, ET_IS_VERBOSE, EM_VIS_MODE, GC_MASK_MODE, GC_MASK_SIZE, GC_SUB_MODE, DO_LOG_EM_EVT, self.stop)
		self.exp.begin()

	def stop(self):
		sys.exit(0)


# ======================================================================================================================
if __name__ == '__main__':
	App().start()
