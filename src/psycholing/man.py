# ----------------------------------------------------------------------------------------------------------------------
#
# SUBTLEX.US
#     www.ugent.be/pp/experimentele-psychologie/en/research/documents/subtlexus/overview.htm
#
#     Word
#         This starts with a capital when the word more often starts with an uppercase letter than with a lowercase
#         letter.
#     FREQcount
#         This is the number of times the word appears in the corpus (i.e., on the total of 51 million words).
#     CDcount
#         This is the number of films in which the word appears (i.e., it has a maximum value of 8,388). CD stands for
#         contextual diversity. A few articles have argues this measure to be better than word frequency (e.g.,
#         Adelman, Brown, and Quesada, 2006; Adelman & Brown, 2008; Perea, Soares, & Comesana, 2013; Yap, Tan, Pexman,
#         & Hargreaves, 2011).
#     FREQlow
#         This is the number of times the word appears in the corpus starting with a lowercase letter. This allows
#         users to further match their stimuli.
#     CDlow
#         This is the number of films in which the word appears starting with a lowercase letter.
#     SUBTL.WF
#         This is the word frequency per million words. It is the measure you would preferably use in your manuscripts,
#         because it is a standard measure of word frequency independent of the corpus size. It is given with two
#         digits precision, in order not to lose precision of the frequency counts.
#     Lg10WF
#         This value is based on log10(FREQcount+1) and has four digit precision. Because FREQcount is based on 51
#         million words, the following conversions apply for SUBTLEXUS:
#
#            Lg10WF	 SUBTLWF
#              1.00      0.2
#              2.00        2
#              3.00       20
#              4.00      200
#              5.00     2000
#     SUBTLCD
#         Indicates in how many percent of the films the word appears. This value has two-digit precision in order not
#         to lose information.
#     Lg10CD
#         This value is based on log10(CDcount+1) and has four digit precision. It is the best value to use if you want
#         to match words on word frequency. As CDcount is based on 8388 films, the following conversions apply:
#            Lg10CD  SUBTLCD
#              0.95      0.1
#              1.93        1
#              2.92       10
#              3.92      100
#     FILE: subtlex-en-us.txt
#         Word  FREQcount  CDcount  FREQlow  Cdlow   SUBTLWF  Lg10WF  SUBTLCD  Lg10CD
#         the     1501908     8388  1339811	  8388  29449.18  6.1766   100.00  3.9237
#         to      1156570     8383  1138435   8380  22677.84  6.0632   99.94   3.9235
#         a       1041179     8382   976941   8380  20415.27  6.0175   99.93   3.9234
#         ...
#
#     ------------
#
#     Dom_PoS_SUBTLEX
#         The dominant (most frequent) PoS of each entry
#     Freq_dom_PoS_SUBTLEX
#         The frequency of the dominant PoS
#     Percentage_dom_PoS
#         The relative frequency of the dominant PoS
#     All_PoS_SUBTLEX
#         All PoS observed for the entry
#     All_freqs_SUBTLEX
#         The frequencies of each PoS
#     FILE: subtlex-en-us-pos.txt
#         Word  FREQcount  CDcount  FREQlow  Cdlow   SUBTLWF  Lg10WF  SUBTLCD  Lg10CD  Dom_PoS_SUBTLEX  Freq_dom_PoS_SUBTLEX  Percentage_dom_PoS  All_PoS_SUBTLEX                                All_freqs_SUBTLEX
#         the     1501908     8388  1339811   8388  29449.18  6.1766   100.00  3.9237  Article                       1499459                1.00  Article.Adverb.Noun.Preposition.Adjective  1499459.774.147.105.1
#         ...
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources
#     DB
#         Python SQLite  [docs.python.org/3/library/sqlite3.html]
#         Advanced SQLite Usage in Python  [pythoncentral.io/advanced-sqlite-usage-in-python]
#         Bulk insert huge data into SQLite using Python  [stackoverflow.com/questions/18219779/bulk-insert-huge-data-into-sqlite-using-python]
#         SQLite transaction for CSV importing  [stackoverflow.com/questions/18062694/sqlite-transaction-for-csv-importing/18063276#18063276]
#         APSW  [rogerbinns.github.io/apsw/index.html]
#     Word frequency
#         Subtitle  [crr.ugent.be/programs-data/subtitle-frequencies]
#         The Zipf-scale: A better standardized measure of word frequency  [crr.ugent.be/archives/1352]
#         A new kid in town: Are the new Google Ngram frequencies better than the SUBTLEX word frequencies?  [crr.ugent.be/archives/432]
#     Word age-of-acquisition, affective, etc. for different languages  [crr.ugent.be/programs-data/word-ratings]
#
# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     Identify cut-off points for low-high frq words
#     Identify cut-off points for low-medium-high frq words
#     Identify range of frq values (around one specific one) to be used as an acceptable frq-based match
#
# ----------------------------------------------------------------------------------------------------------------------

import csv
import os
import sqlite3
import string

from collections import OrderedDict


# ======================================================================================================================
class PsycholingMan(object):
	DEBUG_LVL = 1

	DIR = "psycholing"  # changed in the constructor

	FILE_EN_US_TXT = "subtlex-en-us-pos.txt"  # changed in the constructor
	FILE_EN_US_DB  = "subtlex-en-us-pos.db"   # ^

	ID_NONE = -1
	FRQ_NONE = -1.0

	WF_FRQ_MEAN   = 1.1873  # these values are obtained at the bottom of this file
	WF_FRQ_MEDIAN = 1.0000  # ^
	CD_FRQ_MEAN   = 1.0803  # ^
	CD_FRQ_MEDIAN = 0.9031  # ^

	CORPUS_SIZE_WORDS  = 51000000  # per publication
	CORPUS_SIZE_MOVIES =     8388  # ^

	POS = {
		"adj": { "id":  1, "name": "Adjective"    },  # id in the database
		"adv": { "id":  2, "name": "Adverb"       },
		"art": { "id":  3, "name": "Article"      },
		"con": { "id":  4, "name": "Conjunction"  },
		"det": { "id":  5, "name": "Determiner"   },
		"ex":  { "id":  6, "name": "Ex"           },  # TODO: What is this?
		"int": { "id":  7, "name": "Interjection" },
		"let": { "id":  8, "name": "Letter"       },
		"nam": { "id":  9, "name": "Name"         },
		"not": { "id": 10, "name": "Not"          },  # TODO: What is this?
		"nou": { "id": 11, "name": "Noun"         },
		"num": { "id": 12, "name": "Number"       },
		"pre": { "id": 13, "name": "Preposition"  },
		"pro": { "id": 14, "name": "Pronoun"      },
		"to" : { "id": 15, "name": "To"           },
		"unc": { "id": 16, "name": "Unclassified" },
		"ver": { "id": 17, "name": "Verb"         }
	}

	POS_NAME2ID = {}  # name-to-id mapping
	for k in POS: POS_NAME2ID[POS[k]["name"]] = POS[k]["id"]

	WORD = OrderedDict([
		( "id"         , { "idx-db":  0, "idx-txt": -1 } ),
		( "w"          , { "idx-db":  1, "idx-txt":  0 } ),  # Word
		( "len"        , { "idx-db":  2, "idx-txt": -1 } ),
		( "wf_n"       , { "idx-db":  3, "idx-txt":  1 } ),  # FREQcount
		( "wf_low_n"   , { "idx-db":  4, "idx-txt":  3 } ),  # FREQlow
		( "wf_frq"     , { "idx-db":  5, "idx-txt":  5 } ),  # SUBTLWF  (the measure you would preferably use in your manuscripts, because it is a standard measure of word frequency independent of the corpus size)
		( "wf_frq_l10" , { "idx-db":  6, "idx-txt":  6 } ),  # Lg10WF
		( "cd_n"       , { "idx-db":  7, "idx-txt":  2 } ),  # CDcount
		( "cd_low_n"   , { "idx-db":  8, "idx-txt":  4 } ),  # Cdlow
		( "cd_frq_l10" , { "idx-db":  9, "idx-txt":  8 } ),  # Lg10CD  (the best value to use if you want to match words on word frequency)
		( "cd_ratio"   , { "idx-db": 10, "idx-txt":  7 } ),  # SUBTLCD / 100
		( "pos_dom_id" , { "idx-db": 11, "idx-txt":  9 } ),  # Dom_PoS_SUBTLEX
		( "pos_n"      , { "idx-db": 12, "idx-txt": 10 } ),  # Freq_dom_PoS_SUBTLEX
		( "pos_ratio"  , { "idx-db": 13, "idx-txt": 11 } ),  # Percentage_dom_PoS / 100
		( "pos_all"    , { "idx-db": -1, "idx-txt": 12 } ),  # All_PoS_SUBTLEX
		( "pos_all_n"  , { "idx-db": -1, "idx-txt": 13 } )   # All_freqs_SUBTLEX
	])

	MATCH_WORD_SQL_01 = "SELECT w.w, len, w.wf_frq_l10, w.cd_frq_l10, w.pos_dom_id AS pos_id, p.name AS pos_name FROM word w INNER JOIN pos p ON p.id = w.pos_dom_id WHERE ((len > 4) OR (len <= 4 AND cd_n > 50)) AND"

	# Used by self.word_normalize():
	WORD_PUNCT = tuple(string.punctuation)  # !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
	WORD_START_PUNCT = ("\"", "'", "(")
	WORD_END_PUNCT = (".", "!", "?", ",", ";", ":", ")", "\"", "'")
	WORD_END_PUNCT_DBL = (".\"", "!\"", "?\"", ",\"", ";\"", ":\"", ")\"", "\")", ".'", "!'", "?'", ",'", ";'", ":'", ")'", "')")  # typically occurs at the end of a clause or sentence

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_assets, do_force_db_gen=False):
		self.DIR = os.path.join(dir_assets, self.DIR)
		self.FILE_EN_US_TXT = os.path.join(self.DIR, self.FILE_EN_US_TXT)
		self.FILE_EN_US_DB  = os.path.join(self.DIR, self.FILE_EN_US_DB)

		# if not os.path.exists(self.DIR): os.makedirs(self.DIR)

		self.__db_gen__(do_force_db_gen)

		self.conn = sqlite3.connect(self.FILE_EN_US_DB)

		# # Load the DB into memory:
		# from StringIO import StringIO
		# self.conn = sqlite3.connect(self.FILE_EN_US_DB)
		# f_tmp = StringIO()
		# for line in self.conn.iterdump():
		# 	f_tmp.write('%s\n' % line)
		# self.conn.close()
		# f_tmp.seek(0)
		#
		# self.conn = sqlite3.connect(":memory:")
		# self.conn.cursor().executescript(f_tmp.read())
		# self.conn.commit()
		# self.conn.row_factory = sqlite3.Row

		self.conn.execute("PRAGMA locking_mode=EXCLUSIVE")  # should be close to 2x faster on Windows
		self.conn.execute("PRAGMA synchronous=OFF")         # about 2x speed-up  [MacOS; 2017.06.14]
		# self.conn.execute("PRAGMA query_only=TRUE")         # prevent changes

		self.conn.row_factory = sqlite3.Row  # docs.python.org/3/library/sqlite3.html#sqlite3.Row

		# # APSW:
		# self.conn = apsw.connect(self.FILE_EN_US_DB)
		# # Load the DB into memory:
		# memcon=apsw.Connection(":memory:")
		# with memcon.backup("main", connection, "main") as backup:
		# 	backup.step()
		# for row in memcon.cursor().execute("select * from s"):
		# 	pass

		self.curs = self.conn.cursor()

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.conn.close()

	# ------------------------------------------------------------------------------------------------------------------
	def __db_gen__(self, do_force=False):
		''' Generates the database from the text file. '''

		# NOTE
		#     Foreign key indices below are commented out because the gain in speed is barely noticeable compared to
		#     space costs incured.

		if (not do_force) and (os.path.exists(self.FILE_EN_US_DB)): return

		os.remove(self.FILE_EN_US_DB)

		conn = sqlite3.connect(self.FILE_EN_US_DB)
		conn.isolation_level = None  # autocommit mode (we manage transaction manually due to fucked up Python API)
		curs = conn.cursor()

		try:
			# (1) Table 'pos':
			curs.execute("BEGIN")
			curs.execute(
				"CREATE TABLE pos (" +
				"id   INTEGER PRIMARY KEY ASC, " +
				"name TEXT UNIQUE NOT NULL)"
			)
			curs.executemany("INSERT INTO pos (id, name) VALUES (?, ?)", self.__db_gen__pos__get_values__())
			curs.execute("COMMIT")

			# (2) Tables 'word' and 'word_pos':
			# (2.1) DDL:
			curs.execute("BEGIN")
			curs.execute(
				"CREATE TABLE word (" +
				"id         INTEGER PRIMARY KEY ASC, " +
				"w          TEXT UNIQUE NOT NULL, " +
				"len        INTEGER, " +
				"wf_n       INTEGER, " +
				"wf_low_n   INTEGER, " +
				"wf_frq     REAL, " +
				"wf_frq_l10 REAL, " +
				"cd_n       INTEGER, " +
				"cd_low_n   INTEGER, " +
				"cd_frq_l10 REAL, " +
				"cd_ratio   REAL, " +
				"pos_dom_id INTEGER, " +
				"pos_n      INTEGER, " +
				"pos_ratio  REAL, " +
				"FOREIGN KEY (pos_dom_id) REFERENCES pos (id) ON DELETE CASCADE ON UPDATE CASCADE)"
			)
			# curs.execute("CREATE INDEX idx_fk_word_pos ON word(pos_dom_id)")  # no improvement in speed

			curs.execute(
				"CREATE TABLE word_pos (" +
				"id      INTEGER PRIMARY KEY ASC, " +
				"word_id INTEGER, " +
				"pos_id  INTEGER, " +
				"n       INTEGER, "
				"FOREIGN KEY (word_id) REFERENCES word (id) ON DELETE CASCADE ON UPDATE CASCADE,"
				"FOREIGN KEY (pos_id)  REFERENCES pos  (id) ON DELETE CASCADE ON UPDATE CASCADE)"
			)
			# curs.execute("CREATE INDEX idx_fk_word_pos_word ON word_pos(word_id)")  # no improvement in speed
			# curs.execute("CREATE INDEX idx_fk_word_pos_pos  ON word_pos(pos_id)" )  # no improvement in speed
			curs.execute("COMMIT")

			# (2.2) Data:
			with open(self.FILE_EN_US_TXT, "rb") as f:
				fr = csv.reader(f, delimiter="\t")
				fr.next()  # skip the header row

				parts = self.__db_gen__word__get_partitions__(list(fr))
				for part in parts:
					curs.execute("BEGIN")
					for p in part:
						# Table 'word':
						curs.execute(
							"INSERT INTO word (w, len, wf_n, wf_low_n, wf_frq, wf_frq_l10, cd_n, cd_low_n, cd_frq_l10, cd_ratio, pos_dom_id, pos_n, pos_ratio)" +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
							(
								p[0], len(p[0]), p[1], p[3], p[5], p[6], p[2], p[4], p[8], float(p[7]) / 100.00,
								(None) if (p[ 9] == "#N/A") else (self.POS_NAME2ID[p[9]]),
								(None) if (p[10] == "#N/A") else (p[10]),
								(None) if (p[11] == "#N/A") else (float(p[11]) / 100.00)
							)  # note the variable positions changes based on self.WORD
						)
						word_id = curs.lastrowid

						# Table 'word_pos':
						try:
							pos_name = p[12].split(".")
							pos_n = map(int, p[13].split("."))

							for (name, n) in zip(pos_name, pos_n):
								curs.execute(
									"INSERT INTO word_pos (word_id, pos_id, n) VALUES (?, ?, ?)", (word_id, self.POS_NAME2ID[name], n)
								)
						except:
							pass
					curs.execute("COMMIT")
					# conn.commit()  # TODO: This instead? I don't think so, because we set separation to None earlier.

			# (2.3) Indices:
			curs.execute("CREATE INDEX idx_word_01 ON word (len)")
			# curs.execute("CREATE INDEX idx_word_01 ON word (len, cd_frq_l10, pos_dom_id)")  # outperformed by the simple index above

			# (3) Other:
			curs.execute("ANALYZE")
		except conn.Error as err:
			print(err)
			conn.rollback()

		conn.close()

	# ----^-------------------------------------------------------------------------------------------------------------
	def __db_gen__word__get_partitions__(self, data, row_n=10000):
		for i in range (0, len(data), row_n):
			yield data[i:i + row_n]

	# ----^-------------------------------------------------------------------------------------------------------------
	def __db_gen__pos__get_values__(self):
		for k in self.POS:
			yield (self.POS[k]["id"], self.POS[k]["name"],)

	# ------------------------------------------------------------------------------------------------------------------
	def __word_tuple2dict__(self, t):
		''' Converts a tuple resulting from a call to any of the word_match_...() methods into a dictionary. '''

		return { "does-word-exist": t[0], "is-word-sub": t[1], "word": t[2], "len": t[3], "wf-frq-l10": t[4], "cd-frq-l10": t[5], "pos-id": t[6], "pos-name": t[7] }

	# ------------------------------------------------------------------------------------------------------------------
	def db_get(self, query):
		''' Get a list of lists which may can contain one of more results. '''

		self.curs.execute(query)
		return self.curs.fetchall()

	# ------------------------------------------------------------------------------------------------------------------
	def db_get_int(self, query):
		''' Get an integer. '''

		self.curs.execute(query)
		return int(self.curs.fetchone()[0])

	# ------------------------------------------------------------------------------------------------------------------
	def db_get_one(self, query):
		''' Get exactly one result list. '''

		self.curs.execute(query)
		return self.curs.fetchone()

	# ------------------------------------------------------------------------------------------------------------------
	def db_get_lst(self, query):
		''' Get exactly one result list. '''

		self.curs.execute(query)
		r = self.curs.fetchall()
		return None if (len(r) == 0) else list(r[0])

	# ------------------------------------------------------------------------------------------------------------------
	def db_get_tuple(self, query):
		''' Get exactly one result tuple. '''

		self.curs.execute(query)
		r = self.curs.fetchall()
		return None if (len(r) == 0) else tuple(r[0])

	# ------------------------------------------------------------------------------------------------------------------
	# def db_map(self, query, fn):
	# 	return fn(self.curs.execute(query))

	# ------------------------------------------------------------------------------------------------------------------
	def db_transaction_begin(self):
		''' Begin a transaction. It is beneficial for batches of selects. '''

		self.curs.execute("BEGIN")

	# ------------------------------------------------------------------------------------------------------------------
	def db_transaction_end(self, do_commit=False):
		''' Begin a transaction. It is beneficial for batches of selects. '''

		self.curs.execute("COMMIT" if (do_commit) else "ROLLBACK")

	# ------------------------------------------------------------------------------------------------------------------
	def word_match_by_len(self, w, do_dict=False):
		'''
		If the normalized word has a possessive form that form is used. In other words, the possessive aspect is not
		attented to because the match is not being made on PoS.
		'''

		wn = self.word_normalize(w)

		if not wn["is-usable"]:  # word not usable at start
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		w2 = wn["w-norm"]  # word to be substituted
		wi = wn["db-inf"]

		# (1) Find a random matching word:
		l = self.db_get_lst("%s len = %d ORDER BY RANDOM() LIMIT 1" % (self.MATCH_WORD_SQL_01, len(w2)))
		if l is not None:
			l = [wi is not None, True] + l
			l[2] = wn["w-pre"] + l[2] + wn["w-post"]
			l[3] = len(l[2])

			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (2) Match not found -- return the original word:
		if wi is not None:
			l = [True, False] + wi
			l[2] = w
			l[3] = len(w)

			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (3) Even the original word not found -- return a degenerated tuple:
		l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
		return l if (not do_dict) else self.__word_tuple2dict__(l)

		# return { "does-word-exist": t[0], "is-word-sub": t[1], "word": t[2], "len": t[3], "wf-frq-l10": t[4], "cd-frq-l10": t[5], "pos-id": t[6], "pos-name": t[7] }

	# ------------------------------------------------------------------------------------------------------------------
	def word_match_by_len_frq(self, w, do_dict=False):
		'''
		If the normalized word has a possessive form that form is used. In other words, the possessive aspect is not
		attented to because the match is not being made on PoS.
		'''

		wn = self.word_normalize(w)

		if not wn["is-usable"]:  # word not usable at start
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		w2 = wn["w-norm"]  # word to be substituted
		wi = wn["db-inf"]

		# (1) The original word not found (i.e., frequency unknown so cannot match) -- return a degenerated tuple:
		if wi is None:
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (2) Find a random matching word:
		l = self.db_get_lst("%s len = %d AND cd_frq_l10 >= %f AND cd_frq_l10 < %f AND w.id IN (SELECT id FROM word ORDER BY RANDOM()) LIMIT 1" % (self.MATCH_WORD_SQL_01, len(w2), wi["cd_frq_l10"] - 0.5, wi["cd_frq_l10"] + 0.5))
		if l is not None:
			l = [wi is not None, True] + l
			l[2] = wn["w-pre"] + l[2] + wn["w-post"]
			l[3] = len(l[2])

			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (3) Match not found -- return the original word:
		l = [True, False] + wi
		l[2] = w
		l[3] = len(w)

		return l if (not do_dict) else self.__word_tuple2dict__(l)

	# ------------------------------------------------------------------------------------------------------------------
	def word_match_by_len_pos(self, w, do_dict=False):
		# TODO: We are matching by PoS so we can use the normalized word because we know it's going to be substituted
		#       by another noun (if at all). However, we should account for the case of the substitutee word ending
		#       with the letter S.

		wn = self.word_normalize(w)

		if not wn["is-usable"]:  # word not usable at start
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		w2 = wn["w-norm"]  # word to be substituted
		wi = wn["db-inf"]

		# (1) The original word not found (i.e., PoS unknown so cannot match) -- return a degenerated tuple:
		if wi is None:
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (2) Find a random matching word:
		l = self.db_get_lst("%s len = %d AND pos_dom_id = %d AND w.id IN (SELECT id FROM word ORDER BY RANDOM()) LIMIT 1" % (self.MATCH_WORD_SQL_01, len(w2), wi["pos_id"]))
		if l is not None:
			l = [wi is not None, True] + l
			l[2] = wn["w-pre"] + l[2] + wn["w-post"]
			l[3] = len(l[2])

			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (3) Match not found -- return the original word:
		l = [True, False] + wi
		l[2] = w
		l[3] = len(w)

		return l if (not do_dict) else self.__word_tuple2dict__(l)

	# ------------------------------------------------------------------------------------------------------------------
	def word_match_by_len_frq_pos(self, w, do_dict=False):
		# TODO: We are matching by PoS so we can use the normalized word because we know it's going to be substituted
		#       by another noun (if at all). However, we should account for the case of the substitutee word ending
		#       with the letter S.

		wn = self.word_normalize(w)

		if not wn["is-usable"]:  # word not usable at start
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		w2 = wn["w-norm"]  # word to be substituted
		wi = wn["db-inf"]

		# (1) The original word not found (i.e., frequency and PoS unknown so cannot match) -- return a degenerated tuple:
		if wi is None:
			l = [False, False, w, len(w), self.FRQ_NONE, self.FRQ_NONE, self.ID_NONE, ""]
			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (2) Find a random matching word:
		l = self.db_get_lst("%s len = %d AND cd_frq_l10 >= %f AND cd_frq_l10 < %f AND pos_dom_id = %d AND w.id IN (SELECT id FROM word ORDER BY RANDOM()) LIMIT 1" % (self.MATCH_WORD_SQL_01, len(w2), wi["cd_frq_l10"] - 0.5, wi["cd_frq_l10"] + 0.5, wi["pos_id"]))
		if l is not None:
			l = [wi is not None, True] + l
			l[2] = wn["w-pre"] + l[2] + wn["w-post"]
			l[3] = len(l[2])

			return l if (not do_dict) else self.__word_tuple2dict__(l)

		# (3) Match not found -- return the original word:
		l = [True, False] + wi
		l[2] = w
		l[3] = len(w)

		return l if (not do_dict) else self.__word_tuple2dict__(l)

	# ------------------------------------------------------------------------------------------------------------------
	def word_match(self, w, do_dict=False):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def word_normalize(self, w):
		'''
		Returns a normalized version of the word that's ready to be queried against the DB. A word may or may not be
		judged as usable though.  There will no attempt to find substitutes for unusable words.  The return dictionary
		is of the following form:

			{ "is-usable": True,  "w-norm": "mister",    "word-pre": "'", "word-post": "'s", "w-poss": "mister's", "db-inf": {}   }
			{ "is-usable": False, "w-norm": "mis-ter's", "word-pre": ""   "word-post": "",   "w-poss": "",         "db-inf": None }

		If a word is judged as unusable, the original and unchanged word will be outputted as 'w-norm'.  The 'db-inf'
		element contains word infrmation from the DB:

			{ "w": "mister", "len": 6, "wf_frq_l10": ..., "cd_frq_l10": ..., "pos_id": ..., "p_name": "..." }

		NOTE
			With respect to the possessive form, a word ending in s' could be a closing single quote, but I assume it
			isn't because double quotes are more customary in regular, non-technical texts.

		NOTE
			This method does not break words apart.  Any text chunking (e.g., breaking apart hypenated words) should
			be done outside of it.
		'''

		# TODO: I'll, isn't, aren't, wouldn't, couldn't, won't, can't, don't, doesn't
			# So far I'm leaning towards not handling these cases explicitely.  They will get implicitely classified
			# as unusable words which is fine because that means they simply won't be substituted.

		# (1) Quickly handle a letters-only case:
		if w.isalpha():  # re.match("^[a-zA-Z0-9]+$", w) is not None:
			return { "is-usable": True, "w-norm": w, "w-pre": "", "w-post": "", "w-poss": "", "db-inf": self.db_get_one("%s w = '%s'" % (self.MATCH_WORD_SQL_01, w)) }

		# (2) Not only letters:
		w_norm = w  # the word as it's being stripped off punctuation and such down to it's normalized form
		w_poss = ""  # possessive form of the word

		idx01 = 0
		idx02 = len(w)

		# (2.1) Possessive form:
		if (w_norm.endswith("'s")):
			w_poss = w[idx01:idx02]
			idx02 -= 2
			w_norm = w[idx01:idx02]
		elif (w_norm.endswith("s'")):
			w_poss = w[idx01:idx02]
			idx02 -= 1
			w_norm = w[idx01:idx02]

		# (2.2) Punctuation at the beginning:
		if w_norm.startswith(self.WORD_START_PUNCT):
			idx01 += 1
			w_norm = w[idx01:]

		# (2.3) Punctuation at the end:
		if w_norm.endswith(self.WORD_END_PUNCT_DBL):  # double punctuation (e.g., a quote after a comma)
			idx02 -= 2
			w_norm = w[idx01:idx02]
		elif w_norm.endswith(self.WORD_END_PUNCT):  # single punctuation (e.g., a comma)
			idx02 -= 1
			w_norm = w[idx01:idx02]

		# (2.4) Possessive form (we need this again because of the potential punctuation at the end earlier):
		if (w_norm.endswith("'s")):
			w_poss = w[idx01:idx02]
			idx02 -= 2
			w_norm = w[idx01:idx02]
		elif (w_norm.endswith("s'")):
			w_poss = w[idx01:idx02]
			idx02 -= 1
			w_norm = w[idx01:idx02]

		# (2.5) Check for non-alphabetical characters (e.g., a hyphenated word):
		is_norm_alpha = w_norm.isalpha()

		# (2.6) Return:
		if is_norm_alpha:
			return { "is-usable": True,  "w-norm": w_norm, "w-pre": w[:idx01], "w-post": w[idx02:], "w-poss": w_poss, "db-inf": self.db_get_one("%s w = '%s'" % (self.MATCH_WORD_SQL_01, w_norm)) }
		else:
			return { "is-usable": False, "w-norm": w_norm, "w-pre": w[:idx01], "w-post": w[idx02:], "w-poss": w_poss, "db-inf": None }


# ======================================================================================================================
if __name__ == "__main__":
	import sys
	from inspect import getsourcefile
	__file__ = os.path.abspath(getsourcefile(lambda: None))
	sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

	import numpy

	import fs

	from util import Time

	do_force_db_gen = False
	do_time_exec = True

	w_01 = "welcome"
	w_02 = "congregate"
	w_non_01 = "lkjriucyilalkeroijvlkjsdfnemertmn"  # a non-word
	w_non_02 = "aabbccdd"                           # ^

	plm = PsycholingMan(fs.FileSystem.DIR_ASSETS, do_force_db_gen)
	PLM = PsycholingMan  # shorthand

	# ------------------------------------------------------------------------------------------------------------------
	# (1) Execution time (used for instance to estimate the benefit of database indices):
	if do_time_exec:
		n = 1000

		plm.word_match_by_len(w_non_01)  # discard the first run

		# (1.1) Length (non-word, too long):
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len(w_non_01)
		t_l_non_01 = Time.diff(dt_0, Time.dt()) / n

		# (1.2) Length (non-word):
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len(w_non_02)
		t_l_non_02 = Time.diff(dt_0, Time.dt()) / n

		# (1.3) Length:
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len(w_non_01)
		t_l = Time.diff(dt_0, Time.dt()) / n

		# (1.4) Length and frequency:
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len_frq(w_02)
		t_lf = Time.diff(dt_0, Time.dt()) / n

		# (1.5) Length and part of speech:
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len_pos(w_02)
		t_lp = Time.diff(dt_0, Time.dt()) / n

		# (1.6) Length, frequency, and part of speech:
		dt_0 = Time.dt()
		for i in range (n): plm.word_match_by_len_frq_pos(w_02)
		t_lfp = Time.diff(dt_0, Time.dt()) / n

		# (1.7) Summary:
		print("Execution times for finding one random substitute for '%s' given" % w_02)
		print("    Length (non-word, too long):                     %.6f" % t_l_non_01)
		print("    Length (non-word):                               %.6f" % t_l_non_02)
		print("    Length:                                          %.6f" % t_l)
		print("    Length and frequency (+/- 0.5):                  %.6f" % t_lf)
		print("    Length and part of speech:                       %.6f" % t_lp)
		print("    Length, frequency (+/- 0.5), and part of speech: %.6f" % t_lfp)

		sys.exit(0)

	# (1.8) Summary (hard-coded values obtained ealier):
	print("Execution times (ms; based on 10000 runs; datetime) and sizes (B) for finding one random substitute for '%s' given" % w_02)
	print("    Database indices                                      No       Yes")
	print("    -----------------------------------------------  -------    ------")
	print("    Length (non-word, too long)                        5.446     0.015")
	print("    Length (non-word)                                 11.696    10.770")
	print("    Length                                             5.468     0.013")
	print("    Length and frequency (+/- 0.5)                     0.050     0.032")
	print("    Length and part of speech                          0.044     0.022")
	print("    Length, frequency (+/- 0.5), and part of speech    0.064     0.045")
	print("    -----------------------------------------------  -------  --------")
	print("    Database size                                    8609792   9326592")
	print("    Indices size cost                                          %7d" % (9326592 - 8609792))
	print("    -----------------------------------------------  -------  --------")
	print("    NOTE: See source file for more details on indices")
	print("    NOTE: The above times are from before word normalization. Adding that component made the times a bit")
	print("          longer, but the relations between them have stayed the same as have the influence of indexing.")
	print("")

	# (1.9) Summary of previous evaluations:
	# Size: 8609792 B | no indices (baseline)  [MBP15; time.time(); 2017.09.10]
		# Length (non-word, too long):                     0.005446
		# Length (non-word):                               0.011696
		# Length:                                          0.005468
		# Length and frequency (+/- 0.5):                  0.000050
		# Length and part of speech:                       0.000044
		# Length, frequency (+/- 0.5), and part of speech: 0.000064

	# Size: 9326592 B | word (len)  [MBP15; time.time(); 2017.09.10]
		# Length (non-word, too long):                     0.000037
		# Length (non-word):                               0.010960
		# Length:                                          0.000034
		# Length and frequency (+/- 0.5):                  0.000043
		# Length and part of speech:                       0.000041
		# Length, frequency (+/- 0.5), and part of speech: 0.000062

		# Adding word normalization resulted in about 15% slow-down (but it's proportional)  [MBP15; time.time(); 2017.09.12]
			# Length (non-word, too long):                     0.000044
			# Length (non-word):                               0.013902
			# Length:                                          0.000041
			# Length and frequency (+/- 0.5):                  0.000060
			# Length and part of speech:                       0.000055
			# Length, frequency (+/- 0.5), and part of speech: 0.000082

		# Loading the DB to memory results in a mix of slow-ups and slow-downs (real-life tests reveal 2-3x slow-down)  [MBP15; time.time(); 2017.09.14]
			# Length (non-word, too long):                     0.000019
			# Length (non-word):                               0.012226
			# Length:                                          0.000018
			# Length and frequency (+/- 0.5):                  0.000045
			# Length and part of speech:                       0.000034
			# Length, frequency (+/- 0.5), and part of speech: 0.000116

		# Setting "PRAGMA synchronous=OFF" offers up to 2x speed-up  [MBP15; time.time(); 2017.09.14]  [ *** BEST OPTION *** ]
			# Length (non-word, too long):                     0.000021
			# Length (non-word):                               0.015039
			# Length:                                          0.000018
			# Length and frequency (+/- 0.5):                  0.000034
			# Length and part of speech:                       0.000030
			# Length, frequency (+/- 0.5), and part of speech: 0.000060

		# Setting "temp_store=MEMORY" offers no improvement  [MBP15; time.time(); 2017.09.14]
			# Length (non-word, too long):                     0.000035
			# Length (non-word):                               0.019580
			# Length:                                          0.000021
			# Length and frequency (+/- 0.5):                  0.000040
			# Length and part of speech:                       0.000032
			# Length, frequency (+/- 0.5), and part of speech: 0.000063

		# Setting "PRAGMA page_size=4096" offers no improvement  [MBP15; time.time(); 2017.09.14]
			# Length (non-word, too long):                     0.000037
			# Length (non-word):                               0.043801
			# Length:                                          0.000035
			# Length and frequency (+/- 0.5):                  0.000074
			# Length and part of speech:                       0.000058
			# Length, frequency (+/- 0.5), and part of speech: 0.000095

	# Size: 10133504 B | word (len, cd_frq_l10, pos_dom_id)  [MBP15; time.time(); 2017.09.10]
		# Length (non-word, too long):                     0.000038
		# Length (non-word):                               0.020273  - this lookup is unlikely to happen and be needed in the first place
		# Length:                                          0.000037  - strangely, "ORDER BY RAND()" does better than "WHERE id IN (SELECT id FROM word ORDER BY RAND())"
		# Length and frequency (+/- 0.5):                  0.000044
		# Length and part of speech:                       0.000052
		# Length, frequency (+/- 0.5), and part of speech: 0.000078

	# Conclusions  [MBP15]
		# Definitely create an index on the 'word' table
		# Don't add 'w' to 'idx_word_01'
		# In fact, use only 'len' in the 'idx_word_01'
		# Don't create FK indices
		# Don't load the DB into memory
	# Conclusions  [LRDC 407C]
		# ...

	# ------------------------------------------------------------------------------------------------------------------
	# (2) Word normalization:
	print("Word normalization")
	print("    Letters only (good)")
	print("        %12s  %s" % (w_01, plm.word_normalize(w_01)))

	print("    Hyphenated word (bad)")
	print("        %12s  %s" % ("wel-come", plm.word_normalize("wel-come")))

	print("    Certain special words (bad)")
	print("        %12s  %s" % ("I'll", plm.word_normalize("I'll")))
	print("        %12s  %s" % ("isn't", plm.word_normalize("isn't")))
	print("        %12s  %s" % ("aren't", plm.word_normalize("aren't")))
	print("        %12s  %s" % ("can't", plm.word_normalize("can't")))
	print("        %12s  %s" % ("don't", plm.word_normalize("don't")))
	print("        %12s  %s" % ("doesn't", plm.word_normalize("doesn't")))
	print("        %12s  %s" % ("couldn't", plm.word_normalize("couldn't")))
	print("        %12s  %s" % ("wouldn't", plm.word_normalize("wouldn't")))
	print("        %12s  %s" % ("won't", plm.word_normalize("won't")))
	print("        %12s  %s" % ("ain't", plm.word_normalize("ain't")))

	print("    Punctuation at the beginning (good)")
	for p in PLM.WORD_START_PUNCT: print("        %12s  %s" % ("%s%s" % (p, w_01), plm.word_normalize("%s%s" % (p, w_01))))

	print("    Punctuation at the beginning (bad)")
	print("        %12s  %s" % (",%s" % w_01, plm.word_normalize(",%s" % w_01)))
	print("        %12s  %s" % ("-%s" % w_01, plm.word_normalize("-%s" % w_01)))
	print("        %12s  %s" % ("!%s" % w_01, plm.word_normalize("!%s" % w_01)))

	print("    Single punctuation at the end (good)")
	for p in PLM.WORD_END_PUNCT: print("        %12s  %s" % ("%s%s" % (w_01, p), plm.word_normalize("%s%s" % (w_01, p))))

	print("    Single punctuation at the end (bad)")
	print("        %12s  %s" % ("%s#" % w_01, plm.word_normalize("%s#" % w_01)))
	print("        %12s  %s" % ("%s(" % w_01, plm.word_normalize("%s(" % w_01)))
	print("        %12s  %s" % ("%s-" % w_01, plm.word_normalize("%s-" % w_01)))

	print("    Double punctuation at the end (good)")
	for p in PLM.WORD_END_PUNCT_DBL: print("        %12s  %s" % ("%s%s" % (w_01, p), plm.word_normalize("%s%s" % (w_01, p))))

	print("    Double punctuation at the end (bad)")
	print("        %12s  %s" % ("%s-'"  % w_01, plm.word_normalize("%s-'"  % w_01)))
	print("        %12s  %s" % ("%s-\"" % w_01, plm.word_normalize("%s-\"" % w_01)))
	print("        %12s  %s" % ("%s(\"" % w_01, plm.word_normalize("%s-\"" % w_01)))

	print("    Possessive form (single)")
	print("        %12s  %s" % ("friend's", plm.word_normalize("friend's")))
	print("    Possessive form (plural)")
	print("        %12s  %s" % ("friends'", plm.word_normalize("friends'")))

	print("    Possessive form (single) + single punctuation at the end (good)")
	for p in PLM.WORD_END_PUNCT: print("        %12s  %s" % ("friend's%s" % p, plm.word_normalize("friend's%s" % p)))

	print("    Possessive form (single) + single punctuation at the end (bad)")
	print("        %12s  %s" % ("friend's#", plm.word_normalize("friend's#")))
	print("        %12s  %s" % ("friend's(", plm.word_normalize("friend's(")))
	print("        %12s  %s" % ("friend's-", plm.word_normalize("friend's-")))

	print("    Possessive form (plural) + double punctuation at the end (good)")
	for p in PLM.WORD_END_PUNCT_DBL: print("        %12s  %s" % ("friends'%s" % p, plm.word_normalize("friends'%s" % p)))

	print("    Possessive form (plural) + double punctuation at the end (bad)")
	print("        %12s  %s" % ("friends'-'",  plm.word_normalize("friends'-'" )))
	print("        %12s  %s" % ("friends'-\"", plm.word_normalize("friends'-\"")))
	print("        %12s  %s" % ("friends'(\"", plm.word_normalize("friends'(\"")))
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (3) Database health:
	print("Table row counts")
	print("    pos       %d" % plm.db_get_int("SELECT COUNT(*) FROM pos"))
	print("    word      %d" % plm.db_get_int("SELECT COUNT(*) FROM word"))
	print("    word_pos  %d" % plm.db_get_int("SELECT COUNT(*) FROM word_pos"))
	print("")

	print("Value range checks")
	print("    cd_ratio  (%.4f, %.4f)" % plm.db_get_tuple("SELECT MIN(cd_ratio),  MAX(cd_ratio)  FROM word WHERE cd_ratio  IS NOT NULL"))
	print("    pos_ratio (%.4f, %.4f)" % plm.db_get_tuple("SELECT MIN(pos_ratio), MAX(pos_ratio) FROM word WHERE pos_ratio IS NOT NULL"))
	print("")

	print("Three first parts of speech")
	for r in plm.db_get("SELECT name FROM pos ORDER BY id LIMIT 3"):
		print("    " + r["name"])
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (4) Words, frequencies, and parts of speech:
	print("All parts of speech of a word along with the number of appearances")
	print("    a")
	for r in plm.db_get("SELECT p.name AS name, wp.n AS n FROM pos p INNER JOIN word_pos wp ON wp.pos_id = p.id INNER JOIN word w ON w.id = wp.word_id WHERE w.w = 'a'"):
		print("        %s %d" % (r["name"], r["n"]))

	print("    %s" % w_01)
	for r in plm.db_get("SELECT p.name AS name, wp.n AS n FROM pos p INNER JOIN word_pos wp ON wp.pos_id = p.id INNER JOIN word w ON w.id = wp.word_id WHERE w.w = '%s'" % w_01):
		print("        %s %d" % (r["name"], r["n"]))

	print("    %s" % w_02)
	for r in plm.db_get("SELECT p.name AS name, wp.n AS n FROM pos p INNER JOIN word_pos wp ON wp.pos_id = p.id INNER JOIN word w ON w.id = wp.word_id WHERE w.w = '%s'" % w_02):
		print("        %s %d" % (r["name"], r["n"]))
	print("")

	print("Frequency measures (log10)")
	wf_frq_l10 = map(lambda x: x[0], plm.db_get("SELECT wf_frq_l10 FROM word"))
	wf_frq_l10_5p = numpy.percentile(wf_frq_l10, [0, 25, 50, 75, 100])
	cd_frq_l10 = map(lambda x: x[0], plm.db_get("SELECT cd_frq_l10 FROM word"))
	cd_frq_l10_5p = numpy.percentile(cd_frq_l10, [0, 25, 50, 75, 100])
	print("    WF")
	print("        mean   %.4f" % numpy.mean(wf_frq_l10))
	print("        std    %.4f" % numpy.std(wf_frq_l10))
	print("        -----------")
	print("          0    %.4f" % wf_frq_l10_5p[0])
	print("         25    %.4f" % wf_frq_l10_5p[1])
	print("         50    %.4f" % wf_frq_l10_5p[2])
	print("         75    %.4f" % wf_frq_l10_5p[3])
	print("        100    %.4f" % wf_frq_l10_5p[4])
	print("    CD")
	print("        mean   %.4f" % numpy.mean(cd_frq_l10))
	print("        std    %.4f" % numpy.std(cd_frq_l10))
	print("        -----------")
	print("          0    %.4f" % cd_frq_l10_5p[0])
	print("         25    %.4f" % cd_frq_l10_5p[1])
	print("         50    %.4f" % cd_frq_l10_5p[2])
	print("         75    %.4f" % cd_frq_l10_5p[3])
	print("        100    %.4f" % cd_frq_l10_5p[4])
	print("    WF x CD")
	print("        corr   %.4f" % numpy.corrcoef(wf_frq_l10, cd_frq_l10)[0][1])
	print("")

	print("Words by frequency (first word in alphabetical order)")
	print("    WF frq min: %s" % plm.db_get("SELECT w FROM word WHERE wf_frq_l10 = (SELECT MIN(wf_frq_l10) FROM word) ORDER BY w")[0][0])
	print("    WF frq max: %s" % plm.db_get("SELECT w FROM word WHERE wf_frq_l10 = (SELECT MAX(wf_frq_l10) FROM word) ORDER BY w")[0][0])
	print("    CD frq min: %s" % plm.db_get("SELECT w FROM word WHERE cd_frq_l10 = (SELECT MIN(cd_frq_l10) FROM word) ORDER BY w")[0][0])
	print("    CD frq max: %s" % plm.db_get("SELECT w FROM word WHERE cd_frq_l10 = (SELECT MAX(cd_frq_l10) FROM word) ORDER BY w")[0][0])
	print("")

	print("Number of words: By length and frequency (median-split)")
	print("    len  4  frq l  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  4 AND cd_frq_l10 <  %f" % PLM.CD_FRQ_MEDIAN))
	print("    len  4  frq h  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  4 AND cd_frq_l10 >= %f" % PLM.CD_FRQ_MEDIAN))
	print("    len  8  frq l  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  8 AND cd_frq_l10 <  %f" % PLM.CD_FRQ_MEDIAN))
	print("    len  8  frq h  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  8 AND cd_frq_l10 >= %f" % PLM.CD_FRQ_MEDIAN))
	print("    len 12  frq l  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len = 12 AND cd_frq_l10 <  %f" % PLM.CD_FRQ_MEDIAN))
	print("    len 12  frq h  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len = 12 AND cd_frq_l10 >= %f" % PLM.CD_FRQ_MEDIAN))
	print("")

	print("Number of words: By length, frequency (median-split), and part of speech")
	print("    len  3  frq l  noun  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  3 AND pos_dom_id = %d AND cd_frq_l10 <  %f" % (PLM.POS["nou"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("    len  3  frq h  noun  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  3 AND pos_dom_id = %d AND cd_frq_l10 >= %f" % (PLM.POS["nou"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("    len  5  frq l  verb  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  5 AND pos_dom_id = %d AND cd_frq_l10 <  %f" % (PLM.POS["ver"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("    len  5  frq h  verb  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len =  5 AND pos_dom_id = %d AND cd_frq_l10 >= %f" % (PLM.POS["ver"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("    len 10  frq l  verb  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len = 10 AND pos_dom_id = %d AND cd_frq_l10 <  %f" % (PLM.POS["ver"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("    len 10  frq h  verb  %8d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len = 10 AND pos_dom_id = %d AND cd_frq_l10 >= %f" % (PLM.POS["ver"]["id"], PLM.CD_FRQ_MEDIAN)))
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (5) Word matching (custom):
	wi = plm.db_get_one("SELECT cd_frq_l10 AS frq, pos_dom_id AS pos_id FROM word WHERE w = '%s'" % w_01)  # word inf

	print("Five random substitutes for '%s' given" % w_01)

	# (5.1) Length:
	print("    Length")
	print("        %s  %.4f  %2d" % (w_01, wi["frq"], wi["pos_id"]))
	print("        %s  ------  --" % ("-" * len(w_01)))
	for r in plm.db_get("SELECT w, cd_frq_l10 AS frq, pos_dom_id AS pos_id FROM word WHERE len = %d ORDER BY RANDOM() LIMIT 5" % len(w_01)):
		print("        %s  %.4f  %2d" % (r["w"], r["frq"], r["pos_id"]))

	# (5.2) Length and frequency:
	print("    Length and frequency (+/- 0.5)")
	print("        %s  %.4f  %2d" % (w_01, wi["frq"], wi["pos_id"]))
	print("        %s  ------  --" % ("-" * len(w_01)))
	for r in plm.db_get("SELECT w, cd_frq_l10 AS frq, pos_dom_id AS pos_id FROM word WHERE len = %d AND cd_frq_l10 >= %f AND cd_frq_l10 < %f ORDER BY RANDOM() LIMIT 5" % (len(w_01), wi["frq"] - 0.5, wi["frq"] + 0.5)):
		print("        %s  %.4f  %2d" % (r["w"], r["frq"], r["pos_id"]))

	# (5.3) Length and part of speech:
	print("    Length and part of speech")
	print("        %s  %.4f  %2d" % (w_01, wi["frq"], wi["pos_id"]))
	print("        %s  ------  --" % ("-" * len(w_01)))
	for r in plm.db_get("SELECT w, cd_frq_l10 AS frq FROM word WHERE len = %d AND pos_dom_id = %d ORDER BY RANDOM() LIMIT 5" % (len(w_01), wi["pos_id"])):
		print("        %s  %.4f  %2d" % (r["w"], r["frq"], wi["pos_id"]))

	# (5.4) Length, frewuency, and part of speech:
	print("    Length, frequency (+/- 0.5), and part of speech")
	print("        %s  %.4f  %2d" % (w_01, wi["frq"], wi["pos_id"]))
	print("        %s  ------  --" % ("-" * len(w_01)))
	for r in plm.db_get("SELECT w, cd_frq_l10 AS frq FROM word WHERE len = %d AND cd_frq_l10 >= %f AND cd_frq_l10 < %f AND pos_dom_id = %d ORDER BY RANDOM() LIMIT 5" % (len(w_01), wi["frq"] - 0.5, wi["frq"] + 0.5, wi["pos_id"])):
		print("        %s  %.4f  %2d" % (r["w"], r["frq"], wi["pos_id"]))
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (6) Word matching (built-in; raw output):
	# (6.1) Non-words:
	print("One random substitute for a too long non-word ('%s') given" % w_non_01)
	print("    Length")
	print("        %s" % str(plm.word_match_by_len(w_non_01)))
	print("        %s" % str(plm.word_match_by_len(w_non_01, True)))
	print("")

	print("One random substitute for a non-word ('%s') given" % w_non_02)
	print("    Length")
	print("        %s" % str(plm.word_match_by_len(w_non_02)))
	print("        %s" % str(plm.word_match_by_len(w_non_02, True)))
	print("")

	# (6.2) Existing word:
	print("One random substitute for '%s' given" % w_02)
	print("    Length")
	print("        %s" % str(plm.word_match_by_len(w_02)))
	print("        %s" % str(plm.word_match_by_len(w_02, True)))
	print("    Length and frequency (+/- 0.5)")
	print("        %s" % str(plm.word_match_by_len_frq(w_02)))
	print("        %s" % str(plm.word_match_by_len_frq(w_02, True)))
	print("    Length and part of speech")
	print("        %s" % str(plm.word_match_by_len_pos(w_02)))
	print("        %s" % str(plm.word_match_by_len_pos(w_02, True)))
	print("    Length, frequency (+/- 0.5), and part of speech")
	print("        %s" % str(plm.word_match_by_len_frq_pos(w_02)))
	print("        %s" % str(plm.word_match_by_len_frq_pos(w_02, True)))
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (7) Word matching (built-in; raw output; word normalization test):
	print("One random substitute given: Length, frequency (+/- 0.5), and part of speech")
	print("    %s  %s" % ("(welcome", str(plm.word_match_by_len_frq_pos("(welcome", True))))
	print("    %s  %s" % ("testing!", str(plm.word_match_by_len_frq_pos("testing!", True))))
	print("    %s  %s" % ("acceptable.\"", str(plm.word_match_by_len_frq_pos("acceptable.\"", True))))
	print("")

	# ------------------------------------------------------------------------------------------------------------------
	# (8) Viability of short words:
	#
	#     This is important becasuse many 2-, 3-, and even 4-characters long are junk. That's becuase the corupus is
	#     based on subtitles which are inherently noisy. Therefore, the corpus cannot be used in its basic form and
	#     short words need to be selected more carefully. Words 5+ characters long should be fine.
	#
	#     The selection of short words is hard-coded into the PsycholingMan.MATCH_WORD_SQL_01 class variable.
	print("Viability of short words")
	print("    Word len 5+: %d"      % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len > 4"))
	print("    Word len 1-4")
	print("        All: %5d"         % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5"))
	print("        cd_n >=  10: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5 AND cd_n >=  10"))
	print("        cd_n >=  25: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5 AND cd_n >=  25"))
	print("        cd_n >=  50: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5 AND cd_n >=  50"))
	print("        cd_n >=  75: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5 AND cd_n >=  75"))
	print("        cd_n >= 100: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < 5 AND cd_n >= 100"))
	for wlen in (2, 3, 4):
		print("    Word len: %d" % wlen)
		print("        All: %5d"         % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d" % wlen))
		print("        cd_n >=  10: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d AND cd_n >=  10" % wlen))
		print("        cd_n >=  25: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d AND cd_n >=  25" % wlen))
		print("        cd_n >=  50: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d AND cd_n >=  50" % wlen))
		print("        cd_n >=  75: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d AND cd_n >=  75" % wlen))
		print("        cd_n >= 100: %5d" % plm.db_get_int("SELECT COUNT(*) FROM word WHERE len < %d AND cd_n >= 100" % wlen))
