#
# TODO
#     Add method to compact database (i.e., remove sessions, datasets, and models associated with no folds)
#     Finish generating reports
#         tabulate module
#         stackoverflow.com/questions/3685195/line-up-columns-of-numbers-print-output-in-table-format
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Issues solved
#     Compressing and storing ndarray in SQLite
#         stackoverflow.com/questions/3310584/insert-binary-file-in-sqlite-database-with-python
#         stackoverflow.com/questions/18621513/python-insert-numpy-array-into-sqlite3-database
#         stackoverflow.com/questions/11760095/convert-binary-string-to-numpy-array
#     Conclusions
#         If ndarray of ints    : encode_arr > encode_str
#         If ndarray of dobules : encode_str > encode_arr
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import numpy as np
import bz2
import sqlite3

from array           import array
from collections     import OrderedDict
from sklearn.metrics import accuracy_score, brier_score_loss, cohen_kappa_score, confusion_matrix, f1_score, matthews_corrcoef, precision_score, recall_score, roc_auc_score

from util import Time


# ======================================================================================================================
class DB(object):
	'''
	Statistical model hyperparameter optimization database.

	The class caches IDs of several last used objects (i.e., session, dataset, model, fold, and result) to ease
	referencing.  For example, once a new sessions is added, there is no need to remember its ID and pass it alone
	when adding a new fold -- this class takes care of that mundane task.
	'''

	REP_COL_SEP = '  '

	DDL = OrderedDict()
	DDL['sess'] = '''
		CREATE TABLE IF NOT EXISTS sess (
		id       INTEGER PRIMARY KEY AUTOINCREMENT,
		ts       INT,
		dur      INT,
		dt       TEXT,
		host     TEXT,
		dir_ds   TEXT,
		dir_sess TEXT
		);
		'''
	DDL['schedule'] = '''
		CREATE TABLE IF NOT EXISTS schedule (
		id           INTEGER PRIMARY KEY AUTOINCREMENT,
		sess_id      INT,
		dur          INT,
		is_init      INT,
		i            INT,
		n            INT,
		is_seq       INT,
		do_gpu       INT,
		cpu_n        INT,
		cpu_n_use    INT,
		mem_proc     INT,
		mem_size     INT,
		mem_free_min INT,
		swap_size    INT,
		res_mon_int  INT,
		CONSTRAINT fk__schedule__sess FOREIGN KEY (sess_id) REFERENCES sess (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
	'''
	DDL['comp_res'] = '''
		CREATE TABLE IF NOT EXISTS comp_res (
		id           INTEGER PRIMARY KEY AUTOINCREMENT,
		schedule_id  INT,
		t_sess       INT,
		t_schedule   INT,
		schedule_m   INT,
		mem_used     INT,
		swap_used    INT,
		CONSTRAINT fk__comp_res__schedule FOREIGN KEY (schedule_id) REFERENCES schedule (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
	'''
	DDL['ds'] = '''
		CREATE TABLE IF NOT EXISTS ds (
		id            INTEGER PRIMARY KEY AUTOINCREMENT,
		name          TEXT,
		fname         TEXT,
		n_var_nts     INT,
		n_var_ts      INT,
		n_store       INT,
		n_emit        INT,
		nts_vars_lst  TEXT,
		nts_vars_idx  TEXT,
		nts_vars_name TEXT,
		size_file     INT DEFAULT NULL,
		size_mem      INT DEFAULT NULL
		);
		'''
	DDL['model'] = '''
		CREATE TABLE IF NOT EXISTS model (
		id           INTEGER PRIMARY KEY AUTOINCREMENT,
		gen          TEXT,
		arch         TEXT,
		n_epochs     INT,
		batch_size   INT,
		n_in         INT,
		size_mem     INT DEFAULT NULL
		);
		'''
		# TODO: Should n_epochs and batch_size be here or in fold?
	DDL['model_layer'] = '''
		CREATE TABLE IF NOT EXISTS model_layer (
		id       INTEGER PRIMARY KEY AUTOINCREMENT,
		model_id INT,
		num      INT,
		type     TEXT,
		units    INT,
		act      TEXT,
		CONSTRAINT fk__model_layer__model FOREIGN KEY (model_id) REFERENCES model (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
		'''
	DDL['fold'] = '''
		CREATE TABLE IF NOT EXISTS fold (
		id            INTEGER PRIMARY KEY AUTOINCREMENT,
		sess_id       INT,
		schedule_id   INT,
		ds_id         INT,
		model_id      INT,
		schedule_i    INT,
		cv_id         TEXT,
		fold          INT,
		ts            INT,
		dur           INT,
		n_tr_all      INT,
		n_va_all      INT,
		n_te_all      INT,
		n_tr          INT,
		n_va          INT,
		n_te          INT,
		fit_t         INT,
		loss          REAL,
		acc           REAL,
		mem_proc_max  INT,
		y_prob_dtype  TEXT,
		y             BLOB,
		y_prob        BLOB,
		CONSTRAINT fk__fold__sess     FOREIGN KEY (sess_id)     REFERENCES sess     (id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk__fold__schedule FOREIGN KEY (schedule_id) REFERENCES schedule (id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk__fold__ds       FOREIGN KEY (ds_id)       REFERENCES ds       (id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk__fold__model    FOREIGN KEY (model_id)    REFERENCES model    (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
		'''
	DDL['fold_fit'] = '''
		CREATE TABLE IF NOT EXISTS fold_fit (
		id      INTEGER PRIMARY KEY AUTOINCREMENT,
		fold_id INT,
		epoch   INT,
		loss_tr REAL,
		acc_tr  REAL,
		loss_va REAL,
		acc_va  REAL,
		CONSTRAINT fk__fold_fit__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
		'''
	DDL['fold_eval'] = '''
		CREATE TABLE IF NOT EXISTS fold_eval (
		id        INTEGER PRIMARY KEY AUTOINCREMENT,
		fold_id   INT,
		ds_id_ext INT DEFAULT NULL,
		tn        INT,
		fp        INT,
		fn        INT,
		tp        INT,
		spc       REAL,
		sen       REAL,
		rec       REAL,
		f1        REAL,
		brier     REAL,
		mcc       REAL,
		kappa     REAL,
		acc       REAL,
		roc_auc   REAL,
		CONSTRAINT fk__fold_eval__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
		);
		'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, fpath, rep_col_sep=REP_COL_SEP):
		self.conn  = None
		self.fpath = fpath

		do_create = not os.path.isfile(self.fpath)

		self.conn_open(self.fpath)
		if do_create: self._db_create()

		self.curr_sess_id     = None
		self.curr_schedule_id = None
		self.curr_ds_id       = None
		self.curr_model_id    = None
		self.curr_fold_id     = None

		self.rep_col_sep = rep_col_sep

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.conn_close()

	# ------------------------------------------------------------------------------------------------------------------
	def _db_create(self):
		with self.conn as c:
			c.executescript(''.join(self.DDL.values()))
			c.commit()

	# ------------------------------------------------------------------------------------------------------------------
	def _get_id(self, tbl, where, col='rowid'):
		row = self.conn.execute('SELECT {} FROM {} WHERE {}'.format(col, tbl, where)).fetchone()
		return row[0] if row else None

	# ------------------------------------------------------------------------------------------------------------------
	def _ins(self, q, args):
		with self.conn as c:
			return c.execute(q, args).lastrowid

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__arr(obj, arr_type):
		a = array(arr_type)
		a.frombytes(bz2.decompress(obj))  # BZ2 > GZ for ndarray (especially int)
		return np.asarray(a)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__pickle(obj):
		return bz2.decompress(np.loads(obj))  # BZ2 > GZ for ndarray (when n > ~500; GZ wins for smaller ones)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__str(a, dtype):
		return np.fromstring(bz2.decompress(a), dtype=dtype)  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_enc__arr(arr, arr_type):
		return bz2.compress(array(arr_type, arr))  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_encode__pickle(arr):
		return bz2.compress(arr.dumps())  # BZ2 > GZ for ndarray (when n > ~500; GZ wins for smaller ones)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dbl_enc(arr):
		return DB._ndarray_enc__str(arr)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dbl_dec(obj, dtype):
		return DB._ndarray_dec__str(obj, dtype)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_enc__str(obj):
		return bz2.compress(obj.tostring())  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_int_enc(arr):
		return DB._ndarray_enc__arr(arr, 'i')

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_int_dec(obj):
		return DB._ndarray_dec__arr(obj, 'i')

	# ------------------------------------------------------------------------------------------------------------------
	def _rep_gen(self, query, delim):
		delim = delim or self.rep_col_sep

		rep = []
		for row in self.conn.execute(query).fetchall():
			ln = []
			for field in row:
				ln.append(str(field))
			rep.append(delim.join(ln))
		return '\n'.join(rep)

	# ------------------------------------------------------------------------------------------------------------------
	def _upd(self, q, args):
		with self.conn as c:
			c.execute(q, args)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def bool(i): return 1 if i else 0

	# ------------------------------------------------------------------------------------------------------------------
	def compact(self):
		''' Compacts the databse by removing empty sessions. '''

		pass

	# ------------------------------------------------------------------------------------------------------------------
	def comp_res_add(self, schedule_id, t_sess, t_schedule, schedule_m, mem_used, swap_used):
		self._ins('''
			INSERT INTO comp_res (schedule_id, t_sess, t_schedule, schedule_m, mem_used, swap_used)
			VALUES (?, ?, ?, ?, ?, ?)
			''', [schedule_id, t_sess, t_schedule, schedule_m, mem_used, swap_used])

	# ------------------------------------------------------------------------------------------------------------------
	def conn_close(self):
		if self.conn is None: return

		# self.conn.commit()
		# self.conn.execute('PRAGMA wal_checkpoint')  # no-op for non-WAL mode DB
		# self.conn.execute('VACUUM')

		self.conn.close()
		self.conn = None

	# ------------------------------------------------------------------------------------------------------------------
	def conn_open(self, fpath=None):
		''' Connects to the designated database.  The existing connection to a database is closed beforehand. '''

		if fpath is None: return

		if (self.conn is not None):
			self.conn_close()

		self.fpath = fpath

		self.conn = sqlite3.connect(self.fpath, check_same_thread=False)
		self.conn.execute('PRAGMA foreign_keys = ON')
		self.conn.execute('PRAGMA journal_mode=WAL')  # PRAGMA journal_mode = DELETE

		# self.conn.execute('PRAGMA wal_checkpoint')  # no-op for non-WAL mode DB

		# self.conn.row_factory = sqlite3.Row

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def cv_id_gen(ds_id, model_id):
		return '{}.{}'.format(ds_id, model_id)

	# ------------------------------------------------------------------------------------------------------------------
	def ds_add(self, name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name, size_file, size_mem):
		# Exists:
		ds_id = self.ds_get_id(name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name)
		if ds_id is not None:
			self.curr_ds_id = ds_id
			return self.curr_ds_id

		# Add:
		self.curr_ds_id = self._ins('''
			INSERT INTO ds (name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name, size_file, size_mem)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?)
			''', [name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name, size_file, size_mem])

		return self.curr_ds_id

	# ------------------------------------------------------------------------------------------------------------------
	def ds_get_id(self, name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name):
		return self._get_id('ds', 'name = "{}" AND fname = "{}" AND n_var_nts = {} AND n_var_ts = {} AND n_store = {} AND n_emit = {} AND nts_vars_lst = "{}" AND nts_vars_idx = "{}" AND nts_vars_name = "{}"'.format(name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name))

	# ------------------------------------------------------------------------------------------------------------------
	def debug_disp_fold(self, id):
		y, y_prob, y_pred = self.fold_get_ys(id)

		print('y:      {}'.format(y))
		print('y_pred: {}'.format(y_pred))
		print('y_prob: {}'.format(y_prob))

		print(confusion_matrix(y, y_pred).astype(int).ravel())

	# ------------------------------------------------------------------------------------------------------------------
	def fold_add(self, schedule_id, schedule_i, fold, ts, dur, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te, fit_t, fit_hist, loss, acc, mem_proc_max, y, y_prob, sess_id=None, ds_id=None, model_id=None):
		sess_id  = sess_id  or self.curr_sess_id
		ds_id    = ds_id    or self.curr_ds_id
		model_id = model_id or self.curr_model_id

		# Fold already exists:
		if self.fold_exists(ds_id, model_id, fold):
			return None

		# Fold doesn't exist:
		with self.conn as c:
			# Add fold:
			self.curr_fold_id = c.execute('''
				INSERT INTO fold (sess_id, schedule_id, ds_id, model_id, schedule_i, cv_id, fold, ts, dur, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te, fit_t, loss, acc, mem_proc_max, y_prob_dtype, y, y_prob)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
				''', [sess_id, schedule_id, ds_id, model_id, schedule_i, DB.cv_id_gen(ds_id, model_id), fold, ts, dur, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te, fit_t, loss, acc, mem_proc_max, y_prob.dtype.name, DB._ndarray_int_enc(y), DB._ndarray_dbl_enc(y_prob)]).lastrowid

			# Add fold fit info:
			h = fit_hist  # shorthand
			values = [(self.curr_fold_id, i + 1, h['loss'][i], h['acc'][i], h['val_loss'][i], h['val_acc'][i]) for i in range(len(h['loss']))]
			c.executemany('''
				INSERT INTO fold_fit (fold_id, epoch, loss_tr, acc_tr, loss_va, acc_va)
				VALUES (?, ?, ?, ?, ?, ?)
				''', values)

		return self.curr_fold_id

		# mix: ''', [loss, acc, y_prob.dtype.name, DB._ndarray_int_enc(ts_id), DB._ndarray_int_enc(y), DB._ndarray_dbl_enc(y_prob), fold_id])
		# arr: ''', [loss, acc, 'i', 'i', 'd', DB._ndarray_enc_arr(ts_id, 'i'), DB._ndarray_enc_arr(y, 'i'), DB._ndarray_enc_arr(y_prob, 'd'), fold_id])
		# str: ''', [loss, acc, ts_id.dtype.name, y.dtype.name, y_prob.dtype.name, DB._ndarray_enc_str(ts_id), DB._ndarray_enc_str(y), DB._ndarray_enc_str(y_prob), fold_id])

	# ------------------------------------------------------------------------------------------------------------------
	def fold_comp_eval(self, y, y_prob, y_pred, fold_id, ds_id_ext=None):
		'''
		Computes and persists evaluation metrics of a model based on the arguments provided.  Both internal (i.e., the
		fit-evaluate cycle) and external (i.e., evaluate after fitting with a different but compatible dataset)
		evaluations are handles: 'ds_id_ext=None' triggers the internal one, while a non-None value triggers the
		external one.
		'''

		cm = confusion_matrix(y, y_pred).astype(int).ravel()
		tn = int(cm[0])
		fp = int(cm[1]) if len(cm) > 1 else None
		fn = int(cm[2]) if len(cm) > 1 else None
		tp = int(cm[3]) if len(cm) > 1 else None

		spc     = tn / (tn+fp) if (tp is not None and tn+fp > 0) else None
		sen     = precision_score   (y, y_pred)
		rec     = recall_score      (y, y_pred)
		f1      = f1_score          (y, y_pred)
		brier   = brier_score_loss  (y, y_prob)  # probabilities, not labels!
		mcc     = matthews_corrcoef (y, y_pred)
		kappa   = cohen_kappa_score (y, y_pred)
		acc     = accuracy_score    (y, y_pred)  # use sklearn, coz people report incorrect accuracy in Keras (may have to do with not properly removed zero-padding)

		try:
			roc_auc = roc_auc_score (y, y_prob)  # probabilities, not labels!
		except ValueError:
			# NOTE: We end up here most likely due to only one class being present in 'y' or 'y_prob' so it's not a big
			#       deal to loose this value because it's a degenerated case of a model that would be discarded anyway.
			roc_auc = None

		with self.conn as c:
			# If external evaluation, then remove existing one:
			if ds_id_ext is not None:
				c.execute('DELETE FROM fold_eval WHERE fold_id = ? AND ds_id_ext = ?', [fold_id, ds_id_ext])

			# Add the newly computed evaluation:
			c.execute('''
				INSERT INTO fold_eval (fold_id, ds_id_ext, tn, fp, fn, tp, spc, sen, rec, f1, brier, mcc, kappa, acc, roc_auc)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
				''', [fold_id, ds_id_ext, tn, fp, fn, tp, spc, sen, rec, f1, brier, mcc, kappa, acc, roc_auc])

	# ------------------------------------------------------------------------------------------------------------------
	def eval_ext_stats(self):
		''' Displays external evaluation statistics. '''

		print('Evaluation (external)')

		# (1) Overall:
		roc_auc = [float(i[0]) for i in self.conn.execute('''
			SELECT roc_auc FROM fold_eval fe
			INNER JOIN ds d ON fe.ds_id_ext = d.id
			WHERE fe.ds_id_ext IS NOT NULL AND d.n_store <= 10''').fetchall()]

		print('    Overall ({})'.format(len(roc_auc)))
		print('        roc-auc')
		print('            min   {}'.format(round(min(roc_auc), 4)))
		print('            max   {}'.format(round(max(roc_auc), 4)))
		print('            mean  {}'.format(round(sum(roc_auc) / len(roc_auc), 4)))

		# (2) By dataset:
		print('    By dataset')
		for ds in self.conn.execute('SELECT DISTINCT name FROM ds').fetchall():
			roc_auc = [float(i[0]) for i in self.conn.execute('''
				SELECT roc_auc FROM fold_eval fe
				INNER JOIN ds d ON fe.ds_id_ext = d.id
				WHERE d.name = ? AND d.n_store <= 10
				''', [ds[0]]).fetchall()]

			print('        {} ({})'.format(ds[0], len(roc_auc)))
			print('            roc-auc')
			print('                min   {}'.format(round(min(roc_auc), 4)))
			print('                max   {}'.format(round(max(roc_auc), 4)))
			print('                mean  {}'.format(round(sum(roc_auc) / len(roc_auc), 4)))

	# ------------------------------------------------------------------------------------------------------------------
	def eval_int_stats(self):
		''' Displays internal evaluation statistics. '''

		print('Evaluation (internal)')

		# (1) Overall:
		roc_auc = [float(i[0]) for i in self.conn.execute('''
			SELECT roc_auc FROM fold_eval fe
			INNER JOIN fold f ON fe.fold_id = f.id
			INNER JOIN ds d ON f.ds_id = d.id
			WHERE fe.ds_id_ext IS NULL AND d.n_store <= 10''').fetchall()]

		print('    Overall ({})'.format(len(roc_auc)))
		print('        roc-auc')
		print('            min   {}'.format(round(min(roc_auc), 4)))
		print('            max   {}'.format(round(max(roc_auc), 4)))
		print('            mean  {}'.format(round(sum(roc_auc) / len(roc_auc), 4)))

		# (2) By dataset:
		print('    By dataset')
		for ds in self.conn.execute('SELECT DISTINCT name FROM ds').fetchall():
			roc_auc = [float(i[0]) for i in self.conn.execute('''
				SELECT roc_auc FROM fold_eval fe
				INNER JOIN fold f ON fe.fold_id = f.id
				INNER JOIN ds d ON f.ds_id = d.id
				WHERE d.name = ? AND d.n_store <= 10
				''', [ds[0]]).fetchall()]

			print('        {} ({})'.format(ds[0], len(roc_auc)))
			print('            roc-auc')
			print('                min   {}'.format(round(min(roc_auc), 4)))
			print('                max   {}'.format(round(max(roc_auc), 4)))
			print('                mean  {}'.format(round(sum(roc_auc) / len(roc_auc), 4)))

	# ------------------------------------------------------------------------------------------------------------------
	def fold_comp_eval_ext(self, y, y_prob, y_pred, fold_id, ds_id_ext):
		''' Computes external evaluation metrics for the fold, dataset, and data provided. '''

		self.fold_comp_eval(y, y_prob, y_pred, fold_id, ds_id_ext)

	# ------------------------------------------------------------------------------------------------------------------
	# scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
	#
	# ROC curve plots
	#     scikit-learn.org/stable/modules/generated/sklearn.metrics.auc.html#sklearn.metrics.auc
	#     scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py
	#     scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
	#
	def fold_comp_eval_int_all(self):
		'''
		Computes the internal evaluation metrics for all folds in the database.  This does not affect any existing
		external evaluation records.
		'''

		with self.conn as c:
			c.executescript('DELETE FROM fold_eval WHERE ds_id_ext IS NULL; VACUUM;')

			for row in self.conn.execute('SELECT id FROM fold').fetchall():
				fold_id = row[0]

				y, y_prob, y_pred = self.fold_get_ys(fold_id)

				self.fold_comp_eval(y, y_prob, y_pred, fold_id)

	# ------------------------------------------------------------------------------------------------------------------
	def fold_exists(self, ds_id, model_id, fold):
		n = self.conn.execute('''
			SELECT COUNT(*) FROM fold WHERE cv_id = ? AND fold = ?
			''', [DB.cv_id_gen(ds_id, model_id), fold]).fetchone()[0]
		return n > 0

	# ------------------------------------------------------------------------------------------------------------------
	def fold_get_schedule_n(self, schedule_id=None):
		schedule_id = schedule_id or self.curr_schedule_id
		return self.conn.execute('SELECT COUNT(*) FROM fold WHERE schedule_id = ?', [schedule_id]).fetchone()[0]

	# ------------------------------------------------------------------------------------------------------------------
	def fold_get_sess_n(self, sess_id=None):
		sess_id = sess_id or self.curr_sess_id
		return self.conn.execute('SELECT COUNT(*) FROM fold WHERE sess_id = ?', [sess_id]).fetchone()[0]

	# ------------------------------------------------------------------------------------------------------------------
	def fold_get_ys(self, fold_id):
		row = self.conn.execute('SELECT y, y_prob, y_prob_dtype FROM fold WHERE id = ?', [fold_id]).fetchone()

		y      = DB._ndarray_int_dec(row[0])
		y_prob = DB._ndarray_dbl_dec(row[1], row[2])
		y_pred = DB.np_prob_to_pred(y_prob)

		return (y, y_prob, y_pred)

	# ------------------------------------------------------------------------------------------------------------------
	def model_add(self, gen, arch, layers, n_epochs, batch_size, n_in, size_mem):
		# Exists:
		model_id = self.model_get_id(gen, arch, layers, n_epochs, batch_size, n_in)
		if model_id is not None:
			self.curr_model_id = model_id
			return self.curr_model_id

		# Add:
		with self.conn as c:
			self.curr_model_id = c.execute('''
				INSERT INTO model (gen, arch, n_epochs, batch_size, n_in, size_mem)
				VALUES (?, ?, ?, ?, ?, ?)
				''', [gen, arch, n_epochs, batch_size, n_in, size_mem]).lastrowid

			for l in layers:
				c.execute('''
					INSERT INTO model_layer (model_id, num, type, units, act)
					VALUES (?, ?, ?, ?, ?)
					''', [self.curr_model_id, l.num, l.type, l.units, l.act])

		return self.curr_model_id

	# ------------------------------------------------------------------------------------------------------------------
	def model_get_id(self, gen, arch, layers, n_epochs, batch_size, n_in):
		return self._get_id('model', 'gen = "{}" AND arch = "{}" AND n_epochs = {} AND batch_size = {} AND n_in = {}'.format(gen, arch, n_epochs, batch_size, n_in))

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def np_prob_to_pred(y_prob):
		''' Used to reconstruct 'y_pred' from 'y_prob' to eliminate the need to store 'y_pred'. '''

		return np.where(y_prob > 0.5, 1, 0)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_basic(self, delim=None, sess_id=None):
		sess_id = sess_id or self.curr_sess_id

		q = '''
			SELECT
				s.id, sc.id, m.id, d.id, f.id, SUBSTR(s.dt, 1, 10), f.schedule_i, f.cv_id,
				m.gen, m.arch, ml.n, m.n_epochs, m.batch_size, m.n_in,
				d.name, d.fname, d.n_store, d.n_emit, d.n_var_nts, d.n_var_ts, d.nts_vars_name,
				f.fold, f.dur, f.n_tr_all, f.n_va_all, f.n_te_all, f.n_tr, f.n_va, f.n_te, f.fit_t, ff.n,
				ROUND(ff.acc_tr_max, 4), ROUND(ff.acc_va_max, 4), ROUND(f.acc, 4), LENGTH(f.y), LENGTH(f.y_prob)
			FROM fold f
			INNER JOIN sess s ON s.id = f.sess_id
			INNER JOIN schedule sc ON sc.id = f.schedule_id
			INNER JOIN model m ON m.id = f.model_id
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN (SELECT fold_id, COUNT(*) AS n, MAX(acc_tr) AS acc_tr_max, MAX(acc_va) AS acc_va_max FROM fold_fit GROUP BY fold_id) ff ON ff.fold_id = f.id
			LEFT  JOIN (SELECT model_id, COUNT(*) AS n FROM model_layer GROUP BY model_id) ml ON m.id = ml.model_id
			{}
			GROUP BY m.id, d.id, f.id
			ORDER BY f.schedule_i, f.fold
			'''.format('WHERE s.id = {}'.format(sess_id) if (sess_id > 0) else '')

		return self._rep_gen(q, delim)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_ds(self):
		return self._rep_gen('SELECT * FROM ds', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_model(self):
		return self._rep_gen('SELECT * FROM model', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_sess(self):
		return self._rep_gen('SELECT * FROM sess', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_add(self, ts, host, dir_ds, dir_sess):
		# Exists:
		sess_id = self._get_id('sess', 'ts = {}'.format(ts))
		if sess_id is not None:
			self.curr_sess_id = sess_id
			return self.curr_sess_id

		# Add:
		self.curr_sess_id = self._ins('''
			INSERT INTO sess (ts, dt, host, dir_ds, dir_sess)
			VALUES (?, ?, ?, ?, ?)
			''', [ts, Time.ts2dt(ts), host, dir_ds, dir_sess])

		return self.curr_sess_id

	# ------------------------------------------------------------------------------------------------------------------
	def sess_set_dur(self, dur, sess_id=None):
		sess_id = sess_id or self.curr_sess_id
		self._upd('UPDATE sess SET dur = ? WHERE id = ?', [dur, sess_id])

	# ------------------------------------------------------------------------------------------------------------------
	def schedule_add(self, is_init, i, n, is_seq, do_gpu, cpu_n, cpu_n_use, mem_proc, mem_size, mem_free_min, swap_size, res_mon_int, sess_id=None):
		sess_id  = sess_id or self.curr_sess_id

		# Exists:
		schedule_id = self.schedule_get_id(sess_id, is_init, i, n)
		if schedule_id is not None:
			self.curr_schedule_id = schedule_id
			return self.curr_schedule_id

		# Add:
		self.curr_schedule_id = self._ins('''
			INSERT INTO schedule (sess_id, is_init, i, n, is_seq, do_gpu, cpu_n, cpu_n_use, mem_proc, mem_size, mem_free_min, swap_size, res_mon_int)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			''', [sess_id, int(is_init), i, n, int(is_seq), int(do_gpu), cpu_n, cpu_n_use, mem_proc, mem_size, mem_free_min, swap_size, res_mon_int])

		return self.curr_schedule_id

	# ------------------------------------------------------------------------------------------------------------------
	def schedule_get_id(self, sess_id, is_init, i, n):
		return self._get_id('schedule', 'sess_id = {} AND is_init = {} AND i = {} AND n = {}'.format(sess_id, int(is_init), i, n))

	# ------------------------------------------------------------------------------------------------------------------
	def schedule_set_dur(self, dur, schedule_id=None):
		schedule_id = schedule_id or self.curr_schedule_id
		self._upd('UPDATE schedule SET dur = ? WHERE id = ?', [dur, schedule_id])

	# ------------------------------------------------------------------------------------------------------------------
	def vacuum(self):
		self.conn.execute('VACUUM')

	# ------------------------------------------------------------------------------------------------------------------
	# def x(self):
	# 	from pathlib import Path
	# 	dir_models = os.path.join(Path.home(), 'data', 'eye-link', 'nn-comp', '1508134770060', 'models')
	#
	# 	for row in self.conn.execute('SELECT id, sess_id, batch_id, fold FROM fold').fetchall():
	# 		fname_src = '{}-{}-{}.h5.gz'.format(row[1], row[2], row[3])
	# 		fname_dst = '{}.h5.gz'.format(row[0])
	#
	# 		fpath_src = os.path.join(dir_models, fname_src)
	# 		fpath_dst = os.path.join(dir_models, fname_dst)
	#
	# 		print('{} --> {}'.format(fpath_src, fpath_dst))
	# 		# os.rename(fpath_src, fpath_dst)

	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn-comp', 'db.sqlite3'))
	# db.x()


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	# fpath_db = os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3')
	fpath_db = os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess-comp', 'db.sqlite3')
	db = DB(fpath_db)

	# Compute internal evaluation for all folds:
	# db.fold_comp_eval_int_all()

	# Display evaluation statistics:
	db.eval_int_stats()
	db.eval_ext_stats()

	# Display fold:
	# db.debug_disp_fold(3933)

	# Show basic report:
	# print(db.rep_basic(sess_id=14))
