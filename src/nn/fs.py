import os

from pathlib import Path


# ======================================================================================================================
class FileSystem(object):
	'''
	Handle all filesystem related operations (e.g., creating directory structure).

	This class is used by all parts of the deep learning toolkit.  By abtracting this functionality as a dedicated
	class I create a one place where any filesystem related changes (e.g., the location of the datasets or output
	directories) are made.  Moreover, this class can handle different execution environments (e.g., the development
	computer and the computational box at the LRDC) seemlesly.
	'''

	DNAME_SESS      = 'sess'
	DNAME_SESS_COMP = 'sess-comp'
	DNAME_MODELS    = 'models'

	FNAME_DB       = 'db.sqlite3'
	FNAME_DB_DEBUG = 'db-debug.sqlite3'

	DIR_ROOT      = os.path.join(str(Path.home()), 'data', 'eye-link')
	DIR_DS        = os.path.join(DIR_ROOT, 'ds', 'out')
	DIR_OUT       = os.path.join(DIR_ROOT, 'nn')
	DIR_SESS      = os.path.join(DIR_OUT, DNAME_SESS)
	DIR_SESS_COMP = os.path.join(DIR_OUT, DNAME_SESS_COMP)

	PATH_DB       = os.path.join(DIR_SESS,      FNAME_DB)
	PATH_DB_DEBUG = os.path.join(DIR_SESS,      FNAME_DB_DEBUG)
	PATH_DB_COMP  = os.path.join(DIR_SESS_COMP, FNAME_DB)

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self):
		if not os.path.exists(self.DIR_DS)   : os.makedirs(self.DIR_DS)
		if not os.path.exists(self.DIR_OUT)  : os.makedirs(self.DIR_OUT)
		if not os.path.exists(self.DIR_SESS) : os.makedirs(self.DIR_SESS)
