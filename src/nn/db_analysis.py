#
# TODO
#     Only select folds with len(fold.y) > 10 or so and with fold_eval.fp being non null (i.e., with both classes in the y)
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Matplotlib
#     Gallery
#         matplotlib.org/gallery.html
#     Subplots
#         jonathansoma.com/lede/data-studio/classes/small-multiples/long-explanation-of-using-plt-subplots-to-create-small-multiples
#         stackoverflow.com/questions/20057260/how-to-remove-gaps-between-subplots-in-matplotlib
#         stackoverflow.com/questions/19932553/size-of-figure-when-using-plt-subplots
#         Unequal sized grids
#             scientificallysound.org/2016/06/09/matplotlib-how-to-plot-subplots-of-unequal-sizes
#     Axes ticks
#         Frequency
#             stackoverflow.com/questions/12608788/changing-the-tick-frequency-on-x-or-y-axis-in-matplotlib
#         Direction
#             matplotlib.org/devdocs/api/_as_gen/matplotlib.axes.Axes.tick_params.html
#             stackoverflow.com/questions/6260055/in-matplotlib-how-do-you-draw-r-style-axis-ticks-that-point-outward-from-the-ax
#     Legend
#         http://matplotlib.org/users/legend_guide.html
#     Pattern fills
#         www.packtpub.com/mapt/book/big_data_and_business_intelligence/9781849513265/2/ch02lvl1sec32/controlling-a-fill-pattern
#         www.collindelker.com/2013/03/11/matplotlib-hatch-spacing.html
#         matplotlib.org/devdocs/gallery/lines_bars_and_markers/marker_fillstyle_reference.html#sphx-glr-gallery-lines-bars-and-markers-marker-fillstyle-reference-py
#     Styles
#         Lines   : matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html
#         Markers
#             matplotlib.org/api/markers_api.html?highlight=marker#module-matplotlib.markers
#             matplotlib.org/examples/lines_bars_and_markers/marker_reference.html
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Seaborn
#     Grid plots
#         seaborn.pydata.org/tutorial/axis_grids.html
#     Violin and box plots
#         seaborn.pydata.org/generated/seaborn.violinplot.html
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import itertools
import math
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import re
import sqlite3

from db   import DB
from util import Time


# ======================================================================================================================
class DBAnalysis(object):

	DPI = 200

	DS = ['2010', '2011 aa', '2011 gg']

	TS = [(2, 1), (3, 1), (5, 1), (5, 2), (10, 1), (10, 2), (10, 5), (20, 2), (20, 5)]

	COL_EDGE = { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' }
	COL_FACE = {
		'none'          : { DS[0]: 'none',    DS[1]: 'none',    DS[2]: 'none'    },
		'rspd'          : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'rspd.time'     : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'rspd.time.pre' : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'all'           : { DS[0]: '#000000', DS[1]: '#000000', DS[2]: '#000000' }
	}
	HATCH = { 'none': '', 'all': '', 'rspd': '', 'rspd.time': '||||||||', 'rspd.time.pre': '////////////' }
	MARKER = { '-': 's', 'relu': 'o', 'tanh': 'X' }

	N_TE_MIN = 15
		# the minimum number of observations a test set must have in order for the fold associated with it to be
		# considered for plotting


	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, fpath):
		self.fpath = fpath

		self.db = DB(fpath)
		self.conn = self.db.conn

	# ------------------------------------------------------------------------------------------------------------------
	def _idx_roc(self, orderby, model_gen, ds=None, title=None, fn_xy=None, fn_ts=None, fpath=None):
		'''
		fpath
			None   -->  show figure
			'...'  -->  save figure
		'''

		y_range = (0.4, 1.0)  # math.floor(round(self.conn.execute('SELECT MIN(roc_auc) FROM fold_eval').fetchone()[0]) * 10) / 10.0

		ds = ds or self.DS

		# (1) Prepare data:
		q = '''
			SELECT
				d.name AS ds_name,
				d.nts_vars_name AS ds_nts_vars,
				d.n_store AS ds_n_store,
				d.n_emit AS ds_n_emit,
				ml.units AS model_units,
				ml.act AS model_act,
				ROUND(AVG(fe.roc_auc), 4) AS eval_roc_auc
			FROM fold f
			INNER JOIN model m ON m.id = f.model_id
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval GROUP BY fold_id) fe ON fe.fold_id = f.id
			LEFT  JOIN (SELECT DISTINCT model_id, units, act FROM model_layer GROUP BY model_id) ml ON ml.model_id = m.id
			WHERE m.gen = "{}" AND d.name IN ("{}") AND f.n_te >= {}
			GROUP BY f.cv_id, m.batch_size
			ORDER BY {}
			'''.format(model_gen, '","'.join(ds), self.N_TE_MIN, orderby)

		df = pd.read_sql_query(q, self.conn, params=None)
		df.insert(0, 'idx', range(0, len(df)))
		df['ds_name_full'] = df.apply(lambda row: '{} {} {} {}'.format(row['ds_name'], row['model_units'], row['model_act'], row['ds_nts_vars']), axis=1)

		# plt.style.use('classic')

		# (2) Init plot:
		fig = plt.figure(figsize=(20, 10))
		fig.subplots_adjust(left=0.035, bottom=0.06, top=0.99 if title is None else 0.97, right=0.9975)

		ax = plt.subplot()
		ax.margins(0.01, 0.0)

		# (3) Grid:
		ax.axhline(y=0.5, color='black', linestyle='--', linewidth=1, alpha=0.5, label='chance level', antialiased=True)
		for y in [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
			ax.axhline(y=y, color='black', linestyle=':', linewidth=1, alpha=0.25, antialiased=True)

		# (4) Series:
		if fn_xy is not None: fn_xy(df, y_range[0], ax, self.MARKER, self.COL_EDGE, self.COL_FACE, self.HATCH)
		if fn_ts is not None: fn_ts(df, y_range[0], ax, self.COL_EDGE)

		# (5) Axes:
		ax.tick_params(axis='both', which='both', direction='in')  # labelsize, labelrotation, pad
		ax.set_ylim([y_range[0], y_range[1]])
		ax.set_xlabel('Index', fontsize=15)
		ax.set_ylabel('Mean ROC AUC  (cross-validation, batch size)', fontsize=15)

		# (6) Titles, legends, and anotations:
		if fn_xy != self._idx_roc__xy_ts: ax.legend(ncol=4, framealpha=0.5)

		if title is not None: plt.title(title)

		# (7) Show or save:
		if fpath is not None: plt.savefig('{}.png'.format(fpath), dpi=self.DPI)
		else: plt.show()

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__ts(self, df, y_min, ax, col_edge):
		y_ts_ln  = y_min + 0.02
		y_ts_txt = y_ts_ln + 0.008

		for i, ds in enumerate(self.DS):
			n_store_prev = -1
			for name, group in df.where(df['ds_name'] == ds).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
				x_ln_min = group.idx.iloc[0]
				x_ln_max = group.idx.iloc[-1]

				ax.plot((x_ln_min, x_ln_max), (y_ts_ln, y_ts_ln), color=col_edge[name[0]], linestyle='-', linewidth=1, alpha=0.5, antialiased=True)

				n_store = int(name[3])
				if n_store in [i for (i, j) in self.TS] and n_store != n_store_prev:
					ax.text(x_ln_min, y_ts_txt, str(n_store), fontsize=6, verticalalignment='top')
					n_store_prev = n_store

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__ts_ds(self, df, y_min, ax, col_edge):
		y_ds     = 0.02
		y_ts_ln  = y_min + 0.02 + len(self.DS) * y_ds
		y_ts_txt = y_ts_ln + 0.008

		for i, ds in enumerate(self.DS):
			n_store_prev = -1
			for name, group in df.where(df['ds_name'] == ds).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
				x_ln_min = group.idx.iloc[0]
				x_ln_max = group.idx.iloc[-1]

				ax.plot((x_ln_min, x_ln_max), (y_ts_ln, y_ts_ln), color=col_edge[name[0]], linestyle='-', linewidth=1, alpha=0.5, antialiased=True)

				n_store = int(name[3])
				if n_store in [2, 5, 10] and n_store != n_store_prev:
					ax.text(x_ln_min, y_ts_txt, str(n_store), fontsize=6, verticalalignment='top')
					n_store_prev = n_store

			y_ts_ln  -= y_ds
			y_ts_txt -= y_ds

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__xy(self, df, y_min, ax, marker, col_edge, col_face, hatch):
		for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'ds_nts_vars', 'ds_name_full']):
			ax.scatter(group.idx, group.eval_roc_auc, marker=marker[name[2]], s=round(name[1] * 2), edgecolors=col_edge[name[0]], facecolor=col_face[name[3]][name[0]], hatch=hatch[name[3]], alpha=0.4, label=name[4], antialiased=True)

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__xy_ts(self, df, y_min, ax, marker, col_edge, col_face, hatch):
		for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'ds_nts_vars', 'ds_name_full', 'ds_n_store', 'ds_n_emit']):
			ax.scatter(group.idx, group.eval_roc_auc, marker=r'${}.{}$'.format(name[5], name[6]), s=round(name[1] * 4), edgecolors='none', facecolor=col_face['rspd'][name[0]], antialiased=True)

	# ------------------------------------------------------------------------------------------------------------------
	def grid(self, fpath):
		y_range = (0.4, 1.0)  # math.floor(round(self.conn.execute('SELECT MIN(roc_auc) FROM fold_eval').fetchone()[0]) * 10) / 10.0

		# Prepare dataset (DS) and time series (TS) data to be iterated over:
		# ds_nts_vars = [row[0] for row in self.conn.execute('SELECT DISTINCT nts_vars_name FROM ds').fetchall()]
		ds_nts_vars = ['none']
		DS = [('2010', 'none')] + [(i, j) for (i, j) in itertools.product(['2011 aa', '2011 gg'], ds_nts_vars)]
		TS = self.TS

		# Init the figure:
		fig, axarr = plt.subplots(nrows=len(DS), ncols=len(TS), sharex='col', sharey='row', figsize=(18, 11))
		fig.subplots_adjust(left=0.043, bottom=0.035, top=0.96, right=0.96, wspace=0, hspace=0.05)

		# Generate subplots (one for each combination of dataset and time series):
		for (i_ds, ds), (i_ts, ts) in itertools.product(enumerate(DS), enumerate(TS)):
			# Prepare data:
			q = '''
				SELECT
					COUNT(*) AS n,
					d.name AS ds_name,
					d.nts_vars_name AS ds_nts_vars,
					d.n_store AS ds_n_store,
					d.n_emit AS ds_n_emit,
					IFNULL(ml.units, 20) AS model_units,
					IFNULL(ml.act, '-') AS model_act,
					MIN(fe.roc_auc) AS eval_roc_auc_min,
					MAX(fe.roc_auc) AS eval_roc_auc_max,
					AVG(fe.roc_auc) AS eval_roc_auc_m
				FROM fold f
				INNER JOIN model m ON m.id = f.model_id
				INNER JOIN ds d ON d.id = f.ds_id
				INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval WHERE ds_id_ext IS NOT NULL GROUP BY fold_id) fe ON fe.fold_id = f.id
				LEFT  JOIN (SELECT model_id, units, act FROM model_layer GROUP BY model_id) ml ON ml.model_id = m.id
				WHERE d.name = "{}" AND d.n_store = {} AND d.n_emit = {} AND d.nts_vars_name = "{}" AND f.n_te >= {}
				GROUP BY d.name, d.nts_vars_name, d.n_store, d.n_emit, ml.units, ml.act
				ORDER BY ml.act, ml.units
				'''.format(ds[0], ts[0], ts[1], ds[1], self.N_TE_MIN)
				# INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval WHERE ds_id_ext IS NULL GROUP BY fold_id) fe ON fe.fold_id = f.id

			df = pd.read_sql_query(q, self.conn, params=None)
			df.insert(0, 'idx', range(0, len(df)))
			df.to_csv('/tmp/a-{}-{}.{}.txt'.format(ds[0], ts[0], ts[1]), sep='\t')
			if len(df) == 0:
				continue

			df['ds_name_full'] = df.apply(lambda row: '{} {} {} {}'.format(row['ds_name'], row['model_units'], row['model_act'], row['ds_nts_vars']), axis=1)

			ax = axarr[i_ds, i_ts]
			ax.margins(0.01, 0.0)

			# Grid:
			ax.axhline(y=0.5, color='black', linestyle='--', linewidth=1, alpha=0.25, antialiased=True)
			for y in [0.7, 0.9]:  # [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
				ax.axhline(y=y, color='black', linestyle=':', linewidth=1, alpha=0.25, antialiased=True)

			# Series:
			# for name, group in df.where(df['ds_name'] == ds).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
			for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'ds_nts_vars', 'ds_name_full']):
				# group[group['model_units'] == 1]['model_units'] = 20

				ax.scatter(group.idx, group.eval_roc_auc_max, marker=self.MARKER[name[2]], s=round(name[1] * 2), edgecolors=self.COL_EDGE[name[0]], facecolors=self.COL_FACE[name[3]][name[0]], hatch=self.HATCH[name[3]], alpha=0.4, antialiased=True)  # label=name[4]
				ax.scatter(group.idx, group.eval_roc_auc_m,   marker='_', s=16, edgecolors='k', facecolors='k', alpha=0.4, antialiased=True)
				ax.scatter(group.idx, group.eval_roc_auc_min, marker='v', s=12, edgecolors='k', facecolors='k', alpha=0.4, antialiased=True)
					# from matplotlib.markers import CARETDOWN

			# Axes:
			ax.set_xlim([-1, len(df['idx'])])
			ax.set_ylim([y_range[0], y_range[1]])

			if i_ds == len(DS) - 1: ax.tick_params(axis='x', which='both', direction='in', labelsize=9)
			else:                   ax.tick_params(axis='x', which='both', bottom='off', labelbottom='off')

			if i_ts == len(TS) - 1: ax.tick_params(axis='y', which='both', right='on', labelright='on', direction='in', labelsize=9)
			else:                   ax.tick_params(axis='y', which='both', left='off', labelleft='off')

			if i_ts == 0:
				ax.set_ylabel('\n'.join(ds), fontsize=9)
				ax.set_yticks(list(np.arange(y_range[0] + 0.1, y_range[1], 0.1)) + [y_range[1]])  # ([0.6, 0.7, 0.8, 0.9])
				ax.set_yticklabels(list(np.arange(y_range[0] + 0.1, y_range[1], 0.1)))
			if i_ds == len(DS) - 1:
				ax.set_xticks(range(len(df)))
				ax.set_xticklabels([1] + list(df['model_units'][1:]), fontsize=8)  # rotation=90
					# [1] is for LogReg model

			# Titles:
			if i_ds == 0: ax.set_title('{}.{}'.format(ts[0], ts[1]), fontsize=9)

			# Legends:
			# Activation function legend:
			if (i_ds in [0, 1, 6]) and (i_ts == 0):
				relu = ax.scatter([], [], edgecolors=self.COL_EDGE[ds[0]], facecolors=self.COL_FACE[df['ds_nts_vars'][0]][ds[0]], marker=self.MARKER['relu'], s=32, label='relu', alpha=0.4)
				tanh = ax.scatter([], [], edgecolors=self.COL_EDGE[ds[0]], facecolors=self.COL_FACE[df['ds_nts_vars'][0]][ds[0]], marker=self.MARKER['tanh'], s=32, label='tanh', alpha=0.4)
				ax.legend(handles=[relu, tanh], framealpha=0.5, fontsize=8)

			# Mean and minimum legend:
			if (i_ds == len(DS) - 1) and (i_ts == 0):
				val_min = ax.scatter([], [], edgecolors='k', facecolors='k', marker='v', s=16, label='min',  alpha=0.4)
				val_m   = ax.scatter([], [], edgecolors='k', facecolors='k', marker='_', s=12, label='mean', alpha=0.4)
				ax.legend(handles=[val_m, val_min], framealpha=0.5, fontsize=8)

			# Annotations:
			# Subplot ordinal number:
			# ax.text(-0.92, 0.97, str(i_ts + 1 + i_ds * len(TS)), ha='left', va='top', alpha=0.5, fontsize=8)
			ax.text(-0.9, 0.97, '{}{}'.format(chr(65 + i_ds), i_ts + 1), ha='left', va='top', alpha=0.5, fontsize=8)

		# Plot text:
		fig.text(0.5,    0.985, 'Time series definition (words)', ha='center', fontsize=11, weight='bold')
		fig.text(0.5,    0.005, 'Units',                          ha='center', fontsize=11)
		fig.text(0.0025, 0.5,   'Dataset',                        va='center', fontsize=11, weight='bold', rotation='vertical')
		fig.text(0.985,  0.5,   'ROC AUC (10-fold CV avg)',       va='center', fontsize=11, rotation='vertical')

		# Show or save:
		if fpath is not None: plt.savefig('{}.png'.format(fpath), dpi=self.DPI)
		else: plt.show()

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__batch_ts(self, model_gen, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: computation batch, plot: time series def '''

		orderby = 'ds_name, model_act, model_units, ds_n_store, ds_n_emit'
		title   = 'Order by: Computation batch'
		fpath   = os.path.join(dir_out, '{}idx-roc--batch-ts{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, model_gen, ds, title, self._idx_roc__xy_ts, self._idx_roc__ts, fpath)

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__batch_xy(self, model_gen, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: computation batch, plot: markers '''

		orderby = 'ds_name, model_act, model_units, ds_n_store, ds_n_emit'
		title   = 'Order by: Computation batch'
		fpath   = os.path.join(dir_out, '{}idx-roc--batch-xy{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, model_gen, ds, title, self._idx_roc__xy, self._idx_roc__ts, fpath)

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__roc(self, model_gen, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: roc.auc, plot: markers '''

		orderby = 'eval_roc_auc DESC, ds_n_store, ds_n_emit'
		title   = 'Order by: Mean ROC AUC'
		fpath   = os.path.join(dir_out, '{}idx-roc--roc{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, model_gen, ds, title, self._idx_roc__xy, self._idx_roc__ts_ds, fpath)


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	from util import FS


	dir_root = FS.dir_mk((Path.home(), 'data', 'eye-link', 'nn'))
	dir_out  = FS.dir_mk((dir_root, 'out'))

	fpath_db = os.path.join(dir_root, 'sess-comp', 'db.sqlite3')

	dba = DBAnalysis(fpath_db)

	# (1) Show:
	model_gen = 'FC'
	# model_gen = 'LogReg'

	# ds = None
	# ds = ['2010']
	# ds = ['2011 aa']
	# ds = ['2011 gg']

	# dba.idx_roc__batch_xy (model_gen, ds)
	# dba.idx_roc__batch_ts (model_gen, ds)
	# dba.idx_roc__roc      (model_gen, ds)

	# dba.grid(None)

	# (2) Save:
	# dba.idx_roc__batch_xy (model_gen, None,        '01.', '',          True)
	# dba.idx_roc__batch_ts (model_gen, None,        '02.', '',          True)
	#
	# dba.idx_roc__batch_xy (model_gen, ['2010'],    '03.', '--2010',    True)
	# dba.idx_roc__batch_ts (model_gen, ['2010'],    '04.', '--2010',    True)
	#
	# dba.idx_roc__batch_xy (model_gen, ['2011 aa'], '05.', '--2011-aa', True)
	# dba.idx_roc__batch_ts (model_gen, ['2011 aa'], '06.', '--2011-aa', True)
	#
	# dba.idx_roc__batch_xy (model_gen, ['2011 gg'], '07.', '--2011-gg', True)
	# dba.idx_roc__batch_ts (model_gen, ['2011 gg'], '08.', '--2011-gg', True)
	#
	# dba.idx_roc__roc      (model_gen, None,        '09.', '',          True)
	# dba.idx_roc__roc      (model_gen, ['2010'],    '10.', '--2010',    True)
	# dba.idx_roc__roc      (model_gen, ['2011 aa'], '11.', '--2011-aa', True)
	# dba.idx_roc__roc      (model_gen, ['2011 gg'], '12.', '--2011-gg', True)
	#
	# dba.grid(os.path.join(dir_out, '13.top_n'))
	# dba.grid(os.path.join(dir_out, '13.top_n - ext eval'))
