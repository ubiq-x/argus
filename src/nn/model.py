import numpy as np
import tempfile
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import psutil

from abc             import abstractmethod
from dotmap          import DotMap
from functools       import partial
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models    import load_model

from db   import DB
from util import FS, Time


# ======================================================================================================================
class Model(object):
	DS_DELIM = '\t'

	GEN = '?'  # generation

	VERBOSE_EVAL = 0
	VERBOSE_FIT  = 0

	STDOUT_INDENT = '    '

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_ds, dir_out, db=None, fpath_db=None):
		self.dir_ds  = dir_ds
		self.dir_out = dir_out
		self.fpath_db = fpath_db

		self.db = db
		if self.db is None and self.fpath_db is not None:
			self.db = DB(self.fpath_db)

		self.ds    = None
		self.model = None

		self.proc = psutil.Process(os.getpid())
		self.mem_proc_max = self.proc.memory_full_info().uss
		self.is_proc_killable = True  # set to False when persisting results (which involves the FS and the DB)

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if (self.db is not None) and (self.fpath_db is not None):  # the DB object has not been passed as an argument, but created in the constructor
			self.db.conn_close()

		if self.ds    is not None: del self.ds
		if self.model is not None: del self.model

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def _ds_name_get(self, ds, ts):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	# @staticmethod
	# def _fpath_get_stat(path, fname):
	# 	if path is None:
	# 		return fname
	# 	else:
	# 		return os.path.join(path, fname)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load(self, fold):
		self.ds.tr = self.data_load_one(fold, ['tr'])
		self.ds.va = self.data_load_one(fold, ['va'])
		self.ds.te = self.data_load_one(fold, ['te'])

		self.mem_proc_max = max(self.mem_proc_max, self.proc.memory_full_info().uss)

	# ----^-------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def data_load_one(self, fold, ds_suff_ls, is_init=False):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def memsize_ds(self):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def memsize_model(batch_size, model):
		from keras import backend as kb

		n_shape = 0
		for l in model.layers:
			l_mem = 1  # memory of the layer l
			for s in l.output_shape:
				if s is None:
					continue
				l_mem *= s
			n_shape += l_mem

		n_param_t  = np.sum([kb.count_params(p) for p in set(model.trainable_weights    )])  # trainable parameters
		n_param_nt = np.sum([kb.count_params(p) for p in set(model.non_trainable_weights)])  # non-trainable parameters

		mem_tot = 4.0 * batch_size * (n_shape + n_param_t + n_param_nt)

		return mem_tot

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def model_def(self, fold, is_init=False):
		''' Define the model. '''

		pass

	# ------------------------------------------------------------------------------------------------------------------
	def model_eval_int(self, fold):
		''' Evaluate the model as the last step in the fitting process (i.e., using the test dataset). '''

		x = self.ds.te.x
		y = self.ds.te.y

		y_prob = self.model.predict(x)

		loss, acc = self.model.evaluate(x, y, batch_size=self.batch_size, verbose=self.VERBOSE_EVAL)
			# Score is the evaluation of the loss function for a given input. Training a network is finding parameters
			# that minimize a loss function (or cost function). The cost function here is the binary_crossentropy. For
			# a target T and a network output O, the binary crossentropy can defined as:
			#
			#     f(T,O) = -(T*log(O) + (1-T)*log(1-O))
			#
			# So the score you see is the evaluation of that.
			#
			# [stackoverflow.com/questions/43589842/test-score-vs-test-accuracy-when-evaluating-model-using-keras]

			# def binary_accuracy(y_true, y_pred):
			#     return kb.mean(kb.equal(y_true, kb.round(y_pred)))
			#
			# [github.com/fchollet/keras/blob/master/keras/metrics.py]

		self.mem_proc_max = max(self.mem_proc_max, self.proc.memory_full_info().uss)

		return DotMap(loss=loss, acc=acc, y=y, y_prob=y_prob)

	# ------------------------------------------------------------------------------------------------------------------
	def model_eval_ext(self, fold_id, ds_eval, ts_eval, batch_size):
		'''
		Perform external evaluation of the model using the specified dataset.  No fitting is performed; this is a
		purely prediction-based procedure and requires an already fitted model.
		'''

		# Load the data:
		self.ds_name, self.ds_fname = self._ds_name_get(ds_eval, ts_eval)
		self.nts_vars_lst = []
		self.nts_vars_name = 'none'
		fold = 1  # we use the first fold; this is an arbitrary choice but any other fold would be equally good

		ds = self.data_load_one(fold, ['tr', 'va', 'te'])
		x = ds.x
		y = ds.y

		# Evaluate:
		y_prob = self.model.predict(x)
		y_pred = DB.np_prob_to_pred(y_prob)

		loss, acc = self.model.evaluate(x, y, batch_size=batch_size, verbose=self.VERBOSE_EVAL)
		# print("{:.4f}".format(acc))

		if self.db is not None:
			self.db.fold_comp_eval_ext(y, y_prob, y_pred, fold_id, self.db.curr_ds_id)

	# ------------------------------------------------------------------------------------------------------------------
	def model_fit(self, fold, fpath):
		callbacks = [
			EarlyStopping(monitor='val_loss', patience=2, mode='auto'),
			ModelCheckpoint(fpath, monitor='val_acc', mode='max', save_best_only=True)  # save_weights_only=True
		]

		self.mem_proc_max = max(self.mem_proc_max, self.proc.memory_full_info().uss)
		ts_0 = Time.ts()

		h = self.model.fit(self.ds.tr.x, self.ds.tr.y, validation_data=(self.ds.va.x, self.ds.va.y), epochs=self.epochs, batch_size=self.batch_size, verbose=self.VERBOSE_FIT, callbacks=callbacks).history

		ts_1 = Time.ts()
		self.mem_proc_max = max(self.mem_proc_max, self.proc.memory_full_info().uss)

		return DotMap(dur=round(ts_1 - ts_0), hist=h, fpath=fpath)

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def model_load(self, dir_models, fold_id):
		with tempfile.TemporaryDirectory(prefix='model-') as dir_tmp:
			fpath_gz = os.path.join(dir_models, '{}.h5.gz' .format(fold_id))
			fpath    = os.path.join(dir_tmp,    '{}.h5'    .format(fold_id))

			FS.gz_decomp(fpath_gz, fpath)

			self.model = load_model(fpath)

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def run_comp(self, schedule_id, schedule_i, fold, sess_id=None, is_verbose=True, tee=None):
		'''
		Perform the computations (i.e., the fit-and-evaluate sequence) for the specified cross-validation fold.

		The run_init() method should be called before this one (not necessarily right before, just at some earlier time) to
		ensure the database is properly initialized.

		'sess_id' can be None for sequential execution but should be passed along for parallelized execution to ensure
		the computation results are associated with the correct session.

		Initialization of any structures used by the subclass should be done in the submethod prior to this method
		being called.
		'''

		#@subclasscode

		import psutil

		# Compute:
		fpath_model_tmp = os.path.join(tempfile._get_default_tempdir(), 'model-{}.h5'.format(next(tempfile._get_candidate_names())))

		ts_0 = Time.ts()

		self.ds = DotMap()  # .tr .va .te

		self.data_load(fold + 1)  # sets: self.db.curr_ds_id
		self.model_def(fold + 1)  # sets: self.db.curr_model_id

		if (self.db is not None) and (self.db.fold_exists(self.db.curr_ds_id, self.db.curr_model_id, fold + 1)):  # computation already exists
			if tee is not None:
				tee.flush()
			return

		ret_fit  = self.model_fit(fold + 1, fpath_model_tmp)
		ret_eval = self.model_eval_int (fold + 1)

		ts_1 = Time.ts()

		# Persists results:
		if self.db is not None:
			self.is_proc_killable = False

			# DB:
			fold_id = self.db.fold_add(schedule_id, schedule_i, fold + 1, ts_0, round(ts_1 - ts_0), self.ds.tr.n_all, self.ds.va.n_all, self.ds.te.n_all, self.ds.tr.n, self.ds.va.n, self.ds.te.n, ret_fit.dur, ret_fit.hist, ret_eval.loss, ret_eval.acc, self.mem_proc_max, ret_eval.y, ret_eval.y_prob, sess_id)

			# FS:
			fpath_model = os.path.join(self.dir_out, '{}.h5'.format(fold_id))
			os.rename(fpath_model_tmp, fpath_model)
			FS.gz(fpath_model, do_del=True)  # GZ > BZ2 for models

			self.is_proc_killable = True

		if tee is not None:
			tee.flush()

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def run_init(self, schedule_id, schedule_i, fold, is_verbose=True, tee=None):
		'''
		A run meant only to initialize the computations executed later by run_comp().

		The init run adds all datasets and models to the database (which the coputational run later references) crucially along
		with their estimated in-memory sizes.  Those sizes are critical for the proper parallel computation scheduling, but
		don't play a role in the case of purely sequential computational runs.  The dataset sizes are computed only based on
		the first fold (i.e., fold=1), but they are a good-enough proxy for sizes for other folds as well.

		Initialization of any structures used by the subclass should be done in the submethod prior to this method
		being called.
		'''

		#@subclasscode

		# Estimate the dataset's in-memory size:
		self.data_load_one(1, ['va', 'te', 'tr'], True)
			# 'tr' is last here to avoid even touching it (because it's the largest) in case the dataset is already in
			# the database.  It should be noted though that this is just a precaution as the dataset will not be loaded
			# unless necessary.

		# Estimate the model's in-memory size:
		self.ds = DotMap()  # .va .te .tr
		self.data_load(1)
		self.model_def(1, True)
			# TODO: Loading all three parts of the dataset just for the purpose of using its dimensions in model_def()
			#       is time wasted, but it's easier to implement and compared to the actual computations, this time is
			#       next to nothing.  However, this can be optimized (especially for huge datasets).

		if tee is not None:
			tee.flush()

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def schedule_get_full():
		return []

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def schedule_get_micro():
		return []

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def space_to_n_iter(space):
		''' Returns the number of iterations based on the search space provided. '''

		import functools
		import operator

		return functools.reduce(operator.mul, [len(v) for v in space.values()], 1)


# ======================================================================================================================
if __name__ == '__main__':
	pass
