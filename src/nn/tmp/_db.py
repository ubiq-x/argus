# ----------------------------------------------------------------------------------------------------------------------
#
# *** Add the fold.fold_batch column ***
#
# with self.conn as c:
# 	fold_batch = 0
#
# 	for row in self.conn.execute('SELECT id, fold FROM fold ORDER BY id').fetchall():
# 		fold_id = int(row[0])
# 		fold    = int(row[1])
#
# 		if fold == 1:
# 			fold_batch += 1
#
# 		c.execute('UPDATE fold SET batch_id = ? WHERE id = ?', [fold_batch, fold_id])
#
# ----------------------------------------------------------------------------------------------------------------------

	# ------------------------------------------------------------------------------------------------------------------
	# def _db_add_fk(self):
	# 	'''
	# 	Add all foreign key contraints to the database.  I needed to do that on Oct 4, 2017 upon realizing that SQLite
	# 	doesn't support adding those constraints through the ALTER TABLE statement.  Databases created from scratch
	# 	do not need this run.
	# 	'''
	#
	# 	q = []
	# 	q.append(self._db_add_fk__get_sql('model_layer'))
	# 	q.append(self._db_add_fk__get_sql('fold'))
	# 	q.append(self._db_add_fk__get_sql('fold_fit'))
	# 	q.append(self._db_add_fk__get_sql('fold_eval'))
	# 	q.append('VACUUM;')
	#
	# 	with self.conn as c:
	# 		c.executescript(''.join(q))
	#
	# # ----^----
	# def _db_add_fk__get_sql(self, tbl):
	# 	return '''
	# 		CREATE TEMPORARY TABLE {0}_tmp AS SELECT * FROM {0} WHERE 0;
	# 		INSERT INTO {0}_tmp SELECT * FROM {0};
	# 		DROP TABLE {0};
	# 		{1};
	# 		INSERT INTO {0} SELECT * FROM {0}_tmp;
	# 		DROP TABLE {0}_tmp;
	# 		'''.format(tbl, self.DDL[tbl])

	# ------------------------------------------------------------------------------------------------------------------
	# def debug_rep(self, delim=None, sess_id=None):
	# 	delim = delim or self.rep_col_sep
	#
	# 	sess_id = sess_id or self.curr_sess_id
	#
	# 	q = '''
	# 		SELECT
	# 			n_obs_te_nna,
	# 			LENGTH(y) AS len_y,
	# 			LENGTH(y_prob) AS len_y_prob
	# 		FROM fold
	# 		{}
	# 		ORDER BY n_obs_te_nna, len_y, len_y_prob
	# 		'''.format('WHERE id = {}'.format(sess_id) if (sess_id is not None and sess_id > 0) else '')
	#
	# 	return self._rep_gen(q, delim)

	# ------------------------------------------------------------------------------------------------------------------
	# def x(self):
	# 	with self.conn as c:
	# 		n_ds_a = c.execute('SELECT COUNT(*) FROM ds').fetchone()[0]
	# 		n_ds_b = c.execute('SELECT COUNT(*) FROM ds WHERE n_store = 20 AND n_emit = 10').fetchone()[0]
	# 		n_fold = c.execute('SELECT COUNT(*) FROM fold').fetchone()[0]
	#
	# 		print(n_ds_a)
	# 		print(n_ds_b)
	# 		print(n_fold)
	#
	# 		c.execute('DELETE FROM ds WHERE n_store = 20 AND n_emit = 10')
	#
	# 		n_ds_a = c.execute('SELECT COUNT(*) FROM ds').fetchone()[0]
	# 		n_ds_b = c.execute('SELECT COUNT(*) FROM ds WHERE n_store = 20 AND n_emit = 10').fetchone()[0]
	# 		n_fold = c.execute('SELECT COUNT(*) FROM fold').fetchone()[0]
	#
	# 		print(n_ds_a)
	# 		print(n_ds_b)
	# 		print(n_fold)


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	# Check if computed accuracy equals the one from Keras (conclusion: it does):
	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3'))
	# with db.conn as c:
	# 	for row in c.execute('''
	# 		SELECT ROUND(ABS(f.acc - fe.acc), 2), ROUND(f.acc, 2), ROUND(fe.acc, 2), ROUND(fe.roc_auc, 2)
	# 		FROM fold f
	# 		INNER JOIN ds d on d.id = f.ds_id
	# 		INNER JOIN model m on m.id = f.model_id
	# 		INNER JOIN fold_eval fe on fe.fold_id = f.id
	# 		''', []).fetchall():
	# 		print("{:4}: {:4} {:4}    {:4}".format(row[0], row[1], row[2], row[3]))
	# sys.exit(0)

	# ------------------------------------------------------------------------------------------------------------------
	# Create and use a test database:
	# fpath = os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'db-test.sqlite3')
	# res_cols = [['a', ''], ['b', 0], ['acc', 0.0]]
	#
	# db = DB(fpath)
	#
	# # db._db_empty()
	#
	# db.ds_add('ds-01', 'ds-01.txt', 7, 3, 4, 2)
	# db.model_add('model-01', 32, False, 17)
	#
	# db.sess_add(round(Time.ts()))
	#
	# db.fold_add(1)
	# db.res_add(['well', 1, 0.7012])
	# db.fit_hist_add({"loss": [0.1, 0.2], "acc": [0.2, 0.3], "val_loss": [0.3, 0.4], "val_acc": [0.4, 0.5]})
	#
	# ...
	#
	# print("SESSIONS:")
	# print(db.rep_ls_sess())
	#
	# print("\nMODELS:")
	# print(db.rep_ls_model())
	#
	# print("\nDATASETS:")
	# print(db.rep_ls_ds())
	#
	# print("\nBASIC REPORT (CURRENT SESSION):")
	# print(db.rep_basic())
	#
	# print("\nBASIC REPORT (ALL SESSIONS):")
	# print(db.rep_basic(sess_id=-1))

	# ------------------------------------------------------------------------------------------------------------------
	# def rep_gen_raw2(self):
	# 	rep = []
	#
	# 	e = self.conn.cursor().execute
	#
	# 	# Determin max lenght of each column:
	# 	cols_len = [e('SELECT "{}" FROM res'.format(c[0])).fetchall() for c in self.res_cols]         # column data
	# 	cols_len = [max([len(str(cl[0])) for cl in cols_len[i]]) for i in range(len(self.res_cols))]  # max lenght of column data
	# 	cols_len = [max(cl, len(self.res_cols[i][0])) for i, cl in enumerate(cols_len)]               # max lenght of column data and column name
	#
	# 	# Print header:
	# 	rep.append(self.rep_col_sep.join([('{:%s%d}' % (c[2], cols_len[i])).format(c[0]) for i, c in enumerate(self.res_cols)]))
	#
	# 	# Print data:
	# 	for row in e('SELECT * FROM res').fetchall():
	# 		ln = []
	# 		for i, field in enumerate(row):
	# 			ln.append(('{:%s%d}' % (self.res_cols[i][2], cols_len[i])).format(field))
	#
	# 		rep.append(self.rep_col_sep.join(ln))
	#
	# 	return '\n'.join(rep)
