#
# TODO
#     Fit using GPU
#     Monitor training progress and keep intermediate models to eventually only keep the one that's the best
#         I.e., monitor under- and over-fitting
#     Visualization
#         Fitting process (loss and accuracy, both training and validation)
#         The network itself
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import json
import keras
import numpy as np

from dotmap import DotMap

from keras.models import Model, Sequential
from keras.layers import Activation, Dense, Dropout, Input, LSTM
from keras.callbacks import EarlyStopping

from db import DB
from util import FS, Tee, Time


# ======================================================================================================================
class NN02(object):
	'''
	Multiple input model that combines word, eye-movement, and subject variables.
	'''

	DEBUG_LVL = 1  # 0=none, 1=normal, 2=full

	DS_DELIM = '\t'

	FS_DNAME_SESS       = 'sess'
	FS_DNAME_MODELS     = 'models'
	FS_DNAME_REP        = 'rep'

	FS_FNAME_DB         = 'db.sqlite3'
	FS_FNAME_DB_DEBUG   = 'db-debug.sqlite3'
	FS_FNAME_LOG        = 'log.txt'
	FS_FNAME_MODEL_ARCH = 'model-arch.json'
	FS_FNAME_REP_BASIC  = 'basic.txt'

	NN_MODEL_GEN = 'FC'

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_ds, dir_out):
		self.dir_ds  = dir_ds
		self.dir_out = dir_out

		self.dir_sess = FS.dir_mk((self.dir_out, self.FS_DNAME_SESS))

		self.fpath_db = os.path.join(self.dir_sess, (self.FS_FNAME_DB if (self.DEBUG_LVL == 0) else self.FS_FNAME_DB_DEBUG))

		self.db = DB(self.fpath_db)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load(self, fold):
		self.ds.tr = self.data_load_one(fold, 'tr')
		self.ds.va = self.data_load_one(fold, 'va')
		self.ds.te = self.data_load_one(fold, 'te')

	# ----^-------------------------------------------------------------------------------------------------------------
	def data_load_one(self, fold, ds_suff):
		fpath = os.path.join(self.dir_ds, 'fold-{}'.format(fold), '{}--{}.txt'.format(self.ds_fname, ds_suff))

		# Load meta data:
		with open(fpath, 'r') as f:
			self.ds_meta = DotMap(json.loads(f.readline()))

		m = self.ds_meta  # shorthand

		# Load data:
		data = np.genfromtxt(fpath, delimiter=self.DS_DELIM, skip_header=1)
		n_obs_all = data.shape[0]

		# Remove missing values:
		data = data[~np.isnan(data).any(axis=1)]
			# TODO: Impute missing data
		n_obs_nna = data.shape[0]

		# Y:
		y = data[:, 0].reshape((data.shape[0], 1)).astype(int)
		# x = [] if (m.n_var_nts == 0) else [data[:, range(1, m.n_var_nts + 1)]]

		# X - Non-temporal variables:
		if (not self.use_nts_vars) or (m.n_var_nts == 0):
			x = []
		else:
			x = [data[:, range(1, m.n_var_nts + 1)]]  # start at index 1 because y is at index 0

		# X - Temporal variables:
		for i in range(m.n_var_ts):
			x.append(data[:, range(i * m.n_store + (m.n_var_nts + 1), (i + 1) * m.n_store + (m.n_var_nts + 1))])

		# Save the dataset (on first load only):
		if fold == 1:
			self.db.ds_add(self.ds_name, self.ds_fname, m.n_var_nts, m.n_var_ts, m.n_store, m.n_emit)

		return DotMap(x=x, y=y, n_obs_all=n_obs_all, n_obs_nna=n_obs_nna)

	# ------------------------------------------------------------------------------------------------------------------
	def model_def(self, fold):
		# self.model = Sequential()
		# self.model.add(Input(shape=self.x_tr.shape))
		# self.model.add(Activation('relu'))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(64))
		# self.model.add(Activation('relu'))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(1))
		# self.model.add(Activation('sigmoid'))
		# # self.model.add(Activation(self._act_tanh_17159))

		# if (not self.use_nts_vars) and (self.ds_meta.n_var_nts > 0):
		# 	inputs = [Input(shape=(self.ds.tr.x[i].shape[1],)) for i in range(1, len(self.ds.tr.x))]
		# else:
		# 	inputs = [Input(shape=(self.ds.tr.x[i].shape[1],)) for i in range(   len(self.ds.tr.x))]

		inputs = [Input(shape=(self.ds.tr.x[i].shape[1],)) for i in range(len(self.ds.tr.x))]
		x = keras.layers.concatenate(inputs)

		for l in self.model_def_inf.layers:
			x = Dense(l.units, activation=l.act)(x)
			x = Dropout(0.5)(x)

		output = Dense(1, activation='sigmoid')(x)

		self.model = Model(inputs, output)
		self.model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])  # optimizer: adam, rmsprop

		# Save the model (on first load only):
		if fold == 1:
			n_in = sum([int(i.shape[1]) for i in inputs])

			self.db.model_add(self.NN_MODEL_GEN, self.model_def_inf.arch, self.model_def_inf.layers, self.epochs, self.batch_size, self.use_nts_vars, n_in)

			FS.save_bz2(self.fpath_model_arch, self.model.to_json())

	# ------------------------------------------------------------------------------------------------------------------
	def model_fit(self, fold):
		early_stopping = EarlyStopping(monitor='val_loss', patience=2)
		callbacks = [early_stopping]

		ts_0 = Time.ts()
		h = self.model.fit(self.ds.tr.x, self.ds.tr.y, validation_data=(self.ds.va.x, self.ds.va.y), epochs=self.epochs, batch_size=self.batch_size, callbacks=callbacks).history

		fname_model   = '{}--{}--fold-{}.h5'.format(self.NN_MODEL_GEN, self.ds_fname, fold)
		fname_weights = '{}--{}--fold-{}--weights.h5'.format(self.NN_MODEL_GEN, self.ds_fname, fold)

		self.db.fold_upd_fit(round(Time.ts() - ts_0), h, fname_model, fname_weights)

		self.model.save(os.path.join(self.dir_sess_models, fname_model))
		self.model.save_weights(os.path.join(self.dir_sess_models, fname_weights))

	# ------------------------------------------------------------------------------------------------------------------
	def model_eval(self, fold):
		x = self.ds.te.x
		y = self.ds.te.y

		y_prob = self.model.predict(x)

		loss, acc = self.model.evaluate(x, y, batch_size=self.batch_size)
			# Score is the evaluation of the loss function for a given input. Training a network is finding parameters
			# that minimize a loss function (or cost function). The cost function here is the binary_crossentropy. For
			# a target T and a network output O, the binary crossentropy can defined as:
			#
			#     f(T,O) = -(T*log(O) + (1-T)*log(1-O))
			#
			# So the score you see is the evaluation of that.
			#
			# [stackoverflow.com/questions/43589842/test-score-vs-test-accuracy-when-evaluating-model-using-keras]

			# def binary_accuracy(y_true, y_pred):
			#     return K.mean(K.equal(y_true, K.round(y_pred)))
			#
			# [github.com/fchollet/keras/blob/master/keras/metrics.py]

		# print(y.shape)
		# print(y_prob.shape)
		# print(ts_id.shape)
		# print(ts_id.dtype)

		ts_id = np.asarray([0, 1, 2])  # TODO: Use the actual vector

		self.db.fold_upd_eval(loss, acc, ts_id, y, y_prob)

	# ------------------------------------------------------------------------------------------------------------------
	def run(self, n_folds, ds_fname, ds_name, model_def_inf, epochs, batch_size, use_nts_vars):
		# Init:
		self.tee = Tee(self.fpath_log)

		self.ds_fname = ds_fname
		self.ds_name  = ds_name

		self.model_def_inf = model_def_inf
		self.epochs = epochs
		self.batch_size = batch_size
		self.use_nts_vars = use_nts_vars

		# Run:
		for fold in range(n_folds):
			print('\n\n\n*** DS: {}  FOLD: {} ***\n'.format(self.ds_fname, fold + 1))

			fold_ts_0 = Time.ts()

			self.ds = DotMap()  # .tr .va .te
			self.model = None

			self.data_load(fold + 1)
			self.model_def(fold + 1)

			self.db.fold_add(
				fold + 1,
				fold_ts_0,
				self.ds.tr.n_obs_all,
				self.ds.va.n_obs_all,
				self.ds.te.n_obs_all,
				self.ds.tr.n_obs_nna,
				self.ds.va.n_obs_nna,
				self.ds.te.n_obs_nna
			)

			self.model_fit(fold + 1)
			self.model_eval(fold + 1)

			self.db.fold_set_dur(round(Time.ts() - fold_ts_0))
			self.tee.flush()

		# Clean up:
		self.tee.flush()
		self.tee.end()
		self.tee = None

	# ------------------------------------------------------------------------------------------------------------------
	def sess_begin(self, sess_id=None):
		self.sess_ts_0 = round(Time.ts())

		self.sess_id = sess_id or self.sess_ts_0

		self.dir_sess_root   = FS.dir_mk((self.dir_sess, str(self.sess_id)))
		self.dir_sess_models = FS.dir_mk((self.dir_sess_root, self.FS_DNAME_MODELS))
		self.dir_sess_rep    = FS.dir_mk((self.dir_sess_root, self.FS_DNAME_REP))

		self.fpath_log        = os.path.join(self.dir_sess_root, self.FS_FNAME_LOG)
		self.fpath_model_arch = os.path.join(self.dir_sess_root, self.FS_FNAME_MODEL_ARCH)
		self.fpath_rep_basic  = os.path.join(self.dir_sess_rep, self.FS_FNAME_REP_BASIC)

		self.db.sess_add(self.sess_id, self.dir_ds, self.dir_sess_root)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_end(self):
		'''
		Prints reports and consolidates the session (e.g., compresses certain result files or insert them into the
		database).
		'''

		# (1) Reports:
		# (1.1) Basic:
		rep = self.db.rep_basic()

		print('\n\n\n*** REPORT: BASIC ***\n')
		print('sess-id: {}'.format(self.db.curr_sess_id))
		print(rep)

		FS.save_gz(self.fpath_rep_basic, rep)  # GZ > BZ2 for smaller reports, but the differences are small so I use the more popular one

		# (1.2) Aggregate:
		# ...

		# (2) GZ files:
		FS.gz(self.fpath_log, do_del=True)

		for f in os.listdir(self.dir_sess_models):
			FS.gz(os.path.join(self.dir_sess_models, f), do_del=True)  # GZ > BZ2 for models

		# (3) Clean up:
		self.db.sess_set_dur(round(Time.ts() - self.sess_ts_0))
		self.sess_id = None


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	dir_root = os.path.join(Path.home(), 'data', 'eye-link')
	dir_ds   = os.path.join(dir_root, 'ds', 'out')
	dir_out  = os.path.join(dir_root, 'nn')

	nn = NN02(dir_ds, dir_out)

	# Hyperparemeter optimization (grid search):
	n_folds = 2

	# datasets = [('20.2010', '2010'), ('gg.20.2011', '2011 gg'), ('aa.20.2011', '2011 aa')]
	datasets = [('gg.20.2011', '2011 gg'), ('aa.20.2011', '2011 aa')]
	# datasets = [('20.2010', '2010')]
	# datasets = [('gg.20.2011', '2011 gg')]
	# datasets = [('aa.20.2011', '2011 aa')]

	ns = [(2, 1), (3, 1), (5, 1), (5, 2), (10, 1), (10, 2), (10, 5)]
	# ns = [(10, 2), (10, 5)]
	# ns = [(10, 5)]

	model_defs_gen = DotMap(units=[16, 32, 64], act=['relu', 'tanh'])
	# model_defs_gen = DotMap(units=[16], act=['relu'])

	model_defs = []
	for mdg_u in model_defs_gen.units:
		for mdg_a in model_defs_gen.act:
			model_defs.append(
				DotMap(arch='FC-{0}-{1}.FC-{0}-{1}'.format(mdg_u, mdg_a),
					layers=[
						DotMap(num=1, type='FC', units=mdg_u, act=mdg_a),
						DotMap(num=2, type='FC', units=mdg_u, act=mdg_a)
					]))

	batch_sizes = [32, 64, 128]
	# batch_sizes = [64]

	epochs = 30

	use_nts_vars = True
	# use_nts_vars = False

	# ds_suff = ""
	# ds_suff = "--rspd"
	# ds_suff = "--rspd,time"

	n = n_folds * len(datasets) * len(ns) * len(model_defs) * len(batch_sizes)
	print('Hyperparameter optimization loop size: {}'.format(n))
	sys.exit(0)

	nn.sess_begin()
	for ds in datasets:
		for n in ns:
			for md in model_defs:
				for bs in batch_sizes:
					nn.run(n_folds, 'roi.30-_.ps1.adj.{}--n{}-{}{}'.format(ds[0], n[0], n[1], ds_suff), ds[1], md, epochs, bs, use_nts_vars)
	nn.sess_end()

	# Hyperparemeter optimization (random search):
	# ...
