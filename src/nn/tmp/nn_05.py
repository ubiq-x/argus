#
# TODO
#     Abstract the model into a separate class
#     Fit a single-sigmoid net as a baseline and a way to compare against my dissertation
#     Have Keras remember the best model and save it after fitting is done
#         Or make sure to save it yourself
#     Visualization
#         Fitting process (loss and accuracy, both training and validation)
#             Not particularily useful at this point, but will be later, for the article
#         The network itself
#             keras.io/getting-started/faq/#how-can-i-obtain-the-output-of-an-intermediate-layer
#     Compile TF from sources to make use of extra speed
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Solved issues
#     Missing data imputations
#         Most missing data is associated with not fixated words.  That's most likely due to the input dataset
#         containing words seen as well (i.e., PS=1; perceptual span).  Some GD are missing despite the FFD being
#         there.  For those rows, other variables are missing too.  This is weird and probably goes back to Pegasus.
#         However, this is fairly rare so I leave it as is for now.  Below are the numbers and percentages of
#         missing data (not due to perceptual span) for the 'roi.30-_.ps1.adj.20.2010.txt' dataset.
#
#             var  em_wlen  em_nf  em_nr  em_nb  em_ffd  em_gd  em_let_i_fp  em_let_o_fp  em_sacc_i_roi_fp  em_sacc_o_roi_fp  em_sacc_i_let_fp  em_sacc_o_let_fp  word_nlet  subtl_frq_log10
#             all    21163  21163  21163  21163   21163  19925        20245        20209             20209             20245             20209             20245      19348            19286
#             miss       0      0      0      0       0   1238          918          954               918               954               918               954       1815             1877
#             %          0      0      0      0       0   6.21         4.53         4.72              4.53              4.72              4.53              4.72       9.38             9.73
#
#         At this point I simply ignore the issue as it probably doesn't have a large impact on the results.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import json
import keras
import numpy as np

from dotmap import DotMap

from keras.models import Model, Sequential
from keras.layers import Activation, Dense, Dropout, Input, LSTM
from keras.callbacks import EarlyStopping

from db import DB
from util import FS, Tee, Time

# keras.io/getting-started/faq/#how-can-i-obtain-reproducible-results-using-keras-during-development


# ======================================================================================================================
class NN02(object):
	'''
	Multiple and separate inputs model that combines word, eye-movement, and subject variables.
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	DS_DELIM = '\t'

	FS_DNAME_SESS       = 'sess'
	FS_DNAME_MODELS     = 'models'
	FS_DNAME_REP        = 'rep'

	FS_FNAME_DB         = 'db.sqlite3'
	FS_FNAME_DB_DEBUG   = 'db-debug.sqlite3'
	FS_FNAME_LOG        = 'log.txt'
	FS_FNAME_MODEL_ARCH = 'model-arch.json'
	FS_FNAME_REP_BASIC  = 'basic.txt'

	NN_MODEL_GEN = 'FC'

	VERBOSE_EVAL = 0
	VERBOSE_FIT  = 0

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_ds, dir_out):
		self.dir_ds  = dir_ds
		self.dir_out = dir_out

		self.dir_sess = FS.dir_mk((self.dir_out, self.FS_DNAME_SESS))

		self.fpath_db = os.path.join(self.dir_sess, (self.FS_FNAME_DB if (self.DEBUG_LVL == 0) else self.FS_FNAME_DB_DEBUG))

		self.db = DB(self.fpath_db)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load(self, fold):
		self.ds.tr = self.data_load_one(fold, 'tr')
		self.ds.va = self.data_load_one(fold, 'va')
		self.ds.te = self.data_load_one(fold, 'te')

	# ----^-------------------------------------------------------------------------------------------------------------
	def data_load_one(self, fold, ds_suff):
		fpath = os.path.join(self.dir_ds, 'fold-{}'.format(fold), '{}--{}.txt'.format(self.ds_fname, ds_suff))

		# Load meta data:
		with open(fpath, 'r') as f:
			self.ds_meta = DotMap(json.loads(f.readline()))
			self.ds_meta.n_var_nts_curr = len(self.nts_vars_lst)

		m = self.ds_meta  # shorthand

		# Load data:
		data = np.genfromtxt(fpath, delimiter=self.DS_DELIM, skip_header=1)
		n_all = data.shape[0]

		# Remove missing values:
		data = data[~np.isnan(data).any(axis=1)]
		n = data.shape[0]

		# Y:
		y = data[:, 0].reshape((data.shape[0], 1)).astype(int)

		# X - Non-temporal variables:
		self.nts_vars_idx = [m.vars_nts.index(i) + 1 for i in self.nts_vars_lst]  # add 1 because y is at index 0
		if (m.n_var_nts_curr == 0) or (len(self.nts_vars_idx) == 0):
			x = []
		else:
			x = [data[:, self.nts_vars_idx]]

		# X - Temporal variables:
		for i in range(m.n_var_ts):
			x.append(data[:, range(i * m.n_store + (m.n_var_nts + 1), (i + 1) * m.n_store + (m.n_var_nts + 1))])

		# Save the dataset (on first load only):
		if fold == 1:
			self.db.ds_add(self.ds_name, self.ds_fname, m.n_var_nts, m.n_var_ts, m.n_store, m.n_emit, ','.join(self.nts_vars_lst), ','.join([str(i) for i in self.nts_vars_idx]), self.nts_vars_name)

		return DotMap(x=x, y=y, n_all=n_all, n=n)

	# ------------------------------------------------------------------------------------------------------------------
	def model_def(self, fold):
		# self.model = Sequential()
		# self.model.add(Input(shape=self.x_tr.shape))
		# self.model.add(Activation('relu'))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(64))
		# self.model.add(Activation('relu'))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(1))
		# self.model.add(Activation('sigmoid'))
		# # self.model.add(Activation(self._act_tanh_17159))

		inputs = [Input(shape=(self.ds.tr.x[i].shape[1],)) for i in range(len(self.ds.tr.x))]
		x = keras.layers.concatenate(inputs)

		for l in self.model_def_inf.layers:
			x = Dense(l.units, activation=l.act)(x)
			x = Dropout(0.5)(x)

		output = Dense(1, activation='sigmoid')(x)

		self.model = Model(inputs, output)
		self.model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])  # optimizer: adam, rmsprop

		# Save the model (on first load only):
		if fold == 1:
			n_in = sum([int(i.shape[1]) for i in inputs])

			self.db.model_add(self.NN_MODEL_GEN, self.model_def_inf.arch, self.model_def_inf.layers, self.epochs, self.batch_size, n_in)

			FS.save_bz2(self.fpath_model_arch, self.model.to_json())

	# ------------------------------------------------------------------------------------------------------------------
	def model_fit(self, fold):
		early_stopping = EarlyStopping(monitor='val_loss', patience=2, mode='auto')
		callbacks = [early_stopping]

		ts_0 = Time.ts()
		h = self.model.fit(self.ds.tr.x, self.ds.tr.y, validation_data=(self.ds.va.x, self.ds.va.y), epochs=self.epochs, batch_size=self.batch_size, verbose=self.VERBOSE_FIT, callbacks=callbacks).history

		fname_model   = '{}--{}--fold-{}.h5'.format(self.NN_MODEL_GEN, self.ds_fname, fold)
		fname_weights = '{}--{}--fold-{}--weights.h5'.format(self.NN_MODEL_GEN, self.ds_fname, fold)

		self.db.fold_upd_fit(round(Time.ts() - ts_0), h, fname_model, fname_weights)

		self.model.save(os.path.join(self.dir_sess_models, fname_model))
		self.model.save_weights(os.path.join(self.dir_sess_models, fname_weights))

	# ------------------------------------------------------------------------------------------------------------------
	def model_eval(self, fold):
		x = self.ds.te.x
		y = self.ds.te.y

		y_prob = self.model.predict(x)

		loss, acc = self.model.evaluate(x, y, batch_size=self.batch_size, verbose=self.VERBOSE_EVAL)
			# Score is the evaluation of the loss function for a given input. Training a network is finding parameters
			# that minimize a loss function (or cost function). The cost function here is the binary_crossentropy. For
			# a target T and a network output O, the binary crossentropy can defined as:
			#
			#     f(T,O) = -(T*log(O) + (1-T)*log(1-O))
			#
			# So the score you see is the evaluation of that.
			#
			# [stackoverflow.com/questions/43589842/test-score-vs-test-accuracy-when-evaluating-model-using-keras]

			# def binary_accuracy(y_true, y_pred):
			#     return K.mean(K.equal(y_true, K.round(y_pred)))
			#
			# [github.com/fchollet/keras/blob/master/keras/metrics.py]

		self.db.fold_upd_eval(loss, acc, y, y_prob)

	# ------------------------------------------------------------------------------------------------------------------
	def run(self, n_folds, ds_fname, ds_name, model_def_inf, epochs, batch_size, nts_vars_lst, nts_vars_name):
		# Init:
		self.tee = Tee(self.fpath_log)

		self.ds_fname = ds_fname
		self.ds_name  = ds_name

		self.model_def_inf = model_def_inf
		self.epochs = epochs
		self.batch_size = batch_size
		self.nts_vars_lst = nts_vars_lst
		self.nts_vars_name = nts_vars_name

		# Run:
		for fold in range(n_folds):
			print('    Fold {}'.format(fold + 1))

			fold_ts_0 = Time.ts()

			self.ds = DotMap()  # .tr .va .te
			self.model = None

			self.data_load(fold + 1)
			self.model_def(fold + 1)

			self.db.fold_add(
				fold + 1,
				fold_ts_0,
				self.ds.tr.n_all,
				self.ds.va.n_all,
				self.ds.te.n_all,
				self.ds.tr.n,
				self.ds.va.n,
				self.ds.te.n
			)

			self.model_fit(fold + 1)
			self.model_eval(fold + 1)

			self.db.fold_set_dur(round(Time.ts() - fold_ts_0))
			self.tee.flush()

		# Clean up:
		self.tee.flush()
		self.tee.end()
		self.tee = None

	# ------------------------------------------------------------------------------------------------------------------
	def sess_begin(self, sess_id=None):
		self.sess_ts_0 = round(Time.ts())

		self.sess_id = sess_id or self.sess_ts_0

		self.dir_sess_root   = FS.dir_mk((self.dir_sess, str(self.sess_id)))
		self.dir_sess_models = FS.dir_mk((self.dir_sess_root, self.FS_DNAME_MODELS))
		self.dir_sess_rep    = FS.dir_mk((self.dir_sess_root, self.FS_DNAME_REP))

		self.fpath_log        = os.path.join(self.dir_sess_root, self.FS_FNAME_LOG)
		self.fpath_model_arch = os.path.join(self.dir_sess_root, self.FS_FNAME_MODEL_ARCH)
		self.fpath_rep_basic  = os.path.join(self.dir_sess_rep, self.FS_FNAME_REP_BASIC)

		self.db.sess_add(self.sess_id, self.dir_ds, self.dir_sess_root)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_end(self):
		'''
		Prints reports and consolidates the session (e.g., compresses certain result files or insert them into the
		database).
		'''

		# (1) Reports:
		# (1.1) Basic:
		rep = self.db.rep_basic()

		print('sess-id: {}'.format(self.db.curr_sess_id))
		print(rep)

		FS.save_gz(self.fpath_rep_basic, rep)  # GZ > BZ2 for smaller reports only, but the differences are small so I use the more popular one

		# (1.2) Aggregate:
		# ...

		# (2) GZ files:
		FS.gz(self.fpath_log, do_del=True)

		for f in os.listdir(self.dir_sess_models):
			FS.gz(os.path.join(self.dir_sess_models, f), do_del=True)  # GZ > BZ2 for models

		# (3) Clean up:
		self.db.sess_set_dur(round(Time.ts() - self.sess_ts_0))
		self.sess_id = None


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path


	dir_root = os.path.join(Path.home(), 'data', 'eye-link')
	dir_ds   = os.path.join(dir_root, 'ds', 'out')
	dir_out  = os.path.join(dir_root, 'nn')

	nn = NN02(dir_ds, dir_out)


	# (1) Hyperparemeter optimization (grid search):
	n_folds = 2

	datasets = []
	# datasets.append(('20.2010', '2010'))
	# datasets.append(('aa.20.2011', '2011 aa'))
	# datasets.append(('gg.20.2011', '2011 gg'))

	nts_vars = []
	# nts_vars.append(([], 'none'))
	# nts_vars.append((['sub_id_wm_fl', 'sub_id_sat', 'sub_read_spd_a', 'sub_ctx_time', 'sub_ctx_fat', 'sub_ctx_pre', 'sub_ctx_crv'], 'all'))
	# nts_vars.append((['sub_read_spd_a'], 'rspd'))
	# nts_vars.append((['sub_read_spd_a', 'sub_ctx_time'], 'rspd.time'))
	# nts_vars.append((['sub_read_spd_a', 'sub_ctx_time', 'sub_ctx_pre'], 'rspd.time.pre'))

	ts = []
	# ts.append(( 2,  1))
	# ts.append(( 3,  1))
	# ts.append(( 5,  1))
	# ts.append(( 5,  2))
	# ts.append((10,  1))
	# ts.append((10,  2))
	# ts.append((10,  5))
	# ts.append((20,  2))
	# ts.append((20,  5))
	# ts.append((20, 10))
	# ts.append((40,  5))
	# ts.append((40, 10))
	# ts.append((40, 20))

	model_defs_gen = DotMap(units=[], act=[])

	# model_defs_gen.units.append(16)
	# model_defs_gen.units.append(32)
	# model_defs_gen.units.append(64)

	# model_defs_gen.act.append('relu')
	# model_defs_gen.act.append('tanh')

	model_defs = []
	for mdg_u in model_defs_gen.units:
		for mdg_a in model_defs_gen.act:
			model_defs.append(
				DotMap(
					arch='FC-{0}-{1}.FC-{0}-{1}'.format(mdg_u, mdg_a),
					layers=[
						DotMap(num=1, type='FC', units=mdg_u, act=mdg_a),
						DotMap(num=2, type='FC', units=mdg_u, act=mdg_a)
					]))

	batch_sizes = []
	# batch_sizes.append( 32)
	# batch_sizes.append( 64)
	# batch_sizes.append(128)

	epochs = 30

	i = 0
	n = n_folds * len(datasets) * len(ts) * len(model_defs) * len(batch_sizes) * len(nts_vars)
	print('Loop size: {}'.format(n))
	sys.exit(0)

	nn.sess_begin()
	for ds in datasets:
		for ntsv in nts_vars:
			for t in ts:
				for md in model_defs:
					for bs in batch_sizes:
						i += 1
						print('Iter {} of {}:  DS={}  NTSV={}  TS={}.{}  Arch={}  BS={}'.format(i, n, ds[1], ntsv[1], t[0], t[1], md.arch, bs))

						nn.run(n_folds, 'roi.30-_.ps1.adj.{}--n{}-{}'.format(ds[0], t[0], t[1]), ds[1], md, epochs, bs, ntsv[0], ntsv[1])
	nn.sess_end()


	# (2) Hyperparemeter optimization (random search):
	# ...
