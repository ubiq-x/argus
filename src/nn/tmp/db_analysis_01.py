#
# TODO
#     Grid of plots by
#         Time series definition
#         Model params: Units, activations
#         Dataset params: Year, cal-val, nts vars
#         Fit params: Batch size
#     Top models
#         Plot top N models in a grid by dataset and time series definition to make the final decision
#         Plot the M following models with more transparency to provide context without obfuscating the main purpose
#         X: Time series (7)
#         Y: Dataset (3)
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Matplotlib
#     matplotlib.org/gallery.html
#     Subplots
#         jonathansoma.com/lede/data-studio/classes/small-multiples/long-explanation-of-using-plt-subplots-to-create-small-multiples
#         stackoverflow.com/questions/20057260/how-to-remove-gaps-between-subplots-in-matplotlib
#         stackoverflow.com/questions/19932553/size-of-figure-when-using-plt-subplots
#         Unequal sized grids
#             scientificallysound.org/2016/06/09/matplotlib-how-to-plot-subplots-of-unequal-sizes
#     Pattern fills
#         www.packtpub.com/mapt/book/big_data_and_business_intelligence/9781849513265/2/ch02lvl1sec32/controlling-a-fill-pattern
#         www.collindelker.com/2013/03/11/matplotlib-hatch-spacing.html
#         matplotlib.org/devdocs/gallery/lines_bars_and_markers/marker_fillstyle_reference.html#sphx-glr-gallery-lines-bars-and-markers-marker-fillstyle-reference-py
#     Axes ticks
#         Frequency
#             stackoverflow.com/questions/12608788/changing-the-tick-frequency-on-x-or-y-axis-in-matplotlib
#         Direction
#             matplotlib.org/devdocs/api/_as_gen/matplotlib.axes.Axes.tick_params.html
#             stackoverflow.com/questions/6260055/in-matplotlib-how-do-you-draw-r-style-axis-ticks-that-point-outward-from-the-ax
#     Styles
#         Lines   : matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html
#         Markers : matplotlib.org/api/markers_api.html?highlight=marker#module-matplotlib.markers
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Seaborn
#     Grid plots
#         seaborn.pydata.org/tutorial/axis_grids.html
#     Violin and box plots
#         seaborn.pydata.org/generated/seaborn.violinplot.html
#
# ----------------------------------------------------------------------------------------------------------------------
#
# 	s.id, m.id, d.id, f.id, SUBSTR(s.dt, 1, 10),
# 	m.gen, m.arch, ml.n, m.n_epochs, m.batch_size, m.use_nts_vars, m.n_in,
# 	d.name, d.fname, d.n_store, d.n_emit, d.n_var_nts, d.n_var_ts,
# 	f.fold,
# 	f.dur,
# 	f.n_obs_tr_all,
# 	f.n_obs_va_all,
# 	f.n_obs_te_all,
# 	f.n_obs_tr_nna,
# 	f.n_obs_va_nna,
# 	f.n_obs_te_nna,
# 	f.fit_t,
# 	ff.n,
# 	ROUND(ff.acc_tr_max, 4),
# 	ROUND(ff.acc_va_max, 4),
# 	ROUND(f.acc, 4),
#
# INNER JOIN sess s ON s.id = f.sess_id
# INNER JOIN (SELECT model_id, COUNT(*) AS n FROM model_layer GROUP BY model_id) ml ON m.id = ml.model_id
# INNER JOIN (SELECT fold_id, COUNT(*) AS n, MAX(acc_tr) AS acc_tr_max, MAX(acc_va) AS acc_va_max FROM fold_fit GROUP BY fold_id) ff ON ff.fold_id = f.id
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import itertools
import math
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import re
import sqlite3

from dotmap import DotMap

from db import DB
from util import Time


# ======================================================================================================================
class DBAnalysis(object):

	DPI = 200

	DS = ['2010', '2011 aa', '2011 gg']

	TS = [(2, 1), (3, 1), (5, 1), (5, 2), (10, 1), (10, 2), (10, 5)]

	COL_EDGE = { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' }
	COL_FACE = {
		'none'          : { DS[0]: 'none',    DS[1]: 'none',    DS[2]: 'none'    },
		'rspd'          : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'rspd.time'     : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'rspd.time.pre' : { DS[0]: '#ff9305', DS[1]: '#760094', DS[2]: '#029400' },
		'all'           : { DS[0]: '#000000', DS[1]: '#000000', DS[2]: '#000000' }
	}
	HATCH = { 'none': '', 'all': '', 'rspd': '', 'rspd.time': '||||||||', 'rspd.time.pre': '////////////////' }
	MARKER = { 'relu': 'o', 'tanh': 'X' }


	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, fpath):
		self.fpath = fpath

		self.db = DB(fpath)
		self.conn = self.db.conn

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__batch_ts(self, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: computation batch, plot: time series def '''

		orderby = 'ds_name, model_act, model_units, ds_n_store, ds_n_emit'
		title   = 'Order by: Computation batch'
		fpath   = os.path.join(dir_out, '{}idx-roc--batch-ts{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, ds, title, self._idx_roc__xy_ts, self._idx_roc__ts, fpath)

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__batch_xy(self, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: computation batch, plot: markers '''

		orderby = 'ds_name, model_act, model_units, ds_n_store, ds_n_emit'
		title   = 'Order by: Computation batch'
		fpath   = os.path.join(dir_out, '{}idx-roc--batch-xy{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, ds, title, self._idx_roc__xy, self._idx_roc__ts, fpath)

	# ------------------------------------------------------------------------------------------------------------------
	def idx_roc__roc(self, ds=None, fname_pre='', fname_suff='', do_save=False):
		''' x: idx, y: roc.auc, ord: roc.auc, plot: markers '''

		orderby = 'eval_roc_auc DESC, ds_n_store, ds_n_emit'
		title   = 'Order by: Mean ROC AUC'
		fpath   = os.path.join(dir_out, '{}idx-roc--roc{}'.format(fname_pre, fname_suff)) if do_save else None

		self._idx_roc(orderby, ds, title, self._idx_roc__xy, self._idx_roc__ts_ds, fpath)

	# ------------------------------------------------------------------------------------------------------------------
	# def _gen_ds_name_full(self, row):
	# 	if (row['ds_name'] == '2010') or (row['ds_name'].startswith('2011') and row['model_use_nts_vars'] == 0):
	# 		nts_vars = 'none'
	# 	else:
	# 		m = re.match(r'.*--[nt]\d+-\d+--(.*)', row['ds_fname'])
	# 		nts_vars = m.group(1) if m is not None else 'all'
	#
	# 	return '{} {} {} {}'.format(row['ds_name'], row['model_units'], row['model_act'], nts_vars)

	# ------------------------------------------------------------------------------------------------------------------
	# def _gen_nts_vars(self, row):
	# 	if (row['ds_name'] == '2010') or (row['ds_name'].startswith('2011') and row['model_use_nts_vars'] == 0):
	# 		return 'none'
	# 	else:
	# 		m = re.match(r'.*--[nt]\d+-\d+--(.*)', row['ds_fname'])
	# 		return m.group(1) if m is not None else 'all'

	# ------------------------------------------------------------------------------------------------------------------
	def _idx_roc(self, orderby, ds=None, title=None, fn_xy=None, fn_ts=None, fpath=None):
		'''
		fpath
			None   -->  show figure
			'...'  -->  save figure
		'''

		y_range = (0.4, 1.0)  # math.floor(round(self.conn.execute('SELECT MIN(roc_auc) FROM fold_eval').fetchone()[0]) * 10) / 10.0

		ds = ds or self.DS

		q = '''
			SELECT
				d.name AS ds_name,
				d.nts_vars_name AS ds_nts_vars,
				d.n_store AS ds_n_store,
				d.n_emit AS ds_n_emit,
				ml.units AS model_units,
				ml.act AS model_act,
				ROUND(AVG(fe.roc_auc), 4) AS eval_roc_auc
			FROM fold f
			INNER JOIN model m ON m.id = f.model_id
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN (SELECT DISTINCT model_id, units, act FROM model_layer GROUP BY model_id) ml ON ml.model_id = m.id
			INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval GROUP BY fold_id) fe ON fe.fold_id = f.id
			WHERE d.name IN ("{}")
			GROUP BY f.batch_id, m.batch_size
			ORDER BY {}
			'''.format('","'.join(ds), orderby)

		df = pd.read_sql_query(q, self.conn, params=None)
		df.insert(0, 'idx', range(0, len(df)))
		df['ds_name_full'] = df.apply(lambda row: '{} {} {} {}'.format(row['ds_name'], row['model_units'], row['model_act'], row['ds_nts_vars']), axis=1)

		# plt.style.use('classic')

		fig = plt.figure(figsize=(20, 10))
		fig.subplots_adjust(left=0.035, bottom=0.06, top=0.99 if title is None else 0.97, right=0.9975)

		ax = plt.subplot()
		ax.margins(0.01, 0.0)

		ax.axhline(y=0.5, color='black', linestyle='--', linewidth=1, alpha=0.5, label='chance level', antialiased=True)
		for y in [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
			ax.axhline(y=y, color='black', linestyle=':', linewidth=1, alpha=0.25, antialiased=True)

		if fn_xy is not None: fn_xy(df, y_range[0], ax, self.MARKER, self.COL_EDGE, self.COL_FACE, self.HATCH)
		if fn_ts is not None: fn_ts(df, y_range[0], ax, self.COL_EDGE)

		ax.tick_params(axis='both', which='both', direction='in')  # labelsize, labelrotation, pad
		ax.set_ylim([y_range[0], y_range[1]])
		ax.set_xlabel('Index', fontsize=15)
		ax.set_ylabel('Mean ROC AUC  (cross-validation, batch size)', fontsize=15)

		if fn_xy != self._idx_roc__xy_ts: ax.legend(ncol=3, framealpha=0.5)

		if title is not None: plt.title(title)
		if fpath is not None: plt.savefig('{}.png'.format(fpath), dpi=self.DPI)
		else: plt.show()

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__ts(self, df, y_min, ax, col_edge):
		y_ts_ln  = y_min + 0.02
		y_ts_txt = y_ts_ln + 0.008

		for i, ds in enumerate(self.DS):
			n_store_prev = -1
			for name, group in df.where(df['ds_name'] == ds).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
			# for name, group in df.query('ds_name = "{}"'.format(ds)).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
				x_ln_min = group.idx.iloc[0]
				x_ln_max = group.idx.iloc[-1]

				ax.plot((x_ln_min, x_ln_max), (y_ts_ln, y_ts_ln), color=col_edge[name[0]], linestyle='-', linewidth=1, alpha=0.5, antialiased=True)

				n_store = int(name[3])
				if n_store in [2, 5, 10] and n_store != n_store_prev:
					ax.text(x_ln_min, y_ts_txt, str(n_store), fontsize=6, verticalalignment='top')
					n_store_prev = n_store

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__ts_ds(self, df, y_min, ax, col_edge):
		y_ds     = 0.02
		y_ts_ln  = y_min + 0.02 + len(self.DS) * y_ds
		y_ts_txt = y_ts_ln + 0.008

		for i, ds in enumerate(self.DS):
			n_store_prev = -1
			for name, group in df.where(df['ds_name'] == ds).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
			# for name, group in df.query('ds_name = "{}"'.format(ds)).groupby(['ds_name', 'model_act', 'model_units', 'ds_n_store', 'ds_n_emit', 'ds_nts_vars']):
				x_ln_min = group.idx.iloc[0]
				x_ln_max = group.idx.iloc[-1]

				ax.plot((x_ln_min, x_ln_max), (y_ts_ln, y_ts_ln), color=col_edge[name[0]], linestyle='-', linewidth=1, alpha=0.5, antialiased=True)

				n_store = int(name[3])
				if n_store in [2, 5, 10] and n_store != n_store_prev:
					ax.text(x_ln_min, y_ts_txt, str(n_store), fontsize=6, verticalalignment='top')
					n_store_prev = n_store

			y_ts_ln  -= y_ds
			y_ts_txt -= y_ds

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__xy(self, df, y_min, ax, marker, col_edge, col_face, hatch):
		for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'ds_nts_vars', 'ds_name_full']):
			ax.scatter(group.idx, group.eval_roc_auc, marker=marker[name[2]], s=round(name[1] * 2), edgecolors=col_edge[name[0]], facecolor=col_face[name[3]][name[0]], hatch=hatch[name[3]], alpha=0.4, label=name[4], antialiased=True)

	# ----^-------------------------------------------------------------------------------------------------------------
	def _idx_roc__xy_ts(self, df, y_min, ax, marker, col_edge, col_face, hatch):
		for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'ds_nts_vars', 'ds_name_full', 'ds_n_store', 'ds_n_emit']):
			ax.scatter(group.idx, group.eval_roc_auc, marker=r'${}.{}$'.format(name[5], name[6]), s=round(name[1] * 4), edgecolors='none', facecolor=col_face['rspd'][name[0]], antialiased=True)

	# ------------------------------------------------------------------------------------------------------------------
	def top_n(self, n, m, fpath):
		y_range = (0.4, 1.0)  # math.floor(round(self.conn.execute('SELECT MIN(roc_auc) FROM fold_eval').fetchone()[0]) * 10) / 10.0

		fig, axarr = plt.subplots(nrows=len(self.DS), ncols=len(self.TS), sharex='col', sharey='row', figsize=(20, 5))
		fig.subplots_adjust(left=0.035, bottom=0.06, top=0.99, right=0.9975, wspace=0, hspace=0.05)

		for (i_ds, ds), (i_ts, ts) in itertools.product(enumerate(self.DS), enumerate(self.TS)):
			q = '''
				SELECT
					d.name AS ds_name,
					d.nts_vars_name AS ds_nts_vars,
					d.n_store AS ds_n_store,
					d.n_emit AS ds_n_emit,
					ml.units AS model_units,
					ml.act AS model_act,
					ROUND(AVG(fe.roc_auc), 4) AS eval_roc_auc
				FROM fold f
				INNER JOIN model m ON m.id = f.model_id
				INNER JOIN ds d ON d.id = f.ds_id
				INNER JOIN (SELECT DISTINCT model_id, units, act FROM model_layer GROUP BY model_id) ml ON ml.model_id = m.id
				INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval GROUP BY fold_id) fe ON fe.fold_id = f.id
				WHERE d.name = "{}" AND d.n_store = {} AND d.n_emit = {}
				GROUP BY f.batch_id, m.batch_size
				ORDER BY model_act, model_units
				LIMIT {}
				'''.format(ds, ts[0], ts[1], n)

			df = pd.read_sql_query(q, self.conn, params=None)
			df.insert(0, 'idx', range(0, len(df)))
			df['ds_name_full'] = df.apply(lambda row: '{} {} {} {}'.format(row['ds_name'], row['model_units'], row['model_act'], row['ds_nts_vars']), axis=1)

			ax = axarr[i_ds, i_ts]
			ax.margins(0.01, 0.0)

			ax.axhline(y=0.5, color='black', linestyle='--', linewidth=1, alpha=0.5, label='chance level' if (i_ds == 0 and i_ts == 0) else None, antialiased=True)
			for y in [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
				ax.axhline(y=y, color='black', linestyle=':', linewidth=1, alpha=0.25, antialiased=True)

			self._idx_roc__xy(df, y_range[0], ax, self.MARKER, self.COL_EDGE, self.COL_FACE, self.HATCH)

			ax.set_xlim([-1, max(df['idx']) + 1])
			ax.set_ylim([y_range[0], y_range[1]])
			# ax.get_yaxis().set_ticklabels([''] + list(np.arange(y_range[0] + 0.1, y_range[1], 0.1)) + [''])

			if i_ds == len(self.DS): ax.tick_params(axis='x', which='both', direction='in')
			else:                    ax.tick_params(axis='x', which='both', bottom='off', labelbottom='off')

			if i_ts == 0:            ax.tick_params(axis='y', which='both', direction='in')
			else:                    ax.tick_params(axis='y', which='both', left='off', labelleft='off')

			if i_ts == 0:                ax.set_ylabel(ds,                           fontsize=5)
			if i_ds == len(self.DS) - 1: ax.set_xlabel('{}.{}'.format(ts[0], ts[1]), fontsize=5)

			if i_ts == len(self.TS) - 1: ax.legend(framealpha=0.5)

		# plt.xlabel('Index', fontsize=15)
		# plt.ylabel('Mean ROC AUC  (cross-validation, batch size)', fontsize=15)

		# fig.text(0.5, 0.04, 'Index', ha='center')
		# fig.text(0.04, 0.5, 'Mean ROC AUC  (cross-validation, batch size)', va='center', rotation='vertical')

		if fpath is not None: plt.savefig('{}.png'.format(fpath), dpi=self.DPI)
		else: plt.show()

	# ------------------------------------------------------------------------------------------------------------------
	# def boxplots(self):
	# 	orderby = 'ds_name, model_act, model_units, ds_n_store, ds_n_emit'
	# 	title = "Yo mamma"
	# 	fpath = None
	#
	# 	y_range = (0.4, 1.0)  # math.floor(round(self.conn.execute('SELECT MIN(roc_auc) FROM fold_eval').fetchone()[0]) * 10) / 10.0
	#
	# 	q = '''
	# 		SELECT
	# 			d.name AS ds_name,
	# 			d.fname AS ds_fname,
	# 			d.n_store AS ds_n_store,
	# 			d.n_emit AS ds_n_emit,
	# 			ml.units AS model_units,
	# 			ml.act AS model_act,
	# 			m.use_nts_vars AS model_use_nts_vars,
	# 			ROUND(AVG(fe.roc_auc), 4) AS eval_roc_auc
	# 		FROM fold f
	# 		INNER JOIN model m ON m.id = f.model_id
	# 		INNER JOIN ds d ON d.id = f.ds_id
	# 		INNER JOIN (SELECT model_id, units, act FROM model_layer GROUP BY model_id) ml ON ml.model_id = m.id
	# 		INNER JOIN (SELECT fold_id, roc_auc FROM fold_eval GROUP BY fold_id) fe ON fe.fold_id = f.id
	# 		GROUP BY f.batch_id, m.batch_size
	# 		ORDER BY {}
	# 		'''.format(orderby)
	#
	# 	df = pd.read_sql_query(q, self.conn, params=None)
	# 	df.insert(0, 'idx', range(0, len(df)))
	# 	df['nts_vars']     = df.apply(self._gen_nts_vars,     axis=1)
	# 	df['ds_name_full'] = df.apply(self._gen_ds_name_full, axis=1)
	#
	# 	data = []
	# 	meta = []
	# 	for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'nts_vars', 'ds_name_full']):
	# 		data.append(sorted(group.eval_roc_auc))
	# 		meta.append(DotMap({ 'ds_name': name[0], 'model_units': name[1], 'model_act': name[2], 'nts_vars': name[3], 'ds_name_full': name[4] }))
	#
	# 	fig = plt.figure(figsize=(20, 10))
	# 	fig.subplots_adjust(left=0.035, bottom=0.06, top=0.99 if title is None else 0.97, right=0.9975)
	#
	# 	ax = plt.subplot()
	# 	ax.margins(0.01, 0.0)
	#
	# 	ax.axhline(y=0.5, color='black', linestyle='--', linewidth=1, alpha=0.5, label='chance level', antialiased=True)
	# 	for y in [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
	# 		ax.axhline(y=y, color='black', linestyle=':', linewidth=1, alpha=0.25, antialiased=True)
	#
	# 	# 	p.set_edgecolor(self.COL_EDGE[m.ds_name])
	# 	# 	p.set_facecolor(self.COL_FACE[m.nts_vars][m.ds_name])
	# 	# 	p.set_hatch(self.HATCH[m.nts_vars])
	#
	# 	# https://stackoverflow.com/questions/16592222/matplotlib-group-boxplots
	# 	# i = 1
	# 	# for name, group in df.groupby(['ds_name', 'model_units', 'model_act', 'nts_vars', 'ds_name_full']):
	# 	# 	boxprops = dict(linestyle='-', linewidth=1, color=self.COL_EDGE[name[0]])
	# 	# 	flierprops = dict(marker='o', markerfacecolor=self.COL_EDGE[name[0]], markersize=12, linestyle='none')
	# 	# 	medianprops = dict(linestyle='-.', linewidth=2.5, color='firebrick')
	# 	# 	meanpointprops = dict(marker='D', markeredgecolor='black', markerfacecolor='firebrick')
	# 	# 	meanlineprops = dict(linestyle='--', linewidth=2.5, color='purple')
	# 	#
	# 	# 	ax.boxplot([group.eval_roc_auc], positions=[i], widths=0.5, patch_artist=True, boxprops=boxprops, flierprops=flierprops, meanprops=meanpointprops, medianprops=medianprops)
	# 	# 	i += 1
	# 	#
	# 	# 	# marker=marker[name[2]], s=round(name[1] * 2), edgecolors=col_edge[name[0]], facecolor=col_face[name[3]][name[0]], hatch=hatch[name[3]], label=name[4]
	# 	#
	# 	# ax.set_xlim([0, i])
	#
	#
	# 	# boxprops = [dict(linestyle='-', linewidth=1, color=self.COL_EDGE[m.ds_name]) for m in meta]
	# 	# flierprops = dict(marker='o', markerfacecolor='green', markersize=12, linestyle='none')
	# 	# medianprops = dict(linestyle='-.', linewidth=2.5, color='firebrick')
	# 	# meanpointprops = dict(marker='D', markeredgecolor='black', markerfacecolor='firebrick')
	# 	# meanlineprops = dict(linestyle='--', linewidth=2.5, color='purple')
	# 	#
	# 	# ax.boxplot(data, boxprops=boxprops, flierprops=flierprops, meanprops=meanpointprops, medianprops=medianprops)
	#
	#
	# 	# matplotlib.org/examples/statistics/boxplot_color_demo.html
	# 	# stackoverflow.com/questions/41997493/python-matplotlib-boxplot-color
	# 	# blog.bharatbhole.com/creating-boxplots-with-matplotlib
	# 	# parts = ax.boxplot(data, patch_artist=True)
	# 	# 	# whiskers caps boxes medians fliers means
	# 	# 	# matplotlib.org/api/_as_gen/matplotlib.axes.Axes.boxplot.html
	# 	#
	# 	# for i in range(len(data)):
	# 	# 	m = meta[i]
	# 	#
	# 	# 	box = parts['boxes'][i]
	# 	# 	box.set(edgecolor=self.COL_EDGE[m.ds_name], facecolor=self.COL_FACE[m.nts_vars][m.ds_name], linewidth=2)
	# 	# 	box.set_alpha(0.4)
	# 	#
	# 	# 	whisker = parts['whiskers'][i]
	# 	# 	whisker.set(color=self.COL_EDGE[m.ds_name], linewidth=2)
	# 	# 	whisker.set_alpha(0.4)
	# 	#
	# 	# 	cap = parts['caps'][i]
	# 	# 	cap.set(color=self.COL_EDGE[m.ds_name], linewidth=2)
	# 	# 	cap.set_alpha(0.4)
	# 	#
	# 	# 	median = parts['medians'][i]
	# 	# 	median.set(color=self.COL_EDGE[m.ds_name], linewidth=2)
	# 	# 	median.set_alpha(0.4)
	# 	#
	# 	# 	flier = parts['fliers'][i]
	# 	# 	flier.set(marker=self.MARKER[m.model_act], color=self.COL_EDGE[m.ds_name], alpha=0.5)
	# 	# 	flier.set_alpha(0.4)
	#
	#
	# 	# matplotlib.org/devdocs/gallery/statistics/customized_violin.html
	# 	def adjacent_values(vals, q1, q3):
	# 	    upper_adjacent_value = q3 + (q3 - q1) * 1.5
	# 	    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])
	#
	# 	    lower_adjacent_value = q1 - (q3 - q1) * 1.5
	# 	    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
	# 	    return lower_adjacent_value, upper_adjacent_value
	#
	# 	parts = ax.violinplot(data, showmeans=False, showmedians=False, showextrema=False, widths=0.9)
	# 	for i in range(len(data)):
	# 		p = parts['bodies'][i]
	# 		m = meta[i]
	#
	# 		p.set(edgecolor=self.COL_EDGE[m.ds_name], facecolor=self.COL_FACE[m.nts_vars][m.ds_name], hatch=self.HATCH[m.nts_vars], linewidth=2)
	# 		p.set_alpha(0.4)
	#
	# 		# marker=marker[name[2]], s=round(name[1] * 2), edgecolors=col_edge[name[0]], facecolor=col_face[name[3]][name[0]], hatch=hatch[name[3]], label=name[4]
	#
	# 	quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75], axis=1)
	# 	whiskers = np.array([adjacent_values(d, q1, q3) for d, q1, q3 in zip(data, quartile1, quartile3)])
	# 	whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]
	#
	# 	inds = np.arange(1, len(medians) + 1)
	# 	ax.scatter(inds, medians, marker='o', color='k', s=round(m.model_units * 2), alpha=0.4, zorder=3)
	# 	ax.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=4, alpha=0.4)
	# 	ax.vlines(inds, whiskersMin, whiskersMax, color='k', linestyle='-', lw=1, alpha=0.4)
	#
	# 	# meta.append(DotMap({ 'ds_name': name[0], 'model_units': name[1], 'model_act': name[2], 'nts_vars': name[3], 'ds_name_full': name[4] }))
	#
	# 	# self.MARKER[m.model_act]
	# 	# ax.set_xticks([i + 1 for i in range(len(data))])
	#
	# 	ax.tick_params(axis='both', which='both', direction='in')  # labelsize, labelrotation, pad
	# 	ax.set_ylim([y_range[0], y_range[1]])
	# 	ax.set_xlabel('Index', fontsize=15)
	# 	ax.set_ylabel('Mean ROC AUC  (cross-validation, batch size)', fontsize=15)
	#
	# 	ax.legend(ncol=3, framealpha=0.5)
	#
	# 	if title is not None: plt.title(title)
	# 	if fpath is not None: plt.savefig('{}.png'.format(fpath), dpi=self.DPI)
	# 	else: plt.show()


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	from util import FS


	dir_root = FS.dir_mk((Path.home(), 'data', 'eye-link', 'nn'))
	dir_out  = FS.dir_mk((dir_root, 'out'))

	fpath_db = os.path.join(dir_root, 'sess', 'db.sqlite3')

	dba = DBAnalysis(fpath_db)

	dba.top_n(10, 20, None)  # os.path.join(dir_out, '13.top_n')

	# Show:
	# ds = None
	# ds = ['2010']
	# ds = ['2011 aa']
	# ds = ['2011 gg']

	# dba.idx_roc__batch_xy (ds)
	# dba.idx_roc__batch_ts (ds)
	# dba.idx_roc__roc      (ds)

	# Save:
	# dba.idx_roc__batch_xy (None,        '01.', '',          True)
	# dba.idx_roc__batch_ts (None,        '02.', '',          True)
	#
	# dba.idx_roc__batch_xy (['2010'],    '03.', '--2010',    True)
	# dba.idx_roc__batch_ts (['2010'],    '04.', '--2010',    True)
	#
	# dba.idx_roc__batch_xy (['2011 aa'], '05.', '--2011-aa', True)
	# dba.idx_roc__batch_ts (['2011 aa'], '06.', '--2011-aa', True)
	#
	# dba.idx_roc__batch_xy (['2011 gg'], '07.', '--2011-gg', True)
	# dba.idx_roc__batch_ts (['2011 gg'], '08.', '--2011-gg', True)
	#
	# dba.idx_roc__roc      (None,        '09.', '',          True)
	# dba.idx_roc__roc      (['2010'],    '10.', '--2010',    True)
	# dba.idx_roc__roc      (['2011 aa'], '11.', '--2011-aa', True)
	# dba.idx_roc__roc      (['2011 gg'], '12.', '--2011-gg', True)
