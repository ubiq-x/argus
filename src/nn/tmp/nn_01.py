import numpy as np
import os

from keras        import backend as K
from keras.models import Sequential
from keras.layers import Activation, Dense, Dropout


# (1) Data:
data_tr = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.20.2010--n10-5--tr.txt"), delimiter='\t')
data_va = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.20.2010--n10-5--va.txt"), delimiter='\t')

data_tr = data_tr[~np.isnan(data_tr).any(axis=1)]
data_va = data_va[~np.isnan(data_va).any(axis=1)]

y_tr = data_tr[:, 0].reshape((data_tr.shape[0], 1)).astype(int)
x_tr = data_tr[:, range(1, data_tr.shape[1])]

y_va = data_va[:, 0].reshape((data_va.shape[0], 1)).astype(int)
x_va = data_va[:, range(1, data_va.shape[1])]

y_tr[y_tr == -1] = 0
y_va[y_va == -1] = 0

print("y_tr: {}, x_tr: {}, y_va: {}, x_va: {}".format(y_tr.shape, x_tr.shape, y_va.shape, x_va.shape))

# print("y_tr: {}".format(y_tr[:5]))
# print("x_tr: {}".format(x_tr[:1]))
# print("y_va: {}".format(y_va[:5]))
# print("x_va: {}".format(x_va[:1]))


# (2) Model:
# (2.1) Define:
def act_tanh(x): return 1.7159 * K.tanh((2 / 3) * x)


model = Sequential()
model.add(Dense(64, input_dim=x_tr.shape[1]))  # activation="relu"
model.add(Activation("relu"))
model.add(Dropout(0.5))
model.add(Dense(64))
model.add(Activation("relu"))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation("sigmoid"))
# model.add(Activation(act_tanh))

# (2.2) Train:
model.compile(loss="binary_crossentropy", optimizer="rmsprop", metrics=["accuracy"])  # optimizer: adam, rmsprop

model.fit(x_tr, y_tr, epochs=20, batch_size=128)

# (2.3) Evaluate:
score = model.evaluate(x_va, y_va, batch_size=128)
print(score)


# 2010 20s n10-5    [0.46649855375289917, 0.837837815284729]
# random test data  [0.68710565567016602, 0.61000001430511475]
