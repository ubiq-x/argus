#
# TODO
#     Reject duplicate runs from prior sessions
#     Make a copy of the database at the end of every schedule and every predefined number of iterations
#     Visualization
#         Fitting process (loss and accuracy, both training and validation)
#             Not particularily useful at this point, but will be later, for the article
#         The network itself
#             keras.io/getting-started/faq/#how-can-i-obtain-the-output-of-an-intermediate-layer
#     Compile TF from sources to make use of extra speed
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
#         W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Solved issues
#     Missing data imputations
#         Most missing data is associated with not fixated words.  That's most likely due to the input dataset
#         containing words seen as well (i.e., PS=1; perceptual span).  Some GD are missing despite the FFD being
#         there.  For those rows, other variables are missing too.  This is weird and probably goes back to Pegasus.
#         However, this is fairly rare so I leave it as is for now.  Below are the numbers and percentages of
#         missing data (not due to perceptual span) for the 'roi.30-_.ps1.adj.20.2010.txt' dataset.
#
#             var  em_wlen  em_nf  em_nr  em_nb  em_ffd  em_gd  em_let_i_fp  em_let_o_fp  em_sacc_i_roi_fp  em_sacc_o_roi_fp  em_sacc_i_let_fp  em_sacc_o_let_fp  word_nlet  subtl_frq_log10
#             all    21163  21163  21163  21163   21163  19925        20245        20209             20209             20245             20209             20245      19348            19286
#             miss       0      0      0      0       0   1238          918          954               918               954               918               954       1815             1877
#             %          0      0      0      0       0   6.21         4.53         4.72              4.53              4.72              4.53              4.72       9.38             9.73
#
#         At this point I simply ignore the issue as it probably doesn't have a large impact on the results.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import json
import keras
import numpy as np

from dotmap import DotMap

from db import DB
from util import FS, Tee, Time


# ======================================================================================================================
class HyperParameterOptimizerGrid(object):
	'''
	Works with search space defined by the model object.
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	FS_DNAME_SESS     = 'sess'
	FS_DNAME_MODELS   = 'models'
	FS_DNAME_REP      = 'rep'

	FS_FNAME_DB       = 'db.sqlite3'
	FS_FNAME_DB_DEBUG = 'db-debug.sqlite3'
	FS_FNAME_LOG      = 'log.txt'
	FS_FNAME_REP      = 'rep.txt'

	STDOUT_INDENT = '    '

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_ds, dir_out):
		self.dir_ds  = dir_ds
		self.dir_out = dir_out

		self.dir_sess = FS.dir_mk((self.dir_out, self.FS_DNAME_SESS))

		self.fpath_db = os.path.join(self.dir_sess, (self.FS_FNAME_DB if (self.DEBUG_LVL == 0) else self.FS_FNAME_DB_DEBUG))

		self.db = DB(self.fpath_db)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_begin(self, sess_id=None):
		self.sess_ts_0 = round(Time.ts())

		self.sess_id = sess_id or self.sess_ts_0

		self.dir_sess_root   = FS.dir_mk((self.dir_sess, str(self.sess_id)))
		self.dir_sess_models = FS.dir_mk((self.dir_sess_root, self.FS_DNAME_MODELS))

		self.fpath_log        = os.path.join(self.dir_sess_root, self.FS_FNAME_LOG)
		self.fpath_rep_basic  = os.path.join(self.dir_sess_root, self.FS_FNAME_REP)

		self.db.sess_add(self.sess_id, self.dir_ds, self.dir_sess_root)

		self.tee = Tee(self.fpath_log)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_end(self):
		'''
		Prints reports and consolidates the session (e.g., compresses certain result files or insert them into the
		database).
		'''

		# (1) Reports:
		# (1.1) Basic:
		rep = self.db.rep_basic()

		print('\n\nsess-id: {}'.format(self.db.curr_sess_id))
		print(rep)

		FS.save_gz(self.fpath_rep_basic, rep)  # GZ > BZ2 for smaller reports only, but the differences are small so I use the more popular one

		# (1.2) Aggregate:
		# ...

		# (2) GZ files:
		FS.gz(self.fpath_log, do_del=True)

		# for f in os.listdir(self.dir_sess_models):
		# 	FS.gz(os.path.join(self.dir_sess_models, f), do_del=True)  # GZ > BZ2 for models

		# (3) Clean up:
		self.db.sess_set_dur(round(Time.ts() - self.sess_ts_0))
		self.sess_id = None

		self.tee.flush()
		self.tee.end()
		self.tee = None

	# ------------------------------------------------------------------------------------------------------------------
	def sess_run(self, schedule):
		from itertools import product

		# Determine the total size of the schedule loop:
		iter_n, iter_n_cv = 0, 0
		for s in schedule:
			iter_n    += s.model.get_n(s.space)
			iter_n_cv += s.model.get_n(s.space) * s.folds

		print('Schedule info\n{0}Item count: {1}\n{0}Total loop size: {2}  ({3} with cross-validation)\nSchedule loop'.format(self.STDOUT_INDENT, len(schedule), iter_n, iter_n_cv))

		# Run the schedule loop:
		for i, s in enumerate(schedule):
			model = s.model(self.dir_ds, self.dir_sess_models, self.db)
			folds = s.folds
			space = s.space

			iter_n_ = model.get_n(space)
			print('{}Item {} of {} loop size: {}  ({} with cross-validation)'.format(self.STDOUT_INDENT, i + 1, len(schedule), iter_n_, iter_n_ * folds))

			for j, v in enumerate(list(product(*space.values()))):
				model.run(j, iter_n_, folds, *v, tee=self.tee)


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path

	from model_lr import ModelLogReg
	from model_fc import ModelFC

	dir_root = os.path.join(str(Path.home()), 'data', 'eye-link')
	dir_ds   = os.path.join(dir_root, 'ds', 'out')
	dir_out  = os.path.join(dir_root, 'nn')

	hpo = HyperParameterOptimizerGrid(dir_ds, dir_out)

	# ModelLogReg:
	# hpo.sess_begin()
	# hpo.sess_run(ModelLogReg.schedule_get_micro())
	# hpo.sess_end()

	# ModelFC:
	hpo.sess_begin()
	hpo.sess_run(ModelFC.schedule_get_micro())
	hpo.sess_end()
