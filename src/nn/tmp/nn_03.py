import keras
import numpy as np
import os

from keras        import backend as kb
from keras.models import Model, Sequential
from keras.layers import Activation, Dense, Dropout, Input, LSTM

from sklearn.metrics import roc_auc_score


# ======================================================================================================================
class NN02(object):
	'''
	Multiple input model that combines word and eye-movement variables.
	'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, ds, n_store, n_emit):
		self.ds = ds

		self.n_store = n_store
		self.n_emit  = n_emit
		self.n_var   = None  # infered in data_load()

		self.model = None

	# ------------------------------------------------------------------------------------------------------------------
	def _act_tanh_17159(x):
		return 1.7159 * kb.tanh((2 / 3) * x)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load(self):
		self.data_tr = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.{}--n{}-{}--tr.txt".format(self.ds, self.n_store, self.n_emit)), delimiter='\t')
		self.data_va = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.{}--n{}-{}--va.txt".format(self.ds, self.n_store, self.n_emit)), delimiter='\t')

		self.data_tr = self.data_tr[~np.isnan(self.data_tr).any(axis=1)]
		self.data_va = self.data_va[~np.isnan(self.data_va).any(axis=1)]

		self.n_var = int((self.data_tr.shape[1] - 1) / self.n_store)

		self.y_tr = self.data_tr[:, 0].reshape((self.data_tr.shape[0], 1)).astype(int)
		self.x_tr = []
		for i in range(self.n_var):
			self.x_tr.append(self.data_tr[:, range(i*self.n_store + 1, (i + 1)*self.n_store + 1)])

		self.y_va = self.data_va[:, 0].reshape((self.data_va.shape[0], 1)).astype(int)
		self.x_va = []
		for i in range(self.n_var):
			self.x_va.append(self.data_va[:, range(i*self.n_store + 1, (i + 1)*self.n_store + 1)])

		# self.y_tr[self.y_tr == -1] = 0
		# self.y_va[self.y_va == -1] = 0

		# print("y_tr:    {}, y_va:    {}".format(self.y_tr.shape, self.y_va.shape))
		# for i in range(self.n_var):
		# 	print("x_tr[{0}]: {1}, x_va[{0}]: {2}".format(i, self.x_tr[i].shape, self.x_va[i].shape))

	# ------------------------------------------------------------------------------------------------------------------
	def do(self):
		self.data_load()
		self.model_define()
		self.model_train()
		self.model_eval()

	# ------------------------------------------------------------------------------------------------------------------
	def model_define(self):
		# self.model = Sequential()
		# self.model.add(Input(shape=self.x_tr.shape))
		# self.model.add(Activation("relu"))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(64))
		# self.model.add(Activation("relu"))
		# self.model.add(Dropout(0.5))
		# self.model.add(Dense(1))
		# self.model.add(Activation("sigmoid"))
		# # self.model.add(Activation(self._act_tanh_17159))

		inputs = [Input(shape=(self.x_tr[i].shape[1],)) for i in range(self.n_var)]

		x = keras.layers.concatenate(inputs)

		x = Dense(64, activation="relu")(x)
		x = Dropout(0.5)(x)
		x = Dense(64, activation="relu")(x)
		x = Dropout(0.5)(x)

		output = Dense(1, activation="sigmoid")(x)

		# self.model = Model(inputs=inputs, outputs=[output])
		self.model = Model(inputs, output)

		self.model.compile(loss="binary_crossentropy", optimizer="rmsprop", metrics=["accuracy"])  # optimizer: adam, rmsprop

	# ------------------------------------------------------------------------------------------------------------------
	def model_train(self):
		self.model.fit(self.x_tr, self.y_tr, epochs=20, batch_size=128)

	# ------------------------------------------------------------------------------------------------------------------
	# http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
	def model_eval(self):
		acc = self.model.evaluate(self.x_va, self.y_va, batch_size=128)

		y_va_pred = self.model.predict(self.x_va)
		roc_auc = roc_auc_score(self.y_va, y_va_pred)

		print("Accuracy: {} {}".format(round(acc[0], 6), round(acc[1])))
		print("ROC AUC: {}".format(round(roc_auc, 6)))

		print("acc0  acc1  roc_auc")
		print("{:2f}  {:2f}     {:2f}".format(round(acc[0], 2), round(acc[1], 2), round(roc_auc, 2)))


# ======================================================================================================================
if __name__ == "__main__":
	# nn = NN02("gg.20.2011", 10, 5)

	nn = NN02("20.2010", 10, 5)
	nn.do()

# ---------------------------------
#     n          2010          2011
#  s  e       0     1       0     1
# -----    ----------    ----------
#  2  1    0.66  0.60    0.69  0.54
#  3  1    0.61  0.68    0.69  0.56
#  5  1    0.55  0.70    0.69  0.53
#  5  2    0.52  0.75    0.70  0.52
# 10  1    0.27  0.92    0.67  0.60
# 10  2    0.32  0.89    0.69  0.53
# 10  5    0.28  0.93    0.68  0.59
# ---------------------------------
#                  20s NP resampled
