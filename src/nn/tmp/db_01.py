#
# TODO
#     Move mode.gen and model.arch to a separate dictionary table
#     Add FKs and other contraints
#         Estimate space cost
#     Add aggregate reports
#     Finish generating reports
#         stackoverflow.com/questions/3685195/line-up-columns-of-numbers-print-output-in-table-format
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Issues solved
#     Compressing and storing ndarray in SQLite
#         stackoverflow.com/questions/3310584/insert-binary-file-in-sqlite-database-with-python
#         stackoverflow.com/questions/18621513/python-insert-numpy-array-into-sqlite3-database
#         stackoverflow.com/questions/11760095/convert-binary-string-to-numpy-array
#     Conclusions
#         If ndarray of ints    : encode_arr > encode_str
#         If ndarray of dobules : encode_str > encode_arr
#
# ----------------------------------------------------------------------------------------------------------------------
#
# *** Add the fold.fold_batch column ***
#
# with self.conn as c:
# 	fold_batch = 0
#
# 	for row in self.conn.execute('SELECT id, fold FROM fold ORDER BY id').fetchall():
# 		fold_id = int(row[0])
# 		fold    = int(row[1])
#
# 		if fold == 1:
# 			fold_batch += 1
#
# 		c.execute('UPDATE fold SET batch_id = ? WHERE id = ?', [fold_batch, fold_id])
#
# ----------------------------------------------------------------------------------------------------------------------
#
# *** Add and populate the ds.nts_vars column ***
#
# ...
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import numpy as np
import bz2
import sqlite3

from array import array
from collections import OrderedDict
from sklearn.metrics import accuracy_score, brier_score_loss, cohen_kappa_score, confusion_matrix, f1_score, matthews_corrcoef, precision_score, recall_score, roc_auc_score

from util import Time


# ======================================================================================================================
class DB(object):
	'''
	Statistical model hyperparameter optimization database.

	The class caches IDs of several last used objects (i.e., session, dataset, model, fold, and result) to ease
	referencing.  For example, once a new sessions is added, there is no need to remember its ID and pass it alone
	when adding a new fold -- this class takes care of that mundane task.
	'''

	REP_COL_SEP = '  '

	DDL = OrderedDict()
	DDL['sess'] = '''
		CREATE TABLE IF NOT EXISTS sess (
		id       INTEGER PRIMARY KEY AUTOINCREMENT,
		ts       INT,
		dur      INT,
		dt       TEXT,
		dir_ds   TEXT,
		dir_sess TEXT)
		'''
	DDL['ds'] = '''
		CREATE TABLE IF NOT EXISTS ds (
		id            INTEGER PRIMARY KEY AUTOINCREMENT,
		name          TEXT,
		fname         TEXT,
		n_var_nts     INT,
		n_var_ts      INT,
		n_store       INT,
		n_emit        INT,
		nts_vars_lst  TEXT,
		nts_vars_idx  TEXT,
		nts_vars_name TEXT)
		'''
	DDL['model'] = '''
		CREATE TABLE IF NOT EXISTS model (
		id           INTEGER PRIMARY KEY AUTOINCREMENT,
		gen          TEXT,
		arch         TEXT,
		n_epochs     INT,
		batch_size   INT,
		n_in         INT)
		'''
	DDL['model_layer'] = '''
		CREATE TABLE IF NOT EXISTS model_layer (
		id       INTEGER PRIMARY KEY AUTOINCREMENT,
		model_id INT,
		num      INT,
		type     TEXT,
		units    INT,
		act      TEXT,
		CONSTRAINT fk__model_layer__model FOREIGN KEY (model_id) REFERENCES model (id) ON UPDATE CASCADE ON DELETE CASCADE
		)
		'''
	DDL['fold'] = '''
		CREATE TABLE IF NOT EXISTS fold (
		id            INTEGER PRIMARY KEY AUTOINCREMENT,
		sess_id       INT,
		ds_id         INT,
		model_id      INT,
		batch_id      INT,
		fold          INT,
		ts            INT,
		dur           INT,
		n_tr_all      INT,
		n_va_all      INT,
		n_te_all      INT,
		n_tr          INT,
		n_va          INT,
		n_te          INT,
		fit_t         INT,
		loss          REAL,
		acc           REAL,
		fname_model   TEXT,
		fname_weights TEXT,
		y_prob_dtype  TEXT,
		y             BLOB,
		y_prob        BLOB,
		CONSTRAINT fk__fold__sess  FOREIGN KEY (sess_id)  REFERENCES sess  (id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk__fold__ds    FOREIGN KEY (ds_id)    REFERENCES ds    (id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk__fold__model FOREIGN KEY (model_id) REFERENCES model (id) ON UPDATE CASCADE ON DELETE CASCADE
		)
		'''
	DDL['fold_fit'] = '''
		CREATE TABLE IF NOT EXISTS fold_fit (
		id      INTEGER PRIMARY KEY AUTOINCREMENT,
		fold_id INT,
		epoch   INT,
		loss_tr REAL,
		acc_tr  REAL,
		loss_va REAL,
		acc_va  REAL,
		CONSTRAINT fk__fold_fit__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
		)
		'''
	DDL['fold_eval'] = '''
		CREATE TABLE IF NOT EXISTS fold_eval (
		id      INTEGER PRIMARY KEY AUTOINCREMENT,
		fold_id INT,
		tn      INT,
		fp      INT,
		fn      INT,
		tp      INT,
		spc     REAL,
		sen     REAL,
		rec     REAL,
		f1      REAL,
		brier   REAL,
		mcc     REAL,
		kappa   REAL,
		acc     REAL,
		roc_auc REAL,
		CONSTRAINT fk__fold_eval__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
		)
		'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, fpath, rep_col_sep=REP_COL_SEP):
		for v in self.DDL.values():
			print(v)
		return

		self.fpath = fpath

		self.rep_col_sep = rep_col_sep

		self.conn = sqlite3.connect(self.fpath)
		self.conn.execute('PRAGMA foreign_keys = ON')
		self.conn.execute('PRAGMA auto_vacuum = FULL')

		# self.conn.row_factory = sqlite3.Row

		self._db_create()

		self.curr_sess_id  = None
		self.curr_ds_id    = None
		self.curr_model_id = None
		self.curr_fold_id  = None

		self.fold_batch_id = -1  # used to group together folds belonging to the same batch; set by fold_add()

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.conn.commit()
		self.conn.close()

	# ------------------------------------------------------------------------------------------------------------------
	def _db_add_fk(self):
		'''
		Add all foreign key contraints to the database.  I needed to do that on Oct 4, 2017 upon realizing that SQLite
		doesn't support adding those constraints through the ALTER TABLE statement.  Databases created from scratch
		do not need this run.
		'''

		q = []

		q.append('''
			BEGIN TRANSACTION;
			CREATE TEMPORARY TABLE model_layer_tmp AS SELECT * FROM model_layer WHERE 0;
			INSERT INTO model_layer_tmp SELECT * FROM model_layer;
			DROP TABLE model_layer;
			CREATE TABLE t1(a,b);
			INSERT INTO t1 SELECT a,b FROM t1_backup;
			DROP TABLE t1_backup;
			COMMIT;
			''')

		with self.conn as c:
			c.executescript(";".join(q))

	# ------------------------------------------------------------------------------------------------------------------
	def _db_create(self):
		q = []

		q.append('''
			CREATE TABLE IF NOT EXISTS sess (
			id       INTEGER PRIMARY KEY AUTOINCREMENT,
			ts       INT,
			dur      INT,
			dt       TEXT,
			dir_ds   TEXT,
			dir_sess TEXT)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS ds (
			id            INTEGER PRIMARY KEY AUTOINCREMENT,
			name          TEXT,
			fname         TEXT,
			n_var_nts     INT,
			n_var_ts      INT,
			n_store       INT,
			n_emit        INT,
			nts_vars_lst  TEXT,
			nts_vars_idx  TEXT,
			nts_vars_name TEXT)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS model (
			id           INTEGER PRIMARY KEY AUTOINCREMENT,
			gen          TEXT,
			arch         TEXT,
			n_epochs     INT,
			batch_size   INT,
			n_in         INT)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS model_layer (
			id       INTEGER PRIMARY KEY AUTOINCREMENT,
			model_id INT,
			num      INT,
			type     TEXT,
			units    INT,
			act      TEXT,
			CONSTRAINT fk__model_layer__model FOREIGN KEY (model_id) REFERENCES model (id) ON UPDATE CASCADE ON DELETE CASCADE
			)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS fold (
			id            INTEGER PRIMARY KEY AUTOINCREMENT,
			sess_id       INT,
			ds_id         INT,
			model_id      INT,
			batch_id      INT,
			fold          INT,
			ts            INT,
			dur           INT,
			n_tr_all      INT,
			n_va_all      INT,
			n_te_all      INT,
			n_tr          INT,
			n_va          INT,
			n_te          INT,
			fit_t         INT,
			loss          REAL,
			acc           REAL,
			fname_model   TEXT,
			fname_weights TEXT,
			y_prob_dtype  TEXT,
			y             BLOB,
			y_prob        BLOB,
			CONSTRAINT fk__fold__sess  FOREIGN KEY (sess_id)  REFERENCES sess  (id) ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT fk__fold__ds    FOREIGN KEY (ds_id)    REFERENCES ds    (id) ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT fk__fold__model FOREIGN KEY (model_id) REFERENCES model (id) ON UPDATE CASCADE ON DELETE CASCADE
			)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS fold_fit (
			id      INTEGER PRIMARY KEY AUTOINCREMENT,
			fold_id INT,
			epoch   INT,
			loss_tr REAL,
			acc_tr  REAL,
			loss_va REAL,
			acc_va  REAL,
			CONSTRAINT fk__fold_fit__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
			)
			''')

		q.append('''
			CREATE TABLE IF NOT EXISTS fold_eval (
			id      INTEGER PRIMARY KEY AUTOINCREMENT,
			fold_id INT,
			tn      INT,
			fp      INT,
			fn      INT,
			tp      INT,
			spc     REAL,
			sen     REAL,
			rec     REAL,
			f1      REAL,
			brier   REAL,
			mcc     REAL,
			kappa   REAL,
			acc     REAL,
			roc_auc REAL,
			CONSTRAINT fk__fold_eval__fold FOREIGN KEY (fold_id) REFERENCES fold (id) ON UPDATE CASCADE ON DELETE CASCADE
			)
			''')

		with self.conn as c:
			c.executescript(";".join(q))

	# ------------------------------------------------------------------------------------------------------------------
	def _db_empty(self):
		q = '''
			DELETE FROM sess;
			DELETE FROM ds;
			DELETE FROM model;
			DELETE FROM fold;
			DELETE FROM fold_fit;
			VACUUM;
		'''

		with self.conn as c:
			c.executescript(q)

	# ------------------------------------------------------------------------------------------------------------------
	def _get_id(self, tbl, where):
		row = self.conn.execute('SELECT rowid FROM {} WHERE {}'.format(tbl, where)).fetchone()
		return row[0] if row else None

	# ------------------------------------------------------------------------------------------------------------------
	def _ins(self, q, args):
		with self.conn as c:
			return c.execute(q, args).lastrowid

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__arr(obj, arr_type):
		a = array(arr_type)
		a.frombytes(bz2.decompress(obj))  # BZ2 > GZ for ndarray (especially int)
		return np.asarray(a)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__pickle(obj):
		return bz2.decompress(np.loads(obj))  # BZ2 > GZ for ndarray (when n > ~500; GZ wins for smaller ones)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dec__str(a, dtype):
		return np.fromstring(bz2.decompress(a), dtype=dtype)  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_enc__arr(arr, arr_type):
		return bz2.compress(array(arr_type, arr))  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_encode__pickle(arr):
		return bz2.compress(arr.dumps())  # BZ2 > GZ for ndarray (when n > ~500; GZ wins for smaller ones)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dbl_enc(arr):
		return DB._ndarray_enc__str(arr)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_dbl_dec(obj, dtype):
		return DB._ndarray_dec__str(obj, dtype)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_enc__str(obj):
		return bz2.compress(obj.tostring())  # BZ2 > GZ for ndarray (especially int)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_int_enc(arr):
		return DB._ndarray_enc__arr(arr, 'i')

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def _ndarray_int_dec(obj):
		return DB._ndarray_dec__arr(obj, 'i')

	# ------------------------------------------------------------------------------------------------------------------
	def _rep_gen(self, query, delim):
		delim = delim or self.rep_col_sep

		rep = []
		for row in self.conn.execute(query).fetchall():
			ln = []
			for field in row:
				ln.append(str(field))
			rep.append(delim.join(ln))
		return '\n'.join(rep)

	# ------------------------------------------------------------------------------------------------------------------
	def _upd(self, q, args):
		with self.conn as c:
			c.execute(q, args)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def bool(i): return 1 if i else 0

	# ------------------------------------------------------------------------------------------------------------------
	def ds_add(self, name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name):
		# Exists:
		ds_id = self._get_id('ds', 'name = "{}" AND fname = "{}" AND n_var_nts = {} AND n_var_ts = {} AND n_store = {} AND n_emit = {} AND nts_vars_lst = "{}" AND nts_vars_idx = "{}" AND nts_vars_name = "{}"'.format(name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name))
		if ds_id is not None:
			self.curr_ds_id = ds_id
			return self.curr_ds_id

		# Add:
		self.curr_ds_id = self._ins('''
			INSERT INTO ds (name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
			''', [name, fname, n_var_nts, n_var_ts, n_store, n_emit, nts_vars_lst, nts_vars_idx, nts_vars_name])

		return self.curr_ds_id

	# ------------------------------------------------------------------------------------------------------------------
	def debug_disp_fold(self, id):
		y, y_prob, y_pred = self.fold_get_ys(id)

		# y      = np.append(y, 0)
		# y_pred = np.append(y_pred, 1)

		print('y:      {}'.format(y))
		print('y_pred: {}'.format(y_pred))
		print('y_prob: {}'.format(y_prob))

		print(confusion_matrix(y, y_pred).astype(int).ravel())

	# ------------------------------------------------------------------------------------------------------------------
	# def debug_rep(self, delim=None, sess_id=None):
	# 	delim = delim or self.rep_col_sep
	#
	# 	sess_id = sess_id or self.curr_sess_id
	#
	# 	q = '''
	# 		SELECT
	# 			n_obs_te_nna,
	# 			LENGTH(y) AS len_y,
	# 			LENGTH(y_prob) AS len_y_prob
	# 		FROM fold
	# 		{}
	# 		ORDER BY n_obs_te_nna, len_y, len_y_prob
	# 		'''.format('WHERE id = {}'.format(sess_id) if (sess_id is not None and sess_id > 0) else '')
	#
	# 	return self._rep_gen(q, delim)

	# ------------------------------------------------------------------------------------------------------------------
	def fold_add(self, fold, ts, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te, sess_id=None, ds_id=None, model_id=None):
		'''
		The first fold (i.e., 'fold == 1') will automatically start a new fold batch.
		'''

		sess_id  = sess_id  or self.curr_sess_id
		ds_id    = ds_id    or self.curr_ds_id
		model_id = model_id or self.curr_model_id

		if fold == 1:
			self.fold_batch_id = round(Time.ts())

		self.curr_fold_id = self._ins('''
			INSERT INTO fold (sess_id, ds_id, model_id, batch_id, fold, ts, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			''', [sess_id, ds_id, model_id, self.fold_batch_id, fold, ts, n_tr_all, n_va_all, n_te_all, n_tr, n_va, n_te])

		return self.curr_fold_id

	# ------------------------------------------------------------------------------------------------------------------
	# scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
	#
	# ROC curve plots
	#     scikit-learn.org/stable/modules/generated/sklearn.metrics.auc.html#sklearn.metrics.auc
	#     scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py
	#     scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
	#
	def fold_comp_all_eval(self):
		'''
		Computes the evaluation metrics for all folds in the database.
		'''

		with self.conn as c:
			c.executescript('DELETE FROM fold_eval; VACUUM;')

			for row in self.conn.execute('SELECT id FROM fold').fetchall():
				fold_id = row[0]
				# print(fold_id)

				# comp_ctx = self._fold_comp_all_eval__get_comp_ctx(fold_id)

				y, y_prob, y_pred = self.fold_get_ys(fold_id)

				cm = confusion_matrix(y, y_pred).astype(int).ravel()  # if
				tn = int(cm[0])
				fp = int(cm[1]) if len(cm) > 1 else None
				fn = int(cm[2]) if len(cm) > 1 else None
				tp = int(cm[3]) if len(cm) > 1 else None

				spc     = tn / (tn+fp) if (tp is not None and tn+fp > 0) else None
				sen     = precision_score   (y, y_pred)
				rec     = recall_score      (y, y_pred)
				f1      = f1_score          (y, y_pred)
				brier   = brier_score_loss  (y, y_prob)  # probabilities, not labels!
				mcc     = matthews_corrcoef (y, y_pred)
				kappa   = cohen_kappa_score (y, y_pred)
				acc     = accuracy_score    (y, y_pred)  # use sklearn, coz people report incorrect accuracy in Keras (may have to do with not properly removed zero-padding)

				try:
					roc_auc = roc_auc_score (y, y_prob)  # probabilities, not labels!
				except ValueError:
					roc_auc = None

				c.execute('''
					INSERT INTO fold_eval (fold_id, tn, fp, fn, tp, spc, sen, rec, f1, brier, mcc, kappa, acc, roc_auc)
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
					''', [fold_id, tn, fp, fn, tp, spc, sen, rec, f1, brier, mcc, kappa, acc, roc_auc])

	# ------------------------------------------------------------------------------------------------------------------
	def fold_get_ys(self, fold_id):
		row = self.conn.execute('SELECT y, y_prob, y_prob_dtype FROM fold WHERE id = ?', [fold_id]).fetchone()

		y      = DB._ndarray_int_dec(row[0])
		y_prob = DB._ndarray_dbl_dec(row[1], row[2])
		y_pred = DB.np_prob_to_pred(y_prob)

		return (y, y_prob, y_pred)

	# ------------------------------------------------------------------------------------------------------------------
	def fold_set_dur(self, dur, fold_id=None):
		fold_id = fold_id or self.curr_fold_id
		self._upd('UPDATE fold SET dur = ? WHERE id = ?', [dur, fold_id])

	# ------------------------------------------------------------------------------------------------------------------
	def fold_upd_fit(self, fit_t, fit_hist, fname_model, fname_weights, fold_id=None):
		fold_id = fold_id or self.curr_fold_id

		h = fit_hist  # shorthand
		values = [(i + 1, h['loss'][i], h['acc'][i], h['val_loss'][i], h['val_acc'][i]) for i in range(len(h['loss']))]

		with self.conn as c:
			c.execute('''
				UPDATE fold
				SET fit_t = ?, fname_model = ?, fname_weights = ?
				WHERE id = ?
				''', [fit_t, fname_model, fname_weights, fold_id])
			c.executemany('''
				INSERT INTO fold_fit (fold_id, epoch, loss_tr, acc_tr, loss_va, acc_va)
				VALUES ({}, ?, ?, ?, ?, ?)
				'''.format(fold_id), values)

	# ------------------------------------------------------------------------------------------------------------------
	def fold_upd_eval(self, loss, acc, y, y_prob, fold_id=None):
		fold_id = fold_id or self.curr_fold_id

		with self.conn as c:
			c.execute('''
				UPDATE fold
				SET loss = ?, acc = ?, y_prob_dtype = ?, y = ?, y_prob = ?
				WHERE id = ?
			''', [loss, acc, y_prob.dtype.name, DB._ndarray_int_enc(y), DB._ndarray_dbl_enc(y_prob), fold_id])

			# mix: ''', [loss, acc, y_prob.dtype.name, DB._ndarray_int_enc(ts_id), DB._ndarray_int_enc(y), DB._ndarray_dbl_enc(y_prob), fold_id])
			# arr: ''', [loss, acc, 'i', 'i', 'd', DB._ndarray_enc_arr(ts_id, 'i'), DB._ndarray_enc_arr(y, 'i'), DB._ndarray_enc_arr(y_prob, 'd'), fold_id])
			# str: ''', [loss, acc, ts_id.dtype.name, y.dtype.name, y_prob.dtype.name, DB._ndarray_enc_str(ts_id), DB._ndarray_enc_str(y), DB._ndarray_enc_str(y_prob), fold_id])

	# ------------------------------------------------------------------------------------------------------------------
	def model_add(self, gen, arch, layers, n_epochs, batch_size, n_in):
		# Exists:
		model_id = self._get_id('model', 'gen = "{}" AND arch = "{}" AND n_epochs = {} AND batch_size = {} AND n_in = {}'.format(gen, arch, n_epochs, batch_size, n_in))
		if model_id is not None:
			self.curr_model_id = model_id
			return self.curr_model_id

		# Add:
		with self.conn as c:
			self.curr_model_id = c.execute('''
				INSERT INTO model (gen, arch, n_epochs, batch_size, n_in)
				VALUES (?, ?, ?, ?, ?)
				''', [gen, arch, n_epochs, batch_size, n_in]).lastrowid

			for l in layers:
				c.execute('''
					INSERT INTO model_layer (model_id, num, type, units, act)
					VALUES (?, ?, ?, ?, ?)
					''', [self.curr_model_id, l.num, l.type, l.units, l.act])

		return self.curr_model_id

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def np_prob_to_pred(y_prob):
		''' Used to reconstruct 'y_pred' from 'y_prob' to eliminate the need to store 'y_pred'. '''

		return np.where(y_prob > 0.5, 1, 0)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_basic(self, delim=None, sess_id=None):
		sess_id = sess_id or self.curr_sess_id

		q = '''
			SELECT
				s.id, m.id, d.id, f.id, SUBSTR(s.dt, 1, 10),
				m.gen, m.arch, ml.n, m.n_epochs, m.batch_size, m.n_in,
				d.name, d.fname, d.n_store, d.n_emit, d.n_var_nts, d.n_var_ts, d.nts_vars_name,
				f.fold,
				f.dur,
				f.n_obs_tr_all,
				f.n_obs_va_all,
				f.n_obs_te_all,
				f.n_obs_tr_nna,
				f.n_obs_va_nna,
				f.n_obs_te_nna,
				f.fit_t,
				ff.n,
				ROUND(ff.acc_tr_max, 4),
				ROUND(ff.acc_va_max, 4),
				ROUND(f.acc, 4),
				LENGTH(f.y),
				LENGTH(f.y_prob)
			FROM fold f
			INNER JOIN sess s ON s.id = f.sess_id
			INNER JOIN model m ON m.id = f.model_id
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN (SELECT model_id, COUNT(*) AS n FROM model_layer GROUP BY model_id) ml ON m.id = ml.model_id
			INNER JOIN (SELECT fold_id, COUNT(*) AS n, MAX(acc_tr) AS acc_tr_max, MAX(acc_va) AS acc_va_max FROM fold_fit GROUP BY fold_id) ff ON ff.fold_id = f.id
			{}
			GROUP BY m.id, d.id, f.id
			ORDER BY f.id
			'''.format('WHERE s.id = {}'.format(sess_id) if (sess_id > 0) else '')

		return self._rep_gen(q, delim)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_ds(self):
		return self._rep_gen('SELECT * FROM ds', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_model(self):
		return self._rep_gen('SELECT * FROM model', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def rep_ls_sess(self):
		return self._rep_gen('SELECT * FROM sess', self.rep_col_sep)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_add(self, ts, dir_ds, dir_sess):
		# Exists:
		sess_id = self._get_id('sess', 'ts = {}'.format(ts))
		if sess_id is not None:
			self.curr_sess_id = sess_id
			return self.curr_sess_id

		# Add:
		self.curr_sess_id = self._ins('''
			INSERT INTO sess (ts, dt, dir_ds, dir_sess)
			VALUES (?, ?, ?, ?)
			''', [ts, Time.ts2dt(ts), dir_ds, dir_sess])

		return self.curr_sess_id

	# ------------------------------------------------------------------------------------------------------------------
	def sess_set_dur(self, dur, sess_id=None):
		sess_id = sess_id or self.curr_sess_id
		self._upd('UPDATE sess SET dur = ? WHERE id = ?', [dur, sess_id])

	# ------------------------------------------------------------------------------------------------------------------
	# def x(self):
	# 	with self.conn as c:
	# 		for row in self.conn.execute('SELECT id, name, fname FROM ds ORDER BY id').fetchall():
	# 			ds_id = int(row[0])
	# 			name  = str(row[1])
	# 			fname = str(row[2])
	#
	# 			m = re.match(r'^.*--[nt]\d+-\d+($|--.*$)', fname)
	# 			nts_vars = m.group(1) if m is not None else 'all'
	#
	# 			# c.execute('UPDATE ds SET nts_vars = ? WHERE id = ?', [nts_vars, ds_id])
	# 			print(nts_vars)


# ======================================================================================================================
if __name__ == '__main__':
	from pathlib import Path


	db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3'))


	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db-debug.sqlite3'))
	# db.x()
	# sys.exit(0)


	# Display fold:
	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3'))
	# db.debug_disp_fold(3933)
	# sys.exit(0)


	# Compute evaluation for all folds:
	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3'))
	# db.fold_comp_all_eval()
	# sys.exit(0)


	# Show basic report:
	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db.sqlite3'))
	# print(db.rep_basic(sess_id=10))
	# sys.exit(0)


	# Check if computed accuracy equals the one from Keras (conclusion: it does):
	# db = DB(os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'sess', 'db-debug.sqlite3'))
	# with db.conn as c:
	# 	for row in c.execute('''
	# 		SELECT ROUND(ABS(f.acc - fe.acc), 2), ROUND(f.acc, 2), ROUND(fe.acc, 2), ROUND(fe.roc_auc, 2)
	# 		FROM fold f
	# 		INNER JOIN ds d on d.id = f.ds_id
	# 		INNER JOIN model m on m.id = f.model_id
	# 		INNER JOIN fold_eval fe on fe.fold_id = f.id
	# 		''', []).fetchall():
	# 		print("{:4}: {:4} {:4}    {:4}".format(row[0], row[1], row[2], row[3]))
	# sys.exit(0)


	# ------------------------------------------------------------------------------------------------------------------
	# Create and use a test database:
	# fpath = os.path.join(Path.home(), 'data', 'eye-link', 'nn', 'db-test.sqlite3')
	# res_cols = [['a', ''], ['b', 0], ['acc', 0.0]]
	#
	# db = DB(fpath)
	#
	# # db._db_empty()
	#
	# db.ds_add('ds-01', 'ds-01.txt', 7, 3, 4, 2)
	# db.model_add('model-01', 32, False, 17)
	#
	# db.sess_add(round(Time.ts()))
	#
	# db.fold_add(1)
	# db.res_add(['well', 1, 0.7012])
	# db.fit_hist_add({"loss": [0.1, 0.2], "acc": [0.2, 0.3], "val_loss": [0.3, 0.4], "val_acc": [0.4, 0.5]})
	#
	# ...
	#
	# print("SESSIONS:")
	# print(db.rep_ls_sess())
	#
	# print("\nMODELS:")
	# print(db.rep_ls_model())
	#
	# print("\nDATASETS:")
	# print(db.rep_ls_ds())
	#
	# print("\nBASIC REPORT (CURRENT SESSION):")
	# print(db.rep_basic())
	#
	# print("\nBASIC REPORT (ALL SESSIONS):")
	# print(db.rep_basic(sess_id=-1))

	# ------------------------------------------------------------------------------------------------------------------
	# def rep_gen_raw2(self):
	# 	rep = []
	#
	# 	e = self.conn.cursor().execute
	#
	# 	# Determin max lenght of each column:
	# 	cols_len = [e('SELECT "{}" FROM res'.format(c[0])).fetchall() for c in self.res_cols]         # column data
	# 	cols_len = [max([len(str(cl[0])) for cl in cols_len[i]]) for i in range(len(self.res_cols))]  # max lenght of column data
	# 	cols_len = [max(cl, len(self.res_cols[i][0])) for i, cl in enumerate(cols_len)]               # max lenght of column data and column name
	#
	# 	# Print header:
	# 	rep.append(self.rep_col_sep.join([('{:%s%d}' % (c[2], cols_len[i])).format(c[0]) for i, c in enumerate(self.res_cols)]))
	#
	# 	# Print data:
	# 	for row in e('SELECT * FROM res').fetchall():
	# 		ln = []
	# 		for i, field in enumerate(row):
	# 			ln.append(('{:%s%d}' % (self.res_cols[i][2], cols_len[i])).format(field))
	#
	# 		rep.append(self.rep_col_sep.join(ln))
	#
	# 	return '\n'.join(rep)
