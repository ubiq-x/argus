#
# 2010 20s n10-5 NPS  [0.46649855375289917, 0.837837815284729]
# 2011 20s n10-5 NPS  [0.6681788356149968, 0.61691541547205908]
#
# 2010 20s n10-5 NP   [0.30857762694358826, 0.9189189076423645]
# 2011 20s n10-5 NP   [0.68284478456298736, 0.53757225691927646]
#
# 2010 20s n10-5 NP resampled  [0.59188913959295641, 0.68393783186383816]
# 2011 20s n10-5 NP resampled  [0.69262228311576279, 0.51724138004439213]
#
# random data     [0.68710565567016602, 0.61000001430511475]
#
# ----------------------------------------------------------------------------------------------------------------------

import numpy as np
import os

from keras        import backend as kb
from keras.models import Sequential
from keras.layers import Activation, Dense, Dropout


# ======================================================================================================================
class NN01(object):
	'''
	A sequential model with two FC layers that combines word and eye-movement variables.
	'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, n_store, n_emit):
		self.n_store = n_store
		self.n_emit  = n_emit

		self.model = None

	# ------------------------------------------------------------------------------------------------------------------
	def _act_tanh_17159(x):
		return 1.7159 * kb.tanh((2 / 3) * x)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load(self):
		self.data_tr = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.20.2011--n{}-{}--tr.txt".format(self.n_store, self.n_emit)), delimiter='\t')
		self.data_va = np.genfromtxt(os.path.join("..", "data", "out", "fold-1", "roi.30-_.ps1.adj.20.2011--n{}-{}--va.txt".format(self.n_store, self.n_emit)), delimiter='\t')

		self.data_tr = self.data_tr[~np.isnan(self.data_tr).any(axis=1)]
		self.data_va = self.data_va[~np.isnan(self.data_va).any(axis=1)]

		self.y_tr = self.data_tr[:, 0].reshape((self.data_tr.shape[0], 1)).astype(int)
		self.x_tr = self.data_tr[:, range(1, self.data_tr.shape[1])]

		self.y_va = self.data_va[:, 0].reshape((self.data_va.shape[0], 1)).astype(int)
		self.x_va = self.data_va[:, range(1, self.data_va.shape[1])]

		# self.y_tr[self.y_tr == -1] = 0
		# self.y_va[self.y_va == -1] = 0

		print("y_tr: {}, x_tr: {}, y_va: {}, x_va: {}".format(self.y_tr.shape, self.x_tr.shape, self.y_va.shape, self.x_va.shape))

	# ------------------------------------------------------------------------------------------------------------------
	def do(self):
		self.data_load()
		self.model_define()
		self.model_train()
		self.model_eval()

	# ------------------------------------------------------------------------------------------------------------------
	def model_define(self):
		self.model = Sequential()
		self.model.add(Dense(64, input_dim=self.x_tr.shape[1]))  # activation="relu"
		self.model.add(Activation("relu"))
		self.model.add(Dropout(0.5))
		self.model.add(Dense(64))
		self.model.add(Activation("relu"))
		self.model.add(Dropout(0.5))
		self.model.add(Dense(1))
		self.model.add(Activation("sigmoid"))
		# self.model.add(Activation(self._act_tanh_17159))

		self.model.compile(loss="binary_crossentropy", optimizer="rmsprop", metrics=["accuracy"])  # optimizer: adam, rmsprop

	# ------------------------------------------------------------------------------------------------------------------
	def model_train(self):
		self.model.fit(self.x_tr, self.y_tr, epochs=20, batch_size=128)

	# ------------------------------------------------------------------------------------------------------------------
	def model_eval(self):
		score = self.model.evaluate(self.x_va, self.y_va, batch_size=128)
		print(score)


# ======================================================================================================================
if __name__ == "__main__":
	nn = NN01(10, 5)
	nn.do()
