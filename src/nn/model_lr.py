import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import json
import keras
import numpy as np

from dotmap import DotMap

from keras.models import Model, Sequential
from keras.layers import Activation, Dense, Dropout, Input, LSTM

import model

from db   import DB
from util import FS, Tee, Time


# ======================================================================================================================
class ModelLogReg(model.Model):
	'''
	Logistic regression model implemented as a single sigmoid unit.
	'''

	GEN = 'LogReg'

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, dir_ds, dir_out, db=None, fpath_db=None):
		super(ModelLogReg, self).__init__(dir_ds, dir_out, db, fpath_db)

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		super(ModelLogReg, self).__del__()

	# ------------------------------------------------------------------------------------------------------------------
	def _ds_name_get(self, ds, ts):
		ds_name  = ds[1]
		ds_fname = 'roi.30-_.ps1.adj.{}--n{}-{}'.format(ds[0], ts[0], ts[1])

		return (ds_name, ds_fname)

	# ------------------------------------------------------------------------------------------------------------------
	def data_load_one(self, fold, ds_suff_ls, is_init=False):
		'''
		Loads data from a combination of training ('tr'), validation ('va'), and test ('te') datasets as indicated by
		the 'ds_suff_ls'.  Typically either (1) only one of those files is loaded (for fit-evaluate cycle) or (2) all
		of them are (for external evaluation where all datasets should be combined back into the original one from
		which they were created to begin with).
		'''

		fpath = None
		data = []
		size_file = 0
		meta = None

		# Load data:
		for ds_suff in ds_suff_ls:
			fpath = os.path.join(self.dir_ds, 'fold-{}'.format(fold), '{}--{}.txt'.format(self.ds_fname, ds_suff))

			# Load meta data and exit if dataset exists (executed for the first file only because the files match):
			if meta is None:
				with open(fpath, 'r') as f:
					meta = DotMap(json.loads(f.readline()))
					meta.n_var_nts_curr = len(self.nts_vars_lst)

					vars_idx_nts = [meta.vars_nts.index(i) + 1 for i in self.nts_vars_lst]  # add 1 because y is at index 0
				if is_init:
					ds_id = self.db.ds_get_id(self.ds_name, self.ds_fname, meta.n_var_nts, meta.n_var_ts, meta.n_store, meta.n_emit, ','.join(self.nts_vars_lst), ','.join([str(i) for i in vars_idx_nts]), self.nts_vars_name)
					if ds_id is not None:
						return

			# Load data:
			data.append(np.genfromtxt(fpath, delimiter=self.DS_DELIM, skip_header=1))
			size_file += os.path.getsize(fpath)

		data = np.concatenate(data, axis=0)
		n_all = data.shape[0]

		# Remove missing values:
		data = data[~np.isnan(data).any(axis=1)]
		n = data.shape[0]

		# Y and X:
		# vars_idx_ts = list(range(meta.n_var_nts + 1, data[0].shape[1]))
		vars_idx_ts = list(range(meta.n_var_nts + 1, data.shape[1]))

		y = data[:, 0].reshape((data.shape[0], 1)).astype(np.int8)  # save some memory by casting from np.int64
		x = data[:, vars_idx_nts + vars_idx_ts]

		# Save in the database and set current (or just set current):
		if self.db is not None:
			ds_id = self.db.ds_get_id(self.ds_name, self.ds_fname, meta.n_var_nts, meta.n_var_ts, meta.n_store, meta.n_emit, ','.join(self.nts_vars_lst), ','.join([str(i) for i in vars_idx_nts]), self.nts_vars_name)
			if ds_id is None:
				size_mem = self.memsize_ds(x, y, meta.n_var_nts_curr, meta.n_var_ts, meta.n_store)
				self.db.ds_add(self.ds_name, self.ds_fname, meta.n_var_nts, meta.n_var_ts, meta.n_store, meta.n_emit, ','.join(self.nts_vars_lst), ','.join([str(i) for i in vars_idx_nts]), self.nts_vars_name, size_file, (size_mem.x + size_mem.y))
			else:
				self.db.curr_ds_id = ds_id

		return DotMap(x=x, y=y, n_all=n_all, n=n)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def memsize_ds(x, y, n_var_nts, n_var_ts, n_store):
		size = DotMap()
		size.x = y.shape[0] * (n_var_nts + n_var_ts * n_store) * x[0].itemsize  # sum([xi.nbytes for xi in x])
		size.y = y.shape[0] * y.itemsize
		return size

	# ------------------------------------------------------------------------------------------------------------------
	def model_def(self, fold, is_init=False):
		# Define:
		n_in = self.ds.tr.x.shape[1]

		self.model = Sequential()
		self.model.add(Dense(1, input_dim=n_in, activation="sigmoid"))

		self.model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])  # optimizer: adam, rmsprop

		# Save in the database and set current (or just set current):
		if self.db is not None:
			model_id = self.db.model_get_id(self.GEN, self.GEN, [], self.epochs, self.batch_size, n_in)
			if model_id is None:
				size_mem = model.Model.memsize_model(self.batch_size, self.model)
				self.db.model_add(self.GEN, self.GEN, [], self.epochs, self.batch_size, n_in, size_mem)
			else:
				self.db.curr_model_id = model_id

	# ------------------------------------------------------------------------------------------------------------------
	def _run(self, is_init, schedule_id, iter_i, iter_n, fold, ds, nts_vars, ts, batch_size, epochs, sess_id=None, is_verbose=True, tee=None):
		self.ds_name, self.ds_fname = self._ds_name_get(ds, ts)

		self.nts_vars_lst  = nts_vars[0]
		self.nts_vars_name = nts_vars[1]
		self.ts            = ts
		self.batch_size    = batch_size
		self.epochs        = epochs

		if is_init:
			if is_verbose: print('{}Iter {} of {}  (FOLD={}  DS=\'{}\'  NTSV={}  TS={}.{}  ARCH={}  BS={})'       .format(self.STDOUT_INDENT * 2, iter_i + 1, iter_n, fold + 1, self.ds_name, self.nts_vars_name, self.ts[0], self.ts[1], self.GEN, self.batch_size))
			super(ModelLogReg, self).run_init(schedule_id, iter_i, fold,          is_verbose, tee)
		else:
			if is_verbose: print('{}Iter {} of {}  (FOLD={}  DS=\'{}\'  NTSV={}  TS={}.{}  ARCH={}  BS={}  E={})' .format(self.STDOUT_INDENT * 2, iter_i + 1, iter_n, fold + 1, self.ds_name, self.nts_vars_name, self.ts[0], self.ts[1], self.GEN, self.batch_size, self.epochs))
			super(ModelLogReg, self).run_comp(schedule_id, iter_i, fold, sess_id, is_verbose, tee)

	# ------------------------------------------------------------------------------------------------------------------
	def run_comp(self, schedule_id, iter_i, iter_n, fold, ds, nts_vars, ts, batch_size, epochs, sess_id=None, is_verbose=True, tee=None):
		'''
		'sess_id' is used in parallel execution.
		'''

		self._run(False, schedule_id, iter_i, iter_n, fold, ds, nts_vars, ts, batch_size, epochs, sess_id, is_verbose, tee)

	# ------------------------------------------------------------------------------------------------------------------
	def run_init(self, schedule_id, iter_i, iter_n, fold, ds, nts_vars, ts, batch_size, epochs, sess_id=None, is_verbose=True, tee=None):
		'''
		'sess_id' is not used and is here only for interface equivalence with run_comp().
		'''

		self._run(True,  schedule_id, iter_i, iter_n, fold, ds, nts_vars, ts, batch_size, epochs, sess_id, is_verbose, tee)

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def schedule_get_full():
		return [
			ModelLogReg.schedule_get_full_ds('LogReg 2010',    ('20.2010',    '2010')),
			ModelLogReg.schedule_get_full_ds('LogReg 2011 gg', ('gg.20.2011', '2011 gg')),
			ModelLogReg.schedule_get_full_ds('LogReg 2011 aa', ('aa.20.2011', '2011 aa'))
		]

	# ----^-------------------------------------------------------------------------------------------------------------
	@staticmethod
	def schedule_get_full_ds(name, ds):
		schedule = DotMap(
			name  = name,
			model = ModelLogReg,
			space = DotMap(
				folds       = range(10),
				datasets    = [ds],
				nts_vars    = [],
				ts          = [],
				batch_sizes = [],
				epochs      = [30]
			)
		)

		ss = schedule.space  # shorthand

		if ds[1] == '2010':
			ss.nts_vars.append(([], 'none'))
		else:
			ss.nts_vars.append(([], 'none'))
			ss.nts_vars.append((['sub_id_wm_fl', 'sub_id_sat', 'sub_read_spd_a', 'sub_ctx_time', 'sub_ctx_fat', 'sub_ctx_pre', 'sub_ctx_crv'], 'all'))
			ss.nts_vars.append((['sub_read_spd_a'], 'rspd'))
			ss.nts_vars.append((['sub_read_spd_a', 'sub_ctx_time'], 'rspd.time'))
			ss.nts_vars.append((['sub_read_spd_a', 'sub_ctx_time', 'sub_ctx_pre'], 'rspd.time.pre'))

		ss.ts.append(( 2,  1))
		ss.ts.append(( 3,  1))
		ss.ts.append(( 5,  1))
		ss.ts.append(( 5,  2))
		ss.ts.append((10,  1))
		ss.ts.append((10,  2))
		ss.ts.append((10,  5))
		ss.ts.append((20,  2))
		ss.ts.append((20,  5))

		ss.batch_sizes.append( 32)
		ss.batch_sizes.append( 64)
		ss.batch_sizes.append(128)

		return schedule

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def schedule_get_micro(folds=[0], ts=[(10,  5)], batch_sizes=[32]):
		return [DotMap(
			name  = 'LogReg 2010',
			model = ModelLogReg,
			space = DotMap(
				folds       = folds,
				datasets    = [('20.2010', '2010')],
				nts_vars    = [([], 'none')],
				ts          = ts,
				batch_sizes = batch_sizes,
				epochs      = [30]
			)
		)]


# ======================================================================================================================
if __name__ == '__main__':
	import fs

	fs = fs.FileSystem()
	fs.DIR_OUT = os.path.join(fs.DIR_DS, fs.DIR_OUT, 'tmp')
	fs.PATH_DB = os.path.join(fs.DIR_OUT, fs.FNAME_DB)

	db = DB(fs.PATH_DB)

	m = ModelLogReg(fs.DIR_DS, fs.DIR_OUT, db)

	m.run(iter_i=1, iter_n=1, fold=1, ds=('20.2010', '2010'), nts_vars=([], 'none'), ts=(10, 5), batch_size=32, epochs=30)

	# m.run(iter_i=1, iter_n=1, fold=1, ds=('aa.20.2011', '2011 aa'), nts_vars=([], 'none'), ts=(10, 5), batch_size=32, epochs=30)
	# m.run(iter_i=1, iter_n=1, fold=1, ds=('aa.20.2011', '2011 aa'), nts_vars=(['sub_read_spd_a'], 'rspd'), ts=(10, 5), batch_size=32, epochs=30)
	# m.run(iter_i=1, iter_n=1, fold=1, ds=('aa.20.2011', '2011 aa'), nts_vars=(['sub_id_wm_fl', 'sub_id_sat', 'sub_read_spd_a', 'sub_ctx_time', 'sub_ctx_fat', 'sub_ctx_pre', 'sub_ctx_crv'], 'all'), ts=(10, 5), batch_size=32, epochs=30)
