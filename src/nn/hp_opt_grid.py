#
# TODO
#     Add a monitor process
#         Use queue to send print messages from child processes
#         Use queue to monitor execution progress
#         Monitor resource utilization (mainly memory)
#             Send pushover message when memory utilization reaches a threshold
#         Force it to run on the same CPU as the main process (because both are almost idle)
#             Not easy, if possible at all
#     Optimize run_init
#         Make a list of all datasets and all models before actually adding them to the database
#             One of the fastest ways to do this would be to ignore fold (maybe even don't expand that branch of the
#             search space to begin with)
#     Resource utilization
#         Add process that stores free memory in a file
#             Also monitor the size of child processes
#                 Max memory
#                     Probe the process after a predefined time and then in intervals
#                     The process itself may provide unreliable data because it cannot check itself during fitting
#                 Completion time
#                     Use a queue for the chile process to communicate the time itself
#                     Keep track of average time
#                     Predict estimated time remaining and print it on the screen periodically
#         Compress res_util.txt
#         Visualize it
#     Use snake to send messages
#         SSH
#     Schedule optimizer
#         Estimate memory requirements of each computation loop iteration (i.e., fold)
#         Sort the schedule according to those requirements from largest to smallest computations
#         On computation start and end of fold
#             Start as many computations as possible given the memory size going from the top of the ordered schedule
#     Make a copy of the database at the end of every schedule and every predefined number of iterations
#     Visualization
#         Fitting process (loss and accuracy, both training and validation)
#             Not particularily useful at this point, but will be later, for the article
#         The network itself
#             keras.io/getting-started/faq/#how-can-i-obtain-the-output-of-an-intermediate-layer
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Solved issues
#     Missing data imputations
#         Most missing data is associated with not fixated words.  That's most likely due to the input dataset
#         containing words seen as well (i.e., PS=1; perceptual span).  Some GD are missing despite the FFD being
#         there.  For those rows, other variables are missing too.  This is weird and probably goes back to Pegasus.
#         However, this is fairly rare so I leave it as is for now.  Below are the numbers and percentages of
#         missing data (not due to perceptual span) for the 'roi.30-_.ps1.adj.20.2010.txt' dataset.
#
#             var  em_wlen  em_nf  em_nr  em_nb  em_ffd  em_gd  em_let_i_fp  em_let_o_fp  em_sacc_i_roi_fp  em_sacc_o_roi_fp  em_sacc_i_let_fp  em_sacc_o_let_fp  word_nlet  subtl_frq_log10
#             all    21163  21163  21163  21163   21163  19925        20245        20209             20209             20245             20209             20245      19348            19286
#             miss       0      0      0      0       0   1238          918          954               918               954               918               954       1815             1877
#             %          0      0      0      0       0   6.21         4.53         4.72              4.53              4.72              4.53              4.72       9.38             9.73
#
#         At this point I simply ignore the issue as it probably doesn't have a large impact on the results.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Sqlite3 concurrency
#     https://stackoverflow.com/questions/393554/python-sqlite3-and-concurrency
#     https://bugs.python.org/issue27113
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import gc
import multiprocessing as mp
import numpy as np
import platform
import psutil
import pushover
import time

from dotmap import DotMap


import db

from util import FS, Size, Tee, Time


# ======================================================================================================================
def _run_par(is_init, model, dir_ds, dir_out, fpath_db, run_args):
	''' A multiprocessing pool worker. '''

	m = model(dir_ds, dir_out, None, fpath_db)
	if is_init:
		m.run_init(*run_args)
	else:
		m.run_comp(*run_args)


# ======================================================================================================================
# class ModelRunProc(mp.Process):
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def __init__(self, is_init, model, dir_ds, dir_out, fpath_db):
# 		print(1)
# 		self.fpath_db = fpath_db
#
# 		self.model = model(dir_ds, dir_out)
# 		self.fn = self.model.run_init if (is_init) else self.model.run_comp
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def run(self, args):
# 		print(2)
# 		fn(schedule_id, j, iter_n, *v, tee=self.tee)


# ======================================================================================================================
class ResMonProc(mp.Process):
	T_SLEEP = 1

	PUSHOVER_SOUND = 'classical'

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, sess_ts_0, schedule, mem_free_alert, fpath_db, t_sleep=T_SLEEP):
		super(ResMonProc, self).__init__()

		self.evt_exit = mp.Event()

		self.sess_ts_0 = sess_ts_0
		self.schedule = schedule
		self.mem_free_alert = mem_free_alert
		self.t_sleep = t_sleep

		self.db = db.DB(fpath_db)

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.db.conn_close()

	# ------------------------------------------------------------------------------------------------------------------
	def run(self):
		has_alerted = False  # alert only once
		while not self.evt_exit.is_set():
			ts_1       = round(Time.ts())
			t_sess     = round(ts_1 - self.sess_ts_0)
			t_schedule = round(ts_1 - self.schedule.ts_0)

			mem  = psutil.virtual_memory()  # .total .used .available
			swap = psutil.swap_memory()     # .total .used .free

			self.db.comp_res_add(self.schedule.id, t_sess, t_schedule, -1, mem.used, swap.used)

			if (mem.available < self.mem_free_alert) and (platform.node() == 'comp') and (not has_alerted):
				pushover.Client().send_message('{}/{}: {} ({} free; {:.1f}%; {})' .format(self.schedule.i + 1, self.schedule.n, self.schedule.name, Size.bytes2human(mem.available), (mem.available / mem.total * 100), Time.sec2time(round(t_schedule / 1000))), title='Memory utilization alert', sound=self.PUSHOVER_SOUND)
				has_alerted = True

			time.sleep(self.t_sleep)

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.evt_exit.set()


# ======================================================================================================================
class HyperParamOptGrid(object):
	'''
	Works with search space defined by the model object.
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	FNAME_DB_DEBUG = 'db-debug.sqlite3'
	FNAME_LOG      = 'log.txt'
	FNAME_REP      = 'rep.txt'
	FNAME_RES_UTIL = 'res_util.txt'

	STDOUT_INDENT = '    '

	PUSHOVER_SOUND = 'classical'

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self):
		import fs

		self.fs = fs.FileSystem()

		if self.DEBUG_LVL == 0:
			self.fpath_db = self.fs.PATH_DB
		else:
			self.fpath_db = self.fs.PATH_DB

		self.db = db.DB(self.fpath_db)

		p = psutil.Process(os.getpid())  # psutil.readthedocs.io/en/latest
		self.comp_ctx = DotMap(
			is_seq         = True,  # flag: is computation sequential or parallel?
			proc           = p,
			do_gpu         = False,
			cpu_n          = mp.cpu_count(),
			cpu_n_use      = 1,
			mem_proc       = p.memory_full_info().uss,
			mem_size       = psutil.virtual_memory().total,
			mem_free_min   =  500 * 1024 * 1024,  # [B]
			mem_free_alert = 2000 * 1024 * 1024,  # [B]
			swap_size      = psutil.swap_memory().total,
			res_mon_int    = ResMonProc.T_SLEEP
		)

		self.pushover = pushover.Client()

	# ------------------------------------------------------------------------------------------------------------------
	def comp_ctx_set(self, is_seq, do_gpu, cpu_n, res_mon_int, mem_free_alert=None):
		cc = self.comp_ctx  # shorthand

		cc.is_seq         = is_seq
		cc.do_gpu         = do_gpu
		cc.res_mon_int    = res_mon_int
		cc.mem_free_alert = mem_free_alert or cc.mem_free_alert

		if cc.do_gpu:
			cc.cpu_n_use = 0
		else:
			os.environ["CUDA_DEVICE_ORDER"]    = "PCI_BUS_ID"  # see issue #152
			os.environ["CUDA_VISIBLE_DEVICES"] = "0"

			if cc.is_seq:
				cc.cpu_n_use = 1
			else:
				if cpu_n > 0:
					cc.cpu_n_use = min(cc.cpu_n - 1, cpu_n)
				elif cpu_n < 0:
					cc.cpu_n_use = max(1, cc.cpu_n - (-cpu_n))
				else:
					cc.cpu_n_use = cc.cpu_n - 1

	# ------------------------------------------------------------------------------------------------------------------
	def fs_init(self):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def notify(self, title, msg, do_force=False):
		if (platform.node() == 'comp') or (do_force):
			self.pushover.send_message(msg, title=title, sound=self.PUSHOVER_SOUND)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_begin(self, sess_id=None):
		'''
		Specifying 'sess_id' will force the use of a possibly existing session (both in the DB and in the FS).  This
		should be used with causion because while attaching computations to an prior session in the DB may seem like
		a good idea, the log and report files will be overwritten in the FS.  That option is to be used sparingly and
		in special circumstances only.
		'''

		self.sess_ts_0 = round(Time.ts())

		self.sess_id = sess_id or self.sess_ts_0

		self.dir_sess_root   = FS.dir_mk((self.fs.DIR_SESS, str(self.sess_id)))
		self.dir_sess_models = FS.dir_mk((self.dir_sess_root, self.fs.DNAME_MODELS))

		self.fpath_log        = os.path.join(self.dir_sess_root, self.FNAME_LOG)
		self.fpath_rep_basic  = os.path.join(self.dir_sess_root, self.FNAME_REP)

		self.db.sess_add(self.sess_id, platform.node(), self.fs.DIR_DS, self.dir_sess_root)

		self.tee = Tee(self.fpath_log)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_end(self):
		'''
		Prints reports and consolidates the session (e.g., compresses certain result files or insert them into the
		database).
		'''

		# (1) Report:
		print('\n\nSession')
		print('{}ID: {}'                .format(self.STDOUT_INDENT, self.db.curr_sess_id))
		print('{}Computation count: {}' .format(self.STDOUT_INDENT, self.db.fold_get_sess_n()))

		FS.save_gz(self.fpath_rep_basic, self.db.rep_basic())
			# GZ > BZ2 for smaller reports only, but the differences are small anyway so I use the more popular one

		# (2) GZ files:
		FS.gz(self.fpath_log, do_del=True)

		# (3) Consolidate the session:
		self.db.sess_set_dur(round(Time.ts() - self.sess_ts_0))
		self.db.vacuum()

		self.sess_id = None

		# (4) Clean up:
		self.tee.flush()
		self.tee.end()
		self.tee = None

		self.db.conn_close()

	# ------------------------------------------------------------------------------------------------------------------
	def sess_run_init(self, schedule, is_seq=True, do_gpu=False, cpu_n=1, res_mon_int=ResMonProc.T_SLEEP, mem_free_alert=None):
		self._sess_run(schedule, True,  is_seq, do_gpu, cpu_n, res_mon_int, mem_free_alert)

	# ------------------------------------------------------------------------------------------------------------------
	def sess_run_comp(self, schedule, is_seq=True, do_gpu=False, cpu_n=1, res_mon_int=ResMonProc.T_SLEEP, mem_free_alert=None):
		self._sess_run(schedule, False, is_seq, do_gpu, cpu_n, res_mon_int, mem_free_alert)

	# ------------------------------------------------------------------------------------------------------------------
	def _sess_run(self, schedule, is_init, is_seq, do_gpu, cpu_n, res_mon_int, mem_free_alert):
		from model import Model

		cc  = self.comp_ctx       # shorthand
		ind = self.STDOUT_INDENT  # ^

		# Computational context:
		self.comp_ctx_set(is_seq, do_gpu, cpu_n, res_mon_int, mem_free_alert)

		print('{}'                               .format('Initialization run' if is_init else 'Computation run'))
		print('Computational context')
		print('{}Use GPU: {}'                    .format(ind, int(cc.do_gpu)))
		print('{}CPU count: {}'                  .format(ind, cc.cpu_n))
		print('{}CPU count use: {}'              .format(ind, cc.cpu_n_use))
		print('{}Memory size: {}'                .format(ind, Size.bytes2human(cc.mem_size)))
		print('{}Memory free min: {}'            .format(ind, Size.bytes2human(cc.mem_free_min)))
		print('{}Memory free alert: {}'          .format(ind, Size.bytes2human(cc.mem_free_alert)))
		print('{}Resource monitor interval: {}s' .format(ind, cc.res_mon_int))
		print('{}Execution: {}'                  .format(ind, 'Sequential' if cc.is_seq else 'Parallel'))

		# Computation loop:
		iter_n_tot = sum([s.model.space_to_n_iter(s.space) for s in schedule])  # number of iterations for all schedules

		print('Schedules')
		print('{}Item count: {}'                 .format(ind, len(schedule)))
		print('{}Total iterations: {}'           .format(ind, iter_n_tot))
		print('Loop')

		mem_proc_0 = cc.proc.memory_full_info().uss

		for i, s in enumerate(schedule):
			schedule_ts_0 = round(Time.ts())
			iter_n_s = Model.space_to_n_iter(s.space)  # number of iteration for the current schedule
			schedule_id = self.db.schedule_add(is_init, i, iter_n_s, cc.is_seq, cc.do_gpu, cc.cpu_n, cc.cpu_n_use, cc.mem_proc, cc.mem_size, cc.mem_free_min, cc.swap_size, cc.res_mon_int)
			self.fs_init()
			print('{}Schedule {} of {}: {}'.format(ind, i + 1, len(schedule), s.name))

			res_mon_proc = ResMonProc(self.sess_ts_0, DotMap(id=schedule_id, i=i, n=iter_n_s, name=s.name, ts_0=schedule_ts_0), self.comp_ctx.mem_free_alert, self.fpath_db, cc.res_mon_int)
			try:
				res_mon_proc.start()

				if is_init: space_ls = [[0]] + [s.space[k] for k in s.space if k not in ['folds']]  # exclude folds to reduce iterations in init run
				else:       space_ls = s.space.values()

				if cc.is_seq: self._sess_run_comp_seq(is_init, schedule_id, s.model, space_ls, iter_n_s)
				else:         self._sess_run_comp_par(is_init, schedule_id, s.model, space_ls, iter_n_s)

				schedule_dur = round(Time.ts() - schedule_ts_0)
				self.db.schedule_set_dur(schedule_dur)

				if is_init:
					self.notify('Schedule complete', '{}/{}: {} ({}; {})'     .format(i + 1, len(schedule), s.name, Time.sec2time(round(schedule_dur / 1000)), iter_n_s))
				else:
					comp_n = self.db.fold_get_schedule_n()
					print('{}Computation count: {}'.format(ind, comp_n))
					self.notify('Schedule complete', '{}/{}: {} ({}; {}, {})' .format(i + 1, len(schedule), s.name, Time.sec2time(round(schedule_dur / 1000)), iter_n_s, comp_n))
			finally:
				res_mon_proc.stop()
				res_mon_proc.join()

		gc.collect()
		mem_proc_1 = cc.proc.memory_full_info().uss
		print('{}Potential memory leak size: {} - {} = {}'.format(ind, Size.bytes2human(mem_proc_1), Size.bytes2human(mem_proc_0), Size.bytes2human(max(0, mem_proc_1 - mem_proc_0))))

	# ----^-------------------------------------------------------------------------------------------------------------
	def _sess_run_comp_seq(self, is_init, schedule_id, model, space_ls, iter_n):
		from itertools import product

		model = model(self.fs.DIR_DS, self.dir_sess_models, self.db)  # sequential execution so we can reuse the existing DB connection
		fn = model.run_init if (is_init) else model.run_comp

		for j, v in enumerate(list(product(*space_ls))):
			fn(schedule_id, j, iter_n, *v, tee=self.tee)

	# ----^-------------------------------------------------------------------------------------------------------------
	def _sess_run_comp_par(self, is_init, schedule_id, model, space_ls, iter_n):
		from itertools import product

		args = []
		for j, v in enumerate(list(product(*space_ls))):
			args.append((is_init, model, self.fs.DIR_DS, self.dir_sess_models, self.fpath_db, [schedule_id, j, iter_n, *v, self.db.curr_sess_id],))
				# pack run_args into a list for later unpacking

		with mp.Pool(processes=self.comp_ctx.cpu_n_use, maxtasksperchild=1) as pool:
			pool.starmap(_run_par, args)
			pool.close()

		# try:
		# 	pool.close()
		# except:
		# 	pool.terminate()
		# 	raise
		# finally:
		# 	pool.join()

	# ----^-------------------------------------------------------------------------------------------------------------
	# def sess_run_comp__iter(self, i_iter, n_iter_left, dur):
	# 	# Time:
	# 	self.model_run_dur.append(dur)
	#
	# 	dur_mean = np.mean(self.model_run_dur)
	# 	dur_sum  = np.sum(self.model_run_dur)  # time elapsed
	# 	dur_left = dur_mean * n_iter_left
	#
	# 	# Memory and swap file:
	# 	with self.comp_ctx.proc.oneshot():
	# 		mem_sys_size = psutil.virtual_memory().total
	# 		mem_sys_free = psutil.virtual_memory().available
	# 		mem_proc     = self.comp_ctx.proc.memory_full_info().uss
	#
	# 		# swp_sys_size = psutil.swap_memory().total
	# 		# swp_proc = self.comp_ctx.proc.memory_full_info().swap  # Linux only
	#
	# 	# Report:
	# 	i3 = Model.STDOUT_INDENT * 3
	# 	i4 = Model.STDOUT_INDENT * 4
	# 	i5 = Model.STDOUT_INDENT * 5
	#
	# 	print('{}Resource utilization'       .format(i3))
	# 	print('{}Memory'                     .format(i4))
	# 	print('{}Size: {:6.1f}{}  (100.0%)'  .format(i5, *Size.bytes2human(mem_sys_size, True)))
	# 	print('{}Free: {:6.1f}{}  ({:5.1f}%)'.format(i5, *Size.bytes2human(mem_sys_free, True), round(mem_sys_free / mem_sys_size * 100, 1)))
	# 	print('{}Proc: {:6.1f}{}  ({:5.1f}%)'.format(i5, *Size.bytes2human(mem_proc,     True), round(mem_proc     / mem_sys_size * 100, 1)))
	# 	print('{}Time (run)'                 .format(i4))
	# 	print('{}Last:  {}'                  .format(i5, Time.sec2time(dur)))
	# 	print('{}Mean:  {}'                  .format(i5, Time.sec2time(dur_mean)))
	# 	print('{}Time (total)'               .format(i4))
	# 	print('{}Elapsed:   {}'              .format(i5, Time.sec2time(dur_sum)))
	# 	print('{}Remaining: {}'              .format(i5, Time.sec2time(dur_left)))
	#
	# 	# print('{}Swap'                       .format(i4))
	# 	# print('{}Size: {:6.1f}{}  (100.0%)'  .format(i5, *Size.bytes2human(swp_sys_size, True)))
	# 	# print('{}Self: {:6.1f}{}  ({:5.1f}%)'.format(i5, *Size.bytes2human(swp_proc,     True), round(swp_proc     / swp_sys_size * 100, 1)))

	# ts_0 = Time.ts(False)
	# self.sess_run_comp__iter(i + 1, iter_n, Time.ts(False) - ts_0)


# ======================================================================================================================
if __name__ == '__main__':
	from model_lr import ModelLogReg
	from model_fc import ModelFC

	hpo = HyperParamOptGrid()

	models = [ModelLogReg, ModelFC]
	model_idx = 0

	schedule = models[model_idx].schedule_get_micro(folds=range(2), ts=[(2, 1), (5, 1), (10, 2), (10, 5)], batch_sizes=[32, 64, 128])
	# schedule = models[model_idx].schedule_get_micro(folds=range(1), ts=[(10, 5)], batch_sizes=[32, 64])

	schedule = [
		models[model_idx].schedule_get_micro(folds=range(2), ts=[(10, 2), (10, 5)], batch_sizes=[32, 64, 128])[0],
		models[model_idx].schedule_get_micro(folds=range(2), ts=[( 2, 1), ( 5, 1)], batch_sizes=[32, 64, 128])[0],
	]

	cc = DotMap(  # computation context
		is_seq = False,
		do_gpu = False,
		cpu_n = -1,
		res_mon_int = 1,
		mem_free_alert = 4 * 1024 * 1024 * 1024
	)

	if platform.node() == 'comp':
		# schedule = models[model_idx].schedule_get_full()

		# schedule = [models[model_idx].schedule_get_full_ds('FC 2010',    ('20.2010', '2010'))]
		# schedule = [models[model_idx].schedule_get_full_ds('FC 2011 aa', ('aa.20.2011', '2011 aa'))]
		# schedule = [models[model_idx].schedule_get_full_ds('FC 2011 gg', ('aa.20.2011', '2011 gg'))]

		cc.res_mon_int = 5

	hpo.sess_begin()
	hpo.sess_run_init(schedule, cc.is_seq, cc.do_gpu, cc.cpu_n, cc.res_mon_int, cc.mem_free_alert)
	hpo.sess_run_comp(schedule, cc.is_seq, cc.do_gpu, cc.cpu_n, cc.res_mon_int, cc.mem_free_alert)
	hpo.sess_end()
