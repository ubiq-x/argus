#
# TODO
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import glob
import multiprocessing as mp
import platform
import pushover

from db       import DB
from model_lr import ModelLogReg
from model_fc import ModelFC

from util import FS, Size, Time


# ======================================================================================================================
def _run_par(iter_i, iter_n, model_gen, dir_ds, dir_models, fpath_db, ds_eval, fold_id, n_store, n_emit, batch_size):
	''' A multiprocessing pool worker. '''

	print('    Iter {} of {}'.format(iter_i, iter_n))

	model = {
		'LogReg': ModelLogReg (dir_ds, None, None, fpath_db),  # 'dir_out' can be None as it's only used for storing the model being fitted and we don't do that here
		'FC'    : ModelFC     (dir_ds, None, None, fpath_db)   # ^
	}[model_gen]

	model.model_load(dir_models, fold_id)
	model.model_eval_ext(fold_id, ds_eval, (n_store, n_emit), batch_size)


# ======================================================================================================================
class ExternalEvaluator(object):
	'''
	Loads a trained model and evaluates it on a dataset from a different experiment.

	One important thing to remember is that the external evaluation dataset must contain all the variables of the
 	fitted model.  For instance, any '2010' model can be evaluated on any '2011 aa' and '2011 gg' datasets but not vice
	versa.  That is, only no-temporal-variables '2011' model can be evaluated on '2010' data.
	'''

	PUSHOVER_SOUND = 'classical'

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self):
		import fs
		import sqlite3

		self.fs = fs.FileSystem()

		self.dir_sess = self.fs.DIR_SESS_COMP
		self.fpath_db = self.fs.PATH_DB_COMP

		if platform.node() == 'comp':
			self.dir_sess = self.fs.DIR_SESS
			self.fpath_db = self.fs.PATH_DB

		self.db = DB(self.fpath_db)
		self.db.conn.row_factory = sqlite3.Row

	# ------------------------------------------------------------------------------------------------------------------
	# Algorithm
	#     For each fold for each model from the specified generation and fitted on the specified dataset
	#         Load the model
	#         Load the evaluation dataset (match on 'n_store' and 'n_emit')
	#         Evaluate the model on the evaluation dataset
	def run(self, model_gen, ds_name_fit, ds_eval):
		ts_0 = round(Time.ts())

		iter_n = self.db.conn.execute('''
			SELECT COUNT(*) FROM fold f
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN model m ON m.id = f.model_id
			WHERE m.gen = ? AND d.name = ? AND d.nts_vars_name = 'none'
			''', [model_gen, ds_name_fit]).fetchone()[0]

		rows = self.db.conn.execute('''
			SELECT f.id AS fold_id, f.sess_id, s.ts AS sess_ts, f.ds_id, d.n_store, d.n_emit, d.fname, f.model_id, f.cv_id, f.fold, m.batch_size FROM fold f
			INNER JOIN sess s ON s.id = f.sess_id
			INNER JOIN ds d ON d.id = f.ds_id
			INNER JOIN model m ON m.id = f.model_id
			WHERE m.gen = ? AND d.name = ? AND d.nts_vars_name = 'none'
			''', [model_gen, ds_name_fit]).fetchall()

			# AND d.n_store = 10 AND d.n_emit = 5
			# LIMIT 2

		iter_i = 1
		args = []
		for row in rows:
			dir_models = glob.glob(os.path.join(self.dir_sess, '{}*'.format(row['sess_ts']), self.fs.DNAME_MODELS))[0]
			args.append((iter_i, iter_n, model_gen, self.fs.DIR_DS, dir_models, self.fpath_db, ds_eval, row['fold_id'], row['n_store'], row['n_emit'], row['batch_size'],))
			iter_i += 1

		print('Loop size: {}'.format(iter_n))
		with mp.Pool(processes=mp.cpu_count() - 1, maxtasksperchild=1) as pool:
			pool.starmap(_run_par, args)
			pool.close()

		dur = round(Time.ts() - ts_0)

		if platform.node() == 'comp':
			pushover.Client().send_message('{}: {} <-- {} ({}; {})' .format(model_gen, ds_name_fit, ds_eval[1], Time.sec2time(round(dur / 1000)), iter_n), title='External evaluation complete', sound=self.PUSHOVER_SOUND)


# ======================================================================================================================
if __name__ == '__main__':
	ee = ExternalEvaluator()

	for model_gen in ['LogReg', 'FC']:
		ee.run(model_gen, '2010', ('aa.20.2011', '2011 aa'))
		ee.run(model_gen, '2010', ('gg.20.2011', '2011 gg'))

		ee.run(model_gen, '2011 aa', ('20.2010', '2010'))
		ee.run(model_gen, '2011 aa', ('20.2010', '2010'))
