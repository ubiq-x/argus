import pygame

import et.em_vis
import et.et
import fs
import log.man
import psycholing.man
import stim.img
import stim.txt
import stim.txt_gc
import trial
import trial.t1
import trial.t2

from util import Str, Time


# ======================================================================================================================
class Experiment(object):
	TRIALS = []

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, scr_dim, lang, img_ext, allow_back_nav, ch_num_start, t1_dur, sub_id, et_mode, et_emu_sub_type, et_is_verbose, em_vis_mode, gc_mask_mode, gc_mask_size, gc_sub_mode, do_log_em_evt, fn_end):
		self.fs = fs.FileSystem()  # need an instance

		self.dt_0    = None  # set when the experiment begins
		self.ts_0    = None  # ^
		self.ts_0_et = None  # ^

		self.scr_dim = scr_dim
		self.lang = lang
		self.img_ext = img_ext
		self.allow_back_nav = allow_back_nav
		self.ch_num_start = ch_num_start
		self.t1_dur = t1_dur
		self.sub_id = sub_id
		self.sub_id_dt = None  # set in begin()
		self.et_mode = et_mode
		self.et_emu_sub_type = et_emu_sub_type
		self.et_is_verbose = et_is_verbose
		self.em_vis_mode = em_vis_mode
		self.gc_mask_mode = gc_mask_mode
		self.gc_mask_size = gc_mask_size
		self.gc_sub_mode = gc_sub_mode
		self.do_log_em_evt = do_log_em_evt
		self.fn_end = fn_end

		self.ctx_str = "%dx%d-%s" % (scr_dim[0], scr_dim[1], lang)  # used for assets access (e.g., '1680x1050-en')
		self.fs.exp_init(self.ctx_str)

		self.log_man = None  # set when the experiment begins
		self.et      = None  # ^
		self.em_vis  = None  # ^
		self.plm     = None  # ^
		self.gc      = None  # ^

		self.is_running = False

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if self.is_running:
			self.end()

	# ------------------------------------------------------------------------------------------------------------------
	def begin(self):
		self.et = et.et.EyeTracker(self, self.scr_dim, self.sub_id, self.et_mode, self.et_emu_sub_type, self.et_is_verbose)

		self.ts_0_et = self.et.ts()

		self.dt_0 = Time.dt()
		self.ts_0 = Time.dt2ts(self.dt_0)

		# (1) File system and logs:
		self.sub_id_dt = "%s--%s" % (self.sub_id, self.dt_0.strftime("%Y.%m.%d-%H.%M.%S"))
		self.fs.exp_begin(self.sub_id_dt)

		self.log_man = log.man.LogMan(self.dt_0, "")

		self.log_man.open(self.fs.DIR_RES_EXP, "exp", "exp-app", True, False)
		self.log_man.put("exp-app", "exp-begin sub-id:%s date:%s time:%s dotw:%s ts:%s ts-et:%s" % (self.sub_id, self.dt_0.strftime("%Y-%m-%d"), self.dt_0.strftime("%H:%M:%S.%f"), Time.dotw(self.dt_0), Str.float(round(self.ts_0)), Str.float(round(self.ts_0_et))))  # experiment-begin event (app log)

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "evt")  # all events (i.e., experiment, trial, stimulus, etc.)
		self.log_man.put_head("evt", ["evt", "inf"])
		self.log_man.put("evt", ["exp-begin", "t0-date:%s t0-time:%s t0-ts:%s t0-ts-et:%s sub-id:%s" % (self.dt_0.strftime("%Y-%m-%d"), self.dt_0.strftime("%H:%M:%S.%f"), Str.float(round(self.ts_0)), Str.float(round(self.ts_0_et)), self.sub_id)])  # experiment-begin event (sub log)

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "trial")  # trial info
		self.log_man.put_head("trial", ["sub_id", "trial_idx", "trial_name", "t0_exp", "t_dur"])

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "stim")  # stimulus info
		self.log_man.put_head("stim", ["sub_id", "trial_idx", "trial_name", "stim", "type", "ch_num", "pg_num", "t0_exp", "t0_trial", "t_dur"])

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "quest-eoc")  # end-of-chapter question info
		self.log_man.put_head("quest-eoc", ["sub_id", "trial_idx", "trial_name", "stim", "ch_num", "ans_correct", "ans", "is_correct", "t0_exp", "t0_trial", "rt"])

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "quest-wc")  # within-chapter question info
		self.log_man.put_head("quest-wc", ["sub_id", "trial_idx", "trial_name", "stim", "ch_num", "ans_correct", "ans", "is_correct", "t0_exp", "t0_trial", "rt"])

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "read")  # reading events (e.g., mind-wandering probes and self-reports)
		self.log_man.put_head("read", ["sub_id", "trial_idx", "trial_name", "stim", "ch_num", "pg_num", "evt", "t0_exp", "t0_trial", "t0_stim", "rt"])

		# (2) The rest:
		self.et.start()
		self.et.setup()
		# print(self.et.get_status())  # TODO: Fix this (low priority).

		if self.em_vis_mode != et.em_vis.EyeMovementVis.MODE_NONE:
			self.em_vis = et.em_vis.EyeMovementVis(self.et.disp_get_surf(), self.em_vis_mode)

		self.plm = psycholing.man.PsycholingMan(self.fs.DIR_ASSETS)

		if self.gc_mask_mode == stim.txt_gc.TextStimulusGC.MASK_MODE_NONE:
			self.gc = None
		else:
			self.gc = stim.txt_gc.TextStimulusGC(self.et.disp_get_surf(), self.plm, self.gc_mask_size, True, False, False, self.gc_mask_mode, self.gc_sub_mode, "*", 3)

		self.trial_i = 0
		self.TRIALS.append(trial.t1.Trial_1(self, self.et, self.et.disp_get_surf(), self.ctx_str, self.t1_dur, self.allow_back_nav, self.trial_go_next, "Questions"))
		self.TRIALS.append(trial.t2.Trial_2(self, self.et, self.et.disp_get_surf(), self.ctx_str, None,        self.allow_back_nav, self.trial_go_next, "Probes"   ))

		self.is_running = True

		self.trial_get().begin(None, self.ch_num_start)

		while True:
			for e in pygame.event.get():
				self.do_evt(e)

	# ------------------------------------------------------------------------------------------------------------------
	def do_evt(self, e):
		if e.type == pygame.QUIT:
			self.end()
		elif e.type == pygame.KEYDOWN:
			if e.key == pygame.K_ESCAPE:
				self.end()
			else:
				self.trial_get().do_evt(e)

	# ------------------------------------------------------------------------------------------------------------------
	def end(self):
		if not self.is_running: return

		self.trial_get().end()
		self.et.stop()

		pygame.display.quit()
		pygame.quit()

		dt_1 = Time.dt()

		self.log_man.put("evt", ["exp-end", "sub-id:%s dur:%s" % (self.sub_id, Time.diffs(self.dt_0, dt_1))])  # experiment-end event (sub log)
		self.log_man.put("exp-app", "exp-end sub-id:%s dur:%s" % (self.sub_id, Time.diffs(self.dt_0, dt_1)))   # experiment-end event (app log)

		self.log_man.open(self.fs.DIR_RES_EXP_SUB_SUB, "exp")
		self.log_man.put_head("exp", ["sub-id", "date", "time", "dotw", "t_dur"])
		self.log_man.put("exp", [self.sub_id, self.dt_0.strftime("%Y.%m.%d"), self.dt_0.strftime("%H:%M:%S.%f"), Time.dotw(self.dt_0), Time.diffs(self.dt_0, dt_1)])  # experiment info

		self.is_running = False

		if self.fn_end is not None:
			self.fn_end()

	# ------------------------------------------------------------------------------------------------------------------
	def stim_get(self, stim_seq_i):
		name = stim_seq_i[trial.trial.Trial.STIM_SEQ_IDX_NAME]
		if stim_seq_i[trial.trial.Trial.STIM_SEQ_IDX_TYPE] == trial.trial.Trial.STIM_TYPE_T:
			return stim.txt.TextStimulus(self.fs.DIR_STIM, self.et, name, self.img_ext, self.et.disp_get_surf(), self.em_vis, self.gc)
		else:
			return stim.img.ImageStimulus(self.fs.DIR_STIM, self.et, name, self.img_ext, self.et.disp_get_surf(), self.em_vis, None)

	# ------------------------------------------------------------------------------------------------------------------
	def trial_get(self):
		return self.TRIALS[self.trial_i]

	# ------------------------------------------------------------------------------------------------------------------
	# TODO: Remove this method in the way it is now because it nests recursion and probably doesn't let trial objects end.
	def trial_go_next(self):
		if self.trial_i >= len(self.TRIALS) - 1:
			return self.end()
		else:
			stim_id = self.trial_get().stim_seq_get_id()
			self.trial_i += 1
			self.trial_get().begin(stim_id, None)
