# TODO
#     Modify the way the last fixation is drawn so that it replaces the previously overwritten part of the bg
#
# Resources
#     Examples of games using pygame
#         n0nick.github.io/blog/2012/06/03/quick-dirty-using-pygames-dirtysprite-layered
#         nullege.com/codes/show/src%40d%40i%40dirty_chimp-HEAD%40chimp.py/200/pygame.sprite.LayeredDirty.draw/python
#
# ----------------------------------------------------------------------------------------------------------------------

import pygame

from collections import deque


# ======================================================================================================================
class EyeMovementVis(object):
	MODE_NONE = 0  # don't visualize
	MODE_LAST = 1  # visualize only the most recent one
	MODE_ALL  = 2  # visualize all  # TODO: Test this mode (low priority as it's of limited utility)

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, mode):
		self.surf = surf
		self.mode = mode
		self.bg = None
		self.gaze = (-100, -100)  # current (or last known) gaze location

		self.fix_last = FixationLast(self.surf)

		self.fix_grp = pygame.sprite.LayeredDirty()
		self.fix_grp.add(self.fix_last)

		# self.blink = Blink(self.surf)

		self.sacc = deque()

	# ------------------------------------------------------------------------------------------------------------------
	def bg_set(self, bg):
		self.bg = bg
		self.fix_grp.clear(self.surf, self.bg)

	# ------------------------------------------------------------------------------------------------------------------
	def do_be(self, blink):
		''' pylink.EndBlinkEvent '''

		# self.blink.hide()
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def do_bs(self, blink):
		''' pylink.StartBlinkEvent '''

		# self.blink.show(self.gaze)  # blink events don't contain gaze coords so we need to use the last known one
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def do_fe(self, fix):
		''' pylink.EndFixationEvent '''

		pass

	# ------------------------------------------------------------------------------------------------------------------
	def do_fs(self, fix):
		''' pylink.StartFixationEvent '''

		pass

	# ------------------------------------------------------------------------------------------------------------------
	def do_se(self, sacc):
		'''
		pylink.EndSaccadeEvent

		In anticipation of the eye remaining relatively still for a short time, this method visualizes a fixation at
		the end of a saccade.
		'''

		g = sacc.getEndGaze()
		self.gaze = g

		if self.mode == self.MODE_ALL:
			self.fix_grp.add(FixationAny(self.surf, g))

		self.fix_last.center_set(g)
		self.draw()

	# ------------------------------------------------------------------------------------------------------------------
	def do_ss(self, sacc):
		''' pylink.EndSaccadeEvent '''

		# TODO: Below may not work in mouse simulation mode and may not even work in live mode, so check it.
		# g = sacc.getStartGaze()
		# self.gaze = g
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def draw(self):
		if self.mode == self.MODE_NONE: return

		pygame.display.update(self.fix_grp.draw(self.surf))

	# ------------------------------------------------------------------------------------------------------------------
	def reset(self):
		self.fix_grp.empty()
		self.fix_grp.add(self.fix_last)

		self.sacc.clear()


# ======================================================================================================================
class Fixation(pygame.sprite.DirtySprite):
	# Properties which should be defined by subclasses:
	#     RADIUS
	#     COLOR_FILL
	#     COLOR_BORDER
	#     ALPHA
	#     image

	surf = None

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, center):
		super(Fixation, self).__init__()

		self.blendmode = 0
		self.dirty     = 1
		self.visible   = 1

		self.surf = surf

		if self.image is None:
			self.image = pygame.Surface((self.RADIUS * 2, self.RADIUS * 2))
			self.image.fill((255, 255, 255))
			self.image.set_colorkey((255, 255, 255))
			self.image.set_alpha(self.ALPHA)

			pygame.draw.circle(self.image, self.COLOR_FILL,   (self.RADIUS, self.RADIUS), self.RADIUS, 0)
			pygame.draw.circle(self.image, self.COLOR_BORDER, (self.RADIUS, self.RADIUS), self.RADIUS, 2)

			if self.ALPHA == 255:
				self.image = self.image.convert()
			else:
				self.image = self.image.convert_alpha()

		self.rect = self.image.get_rect()
		self.center_set(center)

	# ------------------------------------------------------------------------------------------------------------------
	def center_set(self, center):
		self.rect.center = center
		self.dirty = 1

	# ------------------------------------------------------------------------------------------------------------------
	def draw(self):
		self.surf.blit(self.image, self.rect)


# ======================================================================================================================
class FixationAny(Fixation):
	RADIUS       = 7  # [px]
	COLOR_FILL   = (255, 0, 0)
	COLOR_BORDER = (190, 0, 0)
	ALPHA        = 100

	image = None
	rect = None

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf, center):
		super(FixationAny, self).__init__(surf, center)


# ======================================================================================================================
class FixationLast(Fixation):
	RADIUS       = 7  # [px]
	COLOR_FILL   = (255, 0, 0)
	COLOR_BORDER = (190, 0, 0)
	ALPHA        = 255

	image = None
	rect = None

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, surf):
		super(FixationLast, self).__init__(surf, (-1000, -1000))  # put off-screen initially


# ======================================================================================================================
# class Blink(pygame.sprite.DirtySprite):  # implementation based on the Fixation class (defined elsewhere in this file)
# 	RADIUS       = 11  # [px]
# 	COLOR_FILL   = (0, 255, 0)
# 	COLOR_BORDER = (190, 0, 0)
# 	ALPHA        = 200
#
# 	image = None
# 	rect = None
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def __init__(self, surf):
# 		super(Blink, self).__init__()
#
# 		self.blendmode = 0
# 		self.dirty     = 1
# 		self.visible   = 0  # make invisible by default (we don't need it too often anyway)
#
# 		self.surf = surf
#
# 		if self.image is None:
# 			self.image = pygame.Surface((self.RADIUS * 2, self.RADIUS * 2))
# 			self.image.fill((255, 255, 255))
# 			self.image.set_colorkey((255, 255, 255))
# 			self.image.set_alpha(self.ALPHA)
#
# 			pygame.draw.circle(self.image, self.COLOR_FILL,   (self.RADIUS, self.RADIUS), self.RADIUS, 0)
# 			pygame.draw.circle(self.image, self.COLOR_BORDER, (self.RADIUS, self.RADIUS), self.RADIUS, 2)
#
# 			if self.ALPHA == 255:
# 				self.image = self.image.convert()
# 			else:
# 				self.image = self.image.convert_alpha()
#
# 		self.rect = self.image.get_rect()
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def hide(self):
# 		self.visible = 0
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def show(self, center):
# 		self.rect.center = center
# 		self.visible = 1
# 		self.dirty = 1
# 		self.draw()
#
# 	# ------------------------------------------------------------------------------------------------------------------
# 	def draw(self):
# 		self.surf.blit(self.image, self.rect)
