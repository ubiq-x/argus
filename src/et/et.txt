# EyeLinkListener
#     acceptTrigger
#     applyDriftCorrect
#     eyeAvailable
#     getCalibrationResult
#     readKeyButton
#     readKeyQueue
#     startData
#     trackerTime / trackerTimeUsec
#     trackerTimeOffset / trackerTimeUsecOffset
#
# EyeLink
#     [all of the EyeLinkListener methods, plus the ones below]
#
#     setCalibrationType
#     enableAutoCalibration / disableAutoCalibration / setAutoCalibrationPacing
#
#     drawText
#     drawCross
#     drawBox / drawFilledBox
#     drawLine
#     clearScreen
#
#     setNoRecordEvents
#     setPupilSizeDiameter
#     setSimulationMode / setScreenSimulationDistance
#
# EyeLinkListener - EDF file playback
#     startPlayBack
#     stopPlayBack
#
# EyeLinkListener - Communication with trackers on the network
#     pollRemotes
#     pollTrackers
#     node*
#     setAddress
#     Also: EyeLinkAddress Class
#
# pylink.getEYELINK().isConnected()  # 0 if link closed, -1 if simulating connection, 1 for normal connection, 2 for broadcast connection
#
# class BroadcastEyelink(EyeLinkListener): def __init__(self):
# 	EyeLinkListener.__init__(self)
# 	def drawCalTarget(self, pos):
# 		x = track2local_x(pos[0]) y = track2local_y(pos[1])
# 		EyeLinkListener.drawCalTarget(self, (x, y))
#
# ----------------------------------------------------------------------------------------------------------------------
#
# The following code is for the EyeLink Data Viewer integration purpose.
# See section "Protocol for EyeLink Data to Viewer Integration" of the EyeLink Data Viewer User Manual
# The IMGLOAD command is used to show an overlay image in Data Viewer
# getEYELINK().sendMessage("!V IMGLOAD FILL  sacrmeto.jpg")
#
# This TRIAL_VAR command specifies a trial variable and value for the given trial.
# Send one message for each pair of trial condition variable and its corresponding value.
# getEYELINK().sendMessage("!V TRIAL_VAR image  sacrmeto.jpg")
# getEYELINK().sendMessage("!V TRIAL_VAR type  gaze_contingent")
#
# ----------------------------------------------------------------------------------------------------------------------
#
# The EyeLink tracker measures eye position 250 or 500 times per second depending on the tracking mode you are working with,
# and computes true gaze position on the display using the head camera data. This data is stored in the EDF file, and made
# available through the link in as little as 3 milliseconds after a physical eye movement.
#
# When reading real-time data through the link, event data will be delayed by 12 to 24 milliseconds from the corresponding
# samples. This is caused by the velocity detector and event validation processing in the EyeLink tracker. The timestamps in
# the event reflect the true (sample) times.
