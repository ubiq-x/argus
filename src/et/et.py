# ----------------------------------------------------------------------------------------------------------------------
#
# EyeLink network adapter settings: <100.1.1.2, 255.255.255.0, empty, empty>
#
# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     Remove all left-over em-vis variables and routines
#     Perhaps stop using 'self.is_verbose' and start using 'self.DEBUG_LVL'
#     pylink.currentTime() vs pylink.EyeLink().trackerTime()
#     Use 'self.el' instead of 'pylink.getEYELINK()'
#     Use getCalibrationResult() after the calibration to ensure good result
#     https://discourse.psychopy.org/users/sol
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import gc
import pygame
import pylink
import shutil
import time

# import el
import mrdet.client
import stim.txt
import stim.txt_gc
import sub.sub

from util import Time


# ======================================================================================================================
class EyeTracker(object):
	'''
	This class records and reads the Eye-tracking Experiment Data (EED) file.  That file is created from either a
	stream of (1) live events and eye-movement samples or (2) events and eye-movement samples played back from an
	EDF file.  This class provides routines to extract events from an EDF file bypassing the need for EyeLink
	connection.  The benefit of the EED file is that it is much smaller than the corresponding EDF file.  It also
	serves as a focused backup for the EDF file (focused because it will contain a subset of the EDF contents).

	Dummy mode (MODE_LIVE_DUMMY) simulates a connection to the tracker and will allow you to bypass all EyeLink
	related code without commenting it out.
	'''

	DEBUG_LVL = 2  # 0=none, 1=normal, 2=full  # TODO: Implement fully.
	DEBUG = True
	DEBUG_CLS_NAME = "et"

	DEV_EYELINK      = 1
	DEV_EYELINK_II   = 2
	DEV_EYELINK_1000 = 3

	EYE_LEFT  =  0
	EYE_RIGHT =  1
	EYE_BOTH  =  2
	EYE_NONE  = -1

	MODE_LIVE       = 10  # live connection; live i/o events; live eye samples/events
	MODE_LIVE_DUMMY = 11  # live connection; live i/o events; no eye samples/events or setup
	MODE_LIVE_MOUSE = 12  # live connection; live i/o events; mouse-simulated eye samples/events
	MODE_EMU        = 20  # no connection; live i/o events; no eye samples/events
	MODE_EMU_MOUSE  = 21  # no connection; live i/o events; mouse-simulated eye samples/events
	MODE_EMU_SUB    = 22  # no connection; live i/o events; randomly-generated eye samples/events (depending on the subject, may be reading-like)
	MODE_EMU_EDF    = 23  # no connection; live i/o events; eye samples/events read from EDF files
	MODE_EMU_ASC    = 24  # no connection; live i/o events; eye samples/events read from ASC files
	MODE_EMU_EED    = 25  # no connection; live i/o events; eye samples/events read from EED files

	# Impossible modes
	#     MODE_LIVE_EDF   =  .  # live connection; live i/o events; eye samples/events read from EDF file

	EMU_SUB_GAZE_START = (10, 10)
	EMU_SUB_RAND_WALK_SACC_LEN_MAX = 100  # [px]
	EMU_SUB_READ_LINE_SACC_LEN = 12  # [char]

	MRD_SERVER_HOST = "localhost"

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, exp, scr_dim, fname_edf="test", mode=MODE_EMU, emu_sub_type=sub.sub.Subject.SUB_TYPE_RAND, is_verbose=False):
		self.FN_MODE = {
			self.MODE_LIVE:       { "trial_begin": self.trial_begin_live,     "trial_end": self.trial_end_live,     "do_loop": self.do_loop_live },
			self.MODE_LIVE_DUMMY: { "trial_begin": self.trial_begin_live,     "trial_end": self.trial_end_live,     "do_loop": self.do_loop_live },
			self.MODE_LIVE_MOUSE: { "trial_begin": self.trial_begin_live,     "trial_end": self.trial_end_live,     "do_loop": self.do_loop_live },
			self.MODE_EMU:        { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": None },
			self.MODE_EMU_MOUSE:  { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": None },
			self.MODE_EMU_SUB:    { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": self.do_loop_emu_sub },
			self.MODE_EMU_EDF:    { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": None },
			self.MODE_EMU_ASC:    { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": None },
			self.MODE_EMU_EED:    { "trial_begin": self.trial_begin_emu,      "trial_end": self.trial_end_emu,      "do_loop": None }
			# self.MODE_LIVE_EDF:   { "trial_begin": self.trial_begin_live_edf, "trial_end": self.trial_end_live_edf, "do_loop": None },
		}

		self.el = None  # the EyeLink instance (which extends EyeLinkListener)

		self.exp = exp
		self.scr_dim = scr_dim
		self.surf = None  # pygame.display.get_surface()

		self.fname_edf = fname_edf
		self.mode = mode
		self.eye = self.EYE_LEFT
		self.gaze = None  # None = gaze location not known (e.g., during a blink)
			# TODO [2017.06.23]: Evaluate that the approach described below is in fact correct for live data.
			#
			# The current gaze location of the subject. It's updated only by the saccade-end and fixation-end events.
			# Of course, keeping in sync with the actual sub-millisecond sampled gaze location through the use of
			# samples which the EyeLink makes available would be better, but (1) it would be much more straining on the
			# already FUCKING slow Python and (2) the samples arrive much earlier than the corresponding events and
			# frankly we may not want to update the gaze location until we have the input from the velocity detector.
			# Here's the relevant bit from the EyeLink docs:
			#
			# "When reading real-time data through the link, event data will be delayed by 12 to 24 milliseconds from
			# the corresponding samples. This is caused by the velocity detector and event validation processing in the
			# EyeLink tracker. The timestamps in the event reflect the true (sample) times."
			#
			# Another reason why I've chosen to use only the saccade-end and fixation-end events is that in my tests in
			# mouse simulation mode, the EyeLink start events (most notably, fixation-start) returned
			# (-INT_MAX, -INT_MAX) as gaze location.
				# TODO: Live mode can return correct coordinates in which case they should be used to further update
				#       the gaze location after the saccade-end event.

		self.is_emu = (mode in [self.MODE_EMU, self.MODE_EMU_MOUSE, self.MODE_EMU_SUB, self.MODE_EMU_EDF, self.MODE_EMU_ASC, self.MODE_EMU_EED])
		self.is_verbose = is_verbose

		self.emu_is_rec = False
		self.emu_sub_type = emu_sub_type
		self.emu_sub = {
			sub.sub.Subject.SUB_TYPE_RAND      : sub.rand.RandomSubject             (self.scr_dim, self.eye, self.EMU_SUB_GAZE_START),
			sub.sub.Subject.SUB_TYPE_RAND_WALK : sub.rand_walk.RandomWalkingSubject (self.scr_dim, self.eye, self.EMU_SUB_GAZE_START, self.EMU_SUB_RAND_WALK_SACC_LEN_MAX),
			sub.sub.Subject.SUB_TYPE_READ_PAGE : sub.read_page.PageReadingSubject   (self.scr_dim, self.eye, self.EMU_SUB_GAZE_START),
			sub.sub.Subject.SUB_TYPE_READ_LINE : sub.read_line.LineReadingSubject   (self.scr_dim, self.eye, self.EMU_SUB_GAZE_START, self.EMU_SUB_READ_LINE_SACC_LEN)
		}.get(self.emu_sub_type)

		self.mrdet_client = mrdet.client.MRDetClient(self.MRD_SERVER_HOST)

		self.trial = None  # this being set denotes a trial recording is going on
		self.is_running = False

		self.do_proc_em = True  # should eye movement be processed or ignored?

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if self.is_running:
			self.stop()

	# ------------------------------------------------------------------------------------------------------------------
	def cmd(self, txt):
		''' Sends a command to the eye tracker. '''

		if not self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".cmd  " + txt)

		if not self.is_emu:
			pylink.getEYELINK().sendCommand(txt)

	# ------------------------------------------------------------------------------------------------------------------
	def cmd_rec_status(self, txt):
		''' Sets the message on the bottom of the eye-tracker screen. '''
		self.cmd("record_status_message '%s'" % txt)
			# TODO: Single quotes needed? If yes, escape them, bro!

	# ------------------------------------------------------------------------------------------------------------------
	def cmd_scr_dim(self, w, h):
		''' Sets screen dimensions. '''
		self.cmd("screen_pixel_coords =  0 0 %d %d" % (w, h))
			# TODO: Double space after = needed?

	# TODO: Expose other commands messages here.

	# ------------------------------------------------------------------------------------------------------------------
	def disp_get_surf(self):
		return self.surf

	# ------------------------------------------------------------------------------------------------------------------
	def disp_set_mouse_visible(self, v):
		pygame.mouse.set_visible(v)

	# ------------------------------------------------------------------------------------------------------------------
	def do_loop_live(self):
		if not self.is_running or not self.is_conn() or not self.is_rec(): return
		if self.is_verbose: print(self.__class__.__name__ + ".do_loop_live")

		ret = None
		while True:
			# (1) Termination conditions:
			is_rec = pylink.getEYELINK().isRecording()
			if is_rec != 0:
				self.trial_end()  # TODO: remove since it's outside of loop
				print("Error: Eye tracker stopped recording (code: %s; %s)" % (is_rec, self.__class__.__name__))
				ret = is_rec
				break

			if self.trial is not None and self.trial.t_dur is not None and (time.time() > self.trial.t_setup_end + self.trial.t_dur):
				self.msg("TIMEOUT")
				self.trial_end()
				ret = pylink.TRIAL_OK
				break

			if pylink.getEYELINK().breakPressed():  # ALT-F4 and CTRL-C
				self.trial_end()
				ret = pylink.ABORT_EXPT
				break

			if self.DEBUG_LVL > 0 and pylink.getEYELINK().escapePressed():  # ESC (debugging only)
				self.trial_end()
				ret = pylink.SKIP_TRIAL
				break

			# (2) Pygame events:
			for e in pygame.event.get():
				if e.type == pygame.QUIT:
					self.trial_end()
					return pylink.SKIP_TRIAL
				elif e.type == pygame.KEYDOWN:
					if e.key == pygame.K_ESCAPE:
						self.trial_end()
						return pylink.SKIP_TRIAL

					if self.DEBUG_LVL > 0:
						s = self.trial.stim_get()

						# GC:
						if s.gc is not None:
							# mode:
							if e.key == pygame.K_q:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_NONE)
							elif e.key == pygame.K_w:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_CHAR)
							elif e.key == pygame.K_e:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_CHAR_WORD)

							# mask show/hide:
							elif e.key == pygame.K_a:
								s.gc.__debug_mask_disp__(True)
							elif e.key == pygame.K_s:
								s.gc.__debug_mask_disp__(False)

							# flags:
							elif e.key == pygame.K_d:
								s.gc.do_invert_set(not s.gc.do_invert)
								print("do-invert: %i" % s.gc.do_invert)

							elif e.key == pygame.K_f:
								s.gc.do_hide_punct_set(not s.gc.do_hide_punct)
								print("do-hide-punct: %i" % s.gc.do_hide_punct)

							elif e.key == pygame.K_g:
								s.gc.do_hide_space_set(not s.gc.do_hide_space)
								print("do-hide-space: %i" % s.gc.do_hide_space)

							# mask size:
							elif (e.key in [pygame.K_LEFT, pygame.K_RIGHT, pygame.K_UP, pygame.K_DOWN]):
								# size up:
								if pygame.key.get_mods() & pygame.KMOD_SHIFT:
									ms = s.gc.mask_size
									if   e.key == pygame.K_LEFT:  ms = (ms[0] + 1, ms[1],     ms[2],     ms[3]    )
									elif e.key == pygame.K_RIGHT: ms = (ms[0],     ms[1] + 1, ms[2],     ms[3]    )
									elif e.key == pygame.K_UP:    ms = (ms[0],     ms[1],     ms[2] + 1, ms[3]    )
									elif e.key == pygame.K_DOWN:  ms = (ms[0],     ms[1],     ms[2],     ms[3] + 1)
									s.gc.mask_size_set(ms)
									ms = s.gc.mask_size
									print("mask-size-set: (%d %d %d %d)" % (ms[0], ms[1], ms[2], ms[3]))
									continue
								# size down:
								if pygame.key.get_mods() & pygame.KMOD_ALT:
									ms = s.gc.mask_size
									if   e.key == pygame.K_LEFT:  ms = (ms[0] - 1, ms[1],     ms[2],     ms[3]    )
									elif e.key == pygame.K_RIGHT: ms = (ms[0],     ms[1] - 1, ms[2],     ms[3]    )
									elif e.key == pygame.K_UP:    ms = (ms[0],     ms[1],     ms[2] - 1, ms[3]    )
									elif e.key == pygame.K_DOWN:  ms = (ms[0],     ms[1],     ms[2],     ms[3] - 1)
									s.gc.mask_size_set(ms)
									ms = s.gc.mask_size
									print("mask-size-set: (%d %d %d %d)" % (ms[0], ms[1], ms[2], ms[3]))
									continue

						# Stimulus display mode:
						if e.key == pygame.K_7:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_NORMAL
							s.display()
						elif e.key == pygame.K_8:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_LN
							s.display()
						elif e.key == pygame.K_9:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_ROI
							s.display()
						elif e.key == pygame.K_0:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_ROI_FILE
							s.display()

					self.trial.do_evt(e)

			# (3) Process eye-movement samples and events:
			if not self.do_proc_em:
				pygame.event.clear()
				time.sleep(0.05)
				continue

			s0 = None  # previous sample
			s1 = pylink.getEYELINK().getNewestSample()  # current sample

			if s1 is not None and (s0 is None or s1.getTime() != s0.getTime()):
				s0 = s1
				gaze = None
				if self.eye == self.EYE_LEFT and s1.isLeftSample():
					gaze = s1.getLeftEye().getGaze()
					# print("gaze-l  %d,%d" % (gaze[0], gaze[1]))
				elif self.eye == self.EYE_RIGHT and s1.isRightSample():
					gaze = s1.getRightEye().getGaze()
					# print("gaze-r  %d,%d" % (gaze[0], gaze[1]))

				if gaze is not None:
					# ... (gaze-contingent code goes here)
					pass

			# (4) Process events and samples from the link data queue:
			while True:
				d = pylink.getEYELINK().getNextData()
				if not d or d == 0 or d is None:  # TODO: 'not d' should be enough here but check that. I took the rest from Psychopy.
					break   # no more events/samples

				d = pylink.getEYELINK().getFloatData()
				# if d is None:  # TODO: Found this in Psychopy. Is this even needed?
				if (not d) or (d is None):
					break

				if   isinstance(d, pylink.EndFixationEvent): self.do_loop_evt_fe(d)
				elif isinstance(d, pylink.EndSaccadeEvent):  self.do_loop_evt_se(d)
				elif isinstance(e, pylink.EndBlinkEvent):    self.do_loop_evt_be(e)

		self.trial_end()
		return ret

	# ------------------------------------------------------------------------------------------------------------------
	def do_loop_emu_sub(self):
		if not self.is_running or not self.is_conn() or not self.is_rec(): return
		if self.is_verbose: print(self.__class__.__name__ + ".do_loop_emu_sub")

		while True:
			# (1) Pygame events:
			for e in pygame.event.get():
				if e.type == pygame.QUIT:
					self.trial_end()
					return pylink.SKIP_TRIAL
				elif e.type == pygame.KEYDOWN:
					if e.key == pygame.K_ESCAPE:
						self.trial_end()
						return pylink.SKIP_TRIAL

					if self.DEBUG_LVL > 0:
						s = self.trial.stim_get()

						# GC:
						if s.gc is not None:
							# mode:
							if e.key == pygame.K_q:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_NONE)
							elif e.key == pygame.K_w:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_CHAR)
							elif e.key == pygame.K_e:
								s.gc.mask_mode_set(stim.txt_gc.TextStimulusGCC.MASK_MODE_CHAR_WORD)

							# mask show/hide:
							elif e.key == pygame.K_a:
								s.gc.__debug_mask_disp__(True)
							elif e.key == pygame.K_s:
								s.gc.__debug_mask_disp__(False)

							# flags:
							elif e.key == pygame.K_d:
								s.gc.do_invert_set(not s.gc.do_invert)
								print("do-invert: %i" % s.gc.do_invert)

							elif e.key == pygame.K_f:
								s.gc.do_hide_punct_set(not s.gc.do_hide_punct)
								print("do-hide-punct: %i" % s.gc.do_hide_punct)

							elif e.key == pygame.K_g:
								s.gc.do_hide_space_set(not s.gc.do_hide_space)
								print("do-hide-space: %i" % s.gc.do_hide_space)

							# mask size:
							elif (e.key in [pygame.K_LEFT, pygame.K_RIGHT, pygame.K_UP, pygame.K_DOWN]):
								# size up:
								if pygame.key.get_mods() & pygame.KMOD_SHIFT:
									ms = s.gc.mask_size
									if   e.key == pygame.K_LEFT:  ms = (ms[0] + 1, ms[1],     ms[2],     ms[3]    )
									elif e.key == pygame.K_RIGHT: ms = (ms[0],     ms[1] + 1, ms[2],     ms[3]    )
									elif e.key == pygame.K_UP:    ms = (ms[0],     ms[1],     ms[2] + 1, ms[3]    )
									elif e.key == pygame.K_DOWN:  ms = (ms[0],     ms[1],     ms[2],     ms[3] + 1)
									s.gc.mask_size_set(ms)
									ms = s.gc.mask_size
									print("mask-size-set: (%d %d %d %d)" % (ms[0], ms[1], ms[2], ms[3]))
									continue
								# size down:
								if pygame.key.get_mods() & pygame.KMOD_ALT:
									ms = s.gc.mask_size
									if   e.key == pygame.K_LEFT:  ms = (ms[0] - 1, ms[1],     ms[2],     ms[3]    )
									elif e.key == pygame.K_RIGHT: ms = (ms[0],     ms[1] - 1, ms[2],     ms[3]    )
									elif e.key == pygame.K_UP:    ms = (ms[0],     ms[1],     ms[2] - 1, ms[3]    )
									elif e.key == pygame.K_DOWN:  ms = (ms[0],     ms[1],     ms[2],     ms[3] - 1)
									s.gc.mask_size_set(ms)
									ms = s.gc.mask_size
									print("mask-size-set: (%d %d %d %d)" % (ms[0], ms[1], ms[2], ms[3]))
									continue

						# Stimulus display mode:
						if e.key == pygame.K_6:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_NORMAL
							s.display()
						elif e.key == pygame.K_7:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_LN
							s.display()
						elif e.key == pygame.K_8:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_ROI
							s.display()
						elif e.key == pygame.K_9:
							s.disp_mode = stim.txt.TextStimulus.DISP_MODE_ROI_FILE
							s.display()

						# Subject control:
						elif e.key == pygame.K_o:
							if self.emu_sub is not None: self.emu_sub.step()
						elif e.key == pygame.K_p:
							if self.emu_sub is not None: self.emu_sub.pause_toggle()
						elif e.key == pygame.K_LEFTBRACKET:
							if self.emu_sub is not None: self.emu_sub.t_mul_times(2.0)
						elif e.key == pygame.K_RIGHTBRACKET:
							if self.emu_sub is not None: self.emu_sub.t_mul_times(0.5)
						elif e.key == pygame.K_BACKSLASH:
							if self.emu_sub is not None: self.emu_sub.t_mul_set(1.0)

						# Screenshot:
						elif e.key == pygame.K_0:
							pygame.image.save(self.surf, '/Users/Tomek/eye-link.png')

					self.trial.do_evt(e)

			# (2) Eye-movement events:
			if (self.emu_sub is None) or (not self.do_proc_em):
				self.emu_sub.reset_evts()
				pygame.event.clear()
				time.sleep(0.05)
				continue

			e = self.emu_sub.evt_get_next()
			if e is None: continue

			if   isinstance(e, pylink.EndFixationEvent): self.do_loop_evt_fe(e)
			elif isinstance(e, pylink.EndSaccadeEvent):  self.do_loop_evt_se(e)
			elif isinstance(e, pylink.EndBlinkEvent):    self.do_loop_evt_be(e)

			# isinstance(e, pylink.Sample)

			# e = pylink.getEYELINK().getFloatData()
			# fn = {
			# 	pylink.ENDFIX: fn_fix,
			# 	pylink.STARTSACC: fn_sacc,
			# 	pylink.ENDBLINK: fn_blink
			# }.get(e.getType(), fn_else)
			# if fn is not None: fn()

		return pylink.DONE_TRIAL

	# ------------------------------------------------------------------------------------------------------------------
	def do_loop_evt_be(self, blink):
		if not self.is_running or self.trial is None: return

		if self.DEBUG_LVL >= 2:
			dur = blink.getEndTime() - blink.getStartTime()
			print("be  %3d" % dur)

		if self.exp.do_log_em_evt:
			t0  = blink.getStartTime()
			dur = blink.getEndTime() - t0

			self.exp.log_man.put("evt", ["blink", "dur:%d ts:%d ts-exp-et:%d ts-trial-et:%d ts-stim-et:%d" % (dur, t0, (t0 - self.exp.ts_0_et), (t0 - self.trial.ts_0_et), (t0 - self.trial.stim_get().ts_0_et))])

		# self.trial.do_evt_be(blink)  # don't escalate blinks

	# ------------------------------------------------------------------------------------------------------------------
	# def do_loop_evt_bs(self, blink):
	# 	if not self.is_running or self.trial is None: return
	#
	# 	self.trial.do_evt_bs(blink)

	# ------------------------------------------------------------------------------------------------------------------
	def do_loop_evt_fe(self, fix):
		if not self.is_running or self.trial is None: return

		if self.DEBUG_LVL >= 2:
			dur = fix.getEndTime() - fix.getStartTime()
			g = fix.getAverageGaze()
			print("fe  %4d,%4d  %3d" % (g[0], g[1], dur))

		if self.exp.do_log_em_evt:
			t0  = fix.getStartTime()
			dur = fix.getEndTime() - t0
			g   = fix.getAverageGaze()

			self.exp.log_man.put("evt", ["fix", "xy:(%d,%d) dur:%d ts:%d ts-exp-et:%d ts-trial-et:%d ts-stim-et:%d" % (g[0], g[1], dur, t0, (t0 - self.exp.ts_0_et), (t0 - self.trial.ts_0_et), (t0 - self.trial.stim_get().ts_0_et))])

		self.gaze = fix.getEndGaze()
		self.trial.do_evt_fe(fix)

	# ------------------------------------------------------------------------------------------------------------------
	def do_loop_evt_se(self, sacc):
		if not self.is_running or self.trial is None: return

		if self.DEBUG_LVL >= 2:
			dur = sacc.getEndTime() - sacc.getStartTime()
			g0 = sacc.getStartGaze()
			g1 = sacc.getEndGaze()
			print("se  %4d,%4d  %3d" % (g0[0], g0[1], dur))
			print("    %4d,%4d"      % (g1[0], g1[1]))

		if self.exp.do_log_em_evt:
			t0  = sacc.getStartTime()
			dur = sacc.getEndTime() - t0
			g0  = sacc.getStartGaze()
			g1  = sacc.getEndGaze()

			self.exp.log_man.put("evt", ["sacc", "xy0:(%d,%d) xy1:(%d,%d) dur:%d ts:%d ts-exp-et:%d ts-trial-et:%d ts-stim-et:%d" % (g0[0], g0[1], g1[0], g1[1], dur, t0, (t0 - self.exp.ts_0_et), (t0 - self.trial.ts_0_et), (t0 - self.trial.stim_get().ts_0_et))])

		self.gaze = sacc.getEndGaze()
		self.trial.do_evt_se(sacc)

	# ------------------------------------------------------------------------------------------------------------------
	def do_proc_em_set(self, do):
		self.do_proc_em = do
		# if do:
		# 	self.FN_MODE.get(self.mode).get("do_loop")()

	# ------------------------------------------------------------------------------------------------------------------
	def get_status(self):
		if not self.is_running: return

		if self.is_emu:
			s = "%s status\n    emulated" % (self.__class__.__name__)
		else:
			ti = pylink.getEYELINK().getTrackerInfo()
			s = (self.__class__.__name__) + " status\n"
			+ "    time " + str(ti.getTime()) + "\n"
			+ "    sample-rate " + str(ti.getSampleRate()) + "\n"
			+ "    left-eye " + str(1 if ti.haveLeftEye() else 0) + "\n"
			+ "    right-eye " + str(1 if ti.haveRightEye() else 0)

		return s

	# ------------------------------------------------------------------------------------------------------------------
	def is_conn(self):
		if self.is_emu:
			return (self.emu_sub is not None)
		else:
			return pylink.getEYELINK().isConnected() != 0
				# 0 if link closed, -1 if simulating connection, 1 for normal connection, 2 for broadcast connection

	# ------------------------------------------------------------------------------------------------------------------
	def is_rec(self):
		if not self.is_running: return False

		if self.is_emu:
			return self.emu_is_rec
		else:
			return pylink.getEYELINK().isRecording() == 0
				# TRIAL_OK (0) if no error
				# REPEAT_TRIAL, SKIP_TRIAL, ABORT_EXPT, TRIAL_ERROR if recording aborted.

	# ------------------------------------------------------------------------------------------------------------------
	def is_live(self):
		return self.mode in [self.MODE_LIVE, self.MODE_LIVE_DUMMY, self.MODE_LIVE_MOUSE]

	# ------------------------------------------------------------------------------------------------------------------
	def is_emu(self):
		return self.mode in [self.MODE_EMU, self.MODE_EMU_SUB, self.MODE_EMU_MOUSE, self.MODE_EMU_EDF, self.MODE_EMU_ASC, self.MODE_EMU_EED]

	# ------------------------------------------------------------------------------------------------------------------
	def msg(self, txt):
		''' Sends a message to the eye tracker. '''

		if not self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".msg  " + txt)

		if not self.is_emu:
			pylink.getEYELINK().sendMessage(txt)

	# ------------------------------------------------------------------------------------------------------------------
	def msg_dv_bg_color(self, color=(255, 255, 255)):
		''' Data Viewer: Sets the background color. '''

		self.msg("!V CLEAR %d %d %d" % (color[0], color[1], color[2]))

	# ------------------------------------------------------------------------------------------------------------------
	def msg_dv_bg_img(self, name):
		''' Data Viewer: Sets the background image. '''

		self.msg("!V IMGLOAD %s" % name)

	# ------------------------------------------------------------------------------------------------------------------
	def msg_dv_trial_var(self, name, val):
		''' Data Viewer: Sets a trial variable. '''

		self.msg("!V TRIAL_VAR %s %s" % (name, val))

	# ------------------------------------------------------------------------------------------------------------------
	def msg_scr_dim(self, w, h):
		''' Sends a message to the eye tracker. '''

		self.msg("DISPLAY_COORDS  0 0 %d %d" % (w, h))

	# ------------------------------------------------------------------------------------------------------------------
	# TODO: Expose other EL messages here.

	# ------------------------------------------------------------------------------------------------------------------
	def on_after_stim_disp(self):
		''' Should be called after a new stimulus has been displayed. '''

		if self.trial is not None:
			s = self.trial.stim_get()
		else:
			s = None

		if self.is_emu:
			self.emu_sub.stop()
			self.emu_sub.reset()
			if self.emu_sub_type in [sub.sub.Subject.SUB_TYPE_READ_LINE]:
				if isinstance(s, stim.txt.TextStimulus) and s.lines_get_cnt() > 0:
					self.emu_sub.stim_set(s.char_w, s.ln_h, s.lines_get_lst(False))
				else:
					self.emu_sub.stim_reset()

			self.emu_sub.start(True)  # TODO: Start paused to control eye-movements
			# self.emu_sub.pause()  # TODO: Paused to control eye-movements

	# ------------------------------------------------------------------------------------------------------------------
	def set_calibration_colors(self, fg, bg):
		if not self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".set_calibration_colors  (%d %d %d) (%d %d %d)" % (fg[0], fg[1], fg[2], bg[0], bg[1], bg[2]))  # TODO: Any shorter way to display colors?

		if not self.is_emu:
			pylink.setCalibrationColors(fg, bg)

	# ------------------------------------------------------------------------------------------------------------------
	def setup(self):
		if not self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".setup")

		# TODO:
		# if not self.is_emu:
		# 	pylink.getEYELINK().doTrackerSetup()

	# ------------------------------------------------------------------------------------------------------------------
	def start(self):
		if self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".start")

		self.mrdet_client.start()
		self.mrdet_client.cmd_reset()
		self.mrdet_client.cmd_begin(self.exp.sub_id_dt)

		if self.is_emu:
			self.start_emu()
		else:
			self.start_live()

		self.surf = pygame.display.get_surface()

		self.is_running = True

	# ------------------------------------------------------------------------------------------------------------------
	def start_emu(self):
		pygame.init()

		di = pygame.display.Info()
		os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (di.current_w - self.scr_dim[0], 0)
			# position window in top-right corner of the screen (making the terminal window and Atom's output pane visible)

		pygame.display.init()
		pygame.mixer.init()
		pygame.display.set_mode(self.scr_dim, pygame.DOUBLEBUF, 32)
		pygame.display.set_caption("EyeLink (emulation mode)")

	# ------------------------------------------------------------------------------------------------------------------
	def start_live(self):
		if self.mode in [self.MODE_LIVE, self.MODE_LIVE_MOUSE]:
			self.el = pylink.EyeLink()
		elif self.mode == self.MODE_LIVE_DUMMY:
			self.el = pylink.EyeLink(None)

		if not self.el:
			print("Error: No eye tracker connection (%s)" % self.__class__.__name__)
			sys.exit(1)

		pylink.openGraphicsEx(EyeLinkCoreGraphicsPyGame(self.scr_dim[0], self.scr_dim[1], self.el, False, True))

		pylink.getEYELINK().openDataFile(self.fname_edf + ".edf")  # 8 or fewer characters; only 0-9, A-Z, and '_' allowed
		pylink.flushGetkeyQueue()
		pylink.getEYELINK().setOfflineMode()
		self.cmd_scr_dim(self.scr_dim[0] - 1, self.scr_dim[1] - 1)
		self.msg_scr_dim(self.scr_dim[0] - 1, self.scr_dim[1] - 1)

		# Fixation detection algorithm:
		tracker_software_ver = 0
		eyelink_ver = pylink.getEYELINK().getTrackerVersion()  # 0=not connected, 1=EyeLink I, 2=EyeLink II
		if eyelink_ver == 3:
			tvstr = pylink.getEYELINK().getTrackerVersionString()
			vindex = tvstr.find("EYELINK CL")
			tracker_software_ver = int(float(tvstr[(vindex + len("EYELINK CL")):].strip()))

		if eyelink_ver >= 2:  # EyeLink II
			self.cmd("select_parser_configuration 0")
			if eyelink_ver == 2:  # turn off scenelink camera stuff
				self.cmd("scene_camera_gazemap = NO")
		else:
			pylink.getEYELINK().setSaccadeVelocityThreshold(35)  # usually 30 for cognitive research, 22 for pursuit and neurological work
			pylink.getEYELINK().setAccelerationThreshold(9500)  # usually 9500 for cognitive research, 5000 for pursuit and neurological work
			# TODO: Also check setMotionThreshold() and setPursuitFixup()

		# EDF file and link data:
		# TODO: Also use setFileEventData() and setLinkEventData()?
		pylink.getEYELINK().setFileEventFilter("LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON,INPUT,MESSAGE")
		pylink.getEYELINK().setLinkEventFilter("LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON,INPUT")
		if tracker_software_ver >= 4:
			pylink.getEYELINK().setFileSampleFilter("LEFT,RIGHT,GAZE,AREA,GAZERES,STATUS,HTARGET,INPUT")
			pylink.getEYELINK().setLinkSampleFilter("LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,HTARGET,INPUT")
		else:
			pylink.getEYELINK().setFileSampleFilter("LEFT,RIGHT,GAZE,AREA,GAZERES,STATUS,INPUT")
			pylink.getEYELINK().setLinkSampleFilter("LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT")

		if not pylink.getEYELINK().isConnected() or pylink.getEYELINK().breakPressed():
			print("Error: No eye tracker connection or break pressed (%s)" % self.__class__.__name__)
			sys.exit(1)

		# pylink.getEYELINK().sendCommand("button_function 5 'accept_target_fixation'")

		pylink.setCalibrationColors((255, 255, 255), (0, 0, 0))
		# pylink.setTargetSize(int(self.scr_dim[0] / 70), int(self.scr_dim[1] / 300))
		pylink.setCalibrationSounds("", "", "")  # use default sounds
		pylink.setDriftCorrectSounds("", "off", "off")  # turn off sounds after drift correction

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		if not self.is_running: return
		if self.is_verbose: print(self.__class__.__name__ + ".stop")

		# TODO: Show info that we are transfering EDF files and such.

		if self.is_emu:
			self.emu_sub.stop()
			# self.emu_sub.disconnect()
			self.emu_sub = None
		else:
			pylink.getEYELINK().exitCalibration()
				# TODO:
				# Program termination can be requested while calibration. For that, we need to call exitCalibration().
				# Is it safe to do that though when tracker may not be in setup or calibration? Check that in MODE_LIVE.
				# This function should be called from a message or event handler if an ongoing call to doDriftCorrect() or doTrackerSetup() should return immediately.

			if self.is_rec():
				self.trial_end()

			if pylink.getEYELINK() is not None:
				pylink.getEYELINK().setOfflineMode()
				pylink.msecDelay(500)

			pylink.getEYELINK().closeDataFile()
			pylink.getEYELINK().receiveDataFile(self.fname_edf + ".edf", self.fname_edf + ".edf")  # TODO: edf_size = ...
			pylink.getEYELINK().close()
			pylink.closeGraphics()  # TODO: Is this needed?

			# shutil.move(self.fname_edf + ".edf", os.path.join(self.exp.self.DIR_DATA, "edf", self.fname_edf + time.strftime("--%Y.%m.%d-%H.%M.%S") + ".edf"))
				# TODO: Remove the line above after testing the one below.
			shutil.move(self.fname_edf + ".edf", os.path.join(self.exp.self.DIR_RES_EXP_EDF, self.exp.sub_id_dt + ".edf"))
				# copy the EDF file to the data dir and give it a longer name

		self.mrdet_client.cmd_end()
		self.mrdet_client.stop()

		self.is_running = False

	# ------------------------------------------------------------------------------------------------------------------
	def trial_begin(self, trial, name, drift_corr_x=None, drift_corr_y=None):
		if not self.is_running or self.trial is not None: return
		if self.is_verbose: print(self.__class__.__name__ + ".trial_begin %s" % name)

		self.trial = trial

		self.mrdet_client.cmd_ts_reset()

		fn = self.FN_MODE.get(self.mode).get("trial_begin")
		if fn is not None: fn(name, drift_corr_x, drift_corr_y)

	# ------------------------------------------------------------------------------------------------------------------
	def trial_begin_emu(self, name, drift_corr_x=None, drift_corr_y=None):
		if self.trial is not None and self.trial.is_setup_done:
			self.emu_sub.start(True)  # TODO: Start paused for control over eye-movements
		self.emu_is_rec = True

		self.FN_MODE.get(self.mode).get("do_loop")()

	# ------------------------------------------------------------------------------------------------------------------
	def trial_begin_live(self, name, drift_corr_x=None, drift_corr_y=None):
		self.cmd_rec_status(name)
		self.msg("TRIALID %s" % name)
			# EyeLink Data Viewer does not parse any messages, events, or samples before the TRIALID message. This
			# message is differnt than the START message which indicates start of recording.
		self.msg("!V TRIAL_VAR_DATA %s" % name)

		# (.) Drift correction and re-do camera setup:
		drift_corr_x = (self.scr_dim[0] // 2) if drift_corr_x is None else drift_corr_x
		drift_corr_y = (self.scr_dim[1] // 2) if drift_corr_y is None else drift_corr_y

		# TODO:
		# while True:
		# 	if not pylink.getEYELINK().isConnected():
		# 		return pylink.ABORT_EXPT
		#
		# 	try:
		# 		key = pylink.getEYELINK().doDriftCorrect(drift_corr_x, drift_corr_y, True, True)  # 0 if successful, 27 if Esc key was pressed to enter Setup menu or abort
		# 		if key != 27:
		# 			break
		# 		else:
		# 			pylink.getEYELINK().doTrackerSetup()
		# 	except:
		# 		pylink.getEYELINK().doTrackerSetup()

		pylink.getEYELINK().setOfflineMode()
		pylink.msecDelay(50)

		# (.) Connect to the eye tracker:
		error = pylink.getEYELINK().startRecording(1, 1, 1, 1)  # default host is 100.1.1.1:4000
		if error:
			return error  # TODO: How to use this error?

		gc.disable()
		pylink.beginRealTimeMode(100)
		t_start = pylink.currentTime()  # trial start time

		# (.) Draw initial stimulus and sync time:
		self.surf.fill((255, 255, 255, 255))
		pygame.display.flip()
		# self.surf.fill((255, 255, 255, 255))  # TODO: correct for drawing time?
		t_display_on = pylink.currentTime() - t_start

		self.msg("%d DISPLAY ON" % t_display_on)
		self.msg("SYNCTIME %d" % t_display_on)

		try:
			pylink.getEYELINK().waitForBlockStart(100, 1, 0)  # TODO: We may need (100, 1, 1) here. Check the docs.
		except RuntimeError:
			if pylink.getLastError()[0] == 0:  # timeout without link data
				self.trial_end()
				print("Error: No link samples received (%s)" % self.__class__.__name__)
				return pylink.TRIAL_ERROR
			else:
				raise

		# (.) Determine active eye:
		self.eye = pylink.getEYELINK().eyeAvailable()
		if self.eye not in [self.EYE_LEFT, self.EYE_RIGHT, self.EYE_BOTH]:
			print("Error: No eye available (%s)" % self.__class__.__name__)
			return pylink.TRIAL_ERROR

		# (.) Clear buffers:
		pylink.flushGetkeyQueue()  # TODO: What's the difference here?
		pylink.getEYELINK().flushKeybuttons(0)  # clear key and button queues on tracker

		self.FN_MODE.get(self.mode).get("do_loop")()

	# ------------------------------------------------------------------------------------------------------------------
	# def trial_begin_live_edf(self, name, fn_do_evt, fn_do_sample, drift_corr_x=None, drift_corr_y=None):
	# 	pylink.getEYELINK().setOfflineMode()
	# 	pylink.msecDelay(50)
	# 	pylink.getEYELINK().startPlayBack()
	# 		# Flushes data from queue and starts data playback. An EDF file must be open and have at least one recorded trial.
	# 		# Use waitForData() method to wait for data: this will time out if the playback failed. Playback begins from start
	# 		# of file or from just after the end of the next-but-last recording block. Link data is determined by file contents,
	# 		# not by link sample and event settings.
	#
	# 	try:
	# 		pylink.getEYELINK().waitForBlockStart(2000, 1, 1)
	# 	except RuntimeError:
	# 		if pylink.getLastError()[0] == 0:  # timeout without link data
	# 			print("Error: No link playback data received (%s)" % self.__class__.__name__)
	# 			return pylink.TRIAL_ERROR
	# 		else:
	# 			raise
	#
	# 	# (.) Determine active eye:
	# 	self.eye = pylink.getEYELINK().eyeAvailable()
	# 	if self.eye not in [self.EYE_LEFT, self.EYE_RIGHT, self.EYE_BOTH]:
	# 		print("Error getting eye information (%s)" % self.__class__.__name__)
	# 		return pylink.TRIAL_ERROR
	#
	# 	# (.) Clear buffers:
	# 	pylink.flushGetkeyQueue()  # TODO: What's the difference here?
	# 	pylink.getEYELINK().flushKeybuttons(0)  # clear key and button queues on tracker
	#
	# 	self.do_loop(fn_do_evt, fn_do_sample)

	# ------------------------------------------------------------------------------------------------------------------
	def trial_end(self):
		''' Out: Recording status (e.g., pylink.TRIAL_OK) '''

		if not self.is_running or self.trial is None: return
		if self.is_verbose: print(self.__class__.__name__ + ".trial_end")

		self.trial = None

		fn = self.FN_MODE.get(self.mode).get("trial_end")
		if fn is not None: return fn()

	# ------------------------------------------------------------------------------------------------------------------
	def trial_end_emu(self):
		''' Out: pylink.TRIAL_OK '''

		self.emu_sub.stop()
		self.emu_is_rec = False
		return pylink.TRIAL_OK

	# ------------------------------------------------------------------------------------------------------------------
	def trial_end_live(self):
		''' Out: Recording status (e.g., pylink.TRIAL_OK) '''

		self.msg("TRIAL_RESULT %d" % pylink.DONE_TRIAL)
			# EyeLink Data Viewer does not parse any messages, events, or samples after the TRIAL_RESULT message. This
			# message is differnt than the END message which indicates end of recording.

		pylink.endRealTimeMode()
		pylink.pumpDelay(100)
		pylink.getEYELINK().stopRecording()
		while pylink.getEYELINK().getkey(): pass
		gc.enable()

		rec_status = pylink.getEYELINK().getRecordingStatus()
		if   (rec_status == pylink.TRIAL_OK):     self.msg("TRIAL OK")
		elif (rec_status == pylink.SKIP_TRIAL):   self.msg("TRIAL ABORTED")
		elif (rec_status == pylink.ABORT_EXPT):   self.msg("EXPERIMENT ABORTED")
		elif (rec_status == pylink.REPEAT_TRIAL): self.msg("TRIAL REPEATED")
		else:                                     self.msg("TRIAL ERROR")

		return rec_status

	# ------------------------------------------------------------------------------------------------------------------
	# def trial_end_live_edf(self):
	# 	pylink.getEYELINK().stopPlayback()
	# 	while pylink.getEYELINK().getkey(): pass
	# 	gc.enable()

	# ------------------------------------------------------------------------------------------------------------------
	def ts(self):
		''' Timestamp (link if connected; local otherwise). '''

		return Time.ts() if (self.is_emu) else pylink.getEYELINK().trackerTime()
