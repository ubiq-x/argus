import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pickle

import sub
import tcp.client


# ======================================================================================================================
class SubjectClient(tcp.client.TCPClient):

	# ------------------------------------------------------------------------------------------------------------------
	# Connect:
	# def connect_load      (self, sub_id):                           return self._connect("sub-load %s"                      % sub_id)

	def connect_rand      (self, scr_dim, eye, gaze):               return self._connect("sub-add %d (%d,%d),%d,(%d,%d)"    % (sub.Subject.SUB_TYPE_RAND,      scr_dim[0], scr_dim[1], eye, gaze[0], gaze[1]))
	def connect_rand_walk (self, scr_dim, eye, gaze, sacc_len_max): return self._connect("sub-add %d (%d,%d),%d,(%d,%d),%d" % (sub.Subject.SUB_TYPE_RAND_WALK, scr_dim[0], scr_dim[1], eye, gaze[0], gaze[1], sacc_len_max))
	def connect_read_page (self, scr_dim, eye, gaze):               return self._connect("sub-add %d (%d,%d),%d,(%d,%d)"    % (sub.Subject.SUB_TYPE_READ_PAGE, scr_dim[0], scr_dim[1], eye, gaze[0], gaze[1]))
	def connect_read_line (self, scr_dim, eye, gaze, sacc_len):     return self._connect("sub-add %d (%d,%d),%d,(%d,%d),%d" % (sub.Subject.SUB_TYPE_READ_LINE, scr_dim[0], scr_dim[1], eye, gaze[0], gaze[1], sacc_len))

	# ------------------------------------------------------------------------------------------------------------------
	# No arguments:
	def evt_get_cnt  (self): return self._exec_cmd("sub-evt-get-cnt")
	def pause        (self): return self._exec_cmd("sub-pause")
	def pause_toggle (self): return self._exec_cmd("sub-pause-toggle")
	def reset        (self): return self._exec_cmd("sub-reset")
	def reset_evts   (self): return self._exec_cmd("sub-reset-evts")
	def restart      (self): return self._exec_cmd("sub-restart")
	def resume       (self): return self._exec_cmd("sub-resume")
	def step         (self): return self._exec_cmd("sub-step")
	def stim_reset   (self): return self._exec_cmd("sub-stim-reset")
	def stop         (self): return self._exec_cmd("sub-stop")

	# ------------------------------------------------------------------------------------------------------------------
	# One argument or time-multiplier related:
	def done_set      (self, is_done):   return self._exec_cmd("sub-done-set %i" % is_done)  # TODO: Unused. Remove?
	def start         (self, is_paused): return self._exec_cmd("sub-start %i" % is_paused)
	def t_mul_plus    (self, x):         return self._exec_cmd("sub-t-mul-plus %g" % x)
	def t_mul_set     (self, t_mul):     return self._exec_cmd("sub-t-mul-set %g" % t_mul)
	def t_mul_set_max (self):            return self._exec_cmd("sub-t-mul-set-max")
	def t_mul_set_min (self):            return self._exec_cmd("sub-t-mul-set-min")
	def t_mul_times   (self, x):         return self._exec_cmd("sub-t-mul-times %g" % x)

	# ------------------------------------------------------------------------------------------------------------------
	# Multiple arguments:
	def stim_set_read(self, char_w, ln_h, lines): return self._exec_cmd("sub-stim-set-read %d %d %s" % (char_w, ln_h, pickle.dumps(lines)))

	# ------------------------------------------------------------------------------------------------------------------
	# Pickling:
	def evt_get_next (self): return self._exec_cmd_get_pickle("sub-evt-get-next")
	def is_paused    (self): return self._exec_cmd_get_pickle("sub-is-paused")


# ======================================================================================================================
if __name__ == "__main__":
	import server

	res = (200, 100)
	eye = 0
	gaze_start = (10, 10)

	c = SubjectClient("localhost", server.SubjectServer.PORT)
	c.connect_rand(res, eye, gaze_start)
	c.start(True)  # start paused
	c.step()
	c.step()
	c.step()
	# c.evt_get_next()
	c.stop()
	c.disconnect()
