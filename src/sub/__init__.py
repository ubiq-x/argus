from sub.sub import Subject
from sub.rand import RandomSubject
from sub.rand_walk import RandomWalkingSubject
from sub.read_line import LineReadingSubject
from sub.read_page import PageReadingSubject
