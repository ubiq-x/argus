# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     Add blinks
#     Subtrack 4 ms from all durations to offset what the internal parser of EyeLink II does?
#     Add arbitrary event to the queue
#     To simplyfy the code, consider pulling 'is_started' and 'is_paused' from the 'self.timer_em'
#         If I did this, I'd need to update SubjectServer too
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Rationale for implementation details
#     1.  Delaying eye-movement samples
#         EyeLink docs
#             The EyeLink tracker measures eye position 250 or 500 times per second depending on the tracking mode you
#             are working with, and computes true gaze position on the display using the head camera data. This data is
#             stored in the EDF file, and made available through the link in as little as 3 milliseconds after a
#             physical eye movement.
#         Decision
#             No action necessary
#         Reason
#             3 ms is short enough not to worry about emulating this delay
#
#     2.  Delaying eye-movement samples
#         EyeLink docs
#             When reading real-time data through the link, event data will be delayed by 12 to 24 milliseconds from
#             the corresponding samples. This is caused by the velocity detector and event validation processing in the
#             EyeLink tracker. The timestamps in the event reflect the true (sample) times.
#         Decision
#             No action necessary
#         Reason
#             At this point (June 2017) I am not interested in samples and therefore they aren't even emulated (only
#             events are)
#
#     3.  Subtracting 4ms from all durations
#         EyeLink docs
#             ...
#         Decision
#             ?
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources
#     PDFs
#         Normal: http://homepage.stat.uiowa.edu/~mbognar/applets/normal.html
#     Estimating Gamma PDF parameters for fixation and saccade generation
#         print(map(lambda i: int(random.gammavariate(3.0, 2.0) * 60), range(30)))  # fix
#         print(map(lambda i: int(random.gammavariate(2.0, 0.5) * 50), range(30)))  # sacc
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Eye-movement event info (from docs and pylink source)
#     EndFixationEvent(self, time, type, eye, read, sttime, gstx, gsty, hstx, hsty, sta, svel, supd_x, supd_y, entime, genx, geny, gavx, gavy, henx, heny, havx, havy, ena, ava, evel, avel, pvel, eupd_x, eupd_y)
#         sta, ena, ava - Pupil size (arbitrary units, area or diameter as selected), at start, and average during
#         fixation of FIXUPDATE interval
#
#         Please note that the \c getStartTime() and \c getEndTime() methods of the event class are the
#         timestamps of the first and last samples in the event.  To compute duration, subtract
#         these two values and add 4 msec (even in 500 Hz tracking modes, the internal parser of
#         %EyeLink II quantizes event times to 4 milliseconds).
#
#     EndSaccadeEvent(time, type, eye, read, sttime, gstx, gsty, hstx, hsty, svel, supd_x, supd_y, entime, genx, geny, henx, heny, evel, avel, pvel, eupd_x, eupd_y)
#         supd_x, supd_y eupd_x, eupd_y - Angular resolution at start and end of saccade or fixation, in screen pixels
#         per visual degree. The average of start and end values may be used to compute magnitude of saccades.
#
#         The average of the start and end angular resolution can be used to compute the size of saccades in degrees.
#         This C code would compute the true magnitude of a saccade from an ENDSACC event stored in the buffer evt:
#
#         dx = (evt.fe.genx - evt.fe.gstx) / ((evt.fe.eupd_x + evt.fe.supd_x)/2.0);
#         dy = (evt.fe.geny - evt.fe.gsty) / ((evt.fe.eupd_y + evt.fe.supd_y)/2.0);
#         dist = sqrt(dx*dx + dy*dy);
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pylink
import random
import threading

from abc         import abstractmethod
from collections import deque

import timer.timer

from util import Time


# ======================================================================================================================
class Subject(object):
	'''
	A cycle of eye-movement events is the following sequence:

		evt_01_fix_s_gen  ()
		evt_02_fix_s_proc ()
		evt_03_fix_e_gen  ()
		evt_04_fix_e_proc ()

		evt_05_sacc_s_gen  ()
		evt_06_sacc_s_proc ()

		evt_07_blink_s_gen  ()    (optional; occurs with p=PROB_BLINK)
		evt_08_blink_s_proc ()    ^
		evt_09_blink_e_gen  ()    ^
		evt_10_blink_e_proc ()    ^

		evt_11_sacc_e_gen  ()
		evt_12_sacc_e_proc ()

	The 's' suffix indicates an event's start and 'e' its end. Methods that generate an event and process it are
	intertwined.

	When the subject is started, this cycle starts at 'evt_fix_s_01().' Despite 'evt_fix_e_02()' appearing early in the
	cycle, it is there where the cycle can be stopped if the subject is paused and is being stepped-through. In other
	words, the subject's inertia prevents it from being stopped at any prior time (e.g., during the programming and
	execution of a saccade).

	Blinks are always surrounded by saccade-start and saccade-end events but are themselves optional and are generated
	infrequently (as dictated by the PROB_BLINK probabibility).

	This is an abstract class.
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	SUB_TYPE_NONE      =   0
	SUB_TYPE_RAND      = 100  # random subjects
	SUB_TYPE_RAND_WALK = 110
	SUB_TYPE_READ_PAGE = 200  # reading subjects
	SUB_TYPE_READ_LINE = 210
	SUB_TYPE_READ_WORD = 220

	EVT_QUEUE_SIZE = 64

	T_MUL_MIN = 0.125  # the event queue fills up and events start getting lost at about t_mul=0.125 (naturally, mostly due to saccades)
	T_MUL_MAX = 8.0

	DUR_EVT_S = 20  # [ms]; a constant duration of a "start" event is realistic because the event detection algorithm could always take the same amount of time to declare the start of an event
	DUR_BLINK = 40  # [ms]

	PROB_BLINK = 0.01  # the probability of a blink occuring (which is always during a saccade, i.e., between the saccade-start and saccade-end events)

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, scr_dim, eye, gaze, t_mul=1.0, evt_lst_size=EVT_QUEUE_SIZE):
		self.type = self.SUB_TYPE_NONE

		self.events = deque("", evt_lst_size)

		self.lock = threading.RLock()

		self.scr_dim = scr_dim  # (w, h)
		self.eye = eye
		self.gaze_init = gaze  # initial gaze coords (x, y)  # TODO: Remove this definition or the one below.
		self.gaze = gaze       # current gaze coords (x, y)
		self.t_mul = t_mul

		self.n_fix   = 0  # the total number of eye-movements the subject has made (this should not be reset; subclasses should use their own properties)
		self.n_sacc  = 0  # ^
		self.n_blink = 0  # ^

		self.timer_em = timer.timer.Timer(1, None)
			# TODO: The name implies this timer is only for eye-movements. After adding keyboard and mouse events
			#       think whether using one or multiple timers is best.

		self.is_started   = False
		self.is_pause_req = False
		self.is_paused    = False  # needs to be started before it can be paused
		self.is_step      = False  # used during stepping to add event to the queue right away (i.e., bypassing the timer)
		self.is_done      = False  # is the subject done interacting with the stimulus? (if it is, it will not generate any further events)

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		if self.is_started:
			self.stop()

	# ------------------------------------------------------------------------------------------------------------------
	def do_done(self):
		with self.lock:
			self.is_done = True

			if self.DEBUG_LVL >= 1: print("%s  do-done" % self.__class__.__name__)

	# ------------------------------------------------------------------------------------------------------------------
	# TODO: This method seems to be unused. Check and remove (also from SubjectClient and SubjectServer).
	def done_set(self, is_done):
		with self.lock:
			self.is_done = is_done

			if self.DEBUG_LVL >= 1: print("%s  done-set %i" % (self.__class__.__name__, is_done))

	# ------------------------------------------------------------------------------------------------------------------
	def evt_01_fix_s_gen(self):  # StartFixationEvent(self, time, type, eye, read, sttime, gstx, gsty, hstx, hsty, sta, svel, supd_x, supd_y):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-01-fix-s-gen" % self.__class__.__name__)

			ts = round(Time.ts())
			evt = pylink.StartFixationEvent(ts + self.DUR_EVT_S, type, self.eye, None, ts, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)  # TODO: Subtrack 4 ms?
			self.evt_schedule(self.evt_02_fix_s_proc, evt, self.DUR_EVT_S / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_02_fix_s_proc(self, e):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-02-fix-s-proc" % self.__class__.__name__)

			self.events.append(e)
			self.evt_03_fix_e_gen()

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def evt_03_fix_e_gen(self):  # EndFixationEvent(self, time, type, eye, read, sttime, gstx, gsty, hstx, hsty, sta, svel, supd_x, supd_y, entime, genx, geny, gavx, gavy, henx, heny, havx, havy, ena, ava, evel, avel, pvel, eupd_x, eupd_y)
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def evt_04_fix_e_proc(self, e):
		'''
		If the subject is paused (and therefore it is being stepped-through), this is the place to end the eye-movement
		cycle and wait for further stepping.
		'''

		with self.lock:
			if self.DEBUG_LVL >= 2:
				dur = e.getEndTime() - e.getStartTime()
				g = e.getAverageGaze()
				print("%s  evt-04-fix-e-proc  %3d  %4d,%4d  [evt-cnt: %d]" % (self.__class__.__name__, dur, g[0], g[1], len(self.events)))

			self.events.append(e)
			self.n_fix += 1

			if (not self.is_paused) and (not self.is_pause_req):  # event generation chain can only be broken after a fixation (i.e., the preceding saccade will always be executed)
				self.evt_05_sacc_s_gen()
			else:
				self.is_pause_req = False
				self.is_paused    = True
				self.is_step      = False  # if the subject is paused then we can only get here by calling step()

			# self.is_step = False  # in case we are stepping we want to break it here anyway

	# ------------------------------------------------------------------------------------------------------------------
	def evt_05_sacc_s_gen(self):  # StartSaccadeEvent(self, time, type, eye, read, sttime, gstx, gsty, hstx, hsty, svel, supd_x, supd_y):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-05-sacc-s-gen" % self.__class__.__name__)

			ts = Time.ts()
			evt = pylink.StartSaccadeEvent(ts + self.DUR_EVT_S, type, self.eye, None, ts, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0)  # TODO: Subtrack 4 ms?
			self.evt_schedule(self.evt_06_sacc_s_proc, evt, self.DUR_EVT_S / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_06_sacc_s_proc(self, e):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-06-sacc-s-proc" % self.__class__.__name__)

			self.events.append(e)

			if random.random() > self.PROB_BLINK:
				self.evt_11_sacc_e_gen()
			else:
				self.evt_07_blink_s_gen()

	# ------------------------------------------------------------------------------------------------------------------
	def evt_07_blink_s_gen(self):  # StartBlinkEvent(EyeEventself, time, type, eye, read, sttime)
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-07-blink-s-gen" % self.__class__.__name__)

			ts = Time.ts()
			evt = pylink.StartBlinkEvent(ts + self.DUR_EVT_S, "STARTBLINK", self.eye, None, ts)  # TODO: Subtrack 4 ms?
			self.evt_schedule(self.evt_08_blink_s_proc, evt, self.DUR_EVT_S / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_08_blink_s_proc(self, e):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-08-blink-s-proc" % self.__class__.__name__)

			self.events.append(e)
			self.evt_09_blink_e_gen()

	# ------------------------------------------------------------------------------------------------------------------
	def evt_09_blink_e_gen(self):  # EndBlinkEvent(self, time, type, eye, read, sttime, entime)
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-09-blink-e-gen" % self.__class__.__name__)

			dur = self.DUR_BLINK  # TODO: Subtrack 4 ms?

			ts = Time.ts()
			ts_0 = ts - self.DUR_EVT_S  # when the corresponding "start" event has happened
			ts_1 = ts_0 + dur           # when this "end" event will have happened

			evt = pylink.EndBlinkEvent(ts_1, "ENDBLINK", self.eye, None, ts_0, ts_1)

			self.evt_schedule(self.evt_10_blink_e_proc, evt, (dur - self.DUR_EVT_S) / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_10_blink_e_proc(self, e):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-10-blink-e-proc" % self.__class__.__name__)

			self.events.append(e)
			self.evt_11_sacc_e_gen()

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def evt_11_sacc_e_gen(self):  # EndSaccadeEvent(time, type, eye, read, sttime, gstx, gsty, hstx, hsty, svel, supd_x, supd_y, entime, genx, geny, henx, heny, evel, avel, pvel, eupd_x, eupd_y)
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def evt_12_sacc_e_proc(self, e):
		with self.lock:
			if self.DEBUG_LVL >= 2:
				dur = e.getEndTime() - e.getStartTime()
				g0 = e.getStartGaze()
				g1 = e.getEndGaze()
				g = self.gaze
				print("%s  evt-12-sacc-e-proc  %3d  %4d,%4d --> %4d,%4d  (%4d,%4d)  [evt-cnt: %d]" % (self.__class__.__name__, dur, g0[0], g0[1], g1[0], g1[1], g[0], g[1], len(self.events)))

			self.events.append(e)
			self.gaze = e.getEndGaze()  # move the subject's gaze
			self.n_sacc += 1

			self.evt_01_fix_s_gen()  # close the eye-movement cycle

	# ------------------------------------------------------------------------------------------------------------------
	def evt_get_cnt(self):
		with self.lock:
			return len(self.events)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_get_next(self):
		with self.lock:
			if len(self.events) > 0:
				e = self.events.popleft()  # FIFO; use pop() for FILO
				return e
			else:
				return None

	# ------------------------------------------------------------------------------------------------------------------
	def evt_schedule(self, fn_proc, evt, dur):
		'''
		Schedules the appearance of the event by the means of starting a timer which calls the event processing
		function 'fn_proc' (which typically is a method of this class or its subclass).

		While this method may seem superfluous, it is necessary because the subject can be paused (and being stepped-
		through) which is when all events should be processed instantaiously.  In a normal programming language they
		would be too.  However, Python lacks tail recursion elimination and as a result the eye-movement cycle
		implemented by this class would quickly lead to a stack overflow.  To work around this, a timer with a short
		timeout is utilized instead of a direct 'fn_proc' call.

		The duration ('dur') should be the timer timeout in seconds, not the event duration in milliseconds stored in
		EyeLink's event objects (e.g., EndFixationEvent).
		'''

		with self.lock:
			if self.t_mul == 0 or self.is_step:
				self.timer_em.start_timeout_fn(1 / 1000.0, lambda: fn_proc(evt), True)
					# This is what you'd ordinarily do here:
					#
					#     fn_proc(e)
					#
					# Of course, that is assuming you'd be dealing with a mature and normal programming language. Python,
					# however, is neither. There is no tail recursion elimination so we need to use fucked up hacks like
					# this one to get around it... Disguisting... Especially when you realize that this can fuck up your
					# software in unpredictable ways long after it's been deployed.
			else:
				self.timer_em.start_timeout_fn(dur * self.t_mul, lambda: fn_proc(evt), True)

		# Useful recursion investigation code:
		#     from inspect import getouterframes, currentframe
		#     print("recursion-lvl: %d" % (len(getouterframes(currentframe(1)))))

	# ------------------------------------------------------------------------------------------------------------------
	def pause(self):
		with self.lock:
			if not self.is_started or self.is_paused or self.is_pause_req: return

			# self.timer_em.pause()
			self.is_pause_req = True

			if self.DEBUG_LVL >= 1: print("%s  pause" % self.__class__.__name__)

	# ------------------------------------------------------------------------------------------------------------------
	def pause_toggle(self):
		with self.lock:
			if not self.is_started or self.is_pause_req: return

			if self.is_paused:
				self.resume()
			else:
				self.pause()

	# ------------------------------------------------------------------------------------------------------------------
	def reset(self):
		with self.lock:
			if self.DEBUG_LVL >= 1: print("%s  reset" % self.__class__.__name__)

			self.events.clear()
			self.gaze = self.gaze_init
			self.is_done = False

	# ------------------------------------------------------------------------------------------------------------------
	def reset_evts(self):
		with self.lock:
			self.events.clear()

			if self.DEBUG_LVL >= 1: print("%s  reset-evts" % self.__class__.__name__)

	# ------------------------------------------------------------------------------------------------------------------
	def resume(self):
		with self.lock:
			if not self.is_started or not self.is_paused or self.is_pause_req: return
			if self.DEBUG_LVL >= 1: print("%s  resume" % self.__class__.__name__)

			self.is_paused = False
			if self.timer_em.is_started:
				self.timer_em.resume()
			else:
				self.evt_05_sacc_s_gen()  # we are here after stepping because step will cancel the timer

	# ------------------------------------------------------------------------------------------------------------------
	def restart(self):
		with self.lock:
			self.stop()
			self.start()

	# ------------------------------------------------------------------------------------------------------------------
	def start(self, is_paused):
		with self.lock:
			# if self.is_done:
			# 	self.gaze = self.gaze_init
			# 	self.line_i = 0
			# 	self.is_done = False

			if self.is_started: return
			if self.DEBUG_LVL >= 1: print("%s  start" % self.__class__.__name__)

			self.is_started = True

			if is_paused:
				self.is_paused = True
			else:
				self.evt_01_fix_s_gen()

	# ------------------------------------------------------------------------------------------------------------------
	def start_toggle(self):
		with self.lock:
			if self.is_started:
				self.stop()
			else:
				self.start()

	# ------------------------------------------------------------------------------------------------------------------
	def step(self):
		''' Generate one saccade-fixation cycle. Only works if paused. '''

		with self.lock:
			if self.is_done or not self.is_started or not self.is_paused: return
			if self.DEBUG_LVL >= 1: print("%s  step" % self.__class__.__name__)

			self.timer_em.cancel()  # prevent any scheduled events from firing
			self.is_step = True
			self.evt_05_sacc_s_gen()

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def stim_reset(self):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def stim_set(self, char_w, ln_h, lines):
		pass

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		with self.lock:
			if not self.is_started: return
			if self.DEBUG_LVL >= 1: print("%s  stop" % self.__class__.__name__)

			self.timer_em.cancel()

			self.is_paused    = False
			self.is_pause_req = False
			self.is_started   = False

	# ------------------------------------------------------------------------------------------------------------------
	def t_mul_plus(self, x):
		with self.lock:
			if (self.t_mul + x) < self.T_MUL_MIN or (self.t_mul + x) > self.T_MUL_MAX: return

			self.t_mul += x

			if self.DEBUG_LVL >= 1: print("%s  t-mul-plus (x: %g; t-mul: %g)" % (self.__class__.__name__, x, self.t_mul))

	# ------------------------------------------------------------------------------------------------------------------
	def t_mul_set(self, t_mul):
		with self.lock:
			if t_mul < self.T_MUL_MIN or t_mul > self.T_MUL_MAX: return

			self.t_mul = t_mul

			if self.DEBUG_LVL >= 1: print("%s  t-mul-set (%g)" % (self.__class__.__name__, t_mul))

	# ------------------------------------------------------------------------------------------------------------------
	def t_mul_set_max(self):
		with self.lock:
			self.t_mul = self.T_MUL_MAX

			if self.DEBUG_LVL >= 1: print("%s  t-mul-set-max (%g)" % (self.__class__.__name__, self.t_mul))

	# ------------------------------------------------------------------------------------------------------------------
	def t_mul_set_min(self):
		with self.lock:
			self.t_mul = self.T_MUL_MIN

			if self.DEBUG_LVL >= 1: print("%s  t-mul-set-min (%g)" % (self.__class__.__name__, self.t_mul))

	# ------------------------------------------------------------------------------------------------------------------
	def t_mul_times(self, x):
		with self.lock:
			if (self.t_mul * x) < self.T_MUL_MIN or (self.t_mul * x) > self.T_MUL_MAX: return

			self.t_mul *= x

			if self.DEBUG_LVL >= 1: print("%s  t-mul-times (x: %g; t-mul: %g)" % (self.__class__.__name__, x, self.t_mul))
