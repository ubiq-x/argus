# TODO
#     Store all EMs to allow later traversal
#         Upon reading end, save the saved EMs to a file
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pylink
import random

import sub
import stim.txt

from util import Time

r_uni   = random.randint
r_gamma = random.gammavariate


# ======================================================================================================================
class LineReadingSubject(sub.Subject):
	DUR_FIX_E_MIN  =   70
	DUR_FIX_E_MAX  = 1000
	DUR_SACC_E_MIN =   40
	DUR_SACC_E_MAX =   70

	CHAR_W = 5  # just a default value (see constructor for more info on how it's used)
	LN_H = 5    # ^

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, scr_dim, eye, gaze, sacc_len=12, fn_sacc_len_rand=lambda: round(random.normalvariate(0.0, 5.0)), t_mul=1.0, evt_lst_size=sub.Subject.EVT_QUEUE_SIZE):
		super(LineReadingSubject, self).__init__(scr_dim, eye, gaze, t_mul, evt_lst_size)

		self.type = self.SUB_TYPE_READ_LINE
		self.sacc_len = sacc_len

		self.char_w = self.CHAR_W  # this value should be set by stim_set(); the default is not 0 so we can see if it actually has been set or not
		self.ln_h = self.LN_H      # ^

		self.lines = []
		self.line_i = 0

		self.fix_cnt_stim = 0
		self.sacc_cnt_stim = 0

		self.fn_sacc_len_rand = fn_sacc_len_rand
		self.fn_y_coord_rand = lambda: round(random.normalvariate(0.0, self.ln_h / 2))
			# make sure to assign a new value if 'self.ln_h is' changed

	# ------------------------------------------------------------------------------------------------------------------
	def do_done(self):
		with self.lock:
			super(LineReadingSubject, self).do_done()

			self.stop()
			# TODO: Go to the next stimulus

	# ------------------------------------------------------------------------------------------------------------------
	def evt_03_fix_e_gen(self):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-03-fix-e-gen" % self.__class__.__name__)

			dur = r_gamma(3.0, 2.0) * 60
			dur = max(dur, self.DUR_FIX_E_MIN)
			dur = min(dur, self.DUR_FIX_E_MAX)  # TODO: Subtrack 4 ms?

			ts = Time.ts()
			ts_0 = ts - self.DUR_EVT_S  # when the corresponding "start" event has happened
			ts_1 = ts_0 + dur           # when this "end" event will have happened

			evt = pylink.EndFixationEvent(ts_1, "ENDFIX", self.eye, None, ts_0, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ts_1, self.gaze[0], self.gaze[1], self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

			self.fix_cnt_stim += 1
			self.evt_schedule(self.evt_04_fix_e_proc, evt, (dur - self.DUR_EVT_S) / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_11_sacc_e_gen(self):
		with self.lock:
			if len(self.lines) == 0 or self.line_i > len(self.lines) - 1: return self.do_done()

			if self.DEBUG_LVL >= 2: print("%s  evt-11-sacc-e-gen" % self.__class__.__name__)

			dur = r_gamma(2.0, 0.5) * 50
			dur = max(dur, self.DUR_SACC_E_MIN)
			dur = min(dur, self.DUR_SACC_E_MAX)  # TODO: Subtrack 4 ms?

			ts = Time.ts()
			ts_0 = ts - self.DUR_EVT_S  # when the corresponding "start" event has happened
			ts_1 = ts_0 + dur           # when this "end" event will have happened

			if self.sacc_cnt_stim == 0:  # the first saccade on the stimulus -- go to the beginning of the first line
				ln = self.line_get()
				gaze_end_x = ln[stim.txt.TextStimulus.LN_IDX_RECT_XY][0][0] + (self.char_w / 2)  # make saccade land in the middle of letter: (self.char_w / 2)
				gaze_end_y = ln[stim.txt.TextStimulus.LN_IDX_Y_MID]
			else:
				gaze_end_x = self.gaze[0] + ((self.sacc_len + self.fn_sacc_len_rand()) * self.char_w) + (self.char_w / 2)  # make saccade land in the middle of letter: (self.char_w / 2)
				if gaze_end_x <= self.line_get()[stim.txt.TextStimulus.LN_IDX_RECT_XY][1][0]:  # continuing reading the current line
					ln = self.line_get()
					gaze_end_y = ln[stim.txt.TextStimulus.LN_IDX_Y_MID] + self.fn_sacc_len_rand()
				else:  # moving on to the next line
					if self.line_i >= len(self.lines) - 1: return self.do_done()

					self.line_i += 1
					ln = self.line_get()
					gaze_end_x = ln[stim.txt.TextStimulus.LN_IDX_RECT_XY][0][0]
					gaze_end_y = ln[stim.txt.TextStimulus.LN_IDX_Y_MID] + self.fn_sacc_len_rand()

			gaze_end = (gaze_end_x, gaze_end_y)

			evt = pylink.EndSaccadeEvent(ts_1, "ENDSACC", self.eye, None, ts_0, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, ts_1, gaze_end[0], gaze_end[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

			self.sacc_cnt_stim += 1
			self.evt_schedule(self.evt_12_sacc_e_proc, evt, (dur - self.DUR_EVT_S) / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def line_get(self):
		return (self.lines[self.line_i]) if (len(self.lines) > 0) else (None)

	# ------------------------------------------------------------------------------------------------------------------
	def reset(self):
		''' Make it as if the subject has not yet started reading the stimulus. '''

		with self.lock:
			self.line_i = 0

			self.fix_cnt_stim = 0
			self.sacc_cnt_stim = 0

			super(LineReadingSubject, self).reset()

	# ------------------------------------------------------------------------------------------------------------------
	def stim_reset(self):
		with self.lock:
			self.char_w = self.CHAR_W
			self.ln_h = self.LN_H
			self.lines = []
			self.line_i = 0

			self.fix_cnt_stim = 0
			self.sacc_cnt_stim = 0

			self.fn_y_coord_rand = lambda: round(random.normalvariate(0.0, self.ln_h / 2))

	# ------------------------------------------------------------------------------------------------------------------
	def stim_set(self, char_w, ln_h, lines):
		with self.lock:
			self.char_w = char_w
			self.ln_h = ln_h
			self.lines = lines
			self.line_i = 0

			self.fix_cnt_stim = 0
			self.sacc_cnt_stim = 0

			self.fn_y_coord_rand = lambda: round(random.normalvariate(0.0, self.ln_h / 2))
