import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pylink
import random

import sub

from util import Time

r_uni = random.randint


# ======================================================================================================================
class RandomSubject(sub.Subject):
	DUR_FIX_E_MIN  =   70  # [ms]
	DUR_FIX_E_MAX  = 1000
	DUR_SACC_E_MIN =   40
	DUR_SACC_E_MAX =   70

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, scr_dim, eye, gaze, t_mul=1.0, evt_lst_size=sub.Subject.EVT_QUEUE_SIZE):
		super(RandomSubject, self).__init__(scr_dim, eye, gaze, t_mul, evt_lst_size)

		self.type = self.SUB_TYPE_RAND

	# ------------------------------------------------------------------------------------------------------------------
	def evt_03_fix_e_gen(self):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-03-fix-e-gen" % self.__class__.__name__)

			dur = r_uni(self.DUR_FIX_E_MIN, self.DUR_FIX_E_MAX)  # TODO: Subtrack 4 ms?

			ts = round(Time.ts())
			ts_0 = ts - self.DUR_EVT_S  # when the corresponding "start" event has happened
			ts_1 = ts_0 + dur           # when this "end" event will have happened

			evt = pylink.EndFixationEvent(ts_1, "ENDFIX", self.eye, None, ts_0, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ts_1, self.gaze[0], self.gaze[1], self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

			self.evt_schedule(self.evt_04_fix_e_proc, evt, (dur - self.DUR_EVT_S) / 1000.0)

	# ------------------------------------------------------------------------------------------------------------------
	def evt_11_sacc_e_gen(self):
		with self.lock:
			if self.DEBUG_LVL >= 2: print("%s  evt-11-sacc-e-gen" % self.__class__.__name__)

			dur = r_uni(self.DUR_SACC_E_MIN, self.DUR_SACC_E_MAX)  # TODO: Subtrack 4 ms?

			ts = round(Time.ts())
			ts_0 = ts - self.DUR_EVT_S  # when the corresponding "start" event has happened
			ts_1 = ts_0 + dur           # when this "end" event will have happened

			gaze_end = (r_uni(0, self.scr_dim[0]), r_uni(0, self.scr_dim[1]))

			evt = pylink.EndSaccadeEvent(ts_1, "ENDSACC", self.eye, None, ts_0, self.gaze[0], self.gaze[1], 0.0, 0.0, 0.0, 0.0, 0.0, ts_1, gaze_end[0], gaze_end[1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

			self.evt_schedule(self.evt_12_sacc_e_proc, evt, (dur - self.DUR_EVT_S) / 1000.0)


# ======================================================================================================================
if __name__ == "__main__":
	s = RandomSubject((100, 100), 0, (0, 0))
	print(s.is_started)
