import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import multiprocessing as mp
import time

import sub


# ======================================================================================================================
class _SubProcess(mp.Process):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, sub, sub_lock, evt_q, t_sleep=0.01):
		super(_SubProcess, self).__init__()

		self.evt_exit = mp.Event()
		self.sub = sub
		self.sub_lock = sub_lock
		self.evt_q = evt_q

		self.t_sleep = t_sleep

	# ------------------------------------------------------------------------------------------------------------------
	def run(self):
		while not self.evt_exit.is_set():
			pass


	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.evt_exit.set()


# ======================================================================================================================
class SubjectMan(object):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self):
		self.mp_man = mp_man or mp.Manager()
		self.evt_q = self.mp_man.Queue()

		self.sub_proc = _SubProcess(self.sub, self.sub_lock, self.evt_q)
		self.sub_proc.daemon = True

	# ------------------------------------------------------------------------------------------------------------------
	def start(self):
		self.sub_proc.start()

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.sub_proc.stop()


# ======================================================================================================================
if __name__ == "__main__":
	man = SubjectMan()
	man.start(True)  # start paused
	man.step()
	man.step()
	man.step()
	man.evt_get_next()
	man.stop()
