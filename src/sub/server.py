# ----
#
# Unhandled exception in thread started by
# sys.excepthook is missing
# lost sys.stderr
# Unhandled exception in thread started by
# sys.excepthook is missing
# lost sys.stderr
#
#
# ----
#
# Unhandled exception in thread started by <function do_client_thread at 0x10672a6e0>
# Traceback (most recent call last):
#   File "sub_server.py", line 105, in do_client_thread
#     }.get(cmd[0], lambda: "2")()                                                             # unknown command
#   File "sub_server.py", line 86, in <lambda>
#     "sub-step"            : lambda: sub_do(sub.step),                                      # cmd:       8sub-step
#   File "sub_server.py", line 153, in sub_do
#     fn()
#   File "/Volumes/D/pitt/prj/eye-link/prj/exp-01/src/sub.py", line 266, in step
#     self.timer_em.cancel()  # prevent any scheduled events from firing
#   File "/Volumes/D/pitt/prj/eye-link/prj/exp-01/src/timer.py", line 36, in cancel
#     self.timer.cancel()
# AttributeError: 'NoneType' object has no attribute 'cancel'
#
# ----
#
# pthread_cond_wait: Resource busy
# SubjectLineReader  start
# SubjectLineReader  pause
# ^CSubjectLineReader  stop
# Unhandled exception in thread started by
# sys.excepthook is missing
# lost sys.stderr
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Resources
#     With python socketserver how can I pass a variable to the constructor of the handler class
#         stackoverflow.com/questions/6875599/with-python-socketserver-how-can-i-pass-a-variable-to-the-constructor-of-the-han
#     How do I modify variables in the SocketServer server instance from within a RequestHandler handler instance in Python?
#         stackoverflow.com/questions/3868132/how-do-i-modify-variables-in-the-socketserver-server-instance-from-within-a-requ
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import pickle
import threading

import tcp.server

# from util import Str, Time


# ======================================================================================================================
class SubjectRequestHandler(tcp.server.RequestHandler):
	# LOG_DO = True
	# LOG_FNAME = "sub_server_log.txt"

	# ------------------------------------------------------------------------------------------------------------------
	def do_cmd(self, cmd, args):
		s = self.server.sub

		# TODO: Call sub_add() before some of the functions below can be called

		# if self.LOG_DO:
		# 	self.log.write(cmd)
		# 	self.log.write("\n")
		# 	self.log.write(args)
		# 	self.log.write("\n")

		return {
			# "sub-load"            : lambda: self.sub_load(args[0]),                                        # cmd:      ??sub-load ...

			"sub-add"             : lambda: self.sub_add(args),                                            # cmd:      35sub-add 100 (1680,1050),0,(840,525)

			"sub-pause"           : lambda: self.sub_do(s.pause),                                          # cmd:       9sub-pause
			"sub-pause-toggle"    : lambda: self.sub_do(s.pause_toggle),                                   # cmd:      16sub-pause-toggle
			"sub-restart"         : lambda: self.sub_do(s.restart),                                        # cmd:      11sub-restart
			"sub-reset"           : lambda: self.sub_do(s.reset),                                          # cmd:      10sub-reset
			"sub-reset-evts"      : lambda: self.sub_do(s.reset_evts),                                     # cmd:      15sub-reset-evts
			"sub-resume"          : lambda: self.sub_do(s.resume),                                         # cmd:      10sub-resume
			"sub-start-toggle"    : lambda: self.sub_do(s.start_toggle),                                   # cmd:      16sub-start-toggle
			"sub-step"            : lambda: self.sub_do(s.step),                                           # cmd:       8sub-step
			"sub-stim-reset"      : lambda: self.sub_do(s.stim_reset),                                     # cmd:      14sub-stim-reset
			"sub-stop"            : lambda: self.sub_do(s.stop),                                           # cmd:       8sub-stop
			"sub-t-mul-set-max"   : lambda: self.sub_do(s.t_mul_set_max),                                  # cmd:      17sub-t-mul-set-max
			"sub-t-mul-set-min"   : lambda: self.sub_do(s.t_mul_set_min),                                  # cmd:      17sub-t-mul-set-min

			"sub-evt-get-cnt"     : lambda: self.sub_get(s.evt_get_cnt),                                   # cmd:      15sub-evt-get-cnt

			"sub-is-paused"       : lambda: self.sub_get_pickle(value=s.is_paused),                        # cmd:      13sub-is-paused

			"sub-evt-get-next"    : lambda: self.sub_get_pickle(fn=s.evt_get_next),                        # cmd:      16sub-evt-get-next

			"sub-stim-set-read"   : lambda: self.sub_stim_set_read(args[0], args[1], " ".join(args[2:])),  # cmd:      ??sub-stim-set-read 10 10 ...

			"sub-t-mul-plus"      : lambda: self.sub_set(s.t_mul_plus,  float, args[0]),                   # cmd:      18sub-t_mul-plus 9.0
			"sub-t-mul-set"       : lambda: self.sub_set(s.t_mul_set,   float, args[0]),                   # cmd:      17sub-t_mul-set 1.0
			"sub-t-mul-times"     : lambda: self.sub_set(s.t_mul_times, float, args[0]),                   # cmd:      19sub-t_mul-times 2.0

			"sub-done-set"        : lambda: self.sub_set(s.done_set, lambda x: bool(int(x)), args[0]),     # cmd:      14sub-set-done 1
			"sub-start"           : lambda: self.sub_set(s.start,    lambda x: bool(int(x)), args[0]),     # cmd:      11sub-start 1
		}.get(cmd, lambda: self.RES_CMD_ERR)()  # unknown command

	# ------------------------------------------------------------------------------------------------------------------
	# def do_verbose_cmd(self, cmd):
	# 	if not cmd.startswith("sub-get-evt-next"):
	# 		print("Command: %s" % cmd)

	# ------------------------------------------------------------------------------------------------------------------
	def handle(self):
		# if self.LOG_DO:
		# 	self.log = open(self.LOG_FNAME, "w")

		# self.sub = None
		tcp.server.RequestHandler.handle(self)

		# if self.LOG_DO:
		# 	self.log.close()

	# ------------------------------------------------------------------------------------------------------------------
	def sub_add(self, args):  # TODO: Substitute 'args' with actual arguments.
		if len(args) != 2: return self.RET_ERR

		import sub
		import rand
		import rand_walk
		import read_page
		import read_line

		cls = {
			sub.Subject.SUB_TYPE_RAND      : [ rand.SubjectRandom,            "SubjectRandom"       ],
			sub.Subject.SUB_TYPE_RAND_WALK : [ rand_walk.SubjectRandomWalker, "SubjectRandomWalker" ],
			sub.Subject.SUB_TYPE_READ_PAGE : [ read_page.SubjectPageReader,   "SubjectPageReader"   ],
			sub.Subject.SUB_TYPE_READ_LINE : [ read_line.SubjectLineReader,   "SubjectLineReader"   ]
		}.get(int(args[0]))

		with self.server.sub_lock:
			try:
				sub_tmp = eval("%s(%s)" % (cls[1], args[1]), {"__builtins__": None}, {cls[1]: cls[0]})

				if self.server.sub is not None:
					self.server.sub.stop()
					self.server.sub = None

				self.server.sub = sub_tmp

				# return self.server.sub_add(sub)
				return self.RET_OK
			except Exception:
				return self.RET_ERR

	# ------------------------------------------------------------------------------------------------------------------
	# def sub_load(self, sub_id):
	# 	''' Should be called if a process needs to access an existing subject. '''
	#
	# 	s = self.server.sub_get(sub_id)
	# 	if s is not None:
	# 		print(11)
	# 		self.sub = s
	# 		return self.RET_OK
	# 	else:
	# 		print(22)
	# 		return self.RET_ERR

	# ------------------------------------------------------------------------------------------------------------------
	def sub_do(self, fn):
		if self.server.sub is None: return self.RET_ERR
		with self.server.sub_lock:
			fn()
		return self.RET_OK

	# ------------------------------------------------------------------------------------------------------------------
	def sub_get(self, fn):
		if self.server.sub is None: return self.RET_ERR
		with self.server.sub_lock:
			res = fn()
		return str(res)

	# ------------------------------------------------------------------------------------------------------------------
	def sub_get_pickle(self, fn=None, value=None):
		'''
		This function will call a function or return a value. If neither is None, only the function will be called.
		'''

		if self.server.sub is None: return self.RET_ERR

		with self.server.sub_lock:
			if fn is not None:
				res = fn()
				return pickle.dumps(bytes(res) if (res is not None) else None)
			elif value is not None:
				return pickle.dumps(bytes(value) if (res is not None) else None)
			else:
				return self.RET_ERR

		# if fn is not None:
		# 	res = pickle.dumps(fn())
		# elif value is not None:
		# 	res = pickle.dumps(value)
		# else:
		# 	res = self.RET_ERR
		#
		# if self.LOG_DO and res != "N.":
		# 	self.log.write("%s  %s\n" % (str(fn), str(value)))
		# 	self.log.write(res)
		# 	self.log.write("\n\n")
		#
		# return res

	# ------------------------------------------------------------------------------------------------------------------
	def sub_set(self, fn, fn_arg, x):
		with self.server.sub_lock:
			try:
				fn(fn_arg(x))
				return self.RET_OK
			except Exception:
				return self.RET_ERR

	# ------------------------------------------------------------------------------------------------------------------
	def sub_stim_set_read(self, char_w, ln_h, lines):
		if self.server.sub is None: return self.RET_ERR

		with self.server.sub_lock:
			try:
				self.server.sub.stim_set(int(char_w), int(ln_h), pickle.loads(lines))
				return self.RET_OK
			except Exception:
				return self.RET_ERR

			return self.RET_OK


# ======================================================================================================================
class SubjectServer(tcp.server.TCPServer):
	PORT = 4000

	sub = None
	sub_lock = threading.Lock()  # ensure only one process is accessing the 'sub' object

	# SUB = {}  # hashed by timestamp string

	# ------------------------------------------------------------------------------------------------------------------
	# def sub_add(self, sub):
	# 	ts = Str.float(Time.ts())
	# 	self.SUB[ts] = sub
	# 	return ts

	# ------------------------------------------------------------------------------------------------------------------
	# def sub_get(self, sub_id):
	# 	return self.SUB[sub_id]


# ======================================================================================================================
if __name__ == "__main__":
	import signal
	import sys

	s = SubjectServer(("localhost", SubjectServer.PORT), SubjectRequestHandler, do_ret=True)

	signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event

	s.serve_forever()
