# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     Test using a large number of randomly generated time series
#     Allow emitting after every new item is added
#
# ----------------------------------------------------------------------------------------------------------------------

from collections     import deque
from multiprocessing import Lock

import mrdet


# ======================================================================================================================
class TimeSeries_TT(object):
	'''
	TT: Time-based storage, time-based emitting

	The first time series is emitted after the total duration of eye-movements stored is at least 't_store'.  After the
	first emission, a new time series is emitted every 't_emit'.  Both times are expected in milliseconds.
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, t_store, t_emit):
		if t_emit > t_store: raise ValueError("'t_emit' must be smaller than 't_store'")

		super(TimeSeries_TT, self).__init__()

		self.t_store = t_store  # [ms]
		self.t_emit  = t_emit   # [ms]

		self.ts      = deque()
		self.ts_lock = Lock()

		self.dur_tot   = 0.0  # total duration of the time series (dur_tot <= t_store)
		self.dur_slice = 0.0  # total duration of the current slice (emission occurs when: dur_slice >= t_emit)

		self.name = "%d.%d" % (self.t_store, self.t_emit)

	# ------------------------------------------------------------------------------------------------------------------
	def item_add(self, i):
		with self.ts_lock:
			dur_i = i[mrdet.MRDet.DATA_IDX_FIX][mrdet.MRDet.DATA_FIX_IDX_DUR]
			dur_0 = list(self.ts)[0][mrdet.MRDet.DATA_IDX_FIX][mrdet.MRDet.DATA_FIX_IDX_DUR] if (len(self.ts) > 0) else 0  # itertools.islice(d, 0, 40).next()

			# Remove the oldest items for as long as the time series cannot accomodate the new item:
			while (self.dur_tot - dur_0 + dur_i >= self.t_store) and (len(self.ts) > 0):
				self.ts.popleft()
				self.dur_tot -= dur_0
				dur_0 = list(self.ts)[0][mrdet.MRDet.DATA_IDX_FIX][mrdet.MRDet.DATA_FIX_IDX_DUR] if (len(self.ts) > 0) else 0

			# Append the new item:
			self.ts.append(i)
			self.dur_tot   += dur_i
			self.dur_slice += dur_i

			# Emit the time series:
			if (self.dur_tot >= self.t_store) and (self.dur_slice >= self.t_emit):
				self.dur_slice = 0.0
				return list(self.ts)
					# TODO: Will the above line release the lock as anticipated?  If not, then maybe
					#       copy.deepcopy(self.ts) should be used instead.
			else:
				return None

	# ------------------------------------------------------------------------------------------------------------------
	def reset(self):
		with self.ts_lock:
			self.ts.clear()

			self.dur_tot   = 0
			self.dur_slice = 0


# ======================================================================================================================
if __name__ == "__main__":
	from test import Test

	# (1) Test:
	# (1.1) Static data:
	times = [(2000, 1000), (2000, 1500), (2000, 2000), (3000, 1000)]  # (t_store, t_emit) pairs

	print("Static data")
	for t in times:
		print("    t: %d %d" % (t[0], t[1]))

		mrd_ts = TimeSeries_TT(t[0], t[1])
		for d in Test.DATA_01:
			ts = mrd_ts.item_add(d)
			if ts is not None:
				Test.ts_print(ts)

	# (1.2) Random data:
	times = [(5000, 2500), (10000, 5000), (10000, 1000), (20000, 10000)]  # (t_store, t_emit) pairs

	print("\nRandom data")
	n = 3  # number of runs for each element of 'times'
	for t in times:
		for i in range(n):
			print("    t: %d %d  (run %d)" % (t[0], t[1], (i + 1)))

			mrd_ts = TimeSeries_TT(t[0], t[1])
			for d in Test.data_gen_n(100, 300, 100):
				ts = mrd_ts.item_add(d)
				if ts is not None:
					Test.ts_print(ts)
