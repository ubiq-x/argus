#
#
# ----------------------------------------------------------------------------------------------------------------------


import multiprocessing as mp
import multiprocessing.managers as mpm
import time

from server import MRDetServer


# ======================================================================================================================
class _ServerResProcess(mp.Process):
	def __init__(self, pipe, t_sleep=0.01):
		super(_ServerResProcess, self).__init__()

		self.evt_exit = mp.Event()
		self.pipe = pipe

		self.t_sleep = t_sleep

	def run(self):
		while not self.evt_exit.is_set():
			if self.pipe.poll():
				i = self.pipe.recv()
				print("p: %.4f" % i)
			time.sleep(self.t_sleep)

	def stop(self):
		self.evt_exit.set()


# ======================================================================================================================
class MRDetClient(object):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, host):
		class ServerManager(mpm.BaseManager): pass
		ServerManager.register("get_pipe_cs")
		ServerManager.register("get_pipe_cr")

		self.server_man = ServerManager(address=(host, MRDetServer.PORT), authkey=MRDetServer.AUTH_KEY)
		self.conn = self.server_man.connect()

		self.pipe_cs = self.server_man.get_pipe_cs()
		self.pipe_cr = self.server_man.get_pipe_cr()

		self.server_res_proc = _ServerResProcess(self.pipe_cr)
		self.server_res_proc.daemon = True

	# ------------------------------------------------------------------------------------------------------------------
	def _exec_cmd(self, cmd, args=None):
		self.pipe_cs.send((cmd, args))

	# ------------------------------------------------------------------------------------------------------------------
	def cmd_begin(self, sub_id):
		self._exec_cmd(MRDetServer.CMD_BEGIN, sub_id)

	def cmd_end(self):
		self._exec_cmd(MRDetServer.CMD_END)

	def cmd_reset(self, t=0.5):
		self._exec_cmd(MRDetServer.CMD_RESET)
		time.sleep(t)

	def cmd_ts_add_item(self, item):
		self._exec_cmd(MRDetServer.CMD_TS_ADD_ITEM, item)

	def cmd_ts_reset(self, t=0.25):
		self._exec_cmd(MRDetServer.CMD_TS_RESET)
		time.sleep(t)

	# ------------------------------------------------------------------------------------------------------------------
	def start(self):
		self.server_res_proc.start()

	def stop(self):
		self.server_res_proc.stop()
		self.server_res_proc.join()


# ======================================================================================================================
if __name__ == "__main__":
	import os
	import sys
	from inspect import getsourcefile

	__file__ = os.path.abspath(getsourcefile(lambda: None))
	sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

	import mrdet

	from test import Test
	from util import Time

	# (1) Test:
	t_mul = 5  # how much faster to submit data items

	c = MRDetClient("localhost")
	c.start()

	# (1.1) Static data:
	sub_id = "client--%s--static" % Time.dt().strftime("%Y.%m.%d-%H.%M.%S")  # in production this will be recieved from the client (i.e., the experiment app)
	c.cmd_reset()
	c.cmd_begin(sub_id)

	for d in Test.DATA_01:
		c.cmd_ts_add_item(d)
		time.sleep(d[mrdet.MRDet.DATA_IDX_FIX][mrdet.MRDet.DATA_FIX_IDX_DUR] / (1000.0 * t_mul))

	c.cmd_end()
	time.sleep(0.5)

	# (1.2) Random data:
	for i in range(3):  # number of runs
		sub_id = "client--%s--random-%02d" % (Time.dt().strftime("%Y.%m.%d-%H.%M.%S"), i + 1)  # in production this will be recieved from the client (i.e., the experiment app)
		c.cmd_begin(sub_id)

		for d in Test.data_gen_n(100, 300, 100):
			c.cmd_ts_add_item(d)
			time.sleep(d[mrdet.MRDet.DATA_IDX_FIX][mrdet.MRDet.DATA_FIX_IDX_DUR] / (1000.0 * t_mul))

		c.cmd_end()
	time.sleep(0.5)

	c.stop()
