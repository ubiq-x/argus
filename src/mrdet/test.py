from __future__ import print_function

import random

from mrdet import MRDet
from util  import Time


# ======================================================================================================================
class Test(object):
	'''
	Mindless reading detection test helper class (debug only).
	'''

	DATA_01 = [  # slightly over 5s of data
		[ [10, 200.0, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ],
		[ [11, 200.1, 1], None, None, ["cat", 3, 2, 0, "unc", False, False, False, False, False, False] ],
		[ [12, 200.2, 2], None, None, ["sat", 3, 3, 0, "unc", False, False, False, False, False, False] ],
		[ [13, 200.3, 2], None, None, ["on",  2, 4, 0, "unc", False, False, False, False, False, False] ],
		[ [14, 200.4, 3], None, None, ["nut", 3, 5, 0, "unc", False, False, False, False, False, False] ],

		[ [20, 201.1, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ],
		[ [21, 201.2, 1], None, None, ["cat", 3, 2, 0, "unc", False, False, False, False, False, False] ],
		[ [22, 201.3, 2], None, None, ["sat", 3, 3, 0, "unc", False, False, False, False, False, False] ],
		[ [23, 201.4, 2], None, None, ["on",  2, 4, 0, "unc", False, False, False, False, False, False] ],
		[ [24, 201.5, 3], None, None, ["nut", 3, 5, 0, "unc", False, False, False, False, False, False] ],

		[ [30, 202.1, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ],
		[ [31, 202.2, 1], None, None, ["cat", 3, 2, 0, "unc", False, False, False, False, False, False] ],
		[ [32, 202.3, 2], None, None, ["sat", 3, 3, 0, "unc", False, False, False, False, False, False] ],
		[ [33, 202.4, 2], None, None, ["on",  2, 4, 0, "unc", False, False, False, False, False, False] ],
		[ [34, 202.5, 3], None, None, ["nut", 3, 5, 0, "unc", False, False, False, False, False, False] ],

		[ [40, 203.1, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ],
		[ [41, 203.2, 1], None, None, ["cat", 3, 2, 0, "unc", False, False, False, False, False, False] ],
		[ [42, 203.3, 2], None, None, ["sat", 3, 3, 0, "unc", False, False, False, False, False, False] ],
		[ [43, 203.4, 2], None, None, ["on",  2, 4, 0, "unc", False, False, False, False, False, False] ],
		[ [44, 203.5, 3], None, None, ["nut", 3, 5, 0, "unc", False, False, False, False, False, False] ],

		[ [50, 204.1, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ],
		[ [51, 204.2, 1], None, None, ["cat", 3, 2, 0, "unc", False, False, False, False, False, False] ],
		[ [52, 204.3, 2], None, None, ["sat", 3, 3, 0, "unc", False, False, False, False, False, False] ],
		[ [53, 204.4, 2], None, None, ["on",  2, 4, 0, "unc", False, False, False, False, False, False] ],
		[ [54, 204.5, 3], None, None, ["nut", 3, 5, 0, "unc", False, False, False, False, False, False] ]
	]

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def data_gen_n(dur_min, dur_max, n):
		i = 0
		while i < n:
			i += 1
			yield [[int(Time.ts()), random.randint(dur_min, dur_max), 0], None, None, None]

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def data_gen_t(dur_min, dur_max, t_tot):
		t = 0
		while t < t_tot:
			d = [[int(Time.ts()), random.randint(dur_min, dur_max), 0], None, None, None]
			t += d[MRDet.DATA_IDX_FIX][MRDet.DATA_FIX_IDX_DUR]
			yield d

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def ts_print(ts):
		ls_dur = [i[MRDet.DATA_IDX_FIX][MRDet.DATA_FIX_IDX_DUR] for i in ts]
		ls_dur_str = map(str, ls_dur)
		t_tot = int(sum(ls_dur))

		print("%12s: [%s]" % (str(t_tot), " ".join(ls_dur_str)))


# ======================================================================================================================
if __name__ == "__main__":
	Test.ts_print(Test.DATA_01[0:3])
	Test.ts_print(Test.DATA_01[0:5])
	Test.ts_print(Test.DATA_01)

	map(lambda x: print(x), Test.data_gen(190, 210, 6))
	map(lambda x: print(x), Test.data_gen(100, 300, 6))
