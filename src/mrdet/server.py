# TODO
#     One of the server processes is taking 100% of CPU -- fix that
#     Add a time-based component to the HMAC key
#         Not gonna work coz the server can be up for dayz
#     Kill all processes in stop() by executing a shell command
#
# ----------------------------------------------------------------------------------------------------------------------

import sys
import multiprocessing as mp
import multiprocessing.managers as mpm
import time

import man
import mrdet
import ts_tt


# ======================================================================================================================
class _ClientCmdProcess(mp.Process):

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, mrdet_man, pipe_sr, pipe_ss, t_sleep=0.01):
		super(_ClientCmdProcess, self).__init__()

		self.evt_exit = mp.Event()

		self.mrdet_man = mrdet_man

		self.pipe_sr = pipe_sr
		self.pipe_ss = pipe_ss

		self.t_sleep = t_sleep

	# ------------------------------------------------------------------------------------------------------------------
	def _do_reset(self):
		self.mrdet_man.reset()
		for t in [(2000, 1000), (2000, 1500), (2000, 2000), (3000, 1000)]:  # (t_store, t_emit) pairs
			self.mrdet_man.mrdet_add(mrdet.MRDet, ts_tt.TimeSeries_TT(t[0], t[1]))

	# ------------------------------------------------------------------------------------------------------------------
	def run(self):
		while not self.evt_exit.is_set():
			if self.pipe_sr.poll():
				cmd, args = self.pipe_sr.recv()
				{
					MRDetServer.CMD_BEGIN        : lambda: self.mrdet_man.begin(args),
					MRDetServer.CMD_END          : lambda: self.mrdet_man.end(),
					MRDetServer.CMD_TS_ADD_ITEM  : lambda: self.mrdet_man.ts_add_item(args),
					MRDetServer.CMD_TS_RESET     : lambda: self.mrdet_man.ts_reset(),
					MRDetServer.CMD_RESET        : self._do_reset
				}.get(cmd)()
			time.sleep(self.t_sleep)

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.evt_exit.set()


# ======================================================================================================================
class MRDetServer(object):
	AUTH_KEY = b"semantics bucket"  # HMAC key

	CMD_BEGIN        = "begin"
	CMD_END          = "end"
	CMD_TS_ADD_ITEM  = "ts-add-item"
	CMD_TS_RESET     = "ts-reset"
	CMD_RESET        = "reset"

	PORT = 5000

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, host):
		self.pipe_sr, self.pipe_cs = mp.Pipe(False)  # server recv, client send
		self.pipe_cr, self.pipe_ss = mp.Pipe(False)  # client recv, server send

		class ServerManager(mpm.BaseManager): pass
		ServerManager.register("get_pipe_cs", callable=lambda: self.pipe_cs)
		ServerManager.register("get_pipe_cr", callable=lambda: self.pipe_cr)

		self.mp_man = mp.Manager()
		self.mp_man = None

		self.server_man = ServerManager(address=(host, self.PORT), authkey=self.AUTH_KEY)

		self.mrdet_man = man.MRDetMan(self.mp_man, self.pipe_ss)

		self.client_cmd_proc = _ClientCmdProcess(self.mrdet_man, self.pipe_sr, self.pipe_ss)

	# ------------------------------------------------------------------------------------------------------------------
	def start(self):
		self.client_cmd_proc.start()

		# self.server_man.get_server().serve_forever()

		server_process = mp.Process(target=self.server_man.get_server().serve_forever)
		server_process.start()
		server_process.join()

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.client_cmd_proc.stop()

		self.pipe_sr.close()
		self.pipe_cs.close()
		self.pipe_cr.close()
		self.pipe_ss.close()

		sys.exit(0)


# ======================================================================================================================
if __name__ == "__main__":
	import signal

	s = MRDetServer("localhost")

	signal.signal(signal.SIGINT, lambda signal, frame: s.stop())  # register CTRL+C event

	s.start()
