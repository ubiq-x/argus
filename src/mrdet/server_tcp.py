#
# Queue on the server
#     stackoverflow.com/questions/25245223/python-queue-queue-wont-work-in-threaded-tcp-stream-handler
#
# ----------------------------------------------------------------------------------------------------------------------
#
# from multiprocessing.managers import BaseManager
# m = BaseManager(address=('127.0.0.1', 5000), authkey='abc')
# s = m.get_server()
# s.serve_forever()
#
# ----
#
# from multiprocessing.managers import BaseManager
# m = BaseManager(address=('127.0.0.1', 5000), authkey='abc')
# m.connect()
#
# ----
#
# m.register(typeid[, callable])
#
# ----
#
# multiprocessing.managers.SyncManager
# create a list proxy and append a mutable object (a dictionary)
# lproxy = manager.list()
# lproxy.append({})
# # now mutate the dictionary
# d = lproxy[0]
# d['a'] = 1
# d['b'] = 2
# # at this point, the changes to d are not yet synced, but by
# # reassigning the dictionary, the proxy is notified of the change
# lproxy[0] = d
#
# ----
#
# from multiprocessing.managers import BaseManager
#
# class MathsClass(object):
#     def add(self, x, y):
#         return x + y
#     def mul(self, x, y):
#         return x * y
#
# class MyManager(BaseManager):
#     pass
#
# MyManager.register('Maths', MathsClass)
#
# if __name__ == '__main__':
#     manager = MyManager()
#     manager.start()
#     maths = manager.Maths()
#     print maths.add(4, 3)         # prints 7
#     print maths.mul(7, 8)         # prints 56
#
# ----
#
# https://docs.python.org/2/library/multiprocessing.html#using-a-remote-manager
#
# Running the following commands creates a server for a single shared queue which remote clients can access:
#
# from multiprocessing.managers import BaseManager
# import Queue
# queue = Queue.Queue()
# class QueueManager(BaseManager): pass
# QueueManager.register('get_queue', callable=lambda:queue)
# m = QueueManager(address=('', 50000), authkey='abracadabra')
# s = m.get_server()
# s.serve_forever()
#
# One client can access the server as follows:
#
# from multiprocessing.managers import BaseManager
# class QueueManager(BaseManager): pass
# QueueManager.register('get_queue')
# m = QueueManager(address=('foo.bar.org', 50000), authkey='abracadabra')
# m.connect()
# queue = m.get_queue()
# queue.put('hello')
#
# Another client can also use it:
#
# from multiprocessing.managers import BaseManager
# class QueueManager(BaseManager): pass
# QueueManager.register('get_queue')
# m = QueueManager(address=('foo.bar.org', 50000), authkey='abracadabra')
# m.connect()
# queue = m.get_queue()
# queue.get()
# 'hello'
#
# Local processes can also access that queue, using the code from above on the client to access it remotely:
#
# from multiprocessing import Process, Queue
# from multiprocessing.managers import BaseManager
# class Worker(Process):
#      def __init__(self, q):
#          self.q = q
#          super(Worker, self).__init__()
#      def run(self):
#          self.q.put('local hello')
#
# queue = Queue()
# w = Worker(queue)
# w.start()
# class QueueManager(BaseManager): pass
# QueueManager.register('get_queue', callable=lambda: queue)
# m = QueueManager(address=('', 50000), authkey='abracadabra')
# s = m.get_server()
# s.serve_forever()
#
# ----
#
# https://docs.python.org/2/library/multiprocessing.html#module-multiprocessing.connection
# https://docs.python.org/2/library/multiprocessing.html#multiprocessing.Queue.cancel_join_thread
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile
__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))


import cPickle as pickle

import mrdet
import ts_tt
import tcp.server


# ======================================================================================================================
class MRDetRequestHandler(tcp.server.RequestHandler):

	# ------------------------------------------------------------------------------------------------------------------
	def do_cmd(self, cmd, args):
		m = self.server.mrdet

		return {
			"begin"       : lambda: m.begin(pickle.loads(args[0])),
			"end"         : lambda: m.end(),
			"ts-item-add" : lambda: self.ts_item_add(pickle.loads(args[0]))
		}.get(cmd, lambda: self.RET_CMD_ERR)()  # unknown command

	# ------------------------------------------------------------------------------------------------------------------
	def handle(self):
		tcp.server.RequestHandler.handle(self)

	# ------------------------------------------------------------------------------------------------------------------
	def ts_item_add(self, item):
		res = self.server.mrdet.ts_item_add(item)

		# import time
		# time.sleep(1)

		if res is not None:
			print(res)


# ======================================================================================================================
class MRDetServer(tcp.server.TCPServer):
	PORT = 5000

	mrdet = mrdet.MRDet(ts_tt.TimeSeries_TT(2000, 1000))

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True):
		# super(MRDetServer, self).__init__ server_address, RequestHandlerClass, bind_and_activate=True, do_ret=False)  # TODO: Why the fuck doesn't this work...?
		tcp.server.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate=True, do_ret=False)


# ======================================================================================================================
if __name__ == "__main__":
	import signal
	import sys

	s = MRDetServer(("localhost", MRDetServer.PORT), MRDetRequestHandler)

	signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event

	s.serve_forever()
