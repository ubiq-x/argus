# ----------------------------------------------------------------------------------------------------------------------
#
# Possible time series implementations considered so far
#     NN: Number-based storage, number-based emitting  [do not implement: number-based storage not intuitive]
#     NT: Number-based storage, number-based emitting  [do not implement: number-based storage not intuitive]
#     TN: Time-based storage,   time-based emitting    [do not implement: not very useful apart from emitting every time an item is added]
#     TT: Time-based storage,   time-based emitting    [implement; allow emitting after each new item]
#
# ----------------------------------------------------------------------------------------------------------------------

from abc import abstractmethod


# ======================================================================================================================
class TimeSeries(object):
	'''
	Mindless reading detection time series (TS).

	The primary function of the sub-classes of this class is to provide the following functionalities:

		- Moving window
		- Multiple moving windows with overlapping

	This is an abstract class.
	'''

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def item_add(self, i):
		'''
		Add a time series item.

		This method should determine if a time series is ready to be emitted and return either that time series or
		None.
		'''

		pass

	# ------------------------------------------------------------------------------------------------------------------
	@abstractmethod
	def reset(self):
		pass
