# Recurrent neural nets (RNNs)
#     Structure
#         Many to one
#     Activation function
#         tanh
#         RrLU
#             f(x) = max(0,x)
#             f(x) = ln(1 + exp(e))  # softplus
#             en.wikipedia.org/wiki/Rectifier_(neural_networks)
# Long short-term memory neural nets (LSTM)
#
#
# ----------------------------------------------------------------------------------------------------------------------
#
# TODO
#     Put timeout on computations
#         stackoverflow.com/questions/8616630/time-out-decorator-on-a-multprocessing-function
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(os.path.join("..", "stim"))
# sys.path.insert(0, os.path.join(".."))

import json
import multiprocessing as mp
import time

import fs

import ts_tt
import stim.txt

# import importlib
# fs       = importlib.import_module(".fs", "src")
# ts_tt    = importlib.import_module("ts_tt")
# txt_stim = importlib.import_module(".stim.txt.TextStimulus", __package__)

from util import MPCounter, Str, Time


# ======================================================================================================================
class _LogProcess(mp.Process):
	'''
	Logs the lifecycle of the MRDet unit.

	Unused if the unit is managed (i.e., is apart of MRDetMan).
	'''

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, fpath, q, n_comp, n_log, t_sleep=0.01):
		super(_LogProcess, self).__init__()

		self.evt_exit = mp.Event()
		self.fpath = fpath
		self.q = q
		self.n_comp = n_comp
		self.n_log = n_log

		self.t_sleep = t_sleep

	# ------------------------------------------------------------------------------------------------------------------
	def run(self):
		log = open(self.fpath, "w")

		while (not self.evt_exit.is_set()) and (self.n_comp.value() > self.n_log.value()):
			if not self.q.empty():
				log.write(self.q.get())
				self.n_log.inc()
				self.q.task_done()
			time.sleep(self.t_sleep)

		log.close()

	# ------------------------------------------------------------------------------------------------------------------
	def stop(self):
		self.evt_exit.set()


# ======================================================================================================================
class MRDet(object):
	'''
	Mindless reading detection unit.

	Computes: p(mindless reading).
	'''

	DEBUG_LVL = 0  # 0=none, 1=normal, 2=full

	DATA_IDX_FIX      = 0
	DATA_IDX_SACC_IN  = 1  # incoming saccade
	DATA_IDX_SACC_OUT = 2  # outgoing saccade
	DATA_IDX_WORD     = 3

	DATA_FIX_IDX_TS   = 0  # timestamp (POSIX time)
	DATA_FIX_IDX_DUR  = 1  # [ms]
	DATA_FIX_IDX_CHAR = 2  # character (based on average gaze)

	DATA_SACC_IDX_TS           = 0  # timestamp (POSIX time)
	DATA_SACC_IDX_DUR          = 1  # [ms]
	DATA_SACC_IDX_LEN          = 2
	DATA_SACC_IDX_SITE_LAUNCH  = 3
	DATA_SACC_IDX_SITE_LANDING = 4
	DATA_SACC_IDX_IS_REG       = 5  # is a regression

	DATA_WORD_IDX_W      =  0  # word
	DATA_WORD_IDX_LEN    =  1  # length
	DATA_WORD_IDX_FRQ    =  2  # frequency
	DATA_WORD_IDX_PRED   =  3  # predictability
	DATA_WORD_IDX_POS    =  4  # part of speech (key in the 'PsycholingMan.POS' dictionary)
	DATA_WORD_IDX_IS_BOL =  5  # is beginning of line
	DATA_WORD_IDX_IS_EOL =  6  # is end       of line
	DATA_WORD_IDX_IS_BOC =  7  # is beginning of clause
	DATA_WORD_IDX_IS_EOC =  8  # is end       of clause
	DATA_WORD_IDX_IS_BOS =  9  # is beginning of sentence
	DATA_WORD_IDX_IS_EOS = 10  # is end       of sentence

	# ------------------------------------------------------------------------------------------------------------------
	def __init__(self, ts, res_q, log_q=None, is_managed=False):
		self.id = id(self)

		self.ts = ts
		self.res_q = res_q
		self.log_q = log_q or mp.Manager().JoinableQueue()  # log_q should be passed from MRDetMan

		self.is_managed = is_managed  # is part of MRDetMan?

		self.log_proc = None  # set by begin(); reset by end()  (unused if is being managed)
		self.sub_id   = None  # ^  (unused if being managed)
		self.sub_dir  = None  # ^  (unused if being managed)
		self.ts_0     = None  # ^
		self.n_comp   = None  # ^
		self.n_log    = None  # ^  (unused if being managed)

		self.name = ts.name

		self.is_started = False

	# ------------------------------------------------------------------------------------------------------------------
	def __del__(self):
		self.end()

	# ------------------------------------------------------------------------------------------------------------------
	def _log(self, msg):
		if self.is_managed:
			self.log_q.put((self.id, msg))
		else:
			self.log_q.put(msg)

	# ------------------------------------------------------------------------------------------------------------------
	def begin(self, sub_id=None, sub_dir=None):
		if self.is_started: return

		self.ts_0 = Time.ts()

		# Computations and results:
		self.n_comp = MPCounter(0)

		# Logs:
		if sub_id is not None:  # should be executed only when the unit is not being managed
			self.sub_id  = sub_id
			self.sub_dir = sub_dir or os.path.join(fs.FileSystem.DIR_RT_MRD, sub_id)
			if not os.path.exists(self.sub_dir): os.makedirs(self.sub_dir)

		if not self.is_managed:
			self.n_log = MPCounter(-2)  # 'begin' and 'end' are two guaranteed log writes so we offset that to start from zero

			self.log_proc = _LogProcess(os.path.join(self.sub_dir, "%d.%d.txt" % (self.ts.t_store, self.ts.t_emit)), self.log_q, self.n_comp, self.n_log)
			self.log_proc.daemon = True
			self.log_proc.start()

		self._log(
			"begin\n" +
			"    dt: %s\n" % Time.ts2dt(self.ts_0) +
			"    ts: %d\n\n" % int(self.ts_0))

		self.is_started = True

	# ------------------------------------------------------------------------------------------------------------------
	def compute(self, n_comp, lst, res_q, log_q, dur_tot):
		'''
		Computes: p(mindless reading).
		'''

		p = sum(map(lambda x: 1 if x[MRDet.DATA_IDX_FIX][MRDet.DATA_FIX_IDX_DUR] < 202 else 0, lst)) / float(len(lst))

		res_q.put(p)
		self._log(
			"compute\n" +
			"    i: %d\n" % n_comp +
			"    ts: %d\n" % int(Time.ts()) +
			"    fix-dur-tot: %s\n" % Str.float(dur_tot) +
			"    fix-n: %d\n" % len(lst) +
			"    fix-ts-0: %d\n" % lst[0][MRDet.DATA_IDX_FIX][MRDet.DATA_FIX_IDX_TS] +
			"    fix-ts-n: %d\n" % lst[-1][MRDet.DATA_IDX_FIX][MRDet.DATA_FIX_IDX_TS] +
			"    p: %s\n" % Str.float(p) +
			"    data: %s\n\n" % json.dumps(lst))

	# ------------------------------------------------------------------------------------------------------------------
	def end(self):
		if not self.is_started: return

		# Logs:
		ts_1 = Time.ts()
		self._log(
			"end\n" +
			"    dt: %s\n" % Time.ts2dt(ts_1) +
			"    ts: %d\n" % int(ts_1) +
			"    t: %d\n"  % int(ts_1 - self.ts_0) +
			"    comp-n: %d\n\n"  % self.n_comp.value())
		time.sleep(0.1)  # make sure the item shows in the log queue (in production, a precausion really but of low cost so why not)

		if not self.is_managed:
			while self.n_comp.value() > self.n_log.value(): pass  # wait for the log subprocess to complete all the writes

		# Computations and results:
		if not self.is_managed:
			self.log_q.join()
			self.log_proc.stop()
			self.log_proc.join()
			self.log_proc = None

		# Other:
		self.ts.reset()

		self.sub_id  = None
		self.sub_dir = None
		self.ts_0    = None
		self.n_comp  = None
		self.n_log   = None

		self.is_started = False

	# ------------------------------------------------------------------------------------------------------------------
	@staticmethod
	def get_data_item(fix, roi):
		'''
		[ [10, 200.0, 0], None, None, ["fat", 3, 1, 0, "unc", False, False, False, False, False, False] ]

		EndFixationEvent(self, time, type, eye, read, sttime, gstx, gsty, hstx, hsty, sta, svel, supd_x, supd_y, entime, genx, geny, gavx, gavy, henx, heny, havx, havy, ena, ava, evel, avel, pvel, eupd_x, eupd_y)

		DATA_FIX_IDX_TS   = 0  # timestamp (POSIX time)
		DATA_FIX_IDX_DUR  = 1  # [ms]
		DATA_FIX_IDX_CHAR = 2  # character (based on average gaze)

		DATA_WORD_IDX_W      =  0  # word
		DATA_WORD_IDX_LEN    =  1  # length
		DATA_WORD_IDX_FRQ    =  2  # frequency
		DATA_WORD_IDX_PRED   =  3  # predictability
		DATA_WORD_IDX_POS    =  4  # part of speech (key in the 'PsycholingMan.POS' dictionary)
		DATA_WORD_IDX_IS_BOL =  5  # is beginning of line
		DATA_WORD_IDX_IS_EOL =  6  # is end       of line
		DATA_WORD_IDX_IS_BOC =  7  # is beginning of clause
		DATA_WORD_IDX_IS_EOC =  8  # is end       of clause
		DATA_WORD_IDX_IS_BOS =  9  # is beginning of sentence
		DATA_WORD_IDX_IS_EOS = 10  # is end       of sentence
		'''

		ts = stim.txt.TextStimulus
		return [
			[fix.getTime(), fix.getEndTime() - fix.getStartTime(), 0],  # TODO: Infer the character (function in TextStimulus?)
			None,
			None,
			[roi[ts.ROI_IDX_WORD], roi[ts.ROI_IDX_W_LEN], roi[ts.ROI_IDX_W_FRQ], roi[ts.ROI_IDX_W_PRED], roi[ts.ROI_IDX_W_POS], False, False, False, False, False, False]
		]

	# ------------------------------------------------------------------------------------------------------------------
	def ts_add_item(self, i):
		if not self.is_started: return

		lst = self.ts.item_add(i)
		if lst is not None:
			self.n_comp.inc()

			# self.pool.apply_async(self.compute, (self.n_comp.value(), lst, self.res_q, self.log_q, self.ts.dur_tot)).get()
			mp.Process(target=self.compute, args=(self.n_comp.value(), lst, self.res_q, self.log_q, self.ts.dur_tot)).start()

	# ------------------------------------------------------------------------------------------------------------------
	def ts_reset(self):
		self.ts.reset()


# ======================================================================================================================
if __name__ == "__main__":
	from test import Test

	# (1) Test:
	# pool = mp.Pool(processes=mp.cpu_count(), maxtasksperchild=1)
	# pool = mp.Pool(processes=1, maxtasksperchild=1)

	res_q = mp.Manager().JoinableQueue()  # typically passed by MRDetMan

	sub_id  = "test--%s" % Time.dt().strftime("%Y.%m.%d-%H.%M.%S")  # in production this will be recieved from the client (i.e., the experiment app)
	sub_dir = os.path.join(fs.FileSystem.DIR_RT_MRD, sub_id)

	print("Results will be written to: %s" % sub_dir)

	# (1.1) Static data:
	times = [(2000, 1000), (2000, 1500), (2000, 2000), (3000, 1000)]  # (t_store, t_emit) pairs

	for t in times:
		mrd = MRDet(ts_tt.TimeSeries_TT(t[0], t[1]), res_q)
		mrd.begin(sub_id, sub_dir)

		for d in Test.DATA_01:
			mrd.ts_add_item(d)
		mrd.end()

	# (1.2) Random data:
	times = [(5000, 2500), (10000, 5000), (10000, 1000), (20000, 10000)]  # (t_store, t_emit) pairs

	for t in times:
		for i in range(3):  # number of runs
			mrd = MRDet(ts_tt.TimeSeries_TT(t[0], t[1]), res_q)
			mrd.DEBUG_LVL = 1
			mrd.begin(sub_id, sub_dir)

			for d in Test.data_gen_n(100, 300, 100):
				mrd.ts_add_item(d)
			mrd.end()

	# pool.close()
	# pool.join()

	# while mp.active_children(): time.sleep(0.1)
