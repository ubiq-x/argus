import cPickle as pickle

import client


# ======================================================================================================================
class TestClient(client.TCPClient):

    # ------------------------------------------------------------------------------------------------------------------
    def connect(self):
        super().connect()

        self.sock.setblocking(False)

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_noargs(self):      self._send('noargs')
    def cmd_args  (self, a,b): self._send('args {} {}'.format(pickle.dumps(a), b))


# ======================================================================================================================
if __name__ == '__main__':
    import math
    import test_server

    c = TestClient('localhost', test_serverServer.PORT)
    c.connect()
    c.noargs()
    c.args('1x', math.pi * 1)
    c.args('2x', math.pi * 2)
    c.args('3x', math.pi * 3)
    c.disconnect()
