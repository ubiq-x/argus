import pickle

import server


# ======================================================================================================================
class TestRequestHandler(server.RequestHandler):

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_noargs(self):
        print('noargs')

    # ------------------------------------------------------------------------------------------------------------------
    def cmd_args(self, a,b):
        print('args: {} {:f3.2}'.format(a,b))

    # ------------------------------------------------------------------------------------------------------------------
    def do_cmd(self, cmd, args):
        try:
            return {
                'noargs' : lambda: self.cmd_noargs(),
                'args'   : lambda: self.cmd_args(args[0], float(args[1])),
            }.get(cmd, lambda: None)()  # unknown command; None instead of self.RET_CMD_ERR suppreses response to client
        except Exception as e:
            print('{} {}'.format(cmd, args))
            print(e)


# ======================================================================================================================
class TestServer(server.TCPServer):
    PORT = 3333

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True):
        # server-specific init goes here

        tcp.server.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate=True, do_ret=True)


# ======================================================================================================================
if __name__ == '__main__':
    import signal
    import sys

    s = TestServer(('192.168.0.105', TestServer.PORT), HectorRequestHandler)

    signal.signal(signal.SIGINT, lambda signal, frame: sys.exit(0))  # register CTRL+C event

    s.serve_forever()
