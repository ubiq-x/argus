import asyncore
import cPickle as pickle
import socket

from server import RequestHandler


# ======================================================================================================================
class TCPClientAsync(asyncore.dispatcher):
    CMD_DISCONNECT = RequestHandler.CMD_DISCONNECT

    MSG_IN_LEN  = RequestHandler.MSG_IN_LEN
    MSG_OUT_LEN = RequestHandler.MSG_OUT_LEN

    MSG_OUT_FORMAT = "%" + str(MSG_OUT_LEN) + "d%s"

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, host, port, sock=None):
        asyncore.dispatcher.__init__(self)

        self.host = host
        self.port = port

        if sock is None:
            self.sock = self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

    # ------------------------------------------------------------------------------------------------------------------
    def _connect(self, cmd=None):
        try:
            self.connect((self.host, self.port))
            if cmd is not None:
                return self._exec_cmd(cmd)
            else:
                return None
        except socket.error:
            print("Error: Cannot connect to the server (%s)" % self.__class__.__name__)
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _exec_cmd(self, cmd):
        try:
            self._send(cmd)
            return self._recv()
        except socket.error:
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _exec_cmd_get_pickle(self, cmd):
        res = ""
        try:
            res = self._exec_cmd(cmd)
            return pickle.loads(res)
        except pickle.UnpicklingError as e:
            # TODO: I catch this exception here because of weird errors that happen when the probe is being presented
            #       in Trial_2 class and when the subject server is being queried for being paused. Those errors are:
            #
            #           invalid load key, ' '.
            #           unpickling stack underflow
            #           bad pickle data
            print("Unpickling error: %s  %s" % (e, res))
            return None
        except EOFError or socket.error:
            return None

    # ------------------------------------------------------------------------------------------------------------------
    def _recv(self):
        msg = self.sock.recv(self._recv_msg_len())
        return msg

    # ------------------------------------------------------------------------------------------------------------------
    def _recv_msg_len(self):
        try:
            return int(self.sock.recv(self.MSG_OUT_LEN))
        except ValueError:
            return 0

    # ------------------------------------------------------------------------------------------------------------------
    def _send(self, msg):
        self.sock.send(self.MSG_OUT_FORMAT % (len(msg), msg))

    # ------------------------------------------------------------------------------------------------------------------
    def connect(self):
        self._connect()

    # ------------------------------------------------------------------------------------------------------------------
    def disconnect(self):
        try:
            self._send(self.CMD_DISCONNECT)
        except socket.error:
            pass
        finally:
            self.sock.close()
